FROM atlas/athanalysis:21.2.111
ADD . /xampp/fakefactors4l
WORKDIR /xampp/build
RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /xampp && \
    cmake ../fakefactors4l && \
    make -j4
