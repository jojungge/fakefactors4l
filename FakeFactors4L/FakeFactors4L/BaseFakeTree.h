#ifndef FAKEFACTORS4L_BASEFAKETREE_H
#define FAKEFACTORS4L_BASEFAKETREE_H
#include <XAMPPplotting/HistoTemplates.h>
#include <XAMPPplotting/PlottingUtils.h>

#include <FakeFactors4L/SelectionWithTitle.h>
#include <FakeFactors4L/Utils.h>
#include <NtupleAnalysisUtils/NtupleBranch.h>

#include <TLorentzVector.h>
#include <TTree.h>
#include <vector>

namespace SUSY4L {
    /// Basic tree implementing functions
    /// used by the FakeFactor analysis scale factor
    /// and hist fitter tree writting
    static constexpr size_t dummy_idx = -1;
    static constexpr float dummy_dR = 7.5;
    static constexpr float max_dR = 5.;

    class BaseFakeTree;
    typedef SelectionWithTitle<BaseFakeTree> BaseCut;

    class BaseFakeTree : public NtupleBranchMgr {
    public:
        BaseFakeTree(TTree* t);
        virtual ~BaseFakeTree() = default;

        /// List of branch members
        NtupleBranch<std::vector<Bool_t>> Ele_CorrIsol;
        NtupleBranch<std::vector<Bool_t>> Ele_Isol;
        NtupleBranch<std::vector<Bool_t>> Ele_PiD;
        NtupleBranch<std::vector<Float_t>> Ele_dRJet;
        NtupleBranch<std::vector<Float_t>> Ele_eta;
        NtupleBranch<std::vector<Float_t>> Ele_phi;
        NtupleBranch<std::vector<Float_t>> Ele_pt;

        NtupleBranch<Bool_t> ElecTrigger;
        NtupleBranch<Bool_t> IsBaseZ;
        NtupleBranch<Bool_t> IsBaseZVeto;
        NtupleBranch<Bool_t> IsBaseZZ;
        NtupleBranch<Bool_t> IsSigZ;
        NtupleBranch<Bool_t> IsSigZVeto;
        NtupleBranch<Bool_t> IsSigZZ;
        NtupleBranch<Bool_t> IsTrigger;
        NtupleBranch<Float_t> Meff;
        NtupleBranch<Float_t> Met;
        NtupleBranch<Float_t> Met_phi;

        NtupleBranch<std::vector<Bool_t>> Muo_CorrIsol;
        NtupleBranch<std::vector<Bool_t>> Muo_Isol;
        NtupleBranch<std::vector<Bool_t>> Muo_PiD;
        NtupleBranch<std::vector<Float_t>> Muo_dRJet;
        NtupleBranch<std::vector<Float_t>> Muo_eta;
        NtupleBranch<std::vector<Float_t>> Muo_phi;
        NtupleBranch<std::vector<Float_t>> Muo_pt;
        NtupleBranch<Bool_t> MuonTrigger;

        NtupleBranch<Double_t> NormWeight;

        NtupleBranch<UInt_t> NumBJets;
        NtupleBranch<UInt_t> NumJets;

        NtupleBranch<std::vector<Float_t>> Tau_eta;
        NtupleBranch<std::vector<Float_t>> Tau_phi;
        NtupleBranch<std::vector<Int_t>> Tau_prongs;
        NtupleBranch<std::vector<Float_t>> Tau_pt;
        NtupleBranch<std::vector<Bool_t>> Tau_signal;
        NtupleBranch<Double_t> prwWeight;

        virtual double eventWeight();

        float baseline_meff();
        float baseline_lep_ht();
        float signal_lep_ht();
        /// Return the lepton multiplicities
        unsigned int get_n_lep(unsigned int multip_mask);

        int get_lepton_flavour(size_t i);

        /// Returns the full bit mask of the i-th lepton
        int get_lepton_bitmask(size_t i);

        /// Is the i-th lepton lepton. Thereby, the lepton flavour
        /// bit must be set in the mask
        bool is_Lep(size_t i, unsigned int multip_mask);

        /// Electron and muon multiplicities
        virtual unsigned int get_n_ele(unsigned int multip_mask);
        virtual unsigned int get_n_muo(unsigned int multip_mask);
        virtual unsigned int get_n_tau(unsigned int multip_mask);

        /// Returns the bitmask of the i-th electron
        virtual int get_ele_bitmask(size_t i);
        /// Returns the bitmask of the i-th muon
        virtual int get_muo_bitmask(size_t i);
        /// Returns the bitmask of the i-th tau
        virtual int get_tau_bitmask(size_t i);

        /// Return the signal flag
        bool Muo_signal(size_t m);
        bool Ele_signal(size_t e);

        inline size_t Ele_size() { return Ele_CorrIsol.size(); }
        inline size_t Muo_size() { return Muo_CorrIsol.size(); }
        inline size_t Tau_size() { return Tau_signal.size(); }

        /// return the pt of the i-th lepton. Lepton flavour
        /// determines if it is electron or muon. If the bit is not set
        /// or the index exceeds the size exception is  thrown
        float get_pt(size_t i, unsigned int multip_mask);
        float get_eta(size_t i, unsigned int multip_mask);
        float get_phi(size_t i, unsigned int multip_mask);

        TLorentzVector get_p4(size_t, unsigned int multip_mask);

        bool pass_id(size_t i, unsigned int multip_mask);
        /// returns the transverse mass with the met
        float get_mt_met(size_t i, unsigned int multip_mask);

        float get_dphi_met(size_t i, unsigned int multip_mask);

        /// Returns the delta eta between the i-th and the j-th lepton
        /// All flavours are considered here
        float get_delta_eta(size_t i, size_t j);
        float get_delta_phi(size_t i, size_t j);
        float get_delta_R(size_t i, size_t j);

        ///   Returns the minimum separation of a light lepton
        ///   to a jet...
        float get_dR_minJet(size_t i, unsigned int multip_mask);

        float global_dR_minLep();
        float global_dR_minJet();

        /// Get the distance to the closest loose Electron
        float get_dR_LooseEle(size_t i);
        /// Get the distance to the closest loose Muon
        float get_dR_LooseMuo(size_t i);
        /// Get the distance to the closest signal electron
        float get_dR_SigEle(size_t i);
        /// Get the distance to the closest signal muon
        float get_dR_SigMuo(size_t i);

        /// The minimum separation to the next lepton irrespective
        /// of the flavour
        float get_dR_minLep(size_t i);

        /// Returns the dR to the closest lepton
        /// Thereby dR: 0 - 5  --> The close-by lepton is loose
        ///         dR: 5 - 10 --> The close-by lepton is signal
        float get_dR_minLooseSigLep(size_t i);

        /// Returns the dR to the closest lepton shifted by the flavour
        /// of the second
        ///         dR: 0 - 5  --> The close-by lepton has different flavour
        ///         dR: 5 - 10 --> The close-by lepton has same flavour
        float get_dR_minFlavLep(size_t i);

        /// Returns the index and the dR of the closest lepton
        const std::pair<size_t, float>& get_closest_lep(size_t i);

        /// That function is somehow more fancy as it tries to
        /// map the minimum separation between loose/signal electrons/muons
        /// to the interval -10 - 10
        ///   -10 - -5: - dR_Loose_Muo
        ///    -5 -  0: - dR_Loose_Ele
        ///     0 -  5:   dR_Signal_Ele
        ///     5 - 10:   dR_Signal_Muo
        float get_dR_LepSelFlav(size_t i);

        /// Transforms indices from the global [0;1,2,n-th_electron,0,1,2,n-th muon, 0,1,2,n-th tau]
        /// scheme to the local scheme and adapts the flavour mask accordingly
        void global_to_local(size_t& i, unsigned int& flav_mask);
        /// Reverse operation of the local_to_global function
        void local_to_global(size_t& i, unsigned int& flav_mask);

        /// Returns true if there is a lepton pair in the event with dR
        /// and the subleading lepton in the pair has pt below the threshold
        bool has_collimated_lep_below(float dR, float pt_cut);

        /// Dump the event to screen
        virtual void dump_event();
        bool isMC() const;

    protected:
        virtual bool cache_numbers();

    private:
        void cache_closest_dR(std::vector<std::pair<size_t, float>>& vec, unsigned int lep_mask);

        Long64_t m_last_cache;
        ///
        /// Signal multiplicities
        ///
        unsigned int m_n_INCL_Ele_Sig;
        unsigned int m_n_INCL_Muo_Sig;

        unsigned int m_n_INCL_Tau1P_Sig;
        unsigned int m_n_INCL_Tau3P_Sig;

        unsigned int m_n_INCL_Ele_Loose;
        unsigned int m_n_INCL_Muo_Loose;
        unsigned int m_n_INCL_Tau1P_Loose;
        unsigned int m_n_INCL_Tau3P_Loose;

        ///
        ///     Inclusive meff
        ///     component
        float m_loose_meff_comp;
        ///     Smallest lepton
        ///     delta R
        ///
        float m_min_lep_dR;
        float m_min_jet_dR;

        float m_base_HT;
        float m_signal_HT;
        ///
        /// Some components to remember
        ///
        /// Store the  indices of the closest
        /// signal and loose muon / electron

        /// Spare the code some time to find out which lepton is closer
        /// in dR and cache the dR to the closest lepton as well in the event
        std::vector<std::pair<size_t, float>> m_closest_signal_ele;
        std::vector<std::pair<size_t, float>> m_closest_signal_muo;

        std::vector<std::pair<size_t, float>> m_closest_loose_ele;
        std::vector<std::pair<size_t, float>> m_closest_loose_muo;

        bool m_isMC;
    };

}  // namespace SUSY4L
#endif  // NTAU__SUSY4L_FF__H
