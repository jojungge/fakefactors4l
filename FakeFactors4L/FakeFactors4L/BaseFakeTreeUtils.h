#ifndef FAKEFACTORS4L_BASEFAKETREEUTILS_H
#define FAKEFACTORS4L_BASEFAKETREEUTILS_H
#include <FakeFactors4L/BaseFakeTree.h>

namespace SUSY4L {

    typedef PlotFillInstructionWithRef<TH1D, BaseFakeTree> BaseFiller1D;
    typedef PlotFillInstructionWithRef<TH2D, BaseFakeTree> BaseFiller2D;
    typedef PlotFillInstructionWithRef<TH3D, BaseFakeTree> BaseFiller3D;

    ///
    ///      Basic helper class
    ///      to store the
    class IBaseFakeTreeFiller {
    public:
        IBaseFakeTreeFiller(std::shared_ptr<TH1> histo, unsigned int lep_mask, const std::string& fill_name = "",
                            const std::string& sample_name = "");

        /// The full lepton mask of the IBaseFakeTreeFiller...
        /// Meaning of each bit is defined in Utils.h
        unsigned int lepton_mask() const;

        /// Convert the  flavour to string
        std::string lepton_flavour() const;
        /// Convert the truth type to string
        std::string lepton_type() const;

        /// Convert the quality of the lepton to string
        std::string lepton_quality() const;

        /// Associated histogram to the Filler class
        std::shared_ptr<TH1> get_histo() const;

        /// Optional name of the sample
        std::string sample_name() const;
        /// Name of the filler
        virtual std::string name() const;

        virtual ~IBaseFakeTreeFiller() = default;

        /// Obtain a generic function to read out the tree
        template <class TreeType> std::function<float(TreeType&, size_t)> make_reader(TAxis* A) const;
        /// Make the  container size function
        template <class TreeType> std::function<size_t(TreeType&)> container_size() const;
        /// Define the lepton selection
        template <class TreeType> std::function<bool(TreeType&, size_t)> lepton_selection(unsigned int lep_sel) const;

    protected:
        bool is_lepton_variable(TAxis* A) const;
        /// Change the bit mask of the tool
        void change_bit_mask(unsigned int mask);

    private:
        unsigned int m_lep_mask;
        std::string m_name;
        std::string m_sample_name;
        std::shared_ptr<TH1> m_ref_histo;
    };

    ///
    /// Basic class to construct the fake-factor histograms
    ///
    class FakeHistoReader : public IBaseFakeTreeFiller {
    public:
        //// Return the fake factor histogram
        std::shared_ptr<TH1> ff_histo() const;
        //// Return the process fraction histogram
        std::shared_ptr<TH1> proc_histo() const;
        /// Return the scale-factor histogram
        std::shared_ptr<TH1> sf_histo() const;

        /// Standard constructor
        /// Lep_mask: Lepton flavour and truth bit
        /// fake_histo: The fake-factor
        /// Process fraction of the given process
        /// Scale factor histogram of a given process
        /// selection: Topologial cut of the fake-factor: E.g. meff > 100
        /// ff_y_reader, proc_y_reader: Analogus pandons
        FakeHistoReader(unsigned int lep_flav, std::shared_ptr<TH1> fake_histo, std::shared_ptr<TH1> proc_frac = nullptr,
                        std::shared_ptr<TH1> sf_histo = nullptr,
                        std::function<bool(BaseFakeTree& t, size_t)> = [](BaseFakeTree&, size_t) { return true; }

        );

        void set_associated_file(std::shared_ptr<TFile> f);
        std::shared_ptr<TFile> associated_file() const;

        /// Returns the fake-factor weight from the histograms
        /// If the selection is not passed then the returned weight is zero
        virtual float get_weight(BaseFakeTree& t, size_t i) const;

        /// prints fakefactor, scalefactor and processfraction
        /// in case something goes wrong (high weigths or 0 weights)
        void dump_weight(BaseFakeTree& t, size_t i) const;

        /// Does the selection pass at all?
        bool pass(BaseFakeTree& t, size_t i) const;

        /// Add systematic histograms for the process-fraction.. As they're not
        /// neccessarily independent from each other they'll need to be handled
        /// externally.
        void add_syst_up(unsigned int mask, std::shared_ptr<TH1> H);
        void add_syst_dn(unsigned int mask, std::shared_ptr<TH1> H);

        ///
        /// Okay here the fun kicks in we need to retrieve the systematic
        ///
        float get_syst_weight(BaseFakeTree& t, size_t i, unsigned int syst_mask) const;

    private:
        int get_bin(BaseFakeTree& t, size_t i, const std::shared_ptr<TH1>& H, const std::function<float(BaseFakeTree&, size_t)>& x,
                    const std::function<float(BaseFakeTree&, size_t)>& y, const std::function<float(BaseFakeTree&, size_t)>& z) const;
        int get_bin(TAxis* A, const float& value) const;

        float get_value(BaseFakeTree& t, size_t i, const std::shared_ptr<TH1>& H, const std::function<float(BaseFakeTree&, size_t)>& x,
                        const std::function<float(BaseFakeTree&, size_t)>& y, const std::function<float(BaseFakeTree&, size_t)>& z) const;

        float get_error(BaseFakeTree& t, size_t i, const std::shared_ptr<TH1>& H, const std::function<float(BaseFakeTree&, size_t)>& x,
                        const std::function<float(BaseFakeTree&, size_t)>& y, const std::function<float(BaseFakeTree&, size_t)>& z) const;

        float get_syst(BaseFakeTree& t, size_t i, const std::shared_ptr<TH1>& H, const std::function<float(BaseFakeTree&, size_t)>& x,
                       const std::function<float(BaseFakeTree&, size_t)>& y, const std::function<float(BaseFakeTree&, size_t)>& z,
                       bool do_up) const;

        unsigned int m_lep_flav;
        std::shared_ptr<TH1> m_ff_histo;
        std::shared_ptr<TH1> m_proc_histo;
        std::shared_ptr<TH1> m_sf_histo;
        std::shared_ptr<TFile> m_assoc_file;

        /// The systematic treatment of the process fractions
        /// is more tricky... We need to ensure that the
        /// unity condition is never violated --> For each
        /// systematic an own process fraction histo

        /// The map stores the proper process fraction histogram for
        /// the systematic under consideration
        std::map<unsigned int, std::shared_ptr<TH1>> m_syst_proc_frac_up;
        std::map<unsigned int, std::shared_ptr<TH1>> m_syst_proc_frac_dn;

        /// Function member to read out the x variable of the
        /// fake-factor histo
        std::function<float(BaseFakeTree&, size_t)> m_ff_x_reader;
        std::function<float(BaseFakeTree&, size_t)> m_ff_y_reader;
        std::function<float(BaseFakeTree&, size_t)> m_ff_z_reader;

        /// Function member to read out the variable of the process fraction
        /// histogram.
        std::function<float(BaseFakeTree&, size_t)> m_proc_x_reader;
        std::function<float(BaseFakeTree&, size_t)> m_proc_y_reader;
        std::function<float(BaseFakeTree&, size_t)> m_proc_z_reader;

        std::function<float(BaseFakeTree&, size_t)> m_sf_x_reader;
        std::function<float(BaseFakeTree&, size_t)> m_sf_y_reader;
        std::function<float(BaseFakeTree&, size_t)> m_sf_z_reader;

        /// Optional selection
        std::function<bool(BaseFakeTree& t, size_t)> m_sel_cut;
    };

    //  Efficiency projection weighter uses
    //  baseline leptons and evaluates for each truth
    //  type the efficiency to pass the signal selection
    //  criteria assuming two reals and two fakes
    //  SR: LLLL = RRFF
    // CR1: LLLl = RRFf + RRfF + RrFF + rRFF
    // CR2: LLll = RRff + RrFf + RrfF + rRFf + rRfF + rrFF
    // The question is how good does the weighting of the distributions by the corresponding
    // efficiencies actually represent the SR/CR1/CR2, respectively

    class EffiWeightHistoReader : public FakeHistoReader {
    public:
        /// Standard constructor
        /// Lep_mask: Lepton flavour, truth type and also the selection of loose or signal
        /// fake_histo: The fake-factor
        /// ff_x_reader: function to read off the x-axis from the fake-factor histogram
        /// selection: Topologial cut of the fake-factor: E.g. meff > 100
        EffiWeightHistoReader(unsigned int lep_flav, std::shared_ptr<TH1> fake_histo,
                              std::function<bool(BaseFakeTree&, size_t)> selec = [](BaseFakeTree&, size_t) { return true; });
        /// Returns the (in)-efficiency of the corresponding i-th lepton
        // If the lepton falvour or truth type does not match 1. is returned
        float get_weight(BaseFakeTree& t, size_t i) const override;

        bool to_signal() const;
    };

}  // namespace SUSY4L
#include <FakeFactors4L/BaseFakeTreeUtils.ixx>

#endif
