#ifndef FAKEFACTORS4L_BASEFAKETREEUTILS_IXX
#define FAKEFACTORS4L_BASEFAKETREEUTILS_IXX
#include <FakeFactors4L/BaseFakeTree.h>
namespace {

    /// The Lepton titles are important yo br kep up to date
    /// they deterimine whether a variable belongs to a lepton variable or not"
    static const std::vector<std::string> pt_titles{"p_{T}", "pT", "Pt", "pt", "p^{close-by}_{T}", "#pmp^{close-by}_{T}"};
    static const std::vector<std::string> abs_eta_titles{"|#eta|", "|eta|", "abs(#eta)", "abs(eta)"};
    static const std::vector<std::string> eta_titles{"#eta", "eta", "#phi"};
    static const std::vector<std::string> dR_titles{
        "#DeltaR(lep,lep)",
        "#DeltaR(lep,jet)",
        "#DeltaR_{signal/loose}",
        "#DeltaR^{e/#mu}_{signal/loose}",
        "#DeltaR^{e/#mu}_{signal/loose}",
        "#DeltaR_{e/#mu}",
        "close-by #it{l}#it{l}",
    };
    static const std::vector<std::string> mass_titles{
        "m_{#it{ll}}",
        "#pmm_{#it{ee/#mu#mu}}",
    };
}  // namespace
namespace SUSY4L {
    template <class TreeType> std::function<float(TreeType&, size_t)> IBaseFakeTreeFiller::make_reader(TAxis* A) const {
        if (!A) {
            return [](TreeType&, size_t) {
                std::cout << "Where is my Libre Office docker container on the grid? Axis not defined" << std::endl;
                return FLT_MAX;
            };
        }
        std::string title = A->GetTitle();
        std::cout << "Got Axis Title " << title << ".  ";
        if (title.find("m_{T}") == 0) {
            std::cout << " Read out the the transverse mass of the lepton" << std::endl;
            return [this](TreeType& t, size_t i) { return t.get_mt_met(i, lepton_mask()); };
        } else if (title.find("#Delta#phi(E^{miss}_{T}") == 0) {
            std::cout << " Read out the delta phi to the missing ET" << std::endl;
            return [this](TreeType& t, size_t i) -> float { return t.get_dphi_met(i, lepton_mask()); };
        } else if (title.find("|#Delta#phi(E^{miss}_{T}") == 0) {
            std::cout << " Read out the absoulte delta phi to the missing ET" << std::endl;
            return [this](TreeType& t, size_t i) -> float { return std::fabs(t.get_dphi_met(i, lepton_mask())); };
        } else if (title.find("|#Delta#eta|") != std::string::npos) {
            std::cout << " Read out the distance in pseudo rapidity" << std::endl;
            return [this](TreeType& t, size_t i) -> float {
                unsigned int mask = lepton_mask();
                t.local_to_global(i, mask);
                const std::pair<size_t, float>& best_j = t.get_closest_lep(i);
                return best_j.first != dummy_idx ? std::fabs(t.get_delta_eta(i, best_j.first)) : dummy_dR;
            };
        } else if (title.find("#Delta#eta") != std::string::npos) {
            std::cout << " Read out the distance in pseudo rapidity" << std::endl;
            return [this](TreeType& t, size_t i) -> float {
                unsigned int mask = lepton_mask();
                t.local_to_global(i, mask);
                const std::pair<size_t, float>& best_j = t.get_closest_lep(i);
                return best_j.first != dummy_idx ? t.get_delta_eta(i, best_j.first) : dummy_dR;
            };
        } else if (title.find("|#Delta#phi|") != std::string::npos) {
            std::cout << " Read out the distance in delta azimuth" << std::endl;
            return [this](TreeType& t, size_t i) -> float {
                unsigned int mask = lepton_mask();
                t.local_to_global(i, mask);
                const std::pair<size_t, float>& best_j = t.get_closest_lep(i);
                return best_j.first != dummy_idx ? std::fabs(t.get_delta_phi(i, best_j.first)) : dummy_dR;
            };
        } else if (title.find("#Delta#phi") != std::string::npos) {
            std::cout << " Read out the distance in delta azimuth" << std::endl;
            return [this](TreeType& t, size_t i) -> float {
                unsigned int mask = lepton_mask();
                t.local_to_global(i, mask);
                const std::pair<size_t, float>& best_j = t.get_closest_lep(i);
                return best_j.first != dummy_idx ? t.get_delta_phi(i, best_j.first) : dummy_dR;
            };
        } else if (isElementSubstr(pt_titles, title)) {
            std::cout << "Read out pt" << std::endl;
            return [this](TreeType& t, size_t i) { return t.get_pt(i, lepton_mask()); };
        } else if (isElementSubstr(abs_eta_titles, title)) {
            std::cout << "Read out abseta" << std::endl;
            return [this](TreeType& t, size_t i) { return std::fabs(t.get_eta(i, lepton_mask())); };
        } else if (isElementSubstr(eta_titles, title)) {
            std::cout << "Read out eta" << std::endl;
            return [this](TreeType& t, size_t i) { return t.get_eta(i, lepton_mask()); };
        } else if (title.find("#DeltaR(lep,lep)") != std::string::npos) {
            std::cout << "Read out closest lepton" << std::endl;
            return [this](TreeType& t, size_t i) {
                unsigned int mask = lepton_mask();
                t.local_to_global(i, mask);
                return t.get_dR_minLep(i);
            };
        } else if (title.find("#DeltaR(lep,jet)") != std::string::npos) {
            std::cout << "Read out closest jet" << std::endl;
            return [this](TreeType& t, size_t i) { return t.get_dR_minJet(i, lepton_mask()); };
        } else if (title.find("#DeltaR_{signal/loose}") != std::string::npos) {
            std::cout << "Read out closest lepton split into signal vs loose" << std::endl;
            return [this](TreeType& t, size_t i) {
                unsigned int mask = lepton_mask();
                t.local_to_global(i, mask);
                return t.get_dR_minLooseSigLep(i);
            };
        } else if (title.find("#DeltaR^{e/#mu}_{signal/loose}") != std::string::npos) {
            std::cout << "Read out closest lepton split into signal vs loose times SF vs DF" << std::endl;
            return [this](TreeType& t, size_t i) {
                unsigned int mask = lepton_mask();
                t.local_to_global(i, mask);
                return t.get_dR_LepSelFlav(i);
            };
        } else if (title.find("#DeltaR_{e/#mu}") != std::string::npos) {
            std::cout << "Read out closest lepton split into SF vs DF." << std::endl;
            return [this](TreeType& t, size_t i) {
                unsigned int mask = lepton_mask();
                t.local_to_global(i, mask);
                return t.get_dR_minFlavLep(i);
            };
        } else if (title.find("#pmm_{#it{ee/#mu#mu}}") != std::string::npos) {
            std::cout << "Read out the invariant mass to the closest lepton flavour dependent" << std::endl;
            return [this](TreeType& t, size_t i) -> float {
                unsigned int mask = lepton_mask();
                t.local_to_global(i, mask);
                size_t best_j = t.get_closest_lep(i).first;
                if (best_j == dummy_idx) return FLT_MAX;
                TLorentzVector v1(t.get_p4(i, mask)), v2(t.get_p4(best_j, LepClass::LepFlav));
                return (v1 + v2).M() * (t.get_lepton_flavour(i) == t.get_lepton_flavour(best_j) ? 1. : -1.);
            };
        } else if (title.find("m_{#it{ll}}") != std::string::npos) {
            std::cout << "Read out the invariant mass of the closest lepton Flavour agnostic" << std::endl;
            return [this](TreeType& t, size_t i) -> float {
                unsigned int mask = lepton_mask();
                t.local_to_global(i, mask);
                size_t best_j = t.get_closest_lep(i).first;
                if (best_j == dummy_idx) return FLT_MAX;
                TLorentzVector v1(t.get_p4(i, mask)), v2(t.get_p4(best_j, LepClass::LepFlav));
                return (v1 + v2).M();
            };
        }
        // Scalar variables
        else if (title.find("H_{T}^{jets} / m_{eff}") != std::string::npos) {
            std::cout << "Read out the H_{T}^{jet} fraction of the meff" << std::endl;
            return [](TreeType& t, size_t) { return (t.Meff() - t.Met() - t.signal_lep_ht()) / t.baseline_meff(); };
        } else if (title.find("E_{T}^{miss} / m_{eff}") != std::string::npos) {
            std::cout << "Read out the H_{T}^{jet} fraction of the meff" << std::endl;
            return [](TreeType& t, size_t) { return t.Met() / t.baseline_meff(); };
        } else if (title.find("H_{T}^{lep} / m_{eff}") != std::string::npos) {
            std::cout << "Read out the H_{T}^{jet} fraction of the meff" << std::endl;
            return [](TreeType& t, size_t) { return (t.signal_lep_ht()) / t.baseline_meff(); };
        } else if (title.find("m_{eff}") != std::string::npos) {
            std::cout << "Read out baseline meff" << std::endl;
            return [](TreeType& t, size_t) { return t.baseline_meff(); };
        } else if (title.find("E^{miss}_{T}") != std::string::npos) {
            std::cout << "Read out met" << std::endl;
            return [](TreeType& t, size_t) { return t.Met(); };
        } else if (title == "N_{jets}") {
            std::cout << "Read out jet multiplicity" << std::endl;
            return [this](TreeType& t, size_t) { return t.NumJets(); };
        } else if (title == "N_{b-jets}") {
            std::cout << "Read out jet multiplicity" << std::endl;
            return [this](TreeType& t, size_t) { return t.NumBJets(); };
        } else if (title.find("H_{T}^{jets}") != std::string::npos) {
            std::cout << "Read out ht jets" << std::endl;
            return [this](TreeType& t, size_t) { return t.Meff() - t.Met() - t.signal_lep_ht(); };
        } else if (title.find("H_{T}^{lep}") != std::string::npos) {
            std::cout << "Read out ht leptons" << std::endl;
            return [this](TreeType& t, size_t) { return t.baseline_lep_ht(); };
        }
        std::cout << "Unkown" << std::endl;
        return [A](TreeType&, size_t) {
            std::cout << "Axis title '" << A->GetTitle()
                      << "' unknown ... Please check the title and adapt in BaseFakeTreeUtils.ixx if neccessary" << std::endl;
            return FLT_MAX;
        };
    }

    template <class TreeType> std::function<size_t(TreeType&)> IBaseFakeTreeFiller::container_size() const {
        return [this](TreeType& t) { return t.get_n_lep(lepton_mask() | LepClass::Baseline | LepClass::Incl); };
    }
    template <class TreeType> std::function<bool(TreeType&, size_t)> IBaseFakeTreeFiller::lepton_selection(unsigned int lep_sel) const {
        return [this, lep_sel](TreeType& t, size_t i) { return t.is_Lep(i, lep_sel | lepton_mask()); };
    }
}  // namespace SUSY4L
#endif
