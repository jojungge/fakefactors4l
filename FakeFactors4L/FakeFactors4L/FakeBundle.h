#ifndef NTAU__FAKEBUNDLE__H
#define NTAU__FAKEBUNDLE__H
#include <XAMPPplotting/PlottingUtils.h>

#include <FakeFactors4L/BaseFakeTreeUtils.h>
#include <FakeFactors4L/FakeFactorTree.h>
#include <FakeFactors4L/SelectionWithTitle.h>
#include <FakeFactors4L/Utils.h>
#include <NtupleAnalysisUtils/Plot.h>
#include <TTree.h>
#include <vector>

namespace SUSY4L {
    /// Helper class to store the  Fake factors per selection
    ///

    typedef PlotFillInstructionWithRef<TH1D, FakeFactorTree> PlotFiller1D;
    typedef PlotFillInstructionWithRef<TH2D, FakeFactorTree> PlotFiller2D;
    typedef PlotFillInstructionWithRef<TH3D, FakeFactorTree> PlotFiller3D;

    class IFakeBundle : public IBaseFakeTreeFiller {
    public:
        /// Public constructor for all FakeBundle types
        /// reference template in any dimension
        /// Selection applied for the plot
        /// Bit_mask encoding which lepton has to be selected
        /// p_name: Plot name additional string if the same variable is filled with different templates
        /// smp_name: sample_name obtained from the Sample instance
        IFakeBundle(std::shared_ptr<TH1> h_ref, Selection<FakeFactorTree> selection, unsigned int lep_mask, const std::string& p_name = "",
                    const std::string& smp_name = "");
        virtual ~IFakeBundle() = default;

        std::string selection_str() const;
        virtual std::string name() const override;

        const Selection<FakeFactorTree>& get_selection() const;
        Selection<FakeFactorTree>& get_selection();

        ///
        /// Write everything into the ROOT file
        ///
        virtual void write_file(TFile* file);

    private:
        /// Helper struct to pipe the different signal selections
        /// To the std::functions
        Selection<FakeFactorTree> m_selection;
    };
    class FakeBundle : public IFakeBundle {
    public:
        /// Standard constructor
        ///     h_ref: Template histogram (TH1D),
        ///     sample: opened file sample, Selection:
        ///     selection: event selection
        ///     lep_mask: truth_type of the lepton and the flavour
        ///     selection_title: String which is printed on the canvas
        ///     p_name: Extra string to be put in the final histoname Useful for test of multiple binnings
        FakeBundle(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection, unsigned int lep_mask,
                   const std::string& p_name = "");

        Plot<TH1D> get_baseline();
        Plot<TH1D> get_loose();
        Plot<TH1D> get_signal();

        virtual PlotContent<TH1D> get_plot(CanvasOptions canvas_opt = CanvasOptions(), const std::string& selection_title = "");

        void write_file(TFile* file) override;

    protected:
        /// Make the filler for the plot dynamically
        /// lep_sel encodes thereby baseline/signal/loose
        PlotFiller1D make_filler(unsigned int lep_sel) const;

    private:
        Plot<TH1D> m_baseline;
        Plot<TH1D> m_signal;
        Plot<TH1D> m_loose;
    };
    /// Version to plot the vanilla versions of
    /// the fake-factors on top
    class FakeBundleVanilla : public FakeBundle {
    public:
        /// Standard constructor
        ///     h_ref: Template histogram (TH1D),
        ///     sample: opened file sample, Selection:
        ///     selection: event selection
        ///     lep_mask: truth_type of the lepton and the flavour
        ///     selection_title: String which is printed on the canvas
        ///     p_name: Extra string to be put in the final histoname Useful for test of multiple binnings
        FakeBundleVanilla(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection,
                          unsigned int lep_mask, const std::string& p_name = "");
        PlotContent<TH1D> get_plot(CanvasOptions canvas_opt = CanvasOptions(), const std::string& selection_title = "") override;

    private:
        Plot<TH1D> m_vanilla_signal;
        Plot<TH1D> m_vanilla_loose;
    };
    class FakeBundle2D : public IFakeBundle {
    public:
        /// Standard constructor
        ///     h_ref: Template histogram (TH2D),
        ///     sample: opened file sample, Selection:
        ///     selection: event selection
        ///     fill: Pt/Eta/AbsEta
        ///     lep_mask: truth_type of the lepton and the flavour
        ///     selection_title: String which is printed on the canvas
        ///     p_name: Extra string to be put in the final histoname Useful for test of multiple binnings
        FakeBundle2D(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection, unsigned int lep_mask,
                     const std::string& p_name = "");

        Plot<TH2D> get_baseline();
        Plot<TH2D> get_loose();
        Plot<TH2D> get_signal();

        PlotContent<TH2D> get_plot(CanvasOptions canvas_opt = CanvasOptions(), const std::string& selection_title = "");

        void write_file(TFile* file) override;

    private:
        /// Make the filler for the plot dynamically
        /// lep_sel encodes thereby baseline/signal/loose
        PlotFiller2D make_filler(unsigned int lep_sel) const;

        Plot<TH2D> m_baseline;
        Plot<TH2D> m_signal;
        Plot<TH2D> m_loose;
    };
    class FakeBundle3D : public IFakeBundle {
    public:
        /// Standard constructor
        ///     h_ref: Template histogram (TH2D),
        ///     sample: opened file sample, Selection:
        ///     selection: event selection
        ///     fill: Pt/Eta/AbsEta
        ///     lep_mask: truth_type of the lepton and the flavour
        ///     selection_title: String which is printed on the canvas
        ///     p_name: Extra string to be put in the final histoname Useful for test of multiple binnings
        FakeBundle3D(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection, unsigned int lep_mask,
                     const std::string& p_name = "");

        Plot<TH3D> get_baseline();
        Plot<TH3D> get_loose();
        Plot<TH3D> get_signal();

        PlotContent<TH3D> get_plot(CanvasOptions canvas_opt = CanvasOptions(), const std::string& selection_title = "");

        void write_file(TFile* file) override;

    private:
        /// Make the filler for the plot dynamically
        /// lep_sel encodes thereby baseline/signal/loose
        PlotFiller3D make_filler(unsigned int lep_sel) const;

        Plot<TH3D> m_baseline;
        Plot<TH3D> m_signal;
        Plot<TH3D> m_loose;
    };
    ///
    /// Helper class to calculate the process fractions of a given fake
    /// process in a control region
    class ProcessFractionBundle : public IFakeBundle {
    public:
        /// Standard constructor for particle variables
        ///     h_ref: Template histogram (TH1D),
        ///     sample: opened file sample, Selection:
        ///     selection: event selection
        ///     lep_mask: truth_types to plot, flavour and the selection
        ///     selection_title: String which is printed on the canvas
        ///     p_name: Extra string to be put in the final histoname Useful for test of multiple binnings
        ProcessFractionBundle(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection,
                              unsigned int lep_mask, const std::string& p_name = "");

        std::string name() const override;

        std::vector<Plot<TH1D>>& get_plots();

        PlotContent<TH1D> get_plot(CanvasOptions canvas_opt, const std::string& selection_title);

        void write_file(TFile* f) override;

    private:
        PlotFiller1D make_filler(unsigned int lep_sel) const;

        std::vector<Plot<TH1D>> m_plots;
    };

    class ProcessFractionBundle2D : public IFakeBundle {
    public:
        /// Standard constructor for particle variables
        ///     h_ref: Template histogram (TH1D),
        ///     sample: opened file sample, Selection:
        ///     selection: event selection
        ///     fill: Pt/Eta/AbsEta
        ///     lep_mask: truth_types to plot, flavour and the selection
        ///     selection_title: String which is printed on the canvas
        ///     p_name: Extra string to be put in the final histoname Useful for test of multiple binnings
        ProcessFractionBundle2D(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection,
                                unsigned int lep_mask, const std::string& p_name = "");

        std::string name() const override;

        std::vector<Plot<TH2D>>& get_plots();

        PlotContent<TH2D> get_plot(CanvasOptions canvas_opt, const std::string& selection_title);

        void write_file(TFile* f) override;

    private:
        PlotFiller2D make_filler(unsigned int lep_sel) const;
        std::vector<Plot<TH2D>> m_plots;
    };

    class ProcessFractionBundle3D : public IFakeBundle {
    public:
        /// Standard constructor for particle variables
        ///     h_ref: Template histogram (TH1D),
        ///     sample: opened file sample, Selection:
        ///     selection: event selection
        ///     fill: Pt/Eta/AbsEta
        ///     lep_mask: truth_types to plot, flavour and the selection
        ///     selection_title: String which is printed on the canvas
        ///     p_name: Extra string to be put in the final histoname Useful for test of multiple binnings
        ProcessFractionBundle3D(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection,
                                unsigned int lep_mask, const std::string& p_name = "");

        std::string name() const override;

        std::string lep_quality() const;
        std::vector<Plot<TH3D>>& get_plots();

        void write_file(TFile* f) override;

        PlotContent<TH3D> get_plot(CanvasOptions canvas_opt, const std::string& selection_title);

    private:
        PlotFiller3D make_filler(unsigned int lep_sel) const;

        std::vector<Plot<TH3D>> m_plots;
    };

    class DeltaRHisto : public IFakeBundle {
    public:
        DeltaRHisto(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection, unsigned int lep_mask,
                    CanvasOptions canvas_opt = CanvasOptions(), const std::string& selection_title = "", const std::string& p_name = "");

        std::string lep_quality();

        Plot<TH1D> get_DeltaR_FF();
        Plot<TH1D> get_DeltaR_RR();
        Plot<TH1D> get_DeltaR_RF();

        PlotContent<TH1D> get_plot();

    private:
        PlotFiller1D make_filler(unsigned int lep_sel1, unsigned int lep_sel2) const;

        Plot<TH1D> m_ff;
        Plot<TH1D> m_rr;
        Plot<TH1D> m_rf;

        PlotContent<TH1D> m_plot;
    };

}  // namespace SUSY4L
#endif  // NTAU__SUSY4L_FF__H
