#ifndef FAKEFACTORS4L_FAKECLOSURETREE_H
#define FAKEFACTORS4L_FAKECLOSURETREE_H
#include <XAMPPplotting/DataDrivenWeights.h>
///
#include <FakeFactors4L/FakeBundle.h>
#include <FakeFactors4L/FakeFactorTree.h>
#include <FakeFactors4L/Utils.h>
namespace SUSY4L {
    class FakeClosureTree;

    typedef PlotFillInstructionWithRef<TH1D, FakeClosureTree> ClosureFiller1D;
    typedef PlotFillInstructionWithRef<TH2D, FakeClosureTree> ClosureFiller2D;
    typedef SelectionWithTitle<FakeClosureTree> ClosureCut;

    typedef std::pair<std::shared_ptr<TFile>, std::string> fake_map_key;
    typedef std::map<fake_map_key, std::vector<std::shared_ptr<TH1>>> fake_map;

    /// Helper service to store the process fraction histogram
    /// to be shared across the FakeWeighter instances
    /// In this way the fractions can be normalized between the samples
    class ProcessFractionHolder {
    public:
        static ProcessFractionHolder* instance();
        ~ProcessFractionHolder();

        /// Returns the fake factors from the file
        std::vector<std::shared_ptr<TH1>> get_fake_factors(std::shared_ptr<TFile> f, const std::string& region);
        /// Returns the scale factors from the file
        std::vector<std::shared_ptr<TH1>> get_scale_factors(std::shared_ptr<TFile> f, const std::string& region);
        /// Returns the process fractions from the file
        std::vector<std::shared_ptr<TH1>> get_process_fractions(std::shared_ptr<TFile> f, const std::string& region);
        /// Returns the fake distributions from the file
        std::vector<std::shared_ptr<TH1>> get_fake_distributions(std::shared_ptr<TFile> f, const std::string& region);

        void new_proc_syst_histo(const std::shared_ptr<TH1>& nominal, const std::shared_ptr<TH1>& syst);

        /// Method to renormalize the process fractions
        /// of the fake-factors across the samples
        void normalize_process_fractions();

    private:
        static ProcessFractionHolder* m_inst;

        ProcessFractionHolder();
        /// Delete the copy constructors to emplace singelton pattern
        ProcessFractionHolder(const ProcessFractionHolder&) = delete;
        void operator=(const ProcessFractionHolder&) = delete;

        /// Check the map if it has loaded the histograms from a particular region and then
        /// cache them to the map otherwise
        std::vector<std::shared_ptr<TH1>> cache_to_map(std::shared_ptr<TFile> f, fake_map& map, const std::string& top_dir,
                                                       const std::string& region);

        std::vector<std::shared_ptr<TH1>> load_from_dir(TDirectory* f, const std::string& region);

        std::vector<std::shared_ptr<TH1>> make_proc_denominators(const std::string& region,
                                                                 const std::vector<std::shared_ptr<TFile>>& to_consider);
        std::vector<std::shared_ptr<TH1>> clone(const std::vector<std::shared_ptr<TH1>>& to_clone) const;

        std::vector<std::shared_ptr<TH1>> make_smp_fractions(const fake_map_key& fake_key,
                                                             const std::vector<std::shared_ptr<TH1>>& denominators);

        void normalize_syst_fractions(const std::shared_ptr<TH1>& num, const std::shared_ptr<TH1>& nominal);

        /// The fake rate histograms
        fake_map m_ff_histos;
        /// The scale-factor histograms
        fake_map m_sf_histos;
        /// The process fraction histograms
        fake_map m_proc_histos;
        /// The fake distributions of a given region
        fake_map m_incl_fake_histos;

        std::map<std::shared_ptr<TH1>, std::vector<std::shared_ptr<TH1>>> m_sys_proc_frac;
    };

    class FakeClosureTree : public FakeFactorTree {
    public:
        FakeClosureTree(TTree* t);

        using FakeFactorTree::baseline_lep_ht;
        using FakeFactorTree::baseline_meff;

        /// Returns the baseline meff in a given loose lepton CR
        /// Thereby only the X loose leptons are considered here
        /// If a CR event contains more than X leptons then the i-th permutation
        /// takes the X leptons from the loose and propagates it to the meff
        float baseline_meff(unsigned int ev_selection, unsigned int permutation);

        /// Holding the same logic here as above
        float baseline_lep_ht(unsigned int ev_selection, unsigned int permutation);

        /// Returns whether the i-th lepton
        /// can be propagated for the fake-factor
        /// method considering the k-th permutation
        bool is_promotable(size_t j, unsigned int ev_selection, unsigned int permutation);

        /// Indices of the loose leptons considering
        /// electrons and muons as a single vector
        /// starting from the electrons
        const std::vector<size_t>& get_loose_indices();

        /// Indices of all leptons in the event sorted
        /// by their truth classification
        const std::vector<size_t>& get_sorted_indices();

        ///
        /// Return the number of
        /// permutations
        unsigned int n_permutations(unsigned int ev_selection);

        std::shared_ptr<XAMPP::DrawElementsFromSet> LightLep_permuter_CR1() const;
        std::shared_ptr<XAMPP::DrawElementsFromSet> LightLep_permuter_CR2() const;
        bool skip_close_by() const;

    protected:
        bool cache_numbers() override;

    private:
        /// Combinatoric selection for CR1 electron/muons
        std::shared_ptr<XAMPP::DrawElementsFromSet> m_EleMuo_permut_cr1;
        /// Combinatoric selection for CR2 electron muons
        std::shared_ptr<XAMPP::DrawElementsFromSet> m_EleMuo_permut_cr2;
        /// Combinatoric selection for CR1 taus
        std::shared_ptr<XAMPP::DrawElementsFromSet> m_Tau_permut_cr1;
        /// Combinatoric selection for CR2 taus
        std::shared_ptr<XAMPP::DrawElementsFromSet> m_Tau_permut_cr2;

        std::vector<size_t> m_loose_idx_lep;

        /// Orderded list of all leptons by
        /// Real < HF < LF < CONV< Other
        /// And then by pt
        std::vector<size_t> m_all_idx;

        bool m_skip_close_by;
    };

    class IClosureFiller : public IBaseFakeTreeFiller {
    public:
        IClosureFiller(std::shared_ptr<TH1> H, unsigned int lep_mask,
                       Selection<FakeClosureTree> sr_sel = Selection<FakeClosureTree>("", [](FakeClosureTree&) { return true; }),
                       const std::string& smp_name = "");
        const Selection<FakeClosureTree>& get_selection() const;

    protected:
        virtual int event_selection(FakeClosureTree& t) const;
        std::function<float(FakeClosureTree&, size_t)> make_reader(TAxis* A) const;

    private:
        Selection<FakeClosureTree> m_selection;
    };

    class FakeWeighter : public IClosureFiller {
    public:
        /// Get the intrinsic sign of the CR
        short sign() const;
        /// Calculate the fake-factor weight for the CR
        double get_weight(FakeClosureTree& t) const;
        /// Calculate the fake-factor weight for the i-th permutation
        double get_weight(FakeClosureTree& t, unsigned int permut) const;

        /// prints fakefactor, scalefactor and processfraction for every faketype
        /// in case something goes wrong (high weigths or 0 weights)
        void dump_weight(FakeClosureTree& t, unsigned int permut) const;

        double get_syst_weight(FakeClosureTree& t, unsigned int syst, unsigned int permut) const;

        /// Design multiplicity
        unsigned int design_multip() const;

        /// Standard constructor
        /// lep_mask: Lepton flavour
        /// design number: How many fakes should go in this fake-factor
        FakeWeighter(unsigned int lepmask, unsigned int design_number, short sign, Selection<FakeClosureTree> apply_in);

        /// Load the histograms from the file
        /// and transform them into FakeHistoReaders
        bool load_histos(unsigned int lep_flav, std::shared_ptr<TFile> f, const std::string& ff_histo, const std::string& ff_region,
                         const std::string& proc_histo, const std::string& proc_region, const std::string& sf_histo,
                         const std::string& sf_region,
                         std::function<bool(BaseFakeTree&, size_t)> sel_func = [](BaseFakeTree&, size_t) { return true; });
        ///
        ///     Version without sf histos
        ///
        bool load_histos(unsigned int lep_flav, std::shared_ptr<TFile> f, const std::string& ff_histo, const std::string& ff_region,
                         const std::string& proc_histo, const std::string& proc_region,
                         std::function<bool(BaseFakeTree&, size_t)> sel_func = [](BaseFakeTree&, size_t) { return true; });

        /// Helper method to
        /// define the systematic variations
        /// The process fraction histograms are copied and each
        /// fraction is varied by 1 sigma while maintaining the boundary conditions
        /// that the fractions sum up to 1
        void setup_systematics();

        ////    How many weighters
        ////    are actually defined
        size_t n_weigthers() const;

        ////    Bit mask encoding the truth_types
        ////    and the fake-components (FF/SF/ProcFrac)
        ////    to be used for systematics
        unsigned int get_syst_mask() const;

        std::vector<std::shared_ptr<FakeHistoReader>> get_components() const;

    private:
        std::vector<std::shared_ptr<TH1>> filter_variable(const std::vector<std::shared_ptr<TH1>>& in_histos, const std::string& h_key,
                                                          unsigned int lep_flav) const;
        std::shared_ptr<TH1> find_truth_histo(const std::vector<std::shared_ptr<TH1>>& vec, unsigned int truth_bit) const;

        unsigned int m_design_fakes;
        short m_sign;
        std::vector<std::shared_ptr<FakeHistoReader>> m_ff_histos;
    };

    class FakeClosureHisto : public IClosureFiller {
    public:
        FakeClosureHisto(std::shared_ptr<TH1> histo, unsigned int lep_mask, Sample<FakeClosureTree> sample,
                         Selection<FakeClosureTree> sr_selection, Selection<FakeClosureTree> cr1_selection,
                         Selection<FakeClosureTree> cr2_selection, const std::string& add_name = "");

        /// Returns the pointer to the CR1 weighter
        std::shared_ptr<FakeWeighter> CR1_weighter() const;
        ///  Returns the pointer to the CR2 weighter
        std::shared_ptr<FakeWeighter> CR2_weighter() const;

        /// Overwrite the CR1 weighter instance
        void set_CR1_weighter(std::shared_ptr<FakeWeighter> W);
        /// Overwrite the CR2 weighter instance
        void set_CR2_weighter(std::shared_ptr<FakeWeighter> W);

        Plot<TH1D>& SR_plot();
        Plot<TH1D>& CR_plot();
        Plot<TH1D>& CR1_component();
        Plot<TH1D>& CR2_component();

        std::string name() const override;

        PlotContent<TH1D> get_plot(const std::vector<std::string>& legend, CanvasOptions canvas_opt);

        ///
        ///     Write non-closure to
        ///     a ROOT file
        void write_file(TFile* file);

    private:
        ClosureFiller1D make_filler(unsigned int reweight) const;
        int event_selection(FakeClosureTree& t) const override;
        /// SR selection piped to the
        Selection<FakeClosureTree> m_cr1_selection;
        Selection<FakeClosureTree> m_cr2_selection;

        std::shared_ptr<FakeWeighter> m_ff_weight_CR1;
        std::shared_ptr<FakeWeighter> m_ff_weight_CR2;

        std::shared_ptr<TH1> m_th1_template;
        std::string m_add_name;
        /// Unweighted SR histogram
        Plot<TH1D> m_sr_histo;
        /// CR1 histogram only
        Plot<TH1D> m_cr1_histo;
        /// abs(CR2) histogram
        Plot<TH1D> m_cr2_histo;

        /// Fully weighted CR histogram

        Plot<TH1D> m_cr_histo;
    };
    ///
    ///
    ///
    class FakeClosureSystematic : public IClosureFiller {
    public:
        /// Calculate the fake-factor weight for the i-th permutation
        double get_weight(FakeClosureTree& t) const;

        /// Standard constructor
        /// lep_mask: Lepton flavour
        /// design number: How many fakes should go in this fake-factor
        FakeClosureSystematic(Selection<FakeClosureTree> apply_in, const std::string& non_closure_for);

        bool load_histos(unsigned int lep_flav, std::shared_ptr<TFile> f, const std::string& ff_histo, const std::string& ff_region,
                         const std::string& proc_histo, const std::string& proc_region,
                         std::function<bool(BaseFakeTree&, size_t)> sel_func = [](BaseFakeTree&, size_t) { return true; });

        std::vector<std::shared_ptr<FakeHistoReader>> get_components() const;
        std::shared_ptr<NtupleBranch<double>> get_branch() const;
        std::string label() const;

        void set_branch(std::shared_ptr<NtupleBranch<double>> br);

    private:
        std::vector<std::shared_ptr<TH1>> filter_variable(const std::vector<std::shared_ptr<TH1>>& in_histos, const std::string& h_key,
                                                          unsigned int lep_flav) const;

        std::vector<std::shared_ptr<FakeHistoReader>> m_ff_histos;
        std::shared_ptr<NtupleBranch<double>> m_branch;
        std::string m_label;
    };

    ///     Helper class managing all the EffiWeight histos and
    ///     then multiplying the weight for the i-the lepton
    ///
    class EffiWeighter : public IClosureFiller {
    public:
        /// Standard constructor
        EffiWeighter();
        bool load_eff_weights(std::shared_ptr<TFile> f, const std::string& ff_dir_ele, const std::string& ff_dir_muo,
                              const std::string& ff_variable = "");

        float get_weight(FakeClosureTree& t) const;

    private:
        std::vector<std::shared_ptr<EffiWeightHistoReader>> m_efficiencies;
    };
}  // namespace SUSY4L
#endif
