#ifndef FAKEFACTORS4L_FAKEFACTORTREE_H
#define FAKEFACTORS4L_FAKEFACTORTREE_H
#include <FakeFactors4L/BaseFakeTree.h>
#include <TTree.h>
#include <vector>

namespace SUSY4L {

    class FakeFactorTree;
    typedef SelectionWithTitle<FakeFactorTree> Cut;

    class FakeFactorTree : public BaseFakeTree {
    public:
        FakeFactorTree(TTree* t);
        virtual ~FakeFactorTree() = default;
        /// List of branch members
        NtupleBranch<std::vector<Bool_t>> Ele_isConv;
        NtupleBranch<std::vector<Bool_t>> Ele_isHF;
        NtupleBranch<std::vector<Bool_t>> Ele_isLF;
        NtupleBranch<std::vector<Bool_t>> Ele_isReal;

        NtupleBranch<std::vector<Bool_t>> Muo_isHF;
        NtupleBranch<std::vector<Bool_t>> Muo_isLF;
        NtupleBranch<std::vector<Bool_t>> Muo_isReal;

        NtupleBranch<std::vector<Bool_t>> Tau_isEle;
        NtupleBranch<std::vector<Bool_t>> Tau_isGJ;
        NtupleBranch<std::vector<Bool_t>> Tau_isHF;
        NtupleBranch<std::vector<Bool_t>> Tau_isLF;
        NtupleBranch<std::vector<Bool_t>> Tau_isReal;

        /// Electron and muon multiplicities
        unsigned int get_n_ele(unsigned int multip_mask) override;
        unsigned int get_n_muo(unsigned int multip_mask) override;
        unsigned int get_n_tau(unsigned int multip_mask) override;

        int get_ele_bitmask(size_t i) override;
        int get_muo_bitmask(size_t i) override;
        int get_tau_bitmask(size_t i) override;

    protected:
        virtual bool cache_numbers() override;

    private:
        ///
        /// Baseline multiplicities
        ///
        unsigned int m_n_REAL_Muo;
        unsigned int m_n_REAL_Ele;
        unsigned int m_n_REAL_Tau1P;
        unsigned int m_n_REAL_Tau3P;

        unsigned int m_n_HF_Ele;
        unsigned int m_n_HF_Muo;
        unsigned int m_n_HF_Tau1P;
        unsigned int m_n_HF_Tau3P;

        unsigned int m_n_LF_Ele;
        unsigned int m_n_LF_Muo;
        unsigned int m_n_LF_Tau1P;
        unsigned int m_n_LF_Tau3P;

        unsigned int m_n_OTHER_Ele;
        unsigned int m_n_OTHER_Muo;
        unsigned int m_n_OTHER_Tau1P;
        unsigned int m_n_OTHER_Tau3P;

        unsigned int m_n_CONV_Ele;

        unsigned int m_n_GJ_Tau1P;
        unsigned int m_n_GJ_Tau3P;

        unsigned int m_n_ELEC_Tau1P;
        unsigned int m_n_ELEC_Tau3P;

        ///
        /// Signal multiplicities
        ///
        unsigned int m_n_REAL_Muo_Sig;
        unsigned int m_n_REAL_Ele_Sig;
        unsigned int m_n_REAL_Tau1P_Sig;
        unsigned int m_n_REAL_Tau3P_Sig;

        unsigned int m_n_HF_Ele_Sig;
        unsigned int m_n_HF_Muo_Sig;
        unsigned int m_n_HF_Tau1P_Sig;
        unsigned int m_n_HF_Tau3P_Sig;

        unsigned int m_n_LF_Ele_Sig;
        unsigned int m_n_LF_Muo_Sig;
        unsigned int m_n_LF_Tau1P_Sig;
        unsigned int m_n_LF_Tau3P_Sig;

        unsigned int m_n_OTHER_Ele_Sig;
        unsigned int m_n_OTHER_Muo_Sig;
        unsigned int m_n_OTHER_Tau1P_Sig;
        unsigned int m_n_OTHER_Tau3P_Sig;

        unsigned int m_n_CONV_Ele_Sig;

        unsigned int m_n_GJ_Tau1P_Sig;
        unsigned int m_n_GJ_Tau3P_Sig;

        unsigned int m_n_ELEC_Tau1P_Sig;
        unsigned int m_n_ELEC_Tau3P_Sig;
    };

}  // namespace SUSY4L
#endif  // NTAU__SUSY4L_FF__H
