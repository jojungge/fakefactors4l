#ifndef FAKEFACTORS4L__FakeTreeWriter__H
#define FAKEFACTORS4L__FakeTreeWriter__H

#include <FakeFactors4L/FakeClosureTree.h>
namespace SUSY4L {
    class FakeTreeWriter : public FakeClosureTree {
    public:
        FakeTreeWriter(TTree* t);
        /// List of branch members
        NtupleBranch<Double_t> EleMuTriggerSF;
        NtupleBranch<Double_t> EleTriggerSF;
        NtupleBranch<Double_t> EleWeight;
        NtupleBranch<std::vector<Int_t>> Ele_IFFType;
        NtupleBranch<std::vector<float>> Ele_d0Sig;
        NtupleBranch<std::vector<Int_t>> Ele_q;
        NtupleBranch<std::vector<float>> Ele_z0Sin;
        NtupleBranch<Int_t> EventNumber;
        NtupleBranch<Double_t> GGM0Weight;
        NtupleBranch<Double_t> GGM100Weight;
        NtupleBranch<Double_t> GGM25Weight;
        NtupleBranch<Double_t> GGM75Weight;

        NtupleBranch<Bool_t> Is2L1T1l;           // CR1_3L1T
        NtupleBranch<Bool_t> Is2L1T1lZ;          // CR1_3L1T
        NtupleBranch<Bool_t> Is2L1T1lZ_bonly;    // CR1_3L1T
        NtupleBranch<Bool_t> Is2L1T1lZ_bveto;    // CR1_3L1T
        NtupleBranch<Bool_t> Is2L1T1l_bonly;     // CR1_3L1T
        NtupleBranch<Bool_t> Is2L1T1l_bveto;     // CR1_3L1T
        NtupleBranch<Bool_t> Is2L1T1lnoZ;        // CR1_3L1T
        NtupleBranch<Bool_t> Is2L1T1lnoZ_bonly;  // CR1_3L1T
        NtupleBranch<Bool_t> Is2L1T1lnoZ_bveto;  // CR1_3L1T

        NtupleBranch<Bool_t> Is2L1T1t;           // CR1_2L2T
        NtupleBranch<Bool_t> Is2L1T1tZ;          // CR1_2L2T
        NtupleBranch<Bool_t> Is2L1T1tZ_bonly;    // CR1_2L2T
        NtupleBranch<Bool_t> Is2L1T1tZ_bveto;    // CR1_2L2T
        NtupleBranch<Bool_t> Is2L1T1t_bonly;     // CR1_2L2T
        NtupleBranch<Bool_t> Is2L1T1t_bveto;     // CR1_2L2T
        NtupleBranch<Bool_t> Is2L1T1tnoZ;        // CR1_2L2T
        NtupleBranch<Bool_t> Is2L1T1tnoZ_bonly;  // CR1_2L2T
        NtupleBranch<Bool_t> Is2L1T1tnoZ_bveto;  // CR1_2L2T

        NtupleBranch<Bool_t> Is2L1l1t;           // CR2_3L1T
        NtupleBranch<Bool_t> Is2L1l1tZ;          // CR2_3L1T
        NtupleBranch<Bool_t> Is2L1l1tZ_bonly;    // CR2_3L1T
        NtupleBranch<Bool_t> Is2L1l1tZ_bveto;    // CR2_3L1T
        NtupleBranch<Bool_t> Is2L1l1t_bonly;     // CR2_3L1T
        NtupleBranch<Bool_t> Is2L1l1t_bveto;     // CR2_3L1T
        NtupleBranch<Bool_t> Is2L1l1tnoZ;        // CR2_3L1T
        NtupleBranch<Bool_t> Is2L1l1tnoZ_bonly;  // CR2_3L1T
        NtupleBranch<Bool_t> Is2L1l1tnoZ_bveto;  // CR2_3L1T

        NtupleBranch<Bool_t> Is2L2l;           // CR2_4L0T
        NtupleBranch<Bool_t> Is2L2lZ;          // CR2_4L0T
        NtupleBranch<Bool_t> Is2L2lZZ;         // CR2_4L0T
        NtupleBranch<Bool_t> Is2L2lZZ_bveto;   // CR2_4L0T
        NtupleBranch<Bool_t> Is2L2lZ_bveto;    // CR2_4L0T
        NtupleBranch<Bool_t> Is2L2l_CR_ZZ;     // CR2_4L0T
        NtupleBranch<Bool_t> Is2L2l_CR_ttZ;    // CR2_4L0T
        NtupleBranch<Bool_t> Is2L2l_bveto;     // CR2_4L0T
        NtupleBranch<Bool_t> Is2L2lnoZ;        // CR2_4L0T
        NtupleBranch<Bool_t> Is2L2lnoZ_bveto;  // CR2_4L0T

        NtupleBranch<Bool_t> Is2L2t;           // CR2_2L2T
        NtupleBranch<Bool_t> Is2L2tZ;          // CR2_2L2T
        NtupleBranch<Bool_t> Is2L2tZ_bonly;    // CR2_2L2T
        NtupleBranch<Bool_t> Is2L2tZ_bveto;    // CR2_2L2T
        NtupleBranch<Bool_t> Is2L2t_bonly;     // CR2_2L2T
        NtupleBranch<Bool_t> Is2L2t_bveto;     // CR2_2L2T
        NtupleBranch<Bool_t> Is2L2tnoZ;        // CR2_2L2T
        NtupleBranch<Bool_t> Is2L2tnoZ_bonly;  // CR2_2L2T
        NtupleBranch<Bool_t> Is2L2tnoZ_bveto;  // CR2_2L2T

        NtupleBranch<Bool_t> Is3L1l;           // CR1_4L0T
        NtupleBranch<Bool_t> Is3L1lZ;          // CR1_4L0T
        NtupleBranch<Bool_t> Is3L1lZZ;         // CR1_4L0T
        NtupleBranch<Bool_t> Is3L1lZZ_bveto;   // CR1_4L0T
        NtupleBranch<Bool_t> Is3L1lZ_bveto;    // CR1_4L0T
        NtupleBranch<Bool_t> Is3L1l_CR_ZZ;     // CR1_4L0T
        NtupleBranch<Bool_t> Is3L1l_CR_ttZ;    // CR1_4L0T
        NtupleBranch<Bool_t> Is3L1l_bveto;     // CR1_4L0T
        NtupleBranch<Bool_t> Is3L1lnoZ;        // CR1_4L0T
        NtupleBranch<Bool_t> Is3L1lnoZ_bveto;  // CR1_4L0T

        NtupleBranch<Bool_t> Is3L1t;           // CR1_3L1T
        NtupleBranch<Bool_t> Is3L1tZ;          // CR1_3L1T
        NtupleBranch<Bool_t> Is3L1tZ_bonly;    // CR1_3L1T
        NtupleBranch<Bool_t> Is3L1tZ_bveto;    // CR1_3L1T
        NtupleBranch<Bool_t> Is3L1t_bonly;     // CR1_3L1T
        NtupleBranch<Bool_t> Is3L1t_bveto;     // CR1_3L1T
        NtupleBranch<Bool_t> Is3L1tnoZ;        // CR1_3L1T
        NtupleBranch<Bool_t> Is3L1tnoZ_bonly;  // CR1_3L1T
        NtupleBranch<Bool_t> Is3L1tnoZ_bveto;  // CR1_3L1T

        NtupleBranch<Bool_t> IsReal2L1T1l;           // CR1_3L1T
        NtupleBranch<Bool_t> IsReal2L1T1lZ;          // CR1_3L1T
        NtupleBranch<Bool_t> IsReal2L1T1lZ_bonly;    // CR1_3L1T
        NtupleBranch<Bool_t> IsReal2L1T1lZ_bveto;    // CR1_3L1T
        NtupleBranch<Bool_t> IsReal2L1T1l_bonly;     // CR1_3L1T
        NtupleBranch<Bool_t> IsReal2L1T1l_bveto;     // CR1_3L1T
        NtupleBranch<Bool_t> IsReal2L1T1lnoZ;        // CR1_3L1T
        NtupleBranch<Bool_t> IsReal2L1T1lnoZ_bonly;  // CR1_3L1T
        NtupleBranch<Bool_t> IsReal2L1T1lnoZ_bveto;  // CR1_3L1T

        NtupleBranch<Bool_t> IsReal2L1T1t;           // CR1_2L2T
        NtupleBranch<Bool_t> IsReal2L1T1tZ;          // CR1_2L2T
        NtupleBranch<Bool_t> IsReal2L1T1tZ_bonly;    // CR1_2L2T
        NtupleBranch<Bool_t> IsReal2L1T1tZ_bveto;    // CR1_2L2T
        NtupleBranch<Bool_t> IsReal2L1T1t_bonly;     // CR1_2L2T
        NtupleBranch<Bool_t> IsReal2L1T1t_bveto;     // CR1_2L2T
        NtupleBranch<Bool_t> IsReal2L1T1tnoZ;        // CR1_2L2T
        NtupleBranch<Bool_t> IsReal2L1T1tnoZ_bonly;  // CR1_2L2T
        NtupleBranch<Bool_t> IsReal2L1T1tnoZ_bveto;  // CR1_2L2T

        NtupleBranch<Bool_t> IsReal2L1l1t;           // CR2_3L1T
        NtupleBranch<Bool_t> IsReal2L1l1tZ;          // CR2_3L1T
        NtupleBranch<Bool_t> IsReal2L1l1tZ_bonly;    // CR2_3L1T
        NtupleBranch<Bool_t> IsReal2L1l1tZ_bveto;    // CR2_3L1T
        NtupleBranch<Bool_t> IsReal2L1l1t_bonly;     // CR2_3L1T
        NtupleBranch<Bool_t> IsReal2L1l1t_bveto;     // CR2_3L1T
        NtupleBranch<Bool_t> IsReal2L1l1tnoZ;        // CR2_3L1T
        NtupleBranch<Bool_t> IsReal2L1l1tnoZ_bonly;  // CR2_3L1T
        NtupleBranch<Bool_t> IsReal2L1l1tnoZ_bveto;  // CR2_3L1T

        NtupleBranch<Bool_t> IsReal2L2l;           // CR2_4L0T
        NtupleBranch<Bool_t> IsReal2L2lZ;          // CR2_4L0T
        NtupleBranch<Bool_t> IsReal2L2lZZ;         // CR2_4L0T
        NtupleBranch<Bool_t> IsReal2L2lZZ_bveto;   // CR2_4L0T
        NtupleBranch<Bool_t> IsReal2L2lZ_bveto;    // CR2_4L0T
        NtupleBranch<Bool_t> IsReal2L2l_CR_ZZ;     // CR2_4L0T
        NtupleBranch<Bool_t> IsReal2L2l_CR_ttZ;    // CR2_4L0T
        NtupleBranch<Bool_t> IsReal2L2l_bveto;     // CR2_4L0T
        NtupleBranch<Bool_t> IsReal2L2lnoZ;        // CR2_4L0T
        NtupleBranch<Bool_t> IsReal2L2lnoZ_bveto;  // CR2_4L0T

        NtupleBranch<Bool_t> IsReal2L2t;           // CR2_2L2T
        NtupleBranch<Bool_t> IsReal2L2tZ;          // CR2_2L2T
        NtupleBranch<Bool_t> IsReal2L2tZ_bonly;    // CR2_2L2T
        NtupleBranch<Bool_t> IsReal2L2tZ_bveto;    // CR2_2L2T
        NtupleBranch<Bool_t> IsReal2L2t_bonly;     // CR2_2L2T
        NtupleBranch<Bool_t> IsReal2L2t_bveto;     // CR2_2L2T
        NtupleBranch<Bool_t> IsReal2L2tnoZ;        // CR2_2L2T
        NtupleBranch<Bool_t> IsReal2L2tnoZ_bonly;  // CR2_2L2T
        NtupleBranch<Bool_t> IsReal2L2tnoZ_bveto;  // CR2_2L2T

        NtupleBranch<Bool_t> IsReal3L1l;           // CR1_4L0T
        NtupleBranch<Bool_t> IsReal3L1lZ;          // CR1_4L0T
        NtupleBranch<Bool_t> IsReal3L1lZZ;         // CR1_4L0T
        NtupleBranch<Bool_t> IsReal3L1lZZ_bveto;   // CR1_4L0T
        NtupleBranch<Bool_t> IsReal3L1lZ_bveto;    // CR1_4L0T
        NtupleBranch<Bool_t> IsReal3L1l_CR_ZZ;     // CR1_4L0T
        NtupleBranch<Bool_t> IsReal3L1l_CR_ttZ;    // CR1_4L0T
        NtupleBranch<Bool_t> IsReal3L1l_bveto;     // CR1_4L0T
        NtupleBranch<Bool_t> IsReal3L1lnoZ;        // CR1_4L0T
        NtupleBranch<Bool_t> IsReal3L1lnoZ_bveto;  // CR1_4L0T

        NtupleBranch<Bool_t> IsReal3L1t;           // CR1_3L1T
        NtupleBranch<Bool_t> IsReal3L1tZ;          // CR1_3L1T
        NtupleBranch<Bool_t> IsReal3L1tZ_bonly;    // CR1_3L1T
        NtupleBranch<Bool_t> IsReal3L1tZ_bveto;    // CR1_3L1T
        NtupleBranch<Bool_t> IsReal3L1t_bonly;     // CR1_3L1T
        NtupleBranch<Bool_t> IsReal3L1t_bveto;     // CR1_3L1T
        NtupleBranch<Bool_t> IsReal3L1tnoZ;        // CR1_3L1T
        NtupleBranch<Bool_t> IsReal3L1tnoZ_bonly;  // CR1_3L1T
        NtupleBranch<Bool_t> IsReal3L1tnoZ_bveto;  // CR1_3L1T

        NtupleBranch<Double_t> JetWeight;
        NtupleBranch<Double_t> LLEWeight;
        NtupleBranch<UInt_t> LumiBlock;

        NtupleBranch<Float_t> Mll_Z1;
        NtupleBranch<Float_t> Mll_Z2;
        NtupleBranch<Float_t> Mt_LepMax;
        NtupleBranch<Float_t> Mt_LepMin;
        NtupleBranch<Double_t> MuTriggerSF;
        NtupleBranch<Double_t> MuoWeight;
        NtupleBranch<std::vector<Int_t>> Muo_IFFType;
        NtupleBranch<std::vector<float>> Muo_d0Sig;
        NtupleBranch<std::vector<Int_t>> Muo_q;
        NtupleBranch<std::vector<float>> Muo_z0Sin;
        NtupleBranch<Int_t> NJets;
        NtupleBranch<Int_t> NLooseElectrons;
        NtupleBranch<Int_t> NLooseLep;
        NtupleBranch<Int_t> NLooseMuons;
        NtupleBranch<Int_t> NLooseTaus;
        NtupleBranch<Int_t> NSignalElec;
        NtupleBranch<Int_t> NSignalLep;
        NtupleBranch<Int_t> NSignalMuon;
        NtupleBranch<Int_t> NSignalTaus;
        NtupleBranch<std::vector<Bool_t>> SigJet_bjet;
        NtupleBranch<std::vector<Float_t>> SigJet_eta;
        NtupleBranch<std::vector<Float_t>> SigJet_phi;
        NtupleBranch<std::vector<Float_t>> SigJet_pt;
        NtupleBranch<Double_t> TauWeight;
        NtupleBranch<std::vector<Int_t>> Tau_q;
        NtupleBranch<Int_t> runNumber;
        NtupleBranch<Double_t> xSection_Uncertainty;
        NtupleBranch<Float_t> FakefactorWeight;

        int get_ele_bitmask(size_t i) override;
        int get_muo_bitmask(size_t i) override;
        int get_tau_bitmask(size_t i) override;

        bool Is3L1T();
        bool Is3L1TZ();
        bool Is3L1TZ_bonly();
        bool Is3L1TZ_bveto();
        bool Is3L1T_bonly();
        bool Is3L1T_bveto();
        bool Is3L1TnoZ();
        bool Is3L1TnoZ_bonly();
        bool Is3L1TnoZ_bveto();

        bool Is2L2T();
        bool Is2L2TZ();
        bool Is2L2TZ_bonly();
        bool Is2L2TZ_bveto();
        bool Is2L2T_bonly();
        bool Is2L2T_bveto();
        bool Is2L2TnoZ();
        bool Is2L2TnoZ_bonly();
        bool Is2L2TnoZ_bveto();

        bool Is4L0T();
        bool Is4L0TZ();
        bool Is4L0TZZ();
        bool Is4L0TnoZ();

        bool Is4L0TZZ_bveto();
        bool Is4L0TZ_bveto();
        bool Is4L0T_bveto();
        bool Is4L0TnoZ_bveto();

        bool Is4L0TZZ_bonly();
        bool Is4L0TZ_bonly();
        bool Is4L0T_bonly();
        bool Is4L0TnoZ_bonly();

        bool Is4L0T_CR_ZZ();
        bool Is4L0T_CR_ttZ();

        ///
        /// Cautious the definitions must be synchronized by hand
        /// what's in the plotting configs of XAMPPmultilep
        ///    https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmultilep/blob/master/XAMPPmultilep/data/RunConf/Regions/SR/SR0.conf
        ///    https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmultilep/blob/master/XAMPPmultilep/data/RunConf/Regions/SR/SR1.conf
        ///    https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmultilep/blob/master/XAMPPmultilep/data/RunConf/Regions/SR/SR2.conf
        ///    https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmultilep/blob/master/XAMPPmultilep/data/RunConf/Regions/SR/VR0.conf
        ///    https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmultilep/blob/master/XAMPPmultilep/data/RunConf/Regions/SR/VR1.conf
        ///    https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmultilep/blob/master/XAMPPmultilep/data/RunConf/Regions/SR/VR2.conf

        bool IsCR_ZZ(unsigned int permutation);
        bool IsCR_ttZ(unsigned int permutation);

        bool IsSR0A(unsigned int permutation);
        bool IsSR0A_bincl(unsigned int permutation);
        bool IsSR0B(unsigned int permutation);
        bool IsSR0B_bincl(unsigned int permutation);
        bool IsSR0C(unsigned int permutation);
        bool IsSR0C_bveto(unsigned int permutation);
        bool IsSR0D(unsigned int permutation);
        bool IsSR0E(unsigned int permutation);
        bool IsSR0F(unsigned int permutation);
        bool IsSR0G(unsigned int permutation);

        bool IsSR1A(unsigned int permutation);
        bool IsSR1A_bincl(unsigned int permutation);
        bool IsSR1A_bonly(unsigned int permutation);
        bool IsSR1B(unsigned int permutation);
        bool IsSR1C(unsigned int permutation);

        bool IsSR2A(unsigned int permutation);
        bool IsSR2A_bincl(unsigned int permutation);
        bool IsSR2B(unsigned int permutation);
        bool IsSR2C(unsigned int permutation);

        bool IsVR0(unsigned int permutation);
        bool IsVR0ZZ(unsigned int permutation);
        bool IsVR0ZZ_bincl(unsigned int permutation);
        bool IsVR0_bincl(unsigned int permutation);
        bool IsVR0_ttZ(unsigned int permutation);

        bool IsVR1(unsigned int permutation);
        bool IsVR1Z(unsigned int permutation);
        bool IsVR1Z_bincl(unsigned int permutation);
        bool IsVR1_bincl(unsigned int permutation);
        bool IsVR2(unsigned int permutation);
        bool IsVR2Z(unsigned int permutation);
        bool IsVR2Z_bincl(unsigned int permutation);
        bool IsVR2_bincl(unsigned int permutation);

        bool Is3L1T_bonly(unsigned int permutation);
        bool Is3L1TZ_bonly(unsigned int permutation);
        bool Is3L1TnoZ_bonly(unsigned int permutation);

        bool Is3L1T_bveto(unsigned int permutation);
        bool Is3L1TZ_bveto(unsigned int permutation);
        bool Is3L1TnoZ_bveto(unsigned int permutation);

        bool Is2L2T_bonly(unsigned int permutation);
        bool Is2L2TZ_bonly(unsigned int permutation);
        bool Is2L2TnoZ_bonly(unsigned int permutation);

        bool Is2L2T_bveto(unsigned int permutation);
        bool Is2L2TZ_bveto(unsigned int permutation);
        bool Is2L2TnoZ_bveto(unsigned int permutation);

        static void set_lumi(double l);
        /// Overload the event weight defined for MC and data:
        ///  MC:    NormWeight*LLEWeight*JetWeight*MuoWeight*EleWeight*TauWeight*EleTriggerSF*LLEWeight...
        ///  Data: 1 / lumi
        double eventWeight() override;
        /// Returns the list of indices to write in the event respecting the permutation
        /// if there are more than one loose particles
        std::vector<size_t> write_particles(unsigned int lep_flav, unsigned int permutation);

        /// Returns the bit mask of the event selection to be applied
        /// for the proper permutation of the loose particles
        unsigned int get_selection();

        ///
        using FakeClosureTree::baseline_meff;
        float baseline_meff(unsigned int permutation);

        UInt_t num_jets(unsigned int permutation);
        UInt_t num_bjets(unsigned int permutation);

        ///     Process fractions are a real nightmare and of course we have
        ///     partially overlapping regions requiring different process fractions
        ///     SR0C/SR0D  vs.   SR0E/SR0F
        ///     X_incl     vs.   X_bveto
        ///     The idea is to write the event multiple times
        ///     exchanging the process fraction in each iteration

        enum BSelectionMode {
            BVeto = 1,            /// Accept only regions with b-veto,
            BSelection = 1 << 1,  /// Accept only regions with b-selection,
            BInclusive = 1 << 2,  /// Accept only regions inclusive in b

            PassThrough = BVeto | BSelection | BInclusive  /// Disable the filter,

        };
        //// To exclusively assign the b-selection process fractions
        //// against the b-veto and b-inclusive process fractions
        //// the b_selection_mode provides the mechanism to do so
        ///  The corresponding regions only react if their proper bit
        ///  is set otherwise they return false
        void set_b_selection_mode(unsigned short mode = BSelectionMode::PassThrough);
        unsigned b_selection_mode() const;

        /// These functions are simply to determine which b selection mode
        /// is applied in the event
        bool select_bjets() const;
        bool veto_bjets() const;
        bool inclusive_bjets() const;

        /// Returns the bitmask which regions responds to a kind of b-selections
        unsigned short possible_b_selection_modes(unsigned int permutation);

        using FakeClosureTree::dump_event;
        void dump_event() override;

    private:
        bool cache_numbers() override;
        static double m_lumi;
        unsigned int m_current_CR;

        /// global indices of the loose taus overlapping with
        /// the jets
        std::vector<size_t> m_or_tau_jets;
        std::vector<UInt_t> m_n_jets;
        std::vector<UInt_t> m_n_bjets;
        //// Cached values of the lepton and jet HT variables
        std::vector<float> m_lep_HT;
        std::vector<float> m_jet_HT;

        unsigned short m_b_sel_mode;
    };

}  // namespace SUSY4L
#endif  // NTAU__FakeTreeWriter__H
