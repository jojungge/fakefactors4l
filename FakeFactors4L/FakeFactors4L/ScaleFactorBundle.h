#ifndef NTAU__SCALEFACTORBUNDLE__H
#define NTAU__SCALEFACTORBUNDLE__H
#include <XAMPPplotting/PlottingUtils.h>

#include <FakeFactors4L/BaseFakeTree.h>
#include <FakeFactors4L/BaseFakeTreeUtils.h>
#include <FakeFactors4L/FakeBundle.h>
#include <FakeFactors4L/ScaleFactorTree.h>
#include <FakeFactors4L/SelectionWithTitle.h>
#include <FakeFactors4L/Utils.h>
#include <NtupleAnalysisUtils/Plot.h>
#include <TH1D.h>
#include <TTree.h>
#include <math.h>
#include <vector>
namespace SUSY4L {

    ///
    /// Helper class to store the  Fake factors per selection
    ///

    typedef PlotFillInstructionWithRef<TH1D, ScaleFactorTree> PlotFiller1DSF;
    typedef PlotFillInstructionWithRef<TH2D, ScaleFactorTree> PlotFiller2DSF;
    typedef PlotFillInstructionWithRef<TH3D, ScaleFactorTree> PlotFiller3DSF;

    class IScaleFactorBundle : public IBaseFakeTreeFiller {
    public:
        /// Public constructor for all ScaleFactorBundle types
        /// reference template in any dimension
        /// Selection applied for the plot
        /// Bit_mask encoding which lepton has to be selected
        /// p_name: Plot name additional string if the same variable is filled with different templates
        /// smp_name: sample_name obtained from the Sample instance
        IScaleFactorBundle(std::shared_ptr<TH1> h_ref, SFCut selection, unsigned int lep_mask, const std::string& p_name = "",
                           size_t tagged_lepton = dummy_idx);
        virtual ~IScaleFactorBundle() = default;

        std::string selection_str() const;

        size_t tagged_lepton() const;
        bool tagging_disabled() const;

        const SFCut& get_selection() const;
        SFCut& get_selection();

        ///
        /// Write everything into the ROOT file
        ///
        virtual void write_file(TFile*);

        virtual std::string name() const override;

        /// Define the lepton mask to be used for the
        /// non signal component of the MC usually LepClass::Incl;
        static void set_non_signal_mc(unsigned int flav_mask);
        static unsigned int non_signal_mc();

    protected:
        /// Obtain a generic function to read out the tree
        std::function<float(ScaleFactorTree&, size_t)> make_sf_reader(TAxis* A) const;

    private:
        /// Helper struct to pipe the different signal selections
        /// To the std::functions
        SFCut m_selection;
        size_t m_tagged_lepton;
        bool m_disableTagging;

        static unsigned int m_non_signal_mc_types;
    };

    ///
    /// FakeFactor bundle for the T&P selection
    ///

    template <class plot_type> class TagProbeFakeFactor : public IScaleFactorBundle {
    public:
        TagProbeFakeFactor(std::shared_ptr<TH1> h_ref, Sample<ScaleFactorTree> smp, SFCut selection, unsigned int lep_mask,
                           const std::string& p_name, size_t tagged_lepton);
        TagProbeFakeFactor(std::shared_ptr<TH1> h_ref, Sample<ScaleFactorTree> smp, SFCut selection, unsigned int lep_mask,
                           const std::string& p_name, size_t tagged_lepton, PlotPostProcessor<plot_type, plot_type> post_proc);

        TagProbeFakeFactor(TagProbeFakeFactor<plot_type>& in, TagProbeFakeFactor<plot_type>& subtract);

        Plot<plot_type>& get_baseline();
        Plot<plot_type>& get_signal();
        Plot<plot_type>& get_loose();

        void populate();

        Plot<plot_type> make_ff(PlotUtils::EfficiencyMode errmode = PlotUtils::defaultErrors);

    private:
        PlotPostProcessor<plot_type, plot_type> subtract_other(Plot<plot_type>& oplot);

        std::function<bool(ScaleFactorTree& t, size_t)> tag_probe_selection(unsigned int lep_sel) const;

        PlotFillInstructionWithRef<plot_type, ScaleFactorTree> make_filler(unsigned int lep_sel) const;
        Plot<plot_type> m_baseline;
        Plot<plot_type> m_signal;
        Plot<plot_type> m_loose;
    };

    template <class plot_type> class ZllFakeFactor : public IScaleFactorBundle {
    public:
        ZllFakeFactor(TagProbeFakeFactor<plot_type>& Zee, TagProbeFakeFactor<plot_type>& Zmumu);

        Plot<plot_type>& get_signal();
        Plot<plot_type>& get_baseline();
        Plot<plot_type>& get_loose();

        void populate();

        Plot<plot_type> make_ff(PlotUtils::EfficiencyMode errmode = PlotUtils::defaultErrors);

    private:
        PlotPostProcessor<plot_type, plot_type> combine(Plot<plot_type>& Zee, Plot<plot_type>& Zmumu);
        Plot<plot_type> m_baseline;
        Plot<plot_type> m_signal;
        Plot<plot_type> m_loose;
    };

    class ScaleFactorBundle : public IScaleFactorBundle {
    public:
        ScaleFactorBundle(std::shared_ptr<TH1> h_ref, Sample<ScaleFactorTree> MCsample, Sample<ScaleFactorTree> Datasample, SFCut selection,
                          unsigned int lep_mask, const std::string& p_name = "", size_t tagged_lepton = dummy_idx);

        /// Makes the fake-factor plot for the signal process type
        PlotContent<TH1D> get_plot_mc(CanvasOptions canvas_opt);
        /// Creates the fake-factor plot for the other types
        PlotContent<TH1D> get_plot_mc_bkg(CanvasOptions canvas_opt);
        /// Creates the fake-factor plot for data where the other types have
        /// been subtracted from it
        PlotContent<TH1D> get_plot_data(CanvasOptions canvas_opt);
        ///
        ///
        PlotContent<TH1D> get_data_mc_plot(CanvasOptions canvas_opt, unsigned int sel_mask);

        /// Plot the fake-factors of the signal process type and data over eachother to
        /// obtain the final scale-factor
        PlotContent<TH1D> CreateSFPlot(CanvasOptions canvas_opt);

        void write_file(TFile* f) override;

        TagProbeFakeFactor<TH1D>& get_TP_mc();
        TagProbeFakeFactor<TH1D>& get_TP_mc_other();
        TagProbeFakeFactor<TH1D>& get_TP_data();
        TagProbeFakeFactor<TH1D>& get_TP_data_raw();

    private:
        /// The Fake factor of interest
        TagProbeFakeFactor<TH1D> m_mc_ff_interest;
        TagProbeFakeFactor<TH1D> m_mc_ff_other;
        /// Raw data histograms
        TagProbeFakeFactor<TH1D> m_data_raw;
        /// Data histograms minus mc_ff_other
        TagProbeFakeFactor<TH1D> m_data;
    };
    class ZllScaleFactorBundle : public IScaleFactorBundle {
    public:
        ZllScaleFactorBundle(std::shared_ptr<ScaleFactorBundle> Zee, std::shared_ptr<ScaleFactorBundle> Zmumu);

        /// Makes the fake-factor plot for the signal process type
        PlotContent<TH1D> get_plot_mc(CanvasOptions canvas_opt);
        /// Creates the fake-factor plot for the other types
        PlotContent<TH1D> get_plot_mc_bkg(CanvasOptions canvas_opt);
        /// Creates the fake-factor plot for data where the other types have
        /// been subtracted from it
        PlotContent<TH1D> get_plot_data(CanvasOptions canvas_opt);

        /// Plot the fake-factors of the signal process type and data over eachother to
        /// obtain the final scale-factor
        PlotContent<TH1D> CreateSFPlot(CanvasOptions canvas_opt);

        void write_file(TFile* f) override;

    private:
        std::shared_ptr<ScaleFactorBundle> m_Zee;
        std::shared_ptr<ScaleFactorBundle> m_Zmumu;

        ZllFakeFactor<TH1D> m_mc_ff_interest;
        ZllFakeFactor<TH1D> m_mc_ff_other;
        ZllFakeFactor<TH1D> m_data;
    };

    class TagProbeProcFracBundle : public IScaleFactorBundle {
    public:
        TagProbeProcFracBundle(std::shared_ptr<TH1> h_ref, Sample<ScaleFactorTree> MCsample, SFCut selection, unsigned int lep_mask,
                               const std::string& p_name = "", size_t tagged_lepton = dummy_idx);

        PlotContent<TH1D> get_plot(CanvasOptions canvas_opt);

    private:
        std::vector<TagProbeFakeFactor<TH1D>> m_break_down;
    };

    class ScaleFactorBundle2D : public IScaleFactorBundle {
    public:
        ScaleFactorBundle2D(std::shared_ptr<TH1> h_ref, Sample<ScaleFactorTree> MCsample, Sample<ScaleFactorTree> Datasample,
                            SFCut selection, unsigned int lep_mask, const std::string& p_name = "", size_t tagged_lepton = dummy_idx,
                            PlotPostProcessor<TH2D, TH2D> = PlotPostProcessor<TH2D, TH2D>("", [](std::shared_ptr<TH2D> h) { return h; }));

        PlotContent<TH2D> get_plot_mc(CanvasOptions canvas_opt);
        PlotContent<TH2D> get_plot_data(CanvasOptions canvas_opt);
        PlotContent<TH2D> CreateSFPlot(CanvasOptions canvas_opt);

        void write_file(TFile* f) override;

    private:
        /// The Fake factor of interest
        TagProbeFakeFactor<TH2D> m_mc_ff_interest;
        TagProbeFakeFactor<TH2D> m_mc_ff_other;
        /// Raw data histograms
        TagProbeFakeFactor<TH2D> m_data_raw;
        /// Data histograms minus mc_ff_other
        TagProbeFakeFactor<TH2D> m_data;
    };

}  // namespace SUSY4L
#include <FakeFactors4L/ScaleFactorBundle.ixx>
#endif  // NTAU__SUSY4L_FF__H
