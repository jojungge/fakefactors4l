#include <FakeFactors4L/ScaleFactorBundle.h>
namespace SUSY4L {
    //####################################
    //      TagProbeFakeFactor<plot_type>
    //####################################
    template <class plot_type> Plot<plot_type>& TagProbeFakeFactor<plot_type>::get_baseline() { return m_baseline; }
    template <class plot_type> Plot<plot_type>& TagProbeFakeFactor<plot_type>::get_loose() { return m_loose; }
    template <class plot_type> Plot<plot_type>& TagProbeFakeFactor<plot_type>::get_signal() { return m_signal; }
    template <class plot_type>
    std::function<bool(ScaleFactorTree& t, size_t)> TagProbeFakeFactor<plot_type>::tag_probe_selection(unsigned int lep_sel) const {
        std::function<bool(ScaleFactorTree & t, size_t)> base_func = lepton_selection<ScaleFactorTree>(lep_sel);
        return [this, base_func](ScaleFactorTree& t, size_t i) { return base_func(t, i) && t.is_probe(i, lepton_mask()); };
    }

    template <class plot_type> void TagProbeFakeFactor<plot_type>::populate() {
        m_baseline.populate();
        m_loose.populate();
        m_signal.populate();
    }
    template <class plot_type> Plot<plot_type> TagProbeFakeFactor<plot_type>::make_ff(PlotUtils::EfficiencyMode error) {
        return Plot<plot_type>(m_signal,
                               PlotPostProcessor<plot_type, plot_type>("FakeFactorMaker" + this->name(),
                                                                       [this, error](std::shared_ptr<plot_type> H) {
                                                                           m_loose.populate();
                                                                           return std::dynamic_pointer_cast<plot_type>(std::shared_ptr<TH1>(
                                                                               PlotUtils::getRatio(H.get(), m_loose.getHisto(), error)));
                                                                       }),
                               lepton_type() + " fake factor");
    }

    template <class plot_type>
    TagProbeFakeFactor<plot_type>::TagProbeFakeFactor(std::shared_ptr<TH1> h_ref, Sample<ScaleFactorTree> smp, SFCut selection,
                                                      unsigned int lep_mask, const std::string& p_name, size_t tagged_lepton) :
        IScaleFactorBundle(h_ref, selection, lep_mask, p_name, tagged_lepton),
        m_baseline(smp, selection, make_filler(LepClass::Baseline), "baseline", "PL", PlotFormat().MarkerStyle(kFullSquare).Color(kBlack)),
        m_signal(smp, selection, make_filler(LepClass::Signal), "signal", "PL",
                 PlotFormat().MarkerStyle(kFullTriangleUp).MarkerSize(1.2).Color(kCyan - 2)),
        m_loose(smp, selection, make_filler(LepClass::Loose), "loose", "PL", PlotFormat().MarkerStyle(kFullDiamond).Color(kRed + 1)) {}

    template <class plot_type>
    TagProbeFakeFactor<plot_type>::TagProbeFakeFactor(std::shared_ptr<TH1> h_ref, Sample<ScaleFactorTree> smp, SFCut selection,
                                                      unsigned int lep_mask, const std::string& p_name, size_t tagged_lepton,
                                                      PlotPostProcessor<plot_type, plot_type> post_proc) :
        IScaleFactorBundle(h_ref, selection, lep_mask, p_name, tagged_lepton),
        m_baseline(smp, selection, make_filler(LepClass::Baseline), post_proc, "baseline", "PL",
                   PlotFormat().MarkerStyle(kFullSquare).Color(kBlack)),
        m_signal(smp, selection, make_filler(LepClass::Signal), post_proc, "signal", "PL",
                 PlotFormat().MarkerStyle(kFullTriangleUp).MarkerSize(1.2).Color(kCyan - 2)),
        m_loose(smp, selection, make_filler(LepClass::Loose), post_proc, "loose", "PL",
                PlotFormat().MarkerStyle(kFullDiamond).Color(kRed + 1)) {}
    template <class plot_type>
    TagProbeFakeFactor<plot_type>::TagProbeFakeFactor(TagProbeFakeFactor<plot_type>& in, TagProbeFakeFactor<plot_type>& other) :
        IScaleFactorBundle(in.get_histo(), in.get_selection(), in.lepton_mask(), in.name(), in.tagged_lepton()),
        m_baseline(in.get_baseline(), subtract_other(other.get_baseline()), "baseline", "PL",
                   PlotFormat().MarkerStyle(kFullSquare).Color(kBlack).MarkerSize(1.5)),
        m_signal(in.get_signal(), subtract_other(other.get_signal()), "signal", "PL",
                 PlotFormat().MarkerStyle(kFullTriangleUp).MarkerSize(1.5).Color(kCyan - 2)),
        m_loose(in.get_loose(), subtract_other(other.get_loose()), "loose", "PL",
                PlotFormat().MarkerStyle(kFullDiamond).Color(kRed + 1).MarkerSize(1.5)) {}

    template <class plot_type>
    PlotPostProcessor<plot_type, plot_type> TagProbeFakeFactor<plot_type>::subtract_other(Plot<plot_type>& oplot) {
        return PlotPostProcessor<plot_type, plot_type>("subtract" + oplot.getName(), [&oplot](std::shared_ptr<plot_type> in) {
            oplot.populate();
            std::shared_ptr<plot_type> other_h = oplot.getSharedHisto();
            in->Add(other_h.get(), -1.);
            truncate(in, 0, DBL_MAX);
            return in;
        });
    }
    template <class plot_type>
    PlotFillInstructionWithRef<plot_type, ScaleFactorTree> TagProbeFakeFactor<plot_type>::make_filler(unsigned int lep_sel) const {
        std::function<float(ScaleFactorTree & t, size_t)> func_x = this->make_sf_reader(get_histo()->GetXaxis());
        std::function<float(ScaleFactorTree & t, size_t)> func_y = this->make_sf_reader(get_histo()->GetYaxis());
        std::function<float(ScaleFactorTree & t, size_t)> func_z = this->make_sf_reader(get_histo()->GetZaxis());

        if (tagging_disabled()) {
            return PlotFillInstructionWithRef<plot_type, ScaleFactorTree>(
                "FakeFactor_" + this->selection_str() + "_" + this->name() + "_" + selection_to_str(lep_sel) + "_" +
                    type_to_str(lep_sel | lepton_mask()),
                this->container_size<ScaleFactorTree>(), this->tag_probe_selection(lep_sel),
                [func_x, func_y, func_z](plot_type* h, ScaleFactorTree& t, size_t i) {
                    int bin = -1;
                    if (h->GetDimension() == 1) {
                        bin = h->GetXaxis()->FindBin(func_x(t, i));
                    } else if (h->GetDimension() == 2) {
                        bin = h->GetBin(h->GetXaxis()->FindBin(func_x(t, i)), h->GetYaxis()->FindBin(func_y(t, i)));
                    } else if (h->GetDimension() == 3) {
                        bin = h->GetBin(h->GetXaxis()->FindBin(func_x(t, i)), h->GetYaxis()->FindBin(func_y(t, i)),
                                        h->GetZaxis()->FindBin(func_z(t, i)));
                    }
                    double w = t.eventWeight();
                    h->SetBinContent(bin, h->GetBinContent(bin) + w);
                    h->SetBinError(bin, std::hypot(h->GetBinError(bin), w));
                },
                dynamic_cast<plot_type*>(get_histo().get()));
        }
        std::function<size_t(ScaleFactorTree&)> size_func = container_size<ScaleFactorTree>();
        std::function<bool(ScaleFactorTree&, size_t)> sel_func = lepton_selection<ScaleFactorTree>(lep_sel);
        return PlotFillInstructionWithRef<plot_type, ScaleFactorTree>(
            "FakeFactor_" + this->selection_str() + "_" + this->name() + "_" + selection_to_str(lep_sel) + "_" +
                type_to_str(lep_sel | lepton_mask()),
            [this, func_x, func_y, func_z, size_func, sel_func, lep_sel](plot_type* h, ScaleFactorTree& t) {
                size_t i = this->tagged_lepton();

                if (size_func(t) <= i || !sel_func(t, i)) return;
                int bin = -1;
                if (h->GetDimension() == 1) {
                    bin = h->GetXaxis()->FindBin(func_x(t, i));
                } else if (h->GetDimension() == 2) {
                    bin = h->GetBin(h->GetXaxis()->FindBin(func_x(t, i)), h->GetYaxis()->FindBin(func_y(t, i)));
                } else if (h->GetDimension() == 3) {
                    bin = h->GetBin(h->GetXaxis()->FindBin(func_x(t, i)), h->GetYaxis()->FindBin(func_y(t, i)),
                                    h->GetZaxis()->FindBin(func_z(t, i)));
                }
                double w = t.eventWeight();
                h->SetBinContent(bin, h->GetBinContent(bin) + w);
                h->SetBinError(bin, std::hypot(h->GetBinError(bin), w));
            },
            dynamic_cast<plot_type*>(get_histo().get()));
    }

    //####################################
    //      TagProbeFakeFactor<plot_type>
    //####################################
    template <class plot_type> Plot<plot_type>& ZllFakeFactor<plot_type>::get_baseline() { return m_baseline; }
    template <class plot_type> Plot<plot_type>& ZllFakeFactor<plot_type>::get_loose() { return m_loose; }
    template <class plot_type> Plot<plot_type>& ZllFakeFactor<plot_type>::get_signal() { return m_signal; }
    template <class plot_type>
    ZllFakeFactor<plot_type>::ZllFakeFactor(TagProbeFakeFactor<plot_type>& Zee, TagProbeFakeFactor<plot_type>& Zmumu) :
        IScaleFactorBundle(Zee.get_histo(), Zee.get_selection() || Zmumu.get_selection(), Zee.lepton_mask() | Zmumu.lepton_mask(),
                           Zee.IBaseFakeTreeFiller::name()),

        m_baseline(Zee.get_baseline(), combine(Zee.get_baseline(), Zmumu.get_baseline()), "baseline", "PL",
                   PlotFormat().MarkerStyle(kFullSquare).Color(kBlack).MarkerSize(1.5)),
        m_signal(Zee.get_signal(), combine(Zee.get_signal(), Zmumu.get_signal()), "signal", "PL",
                 PlotFormat().MarkerStyle(kFullTriangleUp).MarkerSize(1.5).Color(kCyan - 2)),
        m_loose(Zee.get_loose(), combine(Zee.get_loose(), Zmumu.get_loose()), "loose", "PL",
                PlotFormat().MarkerStyle(kFullDiamond).Color(kRed + 1).MarkerSize(1.5)) {}

    template <class plot_type>
    PlotPostProcessor<plot_type, plot_type> ZllFakeFactor<plot_type>::combine(Plot<plot_type>& Zee, Plot<plot_type>& Zmumu) {
        return PlotPostProcessor<plot_type, plot_type>("combine " + Zee.getName() + " and " + Zmumu.getName(),
                                                       [&Zee, &Zmumu](std::shared_ptr<plot_type> in) {
                                                           Zee.populate();
                                                           Zmumu.populate();
                                                           in->Reset();
                                                           in->Add(Zmumu.getHisto());
                                                           in->Add(Zee.getHisto());
                                                           return in;
                                                       });
    }
    template <class plot_type> void ZllFakeFactor<plot_type>::populate() {
        m_baseline.populate();
        m_loose.populate();
        m_signal.populate();
    }

    template <class plot_type> Plot<plot_type> ZllFakeFactor<plot_type>::make_ff(PlotUtils::EfficiencyMode error) {
        return Plot<plot_type>(m_signal,
                               PlotPostProcessor<plot_type, plot_type>("FakeFactorMaker" + this->name(),
                                                                       [this, error](std::shared_ptr<plot_type> H) {
                                                                           m_loose.populate();
                                                                           return std::dynamic_pointer_cast<plot_type>(std::shared_ptr<TH1>(
                                                                               PlotUtils::getRatio(H.get(), m_loose.getHisto(), error)));
                                                                       }),
                               lepton_type() + " fake factor");
    }

}  // namespace SUSY4L
