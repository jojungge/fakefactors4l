#ifndef FAKEFACTORS4L_SCALEFACTORTREE_H
#define FAKEFACTORS4L_SCALEFACTORTREE_H
#include <FakeFactors4L/FakeFactorTree.h>
#include <TTree.h>
#include <vector>

namespace SUSY4L {

    class ScaleFactorTree;
    typedef SelectionWithTitle<ScaleFactorTree> SFCut;

    class ScaleFactorTree : public FakeFactorTree {
    public:
        ScaleFactorTree(TTree* t);
        virtual ~ScaleFactorTree() = default;
        double eventWeight() override;

        // Scale-factors to be applied for MC trees
        NtupleBranch<Double_t> EleMuTriggerSF;
        NtupleBranch<Double_t> EleTriggerSF;
        NtupleBranch<Double_t> MuTriggerSF;
        NtupleBranch<Double_t> EleWeight;
        NtupleBranch<Double_t> MuoWeight;
        NtupleBranch<Double_t> TauWeight;
        NtupleBranch<Double_t> JetWeight;

        NtupleBranch<Bool_t> IsCRttbar;
        NtupleBranch<Bool_t> IsCRttbar_tau;
        NtupleBranch<Bool_t> IsZee;
        NtupleBranch<Bool_t> IsZee_tau;
        NtupleBranch<Bool_t> IsZmumu;
        NtupleBranch<Bool_t> IsZmumu_tau;

        NtupleBranch<Bool_t> IsWmunu;
        NtupleBranch<Bool_t> IsWenu;

        NtupleBranch<std::vector<Bool_t>> Ele_isProbe;
        NtupleBranch<std::vector<Bool_t>> Muo_isProbe;

        NtupleBranch<std::vector<Int_t>> Ele_q;
        NtupleBranch<std::vector<Int_t>> Muo_q;

        static void set_lumi(double l);

        bool is_probe(size_t i, unsigned int multip_mask);
        int get_charge(size_t i, unsigned int multip_mask);

        size_t get_probe();

    private:
        static double m_lumi;
    };

}  // namespace SUSY4L
#endif  // NTAU__SUSY4L_FF__H
