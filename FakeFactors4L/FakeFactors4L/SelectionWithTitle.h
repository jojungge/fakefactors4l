#ifndef SELECTIONWITH_TITLE_H
#define SELECTIONWITH_TITLE_H
#include <NtupleAnalysisUtils/Selection.h>

template <class ProcessThis> class SelectionWithTitle : public Selection<ProcessThis> {
public:
    // Standard constructor for everyday use - give the selection (cut) a name and something to do
    SelectionWithTitle(const std::string& name, const std::string& title, std::function<bool(ProcessThis&)> toApply);
    // copy constructor
    SelectionWithTitle(const SelectionWithTitle<ProcessThis>& other);
    // default, provides a dummy selection accepting everything
    SelectionWithTitle();

    SelectionWithTitle<ProcessThis> operator+(const SelectionWithTitle<ProcessThis>& other) const;
    SelectionWithTitle<ProcessThis> operator||(const SelectionWithTitle<ProcessThis>& other) const;
    // combine with another selection via an AND
    SelectionWithTitle<ProcessThis> operator*(const SelectionWithTitle<ProcessThis>& other) const;
    SelectionWithTitle<ProcessThis> operator&&(const SelectionWithTitle<ProcessThis>& other) const;

    // invert a selection
    SelectionWithTitle<ProcessThis> operator!();

    std::shared_ptr<ISelection> clone() const override;
    void setTitle(const std::string& title);
    std::string getTitle() const;

private:
    std::string m_title;
};
#include <FakeFactors4L/SelectionWithTitle.ixx>
#endif
