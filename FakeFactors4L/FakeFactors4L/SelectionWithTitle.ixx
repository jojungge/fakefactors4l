#ifndef SELECTIONWITH_TITLE_IXX
#define SELECTIONWITH_TITLE_IXX
#include <NtupleAnalysisUtils/Selection.h>

// Standard constructor for everyday use - give the selection (cut) a name and something to do
template <class ProcessThis>
SelectionWithTitle<ProcessThis>::SelectionWithTitle(const std::string &name, const std::string &title,
                                                    std::function<bool(ProcessThis &)> toApply) :
    Selection<ProcessThis>(name, toApply),
    m_title(title) {}

// copy constructor
template <class ProcessThis>
SelectionWithTitle<ProcessThis>::SelectionWithTitle(const SelectionWithTitle<ProcessThis> &other) :
    Selection<ProcessThis>(other),
    m_title(other.m_title) {}
// default, provides a dummy selection accepting everything
template <class ProcessThis> SelectionWithTitle<ProcessThis>::SelectionWithTitle() : Selection<ProcessThis>(), m_title() {}

template <class ProcessThis> std::shared_ptr<ISelection> SelectionWithTitle<ProcessThis>::clone() const {
    return std::make_shared<SelectionWithTitle<ProcessThis>>(*this);
}
template <class ProcessThis> void SelectionWithTitle<ProcessThis>::setTitle(const std::string &title) { m_title = title; }
template <class ProcessThis> std::string SelectionWithTitle<ProcessThis>::getTitle() const { return m_title; }

template <class ProcessThis>
SelectionWithTitle<ProcessThis> SelectionWithTitle<ProcessThis>::operator+(const SelectionWithTitle<ProcessThis> &other) const {
    // OR with a 'return true': always returns true
    if (this->m_isDummy || other.m_isDummy) { return SelectionWithTitle<ProcessThis>(); }

    std::function<bool(ProcessThis &)> f1 = this->m_cut;
    std::function<bool(ProcessThis &)> f2 = other.m_cut;
    std::function<bool(ProcessThis &)> f_or([f1, f2](ProcessThis &t) { return (f1(t) || f2(t)); });
    return SelectionWithTitle<ProcessThis>(std::string(" (") + this->getName() + " OR " + other.getName() + ") ",
                                           getTitle() + "#vee " + other.getTitle(), f_or);
}
template <class ProcessThis>
SelectionWithTitle<ProcessThis> SelectionWithTitle<ProcessThis>::operator*(const SelectionWithTitle<ProcessThis> &other) const {
    // AND with a 'return true': always return the ANDed thing
    if (this->m_isDummy) {
        return other;
    } else if (other.m_isDummy) {
        return SelectionWithTitle<ProcessThis>(*this);
    }
    std::function<bool(ProcessThis &)> f1 = this->m_cut;
    std::function<bool(ProcessThis &)> f2 = other.m_cut;
    std::function<bool(ProcessThis &)> f_and([f1, f2](ProcessThis &t) { return (f1(t) && f2(t)); });
    return SelectionWithTitle<ProcessThis>(std::string(" (") + this->getName() + " AND " + other.getName() + ") ",
                                           getTitle() + "," + other.getTitle(), f_and);
}
template <class ProcessThis>
SelectionWithTitle<ProcessThis> SelectionWithTitle<ProcessThis>::operator||(const SelectionWithTitle<ProcessThis> &other) const {
    return this->operator+(other);
}
template <class ProcessThis>
SelectionWithTitle<ProcessThis> SelectionWithTitle<ProcessThis>::operator&&(const SelectionWithTitle<ProcessThis> &other) const {
    return this->operator*(other);
}
template <class ProcessThis> SelectionWithTitle<ProcessThis> SelectionWithTitle<ProcessThis>::operator!() {
    std::function<bool(ProcessThis &)> f1 = this->m_cut;
    std::function<bool(ProcessThis &)> f_not([f1](ProcessThis &t) { return (!f1(t)); });
    return SelectionWithTitle<ProcessThis>(std::string(" ( NOT ") + this->getName() + ") ", "!" + getTitle(), f_not);
}
#endif
