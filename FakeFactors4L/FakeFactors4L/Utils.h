#ifndef SUSY4LFAKEFACTOR_UTILS_H
#define SUSY4LFAKEFACTOR_UTILS_H
#include <XAMPPplotting/CombinatoricSelection.h>
#include <XAMPPplotting/FinalPlotHelpers.h>
#include <XAMPPplotting/HistoTemplates.h>
#include <XAMPPplotting/PlottingUtils.h>

///
#include <NtupleAnalysisUtils/AtlasStyle.h>
#include <NtupleAnalysisUtils/CanvasOptions.h>
#include <NtupleAnalysisUtils/DefaultPlotting.h>
#include <NtupleAnalysisUtils/Plot.h>
#include <NtupleAnalysisUtils/PlotContent.h>
#include <NtupleAnalysisUtils/PlotUtils.h>
#include <NtupleAnalysisUtils/Selection.h>

#include <TFile.h>
#include <TTree.h>
#include <vector>

namespace SUSY4L {

    enum LepClass {
        /// Flavour
        Ele = 1,
        Muo = 1 << 1,

        Tau1P = 1 << 2,
        Tau3P = 1 << 3,
        Tau = Tau1P | Tau3P,

        LightLep = Ele | Muo,
        LepFlav = LightLep | Tau,

        /// Signal or loose
        Loose = 1 << 4,
        Signal = 1 << 5,
        FailVanillaIso = 1 << 6,
        Baseline = Loose | Signal,
        /// Classification
        Real = 1 << 7,
        HF = 1 << 8,
        LF = 1 << 9,
        CONV = 1 << 10,
        ELEC = 1 << 11,
        GJ = 1 << 12,
        Other = 1 << 13,
        Fake = HF | LF | CONV | Other | ELEC | GJ,
        Incl = Real | Fake,
        VanillaLoose = Loose | FailVanillaIso,
        VanillaSignal = Signal | FailVanillaIso,
        /// Bits to be set in the closure tests
        SR = 1 << 14,
        CR1 = 1 << 15,
        CR2 = 1 << 16,
        AbsCR2 = 1 << 17,
        MixedCR2 = 1 << 18,
        //// Bits to be set for the systematics
        SystFF = 1 << 19,
        SystSF = 1 << 20,
        SystProcFrac = 1 << 21,

        SystComp = SystFF | SystSF | SystProcFrac,
        SystUp = 1 << 22,
        /// Jet bit only needed for Tree writing puposes
        Jets = 1 << 23,

        ///
        /// Now General compound definitions
        AllLep = LightLep | Incl | Baseline,
        AllReals = LightLep | Real | Baseline,

        SignalEle = Ele | Signal,
        SignalMuo = Muo | Signal,

        LooseEle = Ele | Loose,
        LooseMuo = Muo | Loose,

        BaseEle = Ele | Baseline,
        BaseMuo = Muo | Baseline,

        InclSignal = LightLep | Incl | Signal,
        InclLoose = LightLep | Incl | Loose,

    };
    std::string flavour_to_str(unsigned int bit_mask);
    std::string selection_to_str(unsigned int bit_mask);
    std::string type_to_str(unsigned int bit_mask);

    std::string syst_to_str(unsigned int bit_mask);

    bool type_flavour_defined(unsigned int bit_mask);

    int count_non_empty(PlotContent<TH1D>& h);

    bool isElementSubstr(const std::vector<std::string>& vec, const std::string& str);

    void saveProcessFractions(PlotContent<TH2D>& content);
    void saveFakeFactor(PlotContent<TH2D>& content);
    void saveScaleFactor(PlotContent<TH1>& pc);

    std::shared_ptr<TH1> make2DSlice(std::shared_ptr<TH1> H3, unsigned int bin_z);
    /// Makes a histogram with the BinErrors as content
    std::shared_ptr<TH1> pullErrors(const std::shared_ptr<TH1>& H);
    /// Makes a constant histogram
    std::shared_ptr<TH1> makeConstant(const std::shared_ptr<TH1>& H, double value = 1.);
    /// Truncates the histogram
    void truncate(std::shared_ptr<TH1> H, double lower_value = 0, double upper_value = 1.);

    /// Pulls over and underflows into the last and first bin
    void pull_overflow(std::shared_ptr<TH1> H);

    void saveProcessFractions(PlotContent<TH3D>& content);
    void saveFakeFactor(PlotContent<TH3D>& content);

    std::shared_ptr<TFile> make_out_file(const std::string& path);

    enum FileCheck {
        Okay = 0,
        Empty = 1,
        Error = 2,
    };
    FileCheck check_file(const std::string& file_name, const std::string& tree_name);

}  // namespace SUSY4L
#endif
