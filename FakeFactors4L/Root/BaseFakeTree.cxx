#include <FakeFactors4L/BaseFakeTree.h>
#include <FourMomUtils/xAODP4Helpers.h>
namespace SUSY4L {
    BaseFakeTree::BaseFakeTree(TTree* t) :
        NtupleBranchMgr(t),
        Ele_CorrIsol("Ele_CorrIsol", t, this),
        Ele_Isol("Ele_Isol", t, this),
        Ele_PiD("Ele_PiD", t, this),
        Ele_dRJet("Ele_dRJet", t, this),
        Ele_eta("Ele_eta", t, this),
        Ele_phi("Ele_phi", t, this),
        Ele_pt("Ele_pt", t, this),
        ElecTrigger("ElecTrigger", t, this),
        IsBaseZ("IsBaseZ", t, this),
        IsBaseZVeto("IsBaseZVeto", t, this),
        IsBaseZZ("IsBaseZZ", t, this),
        IsSigZ("IsSigZ", t, this),
        IsSigZVeto("IsSigZVeto", t, this),
        IsSigZZ("IsSigZZ", t, this),
        IsTrigger("IsTrigger", t, this),
        Meff("Meff", t, this),
        Met("Met", t, this),
        Met_phi("Met_phi", t, this),
        Muo_CorrIsol("Muo_CorrIsol", t, this),
        Muo_Isol("Muo_Isol", t, this),
        Muo_PiD("Muo_PiD", t, this),
        Muo_dRJet("Muo_dRJet", t, this),
        Muo_eta("Muo_eta", t, this),
        Muo_phi("Muo_phi", t, this),
        Muo_pt("Muo_pt", t, this),
        MuonTrigger("MuonTrigger", t, this),
        NormWeight("NormWeight", t, this),
        NumBJets("NumBJets", t, this),
        NumJets("NumJets", t, this),
        Tau_eta("Tau_eta", t, this),
        Tau_phi("Tau_phi", t, this),
        Tau_prongs("Tau_prongs", t, this),
        Tau_pt("Tau_pt", t, this),
        Tau_signal("Tau_signal", t, this),
        prwWeight("prwWeight", t, this),
        m_last_cache(-1),
        m_n_INCL_Ele_Sig(0),
        m_n_INCL_Muo_Sig(0),
        m_n_INCL_Tau1P_Sig(0),
        m_n_INCL_Tau3P_Sig(0),
        m_n_INCL_Ele_Loose(0),
        m_n_INCL_Muo_Loose(0),
        m_n_INCL_Tau1P_Loose(0),
        m_n_INCL_Tau3P_Loose(0),
        m_loose_meff_comp(0),
        m_min_lep_dR(FLT_MAX),
        m_min_jet_dR(FLT_MAX),
        m_base_HT(0),
        m_signal_HT(0),
        m_closest_signal_ele(),
        m_closest_signal_muo(),
        m_closest_loose_ele(),
        m_closest_loose_muo(),
        m_isMC(false) {
        if (t) getMissedBranches(t);
        m_isMC = t->GetBranch("NormWeight") != nullptr;
    }
    float BaseFakeTree::baseline_lep_ht() {
        cache_numbers();
        return m_base_HT;
    }
    float BaseFakeTree::signal_lep_ht() {
        cache_numbers();
        return m_signal_HT;
    }
    bool BaseFakeTree::Muo_signal(size_t m) { return Muo_PiD(m) && Muo_CorrIsol(m); }
    bool BaseFakeTree::Ele_signal(size_t e) { return Ele_PiD(e) && Ele_CorrIsol(e); }
    bool BaseFakeTree::isMC() const { return m_isMC; }
    double BaseFakeTree::eventWeight() { return m_isMC ? NormWeight() * prwWeight() : 1.; }
    bool BaseFakeTree::cache_numbers() {
        /// check if the event has changed
        if (m_last_cache == getCurrEntry()) { return false; }
        m_last_cache = getCurrEntry();
        /// Reset the counter
        m_n_INCL_Ele_Sig = m_n_INCL_Muo_Sig = 0;
        m_n_INCL_Ele_Loose = m_n_INCL_Muo_Loose = 0;

        m_n_INCL_Tau1P_Sig = m_n_INCL_Tau1P_Loose = 0;
        m_n_INCL_Tau3P_Sig = m_n_INCL_Tau3P_Loose = 0;

        m_loose_meff_comp = 0;
        m_base_HT = 0;
        m_signal_HT = 0;

        for (unsigned int m = 0; m < Muo_size(); ++m) {
            /// Count the number of inclusive signal muons
            /// Otherwise add the muon momentum to the total meff component
            m_base_HT += Muo_pt(m);
            if (Muo_signal(m)) {
                ++m_n_INCL_Muo_Sig;
                m_signal_HT += Muo_pt(m);
            } else {
                ++m_n_INCL_Muo_Loose;
                m_loose_meff_comp += Muo_pt(m);
            }
        }

        for (unsigned int e = 0; e < Ele_size(); ++e) {
            /// Count the number of inclusive signal Elens
            /// Otherwise add the Elen momentum to the total meff component
            m_base_HT += Ele_pt(e);

            if (Ele_signal(e)) {
                ++m_n_INCL_Ele_Sig;
                m_signal_HT += Ele_pt(e);
            } else {
                ++m_n_INCL_Ele_Loose;
                m_loose_meff_comp += Ele_pt(e);
            }
        }
        for (unsigned int t = 0; t < Tau_size(); ++t) {
            if (Tau_prongs(t) == 1) {
                if (Tau_signal(t))
                    ++m_n_INCL_Tau1P_Sig;
                else
                    ++m_n_INCL_Tau1P_Loose;
            } else {
                if (Tau_signal(t))
                    ++m_n_INCL_Tau3P_Sig;
                else
                    ++m_n_INCL_Tau3P_Loose;
            }
        }
        cache_closest_dR(m_closest_signal_ele, LepClass::SignalEle | LepClass::Incl | LepClass::FailVanillaIso);
        cache_closest_dR(m_closest_signal_muo, LepClass::SignalMuo | LepClass::Incl | LepClass::FailVanillaIso);

        cache_closest_dR(m_closest_loose_ele, LepClass::LooseEle | LepClass::Incl | LepClass::FailVanillaIso);
        cache_closest_dR(m_closest_loose_muo, LepClass::LooseMuo | LepClass::Incl | LepClass::FailVanillaIso);

        m_min_lep_dR = FLT_MAX;
        m_min_jet_dR = FLT_MAX;
        unsigned int n_lep = get_n_lep(LepClass::LightLep | LepClass::Baseline | LepClass::Incl);
        for (int i = n_lep - 1; i >= 0; --i) {
            m_min_lep_dR = std::min(get_dR_minLep(i), m_min_lep_dR);
            m_min_jet_dR = std::min(get_dR_minJet(i, LepClass::LightLep), m_min_jet_dR);
        }
        return true;
    }
    void BaseFakeTree::cache_closest_dR(std::vector<std::pair<size_t, float>>& vec, unsigned int lep_mask) {
        vec.clear();
        unsigned int n_lep = get_n_lep(LepClass::LepFlav | LepClass::Baseline | LepClass::Incl);
        if (vec.capacity() < n_lep) vec.reserve(n_lep);
        for (unsigned int l = 0; l < n_lep; ++l) {
            size_t best_idx = dummy_idx;
            float best_dR = dummy_dR;
            for (size_t j = 0; j < n_lep; ++j) {
                /// Be aware we cannot simply use the isLep method as
                /// the index is global and not flavour specific
                unsigned int jth_mask = get_lepton_bitmask(j);
                if (j == l || (jth_mask & lep_mask) != jth_mask) continue;
                float dR = get_delta_R(l, j);
                if (dR < best_dR) {
                    best_dR = dR;
                    best_idx = j;
                }
            }
            vec.emplace_back(best_idx, best_dR);
        }
    }

    unsigned int BaseFakeTree::get_n_lep(unsigned int multip_mask) {
        unsigned int n = 0;
        if (multip_mask & LepClass::Ele) { n += get_n_ele(multip_mask); }
        if (multip_mask & LepClass::Muo) { n += get_n_muo(multip_mask); }
        if (multip_mask & LepClass::Tau) { n += get_n_tau(multip_mask); }
        return n;
    }
    float BaseFakeTree::global_dR_minJet() {
        cache_numbers();
        return m_min_jet_dR;
    }
    float BaseFakeTree::global_dR_minLep() {
        cache_numbers();
        return m_min_lep_dR;
    }
    unsigned int BaseFakeTree::get_n_ele(unsigned int multip_mask) {
        cache_numbers();
        /// Baseline leptons
        if ((multip_mask & LepClass::Baseline) == LepClass::Baseline) {
            return Ele_size();
        } else if (multip_mask & LepClass::Signal) {
            return m_n_INCL_Ele_Sig;
        } else if (multip_mask & LepClass::Loose) {
            return m_n_INCL_Ele_Loose;
        }
        return 0;
    }
    unsigned int BaseFakeTree::get_n_muo(unsigned int multip_mask) {
        cache_numbers();
        /// Baseline leptons
        if ((multip_mask & LepClass::Baseline) == LepClass::Baseline) {
            return Muo_size();
        } else if (multip_mask & LepClass::Signal) {
            return m_n_INCL_Muo_Sig;
        } else if (multip_mask & LepClass::Loose) {
            return m_n_INCL_Muo_Loose;
        }
        return 0;
    }
    unsigned int BaseFakeTree::get_n_tau(unsigned int multip_mask) {
        cache_numbers();
        unsigned int n = 0;
        if (multip_mask & LepClass::Loose) {
            if (multip_mask & LepClass::Tau1P) n += m_n_INCL_Tau1P_Loose;
            if (multip_mask & LepClass::Tau3P) n += m_n_INCL_Tau3P_Loose;
        }
        if (multip_mask & LepClass::Signal) {
            if (multip_mask & LepClass::Tau1P) n += m_n_INCL_Tau1P_Sig;
            if (multip_mask & LepClass::Tau3P) n += m_n_INCL_Tau3P_Sig;
        }
        return n;
    }
    float BaseFakeTree::baseline_meff() {
        cache_numbers();
        return m_loose_meff_comp + Meff();
    }
    void BaseFakeTree::global_to_local(size_t& i, unsigned int& flav_mask) {
        /// Special feature if the both lepton bits are set then
        /// the first [0,1,2,3,N_{ele},] entries are assigned to the electron
        /// the rest to the muon....
        if ((flav_mask & LepClass::Ele) && (flav_mask & (LepClass::Muo | LepClass::Tau))) {
            if (i >= Ele_size()) {
                i -= Ele_size();
                flav_mask &= ~LepClass::Ele;
            } else
                return;
        }

        if ((flav_mask & LepClass::Muo) && (flav_mask & LepClass::Tau)) {
            if (i >= Muo_size()) {
                i -= Muo_size();
                flav_mask &= ~LepClass::Muo;
            } else
                return;
        }
    }
    void BaseFakeTree::local_to_global(size_t& i, unsigned int& flav_mask) {
        /// Explicitly asked for muons or taus
        if (flav_mask & (LepClass::Muo | LepClass::Tau) && !(flav_mask & LepClass::Ele)) {
            flav_mask |= LepClass::Ele;
            i += Ele_size();
        }
        //// Explicitly asked for taus
        if (flav_mask & LepClass::Tau && !(flav_mask & LepClass::Muo)) {
            flav_mask |= LepClass::Muo;
            i += Muo_size();
        }
    }

    bool BaseFakeTree::is_Lep(size_t i, unsigned int multip_mask) {
        size_t i_bkp = i;
        unsigned int multip_bkp = multip_mask;

        global_to_local(i, multip_mask);

        unsigned int lep_mask = 0;
        if (multip_mask & LepClass::Ele)
            lep_mask = get_ele_bitmask(i);
        else if (multip_mask & LepClass::Muo)
            lep_mask = get_muo_bitmask(i);
        else if (multip_mask & LepClass::Tau)
            lep_mask = get_tau_bitmask(i);
        if (!lep_mask) {
            if (!(multip_mask & LepClass::LepFlav)) {
                std::cout << "WARNING: is_Lep(): Flavour bit is not set: " << i_bkp << " " << flavour_to_str(multip_bkp) << " "
                          << selection_to_str(multip_bkp) << " " << type_to_str(multip_bkp) << "." << std::endl;
                dump_event();
            }

            return false;
        }
        /// Old balance between baseline and signal
        if (multip_mask & LepClass::FailVanillaIso && (multip_mask & LepClass::LightLep)) {
            if (multip_mask & LepClass::Signal) {
                if (lep_mask & LepClass::FailVanillaIso) return false;
            } else if (multip_mask & LepClass::Loose) {
                if (!(lep_mask & (LepClass::Loose | LepClass::FailVanillaIso))) return false;
                lep_mask &= ~LepClass::Signal;
                lep_mask |= LepClass::Loose;
            }
        }
        multip_mask |= LepClass::FailVanillaIso;
        return (lep_mask & multip_mask) == lep_mask;
    }
    float BaseFakeTree::get_pt(size_t i, unsigned int multip_mask) {
        global_to_local(i, multip_mask);

        if ((multip_mask & LepClass::Ele) && i < Ele_size()) return Ele_pt(i);
        if ((multip_mask & LepClass::Muo) && i < Muo_size()) return Muo_pt(i);
        if ((multip_mask & LepClass::Tau) && i < Tau_size()) return Tau_pt(i);

        std::cerr << "BaseFakeTree::get_pt() --- WARNING: Index i " << i << " exceeds range or flavour bit not set "
                  << flavour_to_str(multip_mask) << " " << selection_to_str(multip_mask) << " " << type_to_str(multip_mask) << std::endl;
        return FLT_MAX;
    }
    bool BaseFakeTree::pass_id(size_t i, unsigned int multip_mask) {
        global_to_local(i, multip_mask);
        if ((multip_mask & LepClass::Ele) && i < Ele_size()) return Ele_PiD(i);
        if ((multip_mask & LepClass::Muo) && i < Muo_size()) return Muo_PiD(i);
        if ((multip_mask & LepClass::Tau) && i < Tau_size()) return Tau_signal(i);
        return false;
    }
    float BaseFakeTree::get_phi(size_t i, unsigned int multip_mask) {
        global_to_local(i, multip_mask);
        if ((multip_mask & LepClass::Ele) && i < Ele_size()) return Ele_phi(i);
        if ((multip_mask & LepClass::Muo) && i < Muo_size()) return Muo_phi(i);
        if ((multip_mask & LepClass::Tau) && i < Tau_size()) return Tau_phi(i);

        std::cerr << "BaseFakeTree::get_phi() --- WARNING: Index i " << i << " exceeds range or flavour bit not set "
                  << flavour_to_str(multip_mask) << " " << selection_to_str(multip_mask) << " " << type_to_str(multip_mask) << std::endl;
        return FLT_MAX;
    }

    float BaseFakeTree::get_eta(size_t i, unsigned int multip_mask) {
        global_to_local(i, multip_mask);
        if ((multip_mask & LepClass::Ele) && i < Ele_size()) return Ele_eta(i);
        if ((multip_mask & LepClass::Muo) && i < Muo_size()) return Muo_eta(i);
        if ((multip_mask & LepClass::Tau) && i < Tau_size()) return Tau_eta(i);
        std::cerr << "BaseFakeTree::get_eta() --- WARNING: Index i " << i << " exceeds range or flavour bit not set "
                  << flavour_to_str(multip_mask) << " " << selection_to_str(multip_mask) << " " << type_to_str(multip_mask) << std::endl;
        return FLT_MAX;
    }
    TLorentzVector BaseFakeTree::get_p4(size_t i, unsigned int multip_mask) {
        TLorentzVector v;
        v.SetPtEtaPhiM(get_pt(i, multip_mask), get_eta(i, multip_mask), get_phi(i, multip_mask), 0);
        return v;
    }

    int BaseFakeTree::get_lepton_flavour(size_t i) { return get_lepton_bitmask(i) & LepClass::LepFlav; }
    int BaseFakeTree::get_ele_bitmask(size_t i) {
        int bit_mask = 0;
        if (i < Ele_size()) {
            bit_mask |= LepClass::Ele | LepClass::Incl;
            bit_mask |= !Ele_signal(i) ? LepClass::Loose : LepClass::Signal;
            if (!Ele_Isol(i)) bit_mask |= LepClass::FailVanillaIso;
        }
        return bit_mask;
    }
    int BaseFakeTree::get_muo_bitmask(size_t i) {
        int bit_mask = 0;
        if (i < Muo_size()) {
            bit_mask |= LepClass::Muo | LepClass::Incl;
            bit_mask |= !Muo_signal(i) ? LepClass::Loose : LepClass::Signal;
            if (!Muo_Isol(i)) bit_mask |= LepClass::FailVanillaIso;
        }
        return bit_mask;
    }

    int BaseFakeTree::get_tau_bitmask(size_t i) {
        int bit_mask = 0;
        if (i < Tau_size()) {
            bit_mask |= LepClass::Incl;
            bit_mask |= Tau_prongs(i) == 1 ? LepClass::Tau1P : LepClass::Tau3P;
            bit_mask |= !Tau_signal(i) ? LepClass::Loose : LepClass::Signal;
        }
        return bit_mask;
    }
    int BaseFakeTree::get_lepton_bitmask(size_t i) {
        if (i < Ele_size()) { return get_ele_bitmask(i); }
        i -= Ele_size();
        if (i < Muo_size()) { return get_muo_bitmask(i); }
        i -= Muo_size();
        if (i < Tau_size()) { return get_tau_bitmask(i); }
        return 0;
    }

    float BaseFakeTree::get_delta_eta(size_t i, size_t j) { return get_eta(i, LepClass::LepFlav) - get_eta(j, LepClass::LepFlav); }
    float BaseFakeTree::get_delta_phi(size_t i, size_t j) {
        return xAOD::P4Helpers::deltaPhi(get_phi(i, LepClass::LepFlav), get_phi(j, LepClass::LepFlav));
    }
    float BaseFakeTree::get_delta_R(size_t i, size_t j) {
        /// dR values beyond 5. are truncated

        return std::min(std::hypot(get_delta_eta(i, j), get_delta_phi(i, j)), max_dR);
    }
    void BaseFakeTree::dump_event() {
        std::cout << XAMPP::WhiteSpaces(40, "#") << std::endl;
        std::cout << XAMPP::WhiteSpaces(15, " ") << "Dump event: " << currentEntry() << std::endl;
        std::cout << XAMPP::WhiteSpaces(40, "#") << std::endl;
        for (size_t j = 0; j < get_n_lep(LepClass::Incl | LepClass::Baseline | LepClass::LepFlav); ++j) {
            int bit_mask = get_lepton_bitmask(j);
            std::cout << "  -- " << j << " " << std::setw(10) << flavour_to_str(bit_mask) << " " << std::setw(6)
                      << selection_to_str(bit_mask) << " " << std::setw(10) << type_to_str(bit_mask);
            std::cout << "   " << std::setw(8) << get_pt(j, LepClass::LepFlav) << "   " << std::setw(7) << get_eta(j, LepClass::LepFlav)
                      << "   " << std::setw(7) << get_phi(j, LepClass::LepFlav);
            if (bit_mask & LepClass::LightLep) std::cout << "   " << get_dR_minLep(j);
            std::cout << std::endl;
        }
    }
    float BaseFakeTree::get_dR_minJet(size_t i, unsigned int multip_mask) {
        global_to_local(i, multip_mask);
        if ((multip_mask & LepClass::Ele) && i < Ele_size()) return Ele_dRJet(i);
        if ((multip_mask & LepClass::Muo) && i < Muo_size()) return Muo_dRJet(i);
        std::cerr << "BaseFakeTree::get_dR_minJet() --- WARNING: Index i " << i << " exceeds range or flavour bit not set "
                  << flavour_to_str(multip_mask) << " " << selection_to_str(multip_mask) << " " << type_to_str(multip_mask) << std::endl;

        return dummy_dR;
    }
    float BaseFakeTree::get_dR_LooseEle(size_t i) {
        cache_numbers();
        return m_closest_loose_ele[i].second;
    }
    float BaseFakeTree::get_dR_LooseMuo(size_t i) {
        cache_numbers();
        return m_closest_loose_muo[i].second;
    }
    float BaseFakeTree::get_dR_SigEle(size_t i) {
        cache_numbers();
        return m_closest_signal_ele[i].second;
    }
    float BaseFakeTree::get_dR_SigMuo(size_t i) {
        cache_numbers();
        return m_closest_signal_muo[i].second;
    }
    float BaseFakeTree::get_dR_LepSelFlav(size_t i) {
        /// Calculate the dR values to the next leptons
        const std::pair<size_t, float>& close_lep = get_closest_lep(i);
        float shift = 0;
        if (close_lep.first == dummy_idx) return 5 * dummy_dR;
        if (get_lepton_flavour(i) == get_lepton_flavour(close_lep.first)) shift += max_dR;
        if (get_lepton_bitmask(close_lep.first) & LepClass::Signal) shift += 2. * max_dR;
        return close_lep.second + shift;
    }
    float BaseFakeTree::get_dR_minLooseSigLep(size_t i) {
        const std::pair<size_t, float>& close_lep = get_closest_lep(i);
        return close_lep.second + (get_lepton_bitmask(close_lep.first) & LepClass::Signal ? max_dR : 0.);
    }

    float BaseFakeTree::get_dR_minFlavLep(size_t i) {
        const std::pair<size_t, float>& close_lep = get_closest_lep(i);
        if (close_lep.first == dummy_idx) return 5 * dummy_dR;
        return close_lep.second + (get_lepton_flavour(close_lep.first) == get_lepton_flavour(i) ? max_dR : 0.);
    }
    float BaseFakeTree::get_dR_minLep(size_t i) { return get_closest_lep(i).second; }
    const std::pair<size_t, float>& BaseFakeTree::get_closest_lep(size_t i) {
        cache_numbers();
        std::pair<size_t, float>& best_j = m_closest_loose_ele[i];
        if (best_j.second > m_closest_loose_muo[i].second) best_j = m_closest_loose_muo[i];
        if (best_j.second > m_closest_signal_ele[i].second) best_j = m_closest_signal_ele[i];
        if (best_j.second > m_closest_signal_muo[i].second) best_j = m_closest_signal_muo[i];
        return best_j;
    }
    bool BaseFakeTree::has_collimated_lep_below(float dR, float pt_cut) {
        if (global_dR_minLep() > dR) return false;
        size_t n_lep = get_n_lep(LepClass::Incl | LepClass::Baseline | LepClass::LightLep);
        for (size_t l = 0; l < n_lep; ++l) {
            if (get_pt(l, LepClass::LightLep) < pt_cut && get_closest_lep(l).second < dR) return true;
        }
        return false;
    }
    float BaseFakeTree::get_mt_met(size_t i, unsigned int multip_mask) {
        return std::sqrt(2 * get_pt(i, multip_mask) * Met() * (1. - std::cos(get_dphi_met(i, multip_mask))));
    }
    float BaseFakeTree::get_dphi_met(size_t i, unsigned int multip_mask) {
        return xAOD::P4Helpers::deltaPhi(get_phi(i, multip_mask), Met_phi());
    }

}  // namespace SUSY4L
