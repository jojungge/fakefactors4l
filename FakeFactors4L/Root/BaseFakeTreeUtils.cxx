#include <FakeFactors4L/BaseFakeTreeUtils.h>
#include <FakeFactors4L/FakeClosureTree.h>

using namespace XAMPP;
namespace SUSY4L {

    IBaseFakeTreeFiller::IBaseFakeTreeFiller(std::shared_ptr<TH1> histo, unsigned int lep_mask, const std::string& fill_name,
                                             const std::string& smp_name) :
        m_lep_mask(lep_mask),
        m_name(fill_name),
        m_sample_name(smp_name),
        m_ref_histo(histo) {}
    unsigned int IBaseFakeTreeFiller::lepton_mask() const { return m_lep_mask; }
    void IBaseFakeTreeFiller::change_bit_mask(unsigned int mask) { m_lep_mask = mask; }
    std::string IBaseFakeTreeFiller::lepton_flavour() const { return flavour_to_str(lepton_mask()); }
    std::string IBaseFakeTreeFiller::lepton_type() const { return type_to_str(lepton_mask()); }
    std::string IBaseFakeTreeFiller::lepton_quality() const { return selection_to_str(lepton_mask()); }
    std::shared_ptr<TH1> IBaseFakeTreeFiller::get_histo() const { return m_ref_histo; }
    std::string IBaseFakeTreeFiller::sample_name() const { return m_sample_name; }
    std::string IBaseFakeTreeFiller::name() const { return m_name; }
    bool IBaseFakeTreeFiller::is_lepton_variable(TAxis* A) const {
        std::string axis_title = A ? A->GetTitle() : "";
        return !axis_title.empty() && isElementSubstr(pt_titles + abs_eta_titles + eta_titles + dR_titles + mass_titles, axis_title);
    }
    //############################################
    //              FakeHistoReader              #
    //############################################
    void FakeHistoReader::set_associated_file(std::shared_ptr<TFile> f) { m_assoc_file = f; }
    std::shared_ptr<TFile> FakeHistoReader::associated_file() const { return m_assoc_file; }

    FakeHistoReader::FakeHistoReader(unsigned int lep_flav, std::shared_ptr<TH1> fake_histo, std::shared_ptr<TH1> proc_frac,
                                     std::shared_ptr<TH1> sf_histo, std::function<bool(BaseFakeTree&, size_t)> selec) :
        IBaseFakeTreeFiller(fake_histo, lep_flav),
        m_ff_histo(fake_histo),
        m_proc_histo(proc_frac),
        m_sf_histo(sf_histo),
        m_assoc_file(nullptr),
        m_syst_proc_frac_up(),
        m_syst_proc_frac_dn(),
        m_ff_x_reader(make_reader<BaseFakeTree>(fake_histo->GetXaxis())),
        m_ff_y_reader(make_reader<BaseFakeTree>(fake_histo->GetYaxis())),
        m_ff_z_reader(make_reader<BaseFakeTree>(fake_histo->GetZaxis())),
        m_proc_x_reader(make_reader<BaseFakeTree>(proc_frac ? proc_frac->GetXaxis() : nullptr)),
        m_proc_y_reader(make_reader<BaseFakeTree>(proc_frac ? proc_frac->GetYaxis() : nullptr)),
        m_proc_z_reader(make_reader<BaseFakeTree>(proc_frac ? proc_frac->GetZaxis() : nullptr)),
        m_sf_x_reader(make_reader<BaseFakeTree>(sf_histo ? sf_histo->GetXaxis() : nullptr)),
        m_sf_y_reader(make_reader<BaseFakeTree>(sf_histo ? sf_histo->GetYaxis() : nullptr)),
        m_sf_z_reader(make_reader<BaseFakeTree>(sf_histo ? sf_histo->GetZaxis() : nullptr)),
        m_sel_cut(selec) {}
    std::shared_ptr<TH1> FakeHistoReader::ff_histo() const { return m_ff_histo; }
    std::shared_ptr<TH1> FakeHistoReader::proc_histo() const { return m_proc_histo; }
    std::shared_ptr<TH1> FakeHistoReader::sf_histo() const { return m_sf_histo; }

    float FakeHistoReader::get_weight(BaseFakeTree& t, size_t i) const {
        unsigned int lep_in_flav = t.get_lepton_flavour(i);
        if (lep_in_flav != (lepton_mask() & LepClass::LepFlav) || !pass(t, i)) return 0.;
        unsigned int lep_mask = LepClass::LepFlav;
        t.global_to_local(i, lep_mask);
        return get_value(t, i, m_ff_histo, m_ff_x_reader, m_ff_y_reader, m_ff_z_reader) *
               get_value(t, i, m_sf_histo, m_sf_x_reader, m_sf_y_reader, m_sf_z_reader) *
               get_value(t, i, m_proc_histo, m_proc_x_reader, m_proc_y_reader, m_proc_z_reader);
    }

    void FakeHistoReader::dump_weight(BaseFakeTree& t, size_t i) const {
        unsigned int lep_in_flav = t.get_lepton_flavour(i);
        if (lep_in_flav != (lepton_mask() & LepClass::LepFlav) || !pass(t, i)) return;
        unsigned int lep_mask = LepClass::LepFlav;
        t.global_to_local(i, lep_mask);
        std::cout << lepton_flavour() << " " << lepton_type() << std::endl;
        std::cout << "Fakefactor: " << get_value(t, i, m_ff_histo, m_ff_x_reader, m_ff_y_reader, m_ff_z_reader) << std::endl;
        std::cout << "Scalefactor: " << get_value(t, i, m_sf_histo, m_sf_x_reader, m_sf_y_reader, m_sf_z_reader) << std::endl;
        std::cout << "Processfraction: " << get_value(t, i, m_proc_histo, m_proc_x_reader, m_proc_y_reader, m_proc_z_reader) << std::endl;
        return;
    }

    bool FakeHistoReader::pass(BaseFakeTree& t, size_t i) const { return m_sel_cut(t, i); }
    void FakeHistoReader::add_syst_up(unsigned int mask, std::shared_ptr<TH1> H) {
        mask &= LepClass::Incl;
        m_syst_proc_frac_up[mask] = H;
    }
    void FakeHistoReader::add_syst_dn(unsigned int mask, std::shared_ptr<TH1> H) {
        mask &= LepClass::Incl;
        m_syst_proc_frac_dn[mask] = H;
    }
    int FakeHistoReader::get_bin(TAxis* A, const float& value) const {
        int bin = A->FindBin(value);
        if (bin == 0) return 1;
        return bin - XAMPP::isOverflowBin(A, bin);
    }

    int FakeHistoReader::get_bin(BaseFakeTree& t, size_t i, const std::shared_ptr<TH1>& H,
                                 const std::function<float(BaseFakeTree&, size_t)>& x, const std::function<float(BaseFakeTree&, size_t)>& y,
                                 const std::function<float(BaseFakeTree&, size_t)>& z) const {
        if (!H) return -1;

        if (H->GetDimension() == 3) {
            int bin_x(get_bin(H->GetXaxis(), x(t, i))), bin_y(get_bin(H->GetYaxis(), y(t, i))), bin_z(get_bin(H->GetZaxis(), z(t, i)));
            return H->GetBin(bin_x, bin_y, bin_z);

        } else if (H->GetDimension() == 2) {
            int bin_x(get_bin(H->GetXaxis(), x(t, i))), bin_y(get_bin(H->GetYaxis(), y(t, i)));
            return H->GetBin(bin_x, bin_y);
        }
        return get_bin(H->GetXaxis(), x(t, i));
    }

    float FakeHistoReader::get_value(BaseFakeTree& t, size_t i, const std::shared_ptr<TH1>& H,
                                     const std::function<float(BaseFakeTree&, size_t)>& x,
                                     const std::function<float(BaseFakeTree&, size_t)>& y,
                                     const std::function<float(BaseFakeTree&, size_t)>& z) const {
        if (!H) return 1.;
        return H->GetBinContent(get_bin(t, i, H, x, y, z));
    }

    float FakeHistoReader::get_error(BaseFakeTree& t, size_t i, const std::shared_ptr<TH1>& H,
                                     const std::function<float(BaseFakeTree&, size_t)>& x,
                                     const std::function<float(BaseFakeTree&, size_t)>& y,
                                     const std::function<float(BaseFakeTree&, size_t)>& z) const {
        if (!H) return 1.;
        return H->GetBinError(get_bin(t, i, H, x, y, z));
    }
    float FakeHistoReader::get_syst(BaseFakeTree& t, size_t i, const std::shared_ptr<TH1>& H,
                                    const std::function<float(BaseFakeTree&, size_t)>& x,
                                    const std::function<float(BaseFakeTree&, size_t)>& y,
                                    const std::function<float(BaseFakeTree&, size_t)>& z, bool do_up) const {
        int bin = get_bin(t, i, H, x, y, z);
        if (bin == -1) return 1.;
        if (do_up) return H->GetBinContent(bin) + H->GetBinError(bin);
        float sys_dn = H->GetBinContent(bin) - H->GetBinError(bin);
        if (sys_dn > 0) return sys_dn;
        return 0.;
    }
    float FakeHistoReader::get_syst_weight(BaseFakeTree& t, size_t i, unsigned int syst_mask) const {
        unsigned int lep_in_flav = t.get_lepton_flavour(i);
        if (lep_in_flav != (lepton_mask() & LepClass::LepFlav) || !pass(t, i)) return 0.;
        /// Actually the weight here is not really affected... The only
        /// chance here might be the process fraction
        if ((syst_mask & LepClass::Incl) != (lepton_mask() & LepClass::Incl)) {
            if (!(syst_mask & LepClass::SystProcFrac) ||
                !type_flavour_defined((lepton_mask() & LepClass::LepFlav) | (syst_mask & LepClass::Incl))) {
                return get_weight(t, i);
            }
        }
        unsigned int lep_mask = LepClass::LepFlav;
        t.global_to_local(i, lep_mask);
        if (syst_mask & LepClass::SystFF) {
            return get_syst(t, i, m_ff_histo, m_ff_x_reader, m_ff_y_reader, m_ff_z_reader, syst_mask & LepClass::SystUp) *
                   get_value(t, i, m_proc_histo, m_proc_x_reader, m_proc_y_reader, m_proc_z_reader) *
                   get_value(t, i, m_sf_histo, m_sf_x_reader, m_sf_y_reader, m_sf_z_reader);
        } else if (syst_mask & LepClass::SystSF) {
            return get_value(t, i, m_ff_histo, m_ff_x_reader, m_ff_y_reader, m_ff_z_reader) *
                   get_value(t, i, m_proc_histo, m_proc_x_reader, m_proc_y_reader, m_proc_z_reader) *
                   get_syst(t, i, m_sf_histo, m_sf_x_reader, m_sf_y_reader, m_sf_z_reader, syst_mask & LepClass::SystUp);
        }
        std::shared_ptr<TH1> p_frac;
        unsigned int p_mask = syst_mask & LepClass::Incl;
        std::map<unsigned int, std::shared_ptr<TH1>>::const_iterator itr;
        if (syst_mask & LepClass::SystUp) {
            itr = m_syst_proc_frac_up.find(p_mask);
            if (itr != m_syst_proc_frac_up.end()) { p_frac = itr->second; }
        } else {
            itr = m_syst_proc_frac_dn.find(p_mask);
            if (itr != m_syst_proc_frac_dn.end()) { p_frac = itr->second; }
        }
        if (!p_frac) { p_frac = m_proc_histo; }
        return get_value(t, i, m_ff_histo, m_ff_x_reader, m_ff_y_reader, m_ff_z_reader) *
               get_value(t, i, m_sf_histo, m_sf_x_reader, m_sf_y_reader, m_sf_z_reader) *
               get_value(t, i, p_frac, m_proc_x_reader, m_proc_y_reader, m_proc_z_reader);
    }

    //###################################
    //      EffiWeightHistoReader             #
    //###################################
    EffiWeightHistoReader::EffiWeightHistoReader(unsigned int lep_flav, std::shared_ptr<TH1> fake_histo,

                                                 std::function<bool(BaseFakeTree&, size_t)> selec) :
        FakeHistoReader(lep_flav, fake_histo, nullptr, nullptr, selec) {}
    float EffiWeightHistoReader::get_weight(BaseFakeTree& t, size_t i) const {
        /// Make sure that the lepton flavour bit and the
        /// truth type bit actually match
        unsigned int i_th_mask = (t.get_lepton_bitmask(i) & (LepClass::Incl | LepClass::LepFlav));
        if ((lepton_mask() & (LepClass::Incl | LepClass::LepFlav)) != i_th_mask) return 1.;
        double ff = FakeHistoReader::get_weight(t, i);
        /// ff  = e / (1-e) --> (1-e)*f = e
        /// f/(f+1) = e
        double e = ff / (ff + 1.);
        /// The calling instance must know whether it wants to weight
        /// this lepton to loose or signal
        // std::cout<<flavour_to_str(m_full_bit_mask)<<" "<<type_to_str(m_full_bit_mask)<<" "<<e<<std::endl;
        if (to_signal()) return e;
        return 1. - e;
    }
    bool EffiWeightHistoReader::to_signal() const { return lepton_mask() & LepClass::Signal; }

}  // namespace SUSY4L
