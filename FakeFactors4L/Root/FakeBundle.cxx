
// Please add your package name here
#include <FakeFactors4L/FakeBundle.h>
using namespace XAMPP;
namespace SUSY4L {
    namespace {
        static const std::vector<int> colors = {kBlack, kCyan - 2, kRed + 1, kGreen + 1, kOrange + 1, kViolet + 1, kBlue};
        static const std::vector<int> styles = {kFullTriangleUp,   kFullDiamond,        kFullFourTrianglesPlus, kFullStar,
                                                kFullTriangleDown, kFullThreeTriangles, kFullFourTrianglesX};
    }  // namespace
    IFakeBundle::IFakeBundle(std::shared_ptr<TH1> h_ref, Selection<FakeFactorTree> selection, unsigned int lep_mask,
                             const std::string& p_name, const std::string& s_name) :
        IBaseFakeTreeFiller(h_ref, lep_mask, p_name, s_name),
        m_selection(selection) {}
    std::string IFakeBundle::name() const {
        return lepton_flavour() + "_" + XAMPP::ReplaceExpInString(lepton_type(), ", ", "__") +
               (IBaseFakeTreeFiller::name().empty() ? "" : "_") + IBaseFakeTreeFiller::name();
    }
    std::string IFakeBundle::selection_str() const {
        std::string sel_name = XAMPP::EraseWhiteSpaces(get_selection().getName());
        for (std::string c : {"(", ")", "{", "}", "[", "]", "#"}) { sel_name = XAMPP::ReplaceExpInString(sel_name, c, ""); }
        sel_name = XAMPP::ReplaceExpInString(sel_name, " ", "__");
        while (sel_name.find("_") == 0) sel_name = sel_name.substr(1, std::string::npos);
        while (sel_name.rfind("_") == sel_name.size() - 1) sel_name = sel_name.substr(0, sel_name.size() - 1);
        return sel_name;
    }
    const Selection<FakeFactorTree>& IFakeBundle::get_selection() const { return m_selection; }
    Selection<FakeFactorTree>& IFakeBundle::get_selection() { return m_selection; }
    void IFakeBundle::write_file(TFile*) {}
    //########################################
    //           FakeBundle
    //########################################
    FakeBundle::FakeBundle(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection,
                           unsigned int lep_mask, const std::string& p_name) :
        IFakeBundle(h_ref, selection, lep_mask, p_name, sample.getName()),
        m_baseline(sample, selection, make_filler(LepClass::Baseline), "baseline", "PL",
                   PlotFormat().MarkerStyle(kFullSquare).MarkerSize(1.5).Color(kBlack)),
        m_signal(sample, selection, make_filler(LepClass::Signal), "signal", "PL",
                 PlotFormat().MarkerStyle(kFullTriangleUp).MarkerSize(1.5).Color(kCyan - 2)),
        m_loose(sample, selection, make_filler(LepClass::Loose), "loose", "PL",
                PlotFormat().MarkerStyle(kFullDiamond).Color(kRed + 1).MarkerSize(1.5)) {}

    PlotFiller1D FakeBundle::make_filler(unsigned int lep_sel) const {
        std::function<float(FakeFactorTree & t, size_t)> func = make_reader<FakeFactorTree>(get_histo()->GetXaxis());
        return PlotFiller1D("FakeFactor_" + selection_str() + "_" + name() + "_" + selection_to_str(lep_sel),
                            container_size<FakeFactorTree>(), lepton_selection<FakeFactorTree>(lep_sel),
                            [this, func](TH1D* h, FakeFactorTree& t, size_t i) { h->Fill(func(t, i), t.eventWeight()); },
                            dynamic_cast<TH1D*>(get_histo().get()));
    }

    Plot<TH1D> FakeBundle::get_baseline() { return m_baseline; }
    Plot<TH1D> FakeBundle::get_loose() { return m_loose; }
    Plot<TH1D> FakeBundle::get_signal() { return m_signal; }

    PlotContent<TH1D> FakeBundle::get_plot(CanvasOptions canvas_opt, const std::string& selection_title) {
        return PlotContent<TH1D>({m_baseline, m_signal, m_loose},
                                 {
                                     RatioEntry(1, 2, PlotUtils::efficiencyErrors),
                                 },
                                 {lepton_flavour() + ", " + lepton_type() + ", " + sample_name(), selection_title},
                                 name() + "_" + selection_str(), "AllFakeFactors", canvas_opt);
    }
    void FakeBundle::write_file(TFile* f) {
        if (!f) return;
        m_baseline.populate();
        m_loose.populate();
        m_signal.populate();
        if (XAMPP::Integrate(m_loose.getHisto()) == 0. || XAMPP::Integrate(m_signal.getHisto()) == 0) {
            std::cout << "Skip to create the fake factor of " << name() << " in selection " << selection_str() << " as there are "
                      << XAMPP::Integrate(m_loose.getHisto()) << " loose counts and " << XAMPP::Integrate(m_signal.getHisto())
                      << " signal counts" << std::endl;
            return;
        }
        TDirectory* out_dir = XAMPP::mkdir("FF/" + selection_str() + "/", f);
        std::shared_ptr<TH1> ratio = XAMPP::clone(m_signal.getSharedHisto());
        pull_overflow(ratio);
        pull_overflow(m_loose.getSharedHisto());
        ratio->Divide(m_loose.getHisto());
        out_dir->WriteObject(ratio.get(), name().c_str());
    }
    //########################################
    //           FakeBundleVanilla
    //########################################

    FakeBundleVanilla::FakeBundleVanilla(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection,
                                         unsigned int lep_mask, const std::string& p_name) :
        FakeBundle(h_ref, sample, selection, lep_mask, p_name),
        m_vanilla_signal(sample, selection, make_filler(LepClass::VanillaSignal), "signal (vanilla)", "PL",
                         PlotFormat().MarkerStyle(kOpenTriangleUp).MarkerSize(1.5).Color(kGreen - 2)),
        m_vanilla_loose(sample, selection, make_filler(LepClass::VanillaLoose), "loose (vanilla)", "PL",
                        PlotFormat().MarkerStyle(kOpenDiamond).Color(kOrange + 1).MarkerSize(1.5)) {}

    PlotContent<TH1D> FakeBundleVanilla::get_plot(CanvasOptions canvas_opt, const std::string& selection_title) {
        return PlotContent<TH1D>({get_baseline(), get_signal(), get_loose(), m_vanilla_signal, m_vanilla_loose},
                                 {
                                     RatioEntry(1, 2, PlotUtils::efficiencyErrors),
                                     RatioEntry(3, 4, PlotUtils::efficiencyErrors),

                                 },
                                 {lepton_flavour() + ", " + lepton_type() + ", " + sample_name(), selection_title},
                                 name() + "_" + selection_str(), "AllFakeFactors", canvas_opt);
    }

    //########################################
    //           FakeBundle2D
    //########################################
    FakeBundle2D::FakeBundle2D(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection,
                               unsigned int lep_mask, const std::string& p_name) :
        IFakeBundle(h_ref, selection, lep_mask, p_name, sample.getName()),
        m_baseline(sample, selection, make_filler(LepClass::Baseline), "baseline", "PL"),
        m_signal(sample, selection, make_filler(LepClass::Signal), "signal", "PL"),
        m_loose(sample, selection, make_filler(LepClass::Loose), "loose", "PL") {}

    PlotFiller2D FakeBundle2D::make_filler(unsigned int lep_sel) const {
        std::function<float(FakeFactorTree & t, size_t)> func_x = make_reader<FakeFactorTree>(get_histo()->GetXaxis());
        std::function<float(FakeFactorTree & t, size_t)> func_y = make_reader<FakeFactorTree>(get_histo()->GetYaxis());
        return PlotFiller2D(
            "FakeFactor_" + selection_str() + "_" + name() + "_" + selection_to_str(lep_sel), container_size<FakeFactorTree>(),
            lepton_selection<FakeFactorTree>(lep_sel),
            [this, func_x, func_y](TH2D* h, FakeFactorTree& t, size_t i) { h->Fill(func_x(t, i), func_y(t, i), t.eventWeight()); },
            dynamic_cast<TH2D*>(get_histo().get()));
    }

    Plot<TH2D> FakeBundle2D::get_baseline() { return m_baseline; }
    Plot<TH2D> FakeBundle2D::get_loose() { return m_loose; }
    Plot<TH2D> FakeBundle2D::get_signal() { return m_signal; }
    PlotContent<TH2D> FakeBundle2D::get_plot(CanvasOptions canvas_opt, const std::string& selection_title) {
        return PlotContent<TH2D>({m_baseline, m_signal, m_loose},
                                 {
                                     RatioEntry(1, 2, PlotUtils::efficiencyErrors),
                                 },
                                 {lepton_flavour() + ", " + lepton_type() + ", " + sample_name(), selection_title},
                                 name() + "_" + selection_str(), "AllFakeFactors", canvas_opt);
    }
    void FakeBundle2D::write_file(TFile* f) {
        if (!f) return;
        m_baseline.populate();
        m_loose.populate();
        m_signal.populate();
        if (XAMPP::Integrate(m_loose.getHisto()) == 0. || XAMPP::Integrate(m_signal.getHisto()) == 0) {
            std::cout << "Skip to create the fake factor of " << name() << " in selection " << selection_str() << " as there are "
                      << XAMPP::Integrate(m_loose.getHisto()) << " loose counts and " << XAMPP::Integrate(m_signal.getHisto())
                      << " signal counts" << std::endl;
            return;
        }
        TDirectory* out_dir = XAMPP::mkdir("FF/" + selection_str() + "/", f);
        std::shared_ptr<TH1> ratio = XAMPP::clone(m_signal.getSharedHisto());
        pull_overflow(ratio);
        pull_overflow(m_loose.getSharedHisto());
        ratio->Divide(m_loose.getHisto());
        out_dir->WriteObject(ratio.get(), name().c_str());
    }

    //#########################################
    //              FakeBundle3D
    //#########################################
    FakeBundle3D::FakeBundle3D(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection,
                               unsigned int lep_mask, const std::string& p_name) :
        IFakeBundle(h_ref, selection, lep_mask, p_name, sample.getName()),
        m_baseline(sample, selection, make_filler(LepClass::Baseline)),
        m_signal(sample, selection, make_filler(LepClass::Signal)),
        m_loose(sample, selection, make_filler(LepClass::Loose)) {}

    PlotFiller3D FakeBundle3D::make_filler(unsigned int lep_sel) const {
        std::function<float(FakeFactorTree & t, size_t)> func_x = make_reader<FakeFactorTree>(get_histo()->GetXaxis());
        std::function<float(FakeFactorTree & t, size_t)> func_y = make_reader<FakeFactorTree>(get_histo()->GetYaxis());
        std::function<float(FakeFactorTree & t, size_t)> func_z = make_reader<FakeFactorTree>(get_histo()->GetZaxis());

        return PlotFiller3D("FakeFactor_" + selection_str() + "_" + name() + "_" + selection_to_str(lep_sel),
                            container_size<FakeFactorTree>(), lepton_selection<FakeFactorTree>(lep_sel),
                            [func_x, func_y, func_z](TH3D* h, FakeFactorTree& t, size_t i) {
                                h->Fill(func_x(t, i), func_y(t, i), func_z(t, i), t.eventWeight());
                            },
                            dynamic_cast<TH3D*>(get_histo().get()));
    }

    Plot<TH3D> FakeBundle3D::get_baseline() { return m_baseline; }
    Plot<TH3D> FakeBundle3D::get_loose() { return m_loose; }
    Plot<TH3D> FakeBundle3D::get_signal() { return m_signal; }
    void FakeBundle3D::write_file(TFile* f) {
        if (!f) return;
        m_baseline.populate();
        m_loose.populate();
        m_signal.populate();
        if (XAMPP::Integrate(m_loose.getHisto()) == 0. || XAMPP::Integrate(m_signal.getHisto()) == 0) {
            std::cout << "Skip to create the fake factor of " << name() << " in selection " << selection_str() << " as there are "
                      << XAMPP::Integrate(m_loose.getHisto()) << " loose counts and " << XAMPP::Integrate(m_signal.getHisto())
                      << " signal counts" << std::endl;
            return;
        }
        TDirectory* out_dir = XAMPP::mkdir("FF/" + selection_str() + "/", f);
        std::shared_ptr<TH1> ratio = XAMPP::clone(m_signal.getSharedHisto());
        pull_overflow(ratio);
        pull_overflow(m_loose.getSharedHisto());
        ratio->Divide(m_loose.getHisto());
        out_dir->WriteObject(ratio.get(), name().c_str());
    }
    PlotContent<TH3D> FakeBundle3D::get_plot(CanvasOptions canvas_opt, const std::string& selection_title) {
        return PlotContent<TH3D>({m_baseline, m_signal, m_loose},
                                 {
                                     RatioEntry(1, 2, PlotUtils::efficiencyErrors),
                                 },
                                 {lepton_flavour() + ", " + lepton_type() + ", " + sample_name(), selection_title},
                                 name() + "_" + selection_str(), "AllFakeFactors", canvas_opt);
    }

    //########################################
    //           ProcessFractionBundle
    //########################################
    std::string ProcessFractionBundle::name() const {
        return lepton_flavour() + "_" + lepton_quality() + (IBaseFakeTreeFiller::name().empty() ? "" : "_") + IBaseFakeTreeFiller::name();
    }
    ProcessFractionBundle::ProcessFractionBundle(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample,
                                                 Selection<FakeFactorTree> selection, unsigned int lep_mask, const std::string& p_name) :
        IFakeBundle(h_ref, selection, lep_mask & (~LepClass::Incl), p_name, sample.getName()),
        m_plots() {
        m_plots.emplace_back(sample, selection, make_filler(lep_mask & LepClass::Incl), type_to_str(LepClass::Fake), "PL",
                             PlotFormat().MarkerStyle(kFullSquare).Color(kBlack).MarkerSize(1.5));

        int start = min_bit(lep_mask & LepClass::Incl);
        int end = max_bit(lep_mask & LepClass::Incl);
        int c = 1;
        for (int i = start; i <= end; ++i) {
            /// Skip conversions
            int truth_bit = 1 << i;
            if (!type_flavour_defined((lep_mask & LepClass::LepFlav) | truth_bit) || !(lep_mask & truth_bit)) continue;
            m_plots.emplace_back(sample, selection, make_filler(truth_bit), type_to_str(truth_bit), "PL",
                                 PlotFormat().MarkerStyle(styles[c]).Color(colors[c]).MarkerSize(1.5));
            ++c;
        }
    }
    std::vector<Plot<TH1D>>& ProcessFractionBundle::get_plots() { return m_plots; }

    PlotFiller1D ProcessFractionBundle::make_filler(unsigned int lep_sel) const {
        std::function<float(FakeFactorTree & t, size_t)> instruct = make_reader<FakeFactorTree>(get_histo()->GetXaxis());
        if (!is_lepton_variable(get_histo()->GetXaxis())) {
            return PlotFiller1D("ProcFrac_" + selection_str() + "_" + name() + "_" + type_to_str(lep_sel),
                                [this, instruct, lep_sel](TH1D* h, FakeFactorTree& t) {
                                    unsigned int i =
                                        t.get_n_lep(lep_sel | (lepton_mask() & LepClass::LepFlav) | (lepton_mask() & LepClass::Baseline));
                                    if (i > 0) h->Fill(instruct(t, 0), i * t.eventWeight());
                                },
                                dynamic_cast<TH1D*>(get_histo().get()));
        }
        return PlotFiller1D("ProcFrac_" + selection_str() + "_" + name() + "_" + type_to_str(lep_sel), container_size<FakeFactorTree>(),
                            lepton_selection<FakeFactorTree>(lep_sel),
                            [instruct](TH1D* h, FakeFactorTree& t, size_t i) { h->Fill(instruct(t, i), t.eventWeight()); },
                            dynamic_cast<TH1D*>(get_histo().get()));
    }

    PlotContent<TH1D> ProcessFractionBundle::get_plot(CanvasOptions canvas_opt, const std::string& selection_title) {
        std::vector<RatioEntry> ratios;
        for (unsigned int i = 1; i < get_plots().size(); ++i) { ratios.emplace_back(i, 0, PlotUtils::efficiencyErrors); }
        return PlotContent<TH1D>(get_plots(), ratios, {lepton_flavour() + ", " + lepton_quality() + ", " + sample_name(), selection_title},
                                 name() + "_" + selection_str(), "AllProcessFractions", canvas_opt);
    }
    void ProcessFractionBundle::write_file(TFile* f) {
        if (!f || m_plots.empty()) return;
        for (auto& pc : m_plots) { pc.populate(); }
        if (XAMPP::Integrate(m_plots[0].getHisto()) == 0) {
            std::cout << "Skip to create the process fraction of " << name() << " in selection" << selection_str() << std::endl;
            return;
        }
        TDirectory* out_dir = XAMPP::mkdir("ProcFrac/" + selection_str() + "/", f);
        std::shared_ptr<TH1> den = m_plots[0].getSharedHisto();
        pull_overflow(den);
        for (unsigned int i = 1; i < m_plots.size(); ++i) {
            std::shared_ptr<TH1> frac_histo = XAMPP::clone(m_plots[i].getSharedHisto());
            pull_overflow(frac_histo);
            frac_histo->Divide(den.get());
            out_dir->WriteObject(frac_histo.get(), Form("%s_%s", name().c_str(), m_plots[i].getLegendTitle().c_str()));
        }
        out_dir = XAMPP::mkdir("FakeDistributions/" + selection_str() + "/", f);
        out_dir->WriteObject(den.get(), name().c_str());
    }

    //########################################
    //           ProcessFractionBundle2D
    //########################################
    std::string ProcessFractionBundle2D::name() const {
        return lepton_flavour() + "_" + lepton_quality() + (IBaseFakeTreeFiller::name().empty() ? "" : "_") + IBaseFakeTreeFiller::name();
    }

    ProcessFractionBundle2D::ProcessFractionBundle2D(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample,
                                                     Selection<FakeFactorTree> selection, unsigned int lep_mask,
                                                     const std::string& p_name) :
        IFakeBundle(h_ref, selection, lep_mask & (~LepClass::Incl), p_name, sample.getName()),
        m_plots() {
        m_plots.emplace_back(sample, selection, make_filler(lep_mask & LepClass::Incl), type_to_str(LepClass::Fake), "PL");

        int start = min_bit(lep_mask & LepClass::Incl);
        int end = max_bit(lep_mask & LepClass::Incl);
        for (int i = start; i <= end; ++i) {
            /// Skip conversions
            int truth_bit = 1 << i;
            if (!type_flavour_defined((lep_mask & LepClass::LepFlav) | truth_bit) || !(lep_mask & truth_bit)) continue;
            m_plots.emplace_back(sample, selection, make_filler(truth_bit), type_to_str(truth_bit), "PL");
        }
    }

    std::vector<Plot<TH2D>>& ProcessFractionBundle2D::get_plots() { return m_plots; }
    PlotFiller2D ProcessFractionBundle2D::make_filler(unsigned int lep_sel) const {
        std::function<float(FakeFactorTree & t, size_t)> func_x = make_reader<FakeFactorTree>(get_histo()->GetXaxis());
        std::function<float(FakeFactorTree & t, size_t)> func_y = make_reader<FakeFactorTree>(get_histo()->GetYaxis());

        return PlotFiller2D(
            "ProcFrac2D_" + selection_str() + "_" + name() + "_" + type_to_str(lep_sel), container_size<FakeFactorTree>(),
            lepton_selection<FakeFactorTree>(lep_sel),
            [func_x, func_y](TH2D* h, FakeFactorTree& t, size_t i) { h->Fill(func_x(t, i), func_y(t, i), t.eventWeight()); },
            dynamic_cast<TH2D*>(get_histo().get()));
    }

    PlotContent<TH2D> ProcessFractionBundle2D::get_plot(CanvasOptions canvas_opt, const std::string& selection_title) {
        std::vector<RatioEntry> ratios;
        for (unsigned int i = 1; i < get_plots().size(); ++i) { ratios.emplace_back(i, 0, PlotUtils::efficiencyErrors); }
        return PlotContent<TH2D>(get_plots(), ratios, {lepton_flavour() + ", " + lepton_quality() + ", " + sample_name(), selection_title},
                                 name() + "_" + selection_str(), "AllProcessFractions", canvas_opt);
    }

    void ProcessFractionBundle2D::write_file(TFile* f) {
        if (!f || m_plots.empty()) return;
        for (auto& pc : m_plots) { pc.populate(); }
        if (XAMPP::Integrate(m_plots[0].getHisto()) == 0) {
            std::cout << "Skip to create the process fraction of " << name() << " in selection" << selection_str() << std::endl;
            return;
        }
        TDirectory* out_dir = XAMPP::mkdir("ProcFrac/" + selection_str() + "/", f);
        std::shared_ptr<TH1> den = m_plots[0].getSharedHisto();
        pull_overflow(den);
        for (unsigned int i = 1; i < m_plots.size(); ++i) {
            std::shared_ptr<TH1> frac_histo = XAMPP::clone(m_plots[i].getSharedHisto());
            pull_overflow(frac_histo);
            frac_histo->Divide(den.get());
            out_dir->WriteObject(frac_histo.get(), Form("%s_%s", name().c_str(), m_plots[i].getLegendTitle().c_str()));
        }
        out_dir = XAMPP::mkdir("FakeDistributions/" + selection_str() + "/", f);
        out_dir->WriteObject(den.get(), name().c_str());
    }

    //########################################
    //           ProcessFractionBundle3D
    //########################################
    std::string ProcessFractionBundle3D::name() const {
        return lepton_flavour() + "_" + lepton_quality() + (IBaseFakeTreeFiller::name().empty() ? "" : "_") + IBaseFakeTreeFiller::name();
    }
    ProcessFractionBundle3D::ProcessFractionBundle3D(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample,
                                                     Selection<FakeFactorTree> selection, unsigned int lep_mask,
                                                     const std::string& p_name) :
        IFakeBundle(h_ref, selection, lep_mask & (~LepClass::Incl), p_name, sample.getName()),
        m_plots() {
        m_plots.emplace_back(sample, selection, make_filler(lep_mask & LepClass::Incl), type_to_str(LepClass::Fake), "PL");

        int start = min_bit(lep_mask & LepClass::Incl);
        int end = max_bit(lep_mask & LepClass::Incl);
        for (int i = start; i <= end; ++i) {
            /// Skip conversions
            int truth_bit = 1 << i;
            if (!type_flavour_defined((lep_mask & LepClass::LepFlav) | truth_bit) || !(lep_mask & truth_bit)) continue;
            m_plots.emplace_back(sample, selection, make_filler(truth_bit), type_to_str(truth_bit), "PL");
        }
    }
    std::vector<Plot<TH3D>>& ProcessFractionBundle3D::get_plots() { return m_plots; }

    PlotFiller3D ProcessFractionBundle3D::make_filler(unsigned int lep_sel) const {
        std::function<float(FakeFactorTree & t, size_t)> func_x = make_reader<FakeFactorTree>(get_histo()->GetXaxis());
        std::function<float(FakeFactorTree & t, size_t)> func_y = make_reader<FakeFactorTree>(get_histo()->GetYaxis());
        std::function<float(FakeFactorTree & t, size_t)> func_z = make_reader<FakeFactorTree>(get_histo()->GetZaxis());

        return PlotFiller3D("ProcFrac3D_" + selection_str() + "_" + name() + "_" + type_to_str(lep_sel), container_size<FakeFactorTree>(),
                            lepton_selection<FakeFactorTree>(lep_sel),
                            [func_x, func_y, func_z](TH3D* h, FakeFactorTree& t, size_t i) {
                                h->Fill(func_x(t, i), func_y(t, i), func_z(t, i), t.eventWeight());
                            },
                            dynamic_cast<TH3D*>(get_histo().get()));
    }
    void ProcessFractionBundle3D::write_file(TFile* f) {
        if (!f || m_plots.empty()) return;
        for (auto& pc : m_plots) { pc.populate(); }
        if (XAMPP::Integrate(m_plots[0].getHisto()) == 0) {
            std::cout << "Skip to create the process fraction of " << name() << " in selection" << selection_str() << std::endl;
            return;
        }
        TDirectory* out_dir = XAMPP::mkdir("ProcFrac/" + selection_str() + "/", f);
        std::shared_ptr<TH1> den = m_plots[0].getSharedHisto();
        pull_overflow(den);
        for (unsigned int i = 1; i < m_plots.size(); ++i) {
            std::shared_ptr<TH1> frac_histo = XAMPP::clone(m_plots[i].getSharedHisto());
            pull_overflow(frac_histo);
            frac_histo->Divide(den.get());
            out_dir->WriteObject(frac_histo.get(), Form("%s_%s", name().c_str(), m_plots[i].getLegendTitle().c_str()));
        }
        out_dir = XAMPP::mkdir("FakeDistributions/" + selection_str() + "/", f);
        out_dir->WriteObject(den.get(), name().c_str());
    }
    PlotContent<TH3D> ProcessFractionBundle3D::get_plot(CanvasOptions canvas_opt, const std::string& selection_title) {
        std::vector<RatioEntry> ratios;
        for (unsigned int i = 1; i < get_plots().size(); ++i) { ratios.emplace_back(i, 0, PlotUtils::efficiencyErrors); }
        return PlotContent<TH3D>(get_plots(), ratios, {lepton_flavour() + ", " + lepton_quality() + ", " + sample_name(), selection_title},
                                 name() + "_" + selection_str(), "AllProcessFractions", canvas_opt);
    }

    //########################################
    //           DeltaRHisto
    //########################################
    DeltaRHisto::DeltaRHisto(std::shared_ptr<TH1> h_ref, Sample<FakeFactorTree> sample, Selection<FakeFactorTree> selection,
                             unsigned int lep_mask, CanvasOptions canvas_opt, const std::string& selection_title,
                             const std::string& p_name) :
        IFakeBundle(h_ref, selection, lep_mask | LepClass::Incl, p_name),
        m_ff(sample, selection, make_filler((LepClass::Ele | LepClass::Fake), (LepClass::Ele | LepClass::Fake)), "Fake-Fake", "PL",
             PlotFormat().MarkerStyle(kFullSquare).Color(kBlack)),
        m_rr(sample, selection, make_filler((LepClass::Ele | LepClass::Real), (LepClass::Ele | LepClass::Real)), "Real-Real", "PL",
             PlotFormat().MarkerStyle(kFullSquare).Color(kRed)),
        m_rf(sample, selection, make_filler((LepClass::Ele | LepClass::Real), (LepClass::Ele | LepClass::Fake)), "Real-Fake", "PL",
             PlotFormat().MarkerStyle(kFullSquare).Color(kBlue)),

        m_plot({m_ff, m_rr, m_rf},

               {lepton_flavour() + ", " + lepton_type(), selection_title}, selection_str() + "_" + name(), "AllDeltaRs", canvas_opt) {}

    Plot<TH1D> DeltaRHisto::get_DeltaR_FF() { return m_ff; }
    Plot<TH1D> DeltaRHisto::get_DeltaR_RR() { return m_rr; }
    Plot<TH1D> DeltaRHisto::get_DeltaR_RF() { return m_rf; }

    PlotContent<TH1D> DeltaRHisto::get_plot() { return m_plot; }

    PlotFiller1D DeltaRHisto::make_filler(unsigned int lep_sel1, unsigned int lep_sel2) const {
        return PlotFiller1D("DeltaR_" + selection_str() + "_" + name() + "_" + type_to_str(lep_sel1) + "_" + type_to_str(lep_sel2),
                            [this, lep_sel1, lep_sel2](TH1D* h, FakeFactorTree& t) {
                                for (size_t i = 0; i < t.get_n_lep(LepClass::AllLep); ++i) {
                                    if (t.is_Lep(i, lep_sel1)) {
                                        float_t DeltaR = FLT_MAX;
                                        bool secondLeptonFound = false;
                                        for (size_t j = i + 1; j < t.get_n_lep(LepClass::AllLep); ++j) {
                                            if (t.is_Lep(j, lep_sel2)) {
                                                secondLeptonFound = true;
                                                if (t.get_delta_R(i, j) < DeltaR) { DeltaR = t.get_delta_R(i, j); }
                                            }
                                        }
                                        if (secondLeptonFound) { h->Fill(DeltaR, t.eventWeight()); }
                                    }
                                }
                            },
                            dynamic_cast<TH1D*>(get_histo().get()));
    }

}  // namespace SUSY4L
