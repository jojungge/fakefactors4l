
// Please add your package name here
#include <FakeFactors4L/FakeClosureTree.h>

namespace SUSY4L {
    namespace {

        static const float emu_ff_min_dR = 0.6;

        PlotPostProcessor<TH1D, TH1D> overflow_pull("PullOverFlow", [](std::shared_ptr<TH1D> H) {
            pull_overflow(H);
            return H;
        });

    }  // namespace

    //############################################
    //              ProcessFractionHolder        #
    //############################################
    ProcessFractionHolder* ProcessFractionHolder::m_inst = nullptr;
    ProcessFractionHolder* ProcessFractionHolder::instance() {
        if (!m_inst) m_inst = new ProcessFractionHolder();
        return m_inst;
    }
    ProcessFractionHolder::~ProcessFractionHolder() { m_inst = nullptr; }
    ProcessFractionHolder::ProcessFractionHolder() :
        m_ff_histos(),
        m_sf_histos(),
        m_proc_histos(),
        m_incl_fake_histos(),
        m_sys_proc_frac() {}
    std::vector<std::shared_ptr<TH1>> ProcessFractionHolder::get_fake_factors(std::shared_ptr<TFile> f, const std::string& ff_region) {
        return cache_to_map(f, m_ff_histos, "FF/", ff_region);
    }
    std::vector<std::shared_ptr<TH1>> ProcessFractionHolder::get_scale_factors(std::shared_ptr<TFile> f, const std::string& sf_region) {
        return cache_to_map(f, m_sf_histos, "SF/", sf_region);
    }

    std::vector<std::shared_ptr<TH1>> ProcessFractionHolder::get_process_fractions(std::shared_ptr<TFile> f,
                                                                                   const std::string& proc_region) {
        return cache_to_map(f, m_proc_histos, "ProcFrac/", proc_region);
    }
    std::vector<std::shared_ptr<TH1>> ProcessFractionHolder::get_fake_distributions(std::shared_ptr<TFile> f, const std::string& region) {
        return cache_to_map(f, m_incl_fake_histos, "FakeDistributions/", region);
    }

    std::vector<std::shared_ptr<TH1>> ProcessFractionHolder::cache_to_map(std::shared_ptr<TFile> f, fake_map& map,
                                                                          const std::string& top_dir, const std::string& region) {
        fake_map::const_iterator itr = map.find(fake_map_key(f, region));
        if (itr == map.end()) {
            map[fake_map_key(f, region)] = load_from_dir(f->GetDirectory(top_dir.c_str()), region);
            return cache_to_map(f, map, top_dir, region);
        }
        return itr->second;
    }
    std::vector<std::shared_ptr<TH1>> ProcessFractionHolder::load_from_dir(TDirectory* f, const std::string& region) {
        std::vector<std::shared_ptr<TH1>> ret;
        std::vector<std::string> content = XAMPP::recursive_ls(f->GetDirectory(region.c_str()), region + "/");
        std::sort(content.begin(), content.end());
        if (content.empty()) {
            std::cout << "ProcessFractionHolder::load_from_dir(): Directory " << region << ". Does not have anything useful" << std::endl;
        }
        for (auto& c : content) { ret.push_back(XAMPP::fetch_from_file(c, f)); }

        return ret;
    }

    std::vector<std::shared_ptr<TH1>> ProcessFractionHolder::make_proc_denominators(
        const std::string& region, const std::vector<std::shared_ptr<TFile>>& to_consider) {
        std::vector<std::shared_ptr<TH1>> denom;
        for (auto& f : to_consider) {
            std::vector<std::shared_ptr<TH1>> dist = get_fake_distributions(f, region);
            if (dist.empty()) {
                throw std::runtime_error(Form("Could not find fake-distributions for %s  in file %s", region.c_str(), f->GetName()));
            }
            if (denom.empty()) {
                denom = clone(dist);
            } else {
                for (auto& h : dist) {
                    std::vector<std::shared_ptr<TH1>>::iterator itr =
                        std::find_if(denom.begin(), denom.end(), [&h](const std::shared_ptr<TH1>& den_histo) {
                            return std::string(den_histo->GetName()) == std::string(h->GetName());
                        });
                    if (itr == denom.end() || !(*itr)->Add(h.get())) {
                        throw std::runtime_error(Form("Failed to combine %s  from %s with the existing histograms in region %s",
                                                      h->GetName(), f->GetName(), region.c_str()));
                    }
                }
            }
        }
        return denom;
    }
    std::vector<std::shared_ptr<TH1>> ProcessFractionHolder::clone(const std::vector<std::shared_ptr<TH1>>& to_clone) const {
        std::vector<std::shared_ptr<TH1>> cloned;
        for (auto& H : to_clone) {
            cloned.push_back(XAMPP::clone(H));
            cloned.back()->SetName(H->GetName());
        }
        return cloned;
    }
    std::vector<std::shared_ptr<TH1>> ProcessFractionHolder::make_smp_fractions(const fake_map_key& fake_key,
                                                                                const std::vector<std::shared_ptr<TH1>>& denominators) {
        std::vector<std::shared_ptr<TH1>> numerators = clone(get_fake_distributions(fake_key.first, fake_key.second));
        for (auto& H : numerators) {
            std::vector<std::shared_ptr<TH1>>::const_iterator itr_d =
                std::find_if(denominators.begin(), denominators.end(), [&H](const std::shared_ptr<TH1>& den) {
                    std::string den_name = den->GetName();
                    std::string num_name = H->GetName();
                    return den_name == num_name;
                });
            if (itr_d == denominators.end()) {
                throw std::runtime_error(Form("Failed to find the proper denominator for histogram %s in file %s and region %s",
                                              H->GetName(), fake_key.first->GetName(), fake_key.second.c_str()));
            }
            H->Divide((*itr_d).get());
        }
        return numerators;
    }

    void ProcessFractionHolder::normalize_process_fractions() {
        if (m_proc_histos.empty()) return;
        std::set<std::shared_ptr<TFile>> opened_files;
        for (auto& fake_key : m_proc_histos) { opened_files.insert(fake_key.first.first); }
        if (opened_files.size() < 2) {
            throw std::runtime_error("The normalize_process_fractions() must only be called if more than one TFile is loaded");
        }
        std::cout << "ProcessFractionHolder::normalize_process_fractions() -- Start to renormalize the process fractions" << std::endl;
        // typedef std::pair<std::shared_ptr<TFile>, std::string> fake_map_key;
        // typedef std::map<fake_map_key, std::vector<std::shared_ptr<TH1>>> fake_map;

        while (!m_proc_histos.empty()) {
            /// Search a region having two files assigned
            fake_map_key to_look_for = m_proc_histos.begin()->first;
            std::vector<fake_map_key> to_normalize;
            for (auto& fake_key : m_proc_histos) {
                if (fake_key.first.second == to_look_for.second) { to_normalize.push_back(fake_key.first); }
            }

            /// Only now we need to normalize the stuff
            if (to_normalize.size() > 1) {
                /// Load first the fake distributions
                std::vector<std::shared_ptr<TFile>> files_to_norm;
                for (auto& fake_key : to_normalize) { files_to_norm.push_back(fake_key.first); }
                /// Make the denominator
                std::vector<std::shared_ptr<TH1>> denominators = make_proc_denominators(to_look_for.second, files_to_norm);
                for (auto& fake_key : to_normalize) {
                    std::vector<std::shared_ptr<TH1>> numerators = make_smp_fractions(fake_key, denominators);
                    std::vector<std::shared_ptr<TH1>> proc_frac = get_process_fractions(fake_key.first, fake_key.second);
                    for (auto& H : proc_frac) {
                        /// Histogram is named like <Flavour>_<selection>_<Actual_Name>_<FAKE_TYPE>
                        std::string h_name = H->GetName();
                        std::vector<std::shared_ptr<TH1>>::const_iterator itr =
                            std::find_if(numerators.begin(), numerators.end(), [&h_name](const std::shared_ptr<TH1>& sc) {
                                std::string sc_name = sc->GetName();
                                return h_name.find(sc_name) == 0 && h_name.rfind("_") == sc_name.size();
                            });
                        if (itr == numerators.end()) {
                            throw std::runtime_error(Form("Could not find a proper scaling histogram for %s in region %s", h_name.c_str(),
                                                          fake_key.second.c_str()));
                        }
                        H->Multiply(itr->get());
                        normalize_syst_fractions(*itr, H);
                    }
                }
            }
            /// Delete the content in to normalize
            for (auto& to_del : to_normalize) {
                fake_map::iterator itr = m_proc_histos.find(to_del);
                m_proc_histos.erase(itr);
            }
        }
        std::cout << "ProcessFractionHolder::normalize_process_fractions() -- Now the non-closure fractions " << std::endl;

        /// We are using for the fake-closure systematic the inclusive fake distributions
        /// and renormalize them to unity but this time with no clone.
        while (!m_incl_fake_histos.empty()) {
            /// Search a region having two files assigned
            fake_map_key to_look_for = m_incl_fake_histos.begin()->first;
            std::vector<fake_map_key> to_normalize;
            for (auto& fake_key : m_incl_fake_histos) {
                if (fake_key.first.second == to_look_for.second) { to_normalize.push_back(fake_key.first); }
            }
            /// One bin histograms
            if (to_normalize.size() == 1) {
                for (auto& H : m_incl_fake_histos.begin()->second) {
                    /// If there is more than one shared_ptr
                    /// we can be sure that the pointer is used in the FakeClosuresystematic
                    if (H.use_count() == 1) continue;
                    H->Reset();
                    for (unsigned int i = 1; i < XAMPP::GetNbins(H); ++i) {
                        H->SetBinContent(i, 1.);
                        H->SetBinError(i, 0.);
                    }
                }
            } else {
                std::vector<std::shared_ptr<TFile>> files_to_norm;
                for (auto& fake_key : to_normalize) { files_to_norm.push_back(fake_key.first); }
                /// Make the denominator
                std::vector<std::shared_ptr<TH1>> denominators = make_proc_denominators(to_look_for.second, files_to_norm);
                for (auto& to_norm : to_normalize) {
                    for (auto& H : m_incl_fake_histos[to_norm]) {
                        if (H.use_count() == 1) continue;
                        std::string h_name = H->GetName();
                        std::vector<std::shared_ptr<TH1>>::const_iterator itr =
                            std::find_if(denominators.begin(), denominators.end(), [&h_name](const std::shared_ptr<TH1>& sc) {
                                std::string sc_name = sc->GetName();
                                return h_name == h_name;
                            });
                        if (itr == denominators.end()) {
                            throw std::runtime_error(Form("Could not find a proper scaling histogram for %s in region %s", h_name.c_str(),
                                                          to_norm.second.c_str()));
                        }
                        H->Divide(itr->get());
                    }
                }
            }
            /// Delete the content in to normalize
            for (auto& to_del : to_normalize) {
                fake_map::iterator itr = m_incl_fake_histos.find(to_del);
                m_incl_fake_histos.erase(itr);
            }
        }
        /// We do not need the maps anymore
        m_ff_histos.clear();
        m_sf_histos.clear();
        m_incl_fake_histos.clear();
        m_sys_proc_frac.clear();
        m_proc_histos.clear();
    }

    void ProcessFractionHolder::new_proc_syst_histo(const std::shared_ptr<TH1>& nominal, const std::shared_ptr<TH1>& syst) {
        m_sys_proc_frac[nominal].push_back(syst);
    }
    void ProcessFractionHolder::normalize_syst_fractions(const std::shared_ptr<TH1>& num, const std::shared_ptr<TH1>& nominal) {
        std::map<std::shared_ptr<TH1>, std::vector<std::shared_ptr<TH1>>>::iterator itr = m_sys_proc_frac.find(nominal);
        if (itr != m_sys_proc_frac.end()) {
            for (auto& h : itr->second) { h->Multiply(num.get()); }
        }
    }

    //############################################
    //              FakeClosureTree              #
    //############################################
    FakeClosureTree::FakeClosureTree(TTree* t) :
        FakeFactorTree(t),
        m_EleMuo_permut_cr1(std::make_shared<XAMPP::DrawElementsFromSet>(1, 1)),
        m_EleMuo_permut_cr2(std::make_shared<XAMPP::DrawElementsFromSet>(2, 2)),
        m_Tau_permut_cr1(std::make_shared<XAMPP::DrawElementsFromSet>(1, 1)),
        m_Tau_permut_cr2(std::make_shared<XAMPP::DrawElementsFromSet>(2, 2)),
        m_loose_idx_lep(),
        m_all_idx(),
        m_skip_close_by(false) {}
    bool FakeClosureTree::skip_close_by() const { return m_skip_close_by; }
    bool FakeClosureTree::cache_numbers() {
        if (!FakeFactorTree::cache_numbers()) return false;
        m_loose_idx_lep.clear();
        m_skip_close_by = false;
        /// Cache HT variables
        unsigned int loose_lep = get_n_lep(LepClass::LightLep | LepClass::Loose | LepClass::Incl);
        unsigned int loose_tau = get_n_lep(LepClass::Tau | LepClass::Loose | LepClass::Incl);

        if (loose_lep + loose_tau > m_loose_idx_lep.capacity()) m_loose_idx_lep.reserve(loose_lep + loose_tau);

        m_all_idx = XAMPP::sequence(get_n_lep(LepClass::LepFlav | LepClass::Baseline | LepClass::Incl), 0);

        /// Sort the indices such that the reals are sorted in first
        std::sort(m_all_idx.begin(), m_all_idx.end(), [this](const size_t& a, const size_t& b) {
            if ((get_lepton_bitmask(a) & LepClass::Incl) != (get_lepton_bitmask(b) & LepClass::Incl))
                return (get_lepton_bitmask(a) & LepClass::Incl) < (get_lepton_bitmask(b) & LepClass::Incl);
            return get_pt(a, LepClass::LepFlav) > get_pt(b, LepClass::LepFlav);
        });
        for (auto& l : m_all_idx) {
            if (is_Lep(l, LepClass::LepFlav | LepClass::Loose | LepClass::Incl)) m_loose_idx_lep.push_back(l);
        }

        if (m_loose_idx_lep.size() != loose_lep + loose_tau) {
            std::cout << m_all_idx << std::endl;
            dump_event();
        }

        if (false && global_dR_minLep() < emu_ff_min_dR) {
            for (auto& l : m_all_idx) {
                if (!(get_lepton_flavour(l) & LepClass::LightLep) || !pass_id(l, LepClass::LepFlav) || get_dR_minLep(l) > emu_ff_min_dR)
                    continue;
                if (pass_id(get_closest_lep(l).first, LepClass::LepFlav)) {
                    m_skip_close_by = true;
                    break;
                }
            }
        }
        m_skip_close_by = false;

        /// Sort the loose indices by flavour this time
        std::sort(m_loose_idx_lep.begin(), m_loose_idx_lep.end(), [this, loose_lep](const size_t& a, const size_t& b) {
            unsigned int mask_a = get_lepton_bitmask(a);
            unsigned int mask_b = get_lepton_bitmask(b);
            if (m_skip_close_by && (mask_a & LepClass::LightLep) && (mask_b & LepClass::LightLep)) {
                float dR_a = get_dR_minLep(a);
                float dR_b = get_dR_minLep(b);
                if (dR_a > emu_ff_min_dR || dR_b > emu_ff_min_dR) return dR_a < dR_b;
                if ((mask_a & LepClass::LightLep) != (mask_b & LepClass::LightLep))
                    return (mask_a & LepClass::LightLep) > (mask_b & LepClass::LightLep);
                return get_pt(a, LepClass::LightLep) < get_pt(b, LepClass::LightLep);
            }
            return (mask_a & LepClass::LepFlav) < (mask_b & LepClass::LepFlav);
        });
        if (loose_lep > 0 && loose_lep != m_EleMuo_permut_cr1->n()) {
            m_EleMuo_permut_cr1 = std::make_shared<XAMPP::DrawElementsFromSet>(loose_lep, 1);
        }
        if (loose_lep > 1 && loose_lep != m_EleMuo_permut_cr2->n()) {
            m_EleMuo_permut_cr2 = std::make_shared<XAMPP::DrawElementsFromSet>(loose_lep, 2);
        }
        if (loose_tau > 0 && (loose_tau != m_Tau_permut_cr1->n() || loose_lep != m_Tau_permut_cr1->offset())) {
            m_Tau_permut_cr1 = std::make_shared<XAMPP::DrawElementsFromSet>(loose_tau, 1, loose_lep);
        }
        if (loose_tau > 1 && (loose_tau != m_Tau_permut_cr2->n() || loose_lep != m_Tau_permut_cr2->offset())) {
            m_Tau_permut_cr2 = std::make_shared<XAMPP::DrawElementsFromSet>(loose_tau, 2, loose_lep);
        }
        return true;
    }
    float FakeClosureTree::baseline_meff(unsigned int ev_selection, unsigned int permutation) {
        /// Actually we only need to care about the light leptons in the meff definition
        /// for taus we only exchange the jet momentum by the tau momentum which should roughly
        /// be the same
        unsigned int loose_lep = get_n_lep(LepClass::Incl | LepClass::LightLep | LepClass::Loose);
        if (loose_lep > 1 && (ev_selection & (LepClass::CR1 | LepClass::MixedCR2))) {
            permutation = permutation % m_EleMuo_permut_cr1->size();
            return Meff() + get_pt(m_loose_idx_lep[m_EleMuo_permut_cr1->value_in_tuple(permutation, 0)], LepClass::LepFlav);
        }
        if (loose_lep > 2 && (ev_selection & LepClass::CR2)) {
            permutation = permutation % m_EleMuo_permut_cr1->size();
            return Meff() + get_pt(m_loose_idx_lep[m_EleMuo_permut_cr2->value_in_tuple(permutation, 0)], LepClass::LepFlav) +
                   get_pt(m_loose_idx_lep[m_EleMuo_permut_cr2->value_in_tuple(permutation, 1)], LepClass::LepFlav);
        }
        return baseline_meff();
    }
    float FakeClosureTree::baseline_lep_ht(unsigned int ev_selection, unsigned int permutation) {
        unsigned int loose_lep = get_n_lep(LepClass::Incl | LepClass::LightLep | LepClass::Loose);
        if (loose_lep > 1 && (ev_selection & LepClass::CR1)) {
            permutation = permutation % m_EleMuo_permut_cr1->size();
            return signal_lep_ht() + get_pt(m_loose_idx_lep[m_EleMuo_permut_cr1->value_in_tuple(permutation, 0)], LepClass::LepFlav);
        }
        if (loose_lep > 2 && (ev_selection & LepClass::CR2)) {
            permutation = permutation % m_EleMuo_permut_cr2->size();
            return signal_lep_ht() + get_pt(m_loose_idx_lep[m_EleMuo_permut_cr2->value_in_tuple(permutation, 0)], LepClass::LepFlav) +
                   get_pt(m_loose_idx_lep[m_EleMuo_permut_cr2->value_in_tuple(permutation, 1)], LepClass::LepFlav);
        }
        return baseline_lep_ht();
    }
    const std::vector<size_t>& FakeClosureTree::get_loose_indices() {
        cache_numbers();
        return m_loose_idx_lep;
    }
    const std::vector<size_t>& FakeClosureTree::get_sorted_indices() {
        cache_numbers();
        return m_all_idx;
    }
    unsigned int FakeClosureTree::n_permutations(unsigned int ev_selection) {
        unsigned int n_loose = get_n_lep(LepClass::Incl | LepClass::LightLep | LepClass::Loose);
        unsigned int n_loose_tau = get_n_lep(LepClass::Incl | LepClass::Tau | LepClass::Loose);

        if (ev_selection & LepClass::CR1) {
            if (ev_selection & LepClass::MixedCR2) {
                unsigned int n_signal = get_n_lep(LepClass::Incl | LepClass::LightLep | LepClass::Signal);
                if (n_signal == 3) return m_Tau_permut_cr1->size();
                if (n_signal == 2) return m_EleMuo_permut_cr1->size();
            }
            unsigned int n_comb = 0;
            if (n_loose > 0 && ev_selection & LepClass::LightLep) n_comb = m_EleMuo_permut_cr1->size();
            if (n_loose_tau > 0 && ev_selection & LepClass::Tau) n_comb = m_Tau_permut_cr1->size() * (n_comb > 0 ? n_comb : 1);
            return n_comb;
        }
        if (ev_selection & LepClass::CR2) {
            unsigned int n_comb = 0;
            if (n_loose > 1 && ev_selection & LepClass::LightLep) { n_comb = m_EleMuo_permut_cr2->size(); }
            if (n_loose_tau > 1 && ev_selection & LepClass::Tau) n_comb = m_Tau_permut_cr2->size() * (n_comb > 0 ? n_comb : 1);
            if ((ev_selection & LepClass::MixedCR2) && ((ev_selection & LepClass::LepFlav) == LepClass::LepFlav)) {
                n_comb = m_EleMuo_permut_cr1->size() * m_Tau_permut_cr1->size();
            }
            return n_comb;
        }
        return 0;
    }

    bool FakeClosureTree::is_promotable(size_t j, unsigned int ev_selection, unsigned int permutation) {
        unsigned int jth_mask = get_lepton_bitmask(j);
        unsigned int loose_lep = get_n_lep(LepClass::Incl | LepClass::LightLep | LepClass::Loose);
        if (!((jth_mask & LepClass::Baseline) == LepClass::Loose) || !(jth_mask & LepClass::LepFlav & ev_selection) ||
            n_permutations(ev_selection) <= permutation) {
            return false;
        }
        std::shared_ptr<XAMPP::DrawElementsFromSet> ele_muo_permut;
        if (ev_selection & (LepClass::CR1 | LepClass::MixedCR2)) {
            ele_muo_permut = m_EleMuo_permut_cr1;
        } else if (ev_selection & LepClass::CR2) {
            ele_muo_permut = m_EleMuo_permut_cr2;
        }
        if (jth_mask & LepClass::LightLep && loose_lep) {
            if ((ev_selection & LepClass::CR1) && (ev_selection & LepClass::MixedCR2) &&
                get_n_lep(LepClass::Incl | LepClass::LightLep | LepClass::Signal) == 3) {
                return false;
            }
            unsigned int emu_permut = permutation % ele_muo_permut->size();
            for (unsigned int n = 0; n < ele_muo_permut->m(); ++n) {
                if (m_loose_idx_lep[ele_muo_permut->value_in_tuple(emu_permut, n)] == j) { return true; }
            }
        }
        if (jth_mask & LepClass::Tau && get_n_lep(LepClass::Incl | LepClass::Tau | LepClass::Loose)) {
            if ((ev_selection & LepClass::CR1) && (ev_selection & LepClass::MixedCR2) &&
                get_n_lep(LepClass::Incl | LepClass::LightLep | LepClass::Signal) == 2) {
                return false;
            }
            std::shared_ptr<XAMPP::DrawElementsFromSet> permut;
            if (ev_selection & (LepClass::CR1 | LepClass::MixedCR2)) {
                permut = m_Tau_permut_cr1;
            } else if (ev_selection & LepClass::CR2) {
                permut = m_Tau_permut_cr2;
            }
            unsigned int tau_permut = 0;
            if ((ev_selection & LepClass::CR1 && loose_lep > 0) || (ev_selection & LepClass::CR2 && loose_lep > 1)) {
                tau_permut = ((permutation - (permutation % ele_muo_permut->size())) / ele_muo_permut->size()) % permut->size();
            } else {
                tau_permut = permutation % permut->size();
            }
            for (unsigned int n = 0; n < permut->m(); ++n) {
                if (m_loose_idx_lep[permut->value_in_tuple(tau_permut, n)] == j) { return true; }
            }
        }
        return false;
    }
    std::shared_ptr<XAMPP::DrawElementsFromSet> FakeClosureTree::LightLep_permuter_CR1() const { return m_EleMuo_permut_cr1; }
    std::shared_ptr<XAMPP::DrawElementsFromSet> FakeClosureTree::LightLep_permuter_CR2() const { return m_EleMuo_permut_cr2; }
    //#########################################
    //              IClosureFiller            #
    //#########################################
    int IClosureFiller::event_selection(FakeClosureTree&) const { return 0; }
    IClosureFiller::IClosureFiller(std::shared_ptr<TH1> H, unsigned int lep_mask, Selection<FakeClosureTree> sel,
                                   const std::string& smp_name) :
        IBaseFakeTreeFiller(H, lep_mask, smp_name),
        m_selection(sel) {}
    const Selection<FakeClosureTree>& IClosureFiller::get_selection() const { return m_selection; }

    std::function<float(FakeClosureTree&, size_t)> IClosureFiller::make_reader(TAxis* A) const {
        if (A) {
            std::string title = A->GetTitle();
            std::cout << "Got Axis Title " << title << ".  ";
            if (title.find("H_{T}^{jets} / m_{eff}") != std::string::npos) {
                std::cout << "Read out the H_{T}^{jet} fraction of the meff" << std::endl;
                return [this](FakeClosureTree& t, size_t i) {
                    return (t.Meff() - t.Met() - t.signal_lep_ht()) / t.baseline_meff(event_selection(t), i);
                };
            } else if (title.find("E_{T}^{miss} / m_{eff}") != std::string::npos) {
                std::cout << "Read out the H_{T}^{jet} fraction of the meff" << std::endl;
                return [this](FakeClosureTree& t, size_t i) { return t.Met() / t.baseline_meff(event_selection(t), i); };
            } else if (title.find("H_{T}^{lep} / m_{eff}") != std::string::npos) {
                std::cout << "Read out the H_{T}^{jet} fraction of the meff" << std::endl;
                return [this](FakeClosureTree& t, size_t i) {
                    return (t.baseline_lep_ht(event_selection(t), i)) / t.baseline_meff(event_selection(t), i);
                };
            } else if (title.find("m_{eff}") != std::string::npos) {
                std::cout << "Read out meff" << std::endl;
                return [this](FakeClosureTree& t, size_t i) { return t.baseline_meff(event_selection(t), i); };
            } else if (title.find("H_{T}^{jets}") != std::string::npos) {
                std::cout << "Read out ht jets" << std::endl;
                return [this](FakeClosureTree& t, size_t i) {
                    return t.baseline_meff(event_selection(t), i) - t.baseline_lep_ht(event_selection(t), i) - t.Met();
                };
            } else if (title.find("H_{T}^{lep}") != std::string::npos) {
                std::cout << "Read out ht leptons" << std::endl;
                return [this](FakeClosureTree& t, size_t i) { return t.baseline_lep_ht(event_selection(t), i); };
            }
        }
        std::function<float(FakeFactorTree & t, size_t)> func = IBaseFakeTreeFiller::make_reader<FakeFactorTree>(A);
        return [func](FakeClosureTree& t, size_t i) { return func(t, i); };
    }

    //#########################################
    //              FakeWeighter              #
    //#########################################
    FakeWeighter::FakeWeighter(unsigned int lepmask, unsigned int design_number, short sign, Selection<FakeClosureTree> apply_in) :
        IClosureFiller(nullptr, (lepmask & ~(LepClass::Signal | LepClass::LepFlav)) | LepClass::Incl, apply_in),
        m_design_fakes(design_number),
        m_sign(sign > 0 ? 1 : -1),
        m_ff_histos() {}
    std::vector<std::shared_ptr<FakeHistoReader>> FakeWeighter::get_components() const { return m_ff_histos; }
    short FakeWeighter::sign() const { return m_sign; }
    double FakeWeighter::get_weight(FakeClosureTree& t) const {
        /// No fake weight is going to be applied
        if (!get_selection()(t)) return 1.;

        double w = sign();
        unsigned int n_applied = 0;
        for (size_t j : t.get_loose_indices()) {
            if (!t.is_promotable(j, lepton_mask(), 0)) continue;
            ++n_applied;
            double w_lep = 0.;
            double real_lep = 0;
            for (auto& ff : m_ff_histos) {
                if (ff->lepton_mask() & LepClass::Real)
                    real_lep += ff->get_weight(t, j);
                else
                    w_lep += ff->get_weight(t, j);
            }
            w *= w_lep;
        }
        // if (t.skip_close_by() && (lepton_mask() & LepClass::CR1) &&
        //    t.get_lepton_flavour(t.get_loose_indices()[0]) == LepClass::Muo
        //) {w= sign()*std::sqrt(std::fabs(w));}

        return n_applied ? w : 1;
    }
    double FakeWeighter::get_weight(FakeClosureTree& t, unsigned int permut) const {
        if (!get_selection()(t)) return 1.;
        double w = sign();
        double T = 1;
        bool real_ineff = false;
        unsigned int n_applied = 0;
        for (size_t j : t.get_loose_indices()) {
            if (!t.is_promotable(j, lepton_mask(), permut)) continue;
            ++n_applied;
            double w_lep = 0;
            double real_lep = 0;

            for (auto& ff : m_ff_histos) {
                if (ff->lepton_mask() & LepClass::Real) {
                    real_lep += ff->get_weight(t, j);
                } else {
                    w_lep += ff->get_weight(t, j);
                }
            }
            if (real_ineff && real_lep > 0) {
                w *= (1 + w_lep / real_lep) * w_lep;
                T *= w_lep / real_lep;
            } else {
                w *= w_lep;
            }
        }
        if (n_applied != design_multip()) {
            t.dump_event();
            for (auto ff : m_ff_histos) { std::cout << ff->ff_histo()->GetName() << " " << ff->lepton_flavour() << std::endl; }
            throw std::runtime_error(Form("Stonjek wrong number of leptons were considered in %s (%s) permutation %u weights... %u vs. %u",
                                          get_selection().getName().c_str(), lepton_flavour().c_str(), permut, n_applied, design_multip()));
        }
        return w * (!real_ineff || design_multip() == 1 ? 1. : 1. - T);
    }

    void FakeWeighter::dump_weight(FakeClosureTree& t, unsigned int permut) const {
        for (size_t j : t.get_loose_indices()) {
            if (!t.is_promotable(j, lepton_mask(), permut)) continue;

            for (auto& ff : m_ff_histos) { ff->dump_weight(t, j); }
        }
    }

    double FakeWeighter::get_syst_weight(FakeClosureTree& t, unsigned int syst, unsigned int permut) const {
        if (!get_selection()(t)) return 1.;
        double w = sign();
        double T = 1;
        bool real_ineff = false;
        unsigned int n_applied = 0;
        for (size_t j : t.get_loose_indices()) {
            if (!t.is_promotable(j, lepton_mask(), permut)) continue;

            ++n_applied;
            double w_lep = 0;
            double real_lep = 0;

            for (auto& ff : m_ff_histos) {
                if (ff->lepton_mask() & LepClass::Real) {
                    real_lep += ff->get_syst_weight(t, j, syst);
                } else {
                    w_lep += ff->get_syst_weight(t, j, syst);
                }
            }
            if (real_ineff && real_lep > 0) {
                w *= (1 + w_lep / real_lep) * w_lep;
                T *= w_lep / real_lep;
            } else {
                w *= w_lep;
            }
        }
        return w * (!real_ineff || design_multip() == 1 ? 1. : 1. - T);
    }

    unsigned int FakeWeighter::design_multip() const { return m_design_fakes; }
    size_t FakeWeighter::n_weigthers() const { return m_ff_histos.size(); }

    void FakeWeighter::setup_systematics() {
        std::function<void(const std::vector<std::shared_ptr<TH1>>&)> renormalize_fractions =
            [](const std::vector<std::shared_ptr<TH1>>& frac) {
                std::shared_ptr<TH1> denominator = XAMPP::clone(frac[0], true);
                for (auto& H : frac) { denominator->Add(H.get()); }
                for (auto& H : frac) { H->Divide(denominator.get()); }
            };

        for (int flav : {LepClass::Ele, LepClass::Muo, LepClass::Tau1P, LepClass::Tau3P}) {
            std::vector<std::shared_ptr<FakeHistoReader>> flavour_histos;
            XAMPP::CopyVector<std::shared_ptr<FakeHistoReader>>(
                m_ff_histos, flavour_histos, [flav](const std::shared_ptr<FakeHistoReader>& H) {
                    return H->proc_histo() != nullptr && (H->lepton_mask() & flav) && H->associated_file();
                });
            /// Loop over the obtained histograms
            for (auto& ff_h : flavour_histos) {
                std::shared_ptr<TH1> one_sigma = pullErrors(ff_h->proc_histo());
                std::shared_ptr<TH1> one_up = XAMPP::clone(ff_h->proc_histo());
                std::shared_ptr<TH1> one_dn = XAMPP::clone(ff_h->proc_histo());

                ProcessFractionHolder::instance()->new_proc_syst_histo(ff_h->proc_histo(), one_up);
                ProcessFractionHolder::instance()->new_proc_syst_histo(ff_h->proc_histo(), one_dn);

                /// Add the one sigma uncertainties
                one_up->Add(one_sigma.get());
                one_dn->Add(one_sigma.get(), -1);
                /// Make sure that the uncertainties themselves
                /// do not exceed 1 and 0

                truncate(one_up, 0, DBL_MAX);
                truncate(one_dn);

                ff_h->add_syst_up(ff_h->lepton_mask(), one_up);
                ff_h->add_syst_dn(ff_h->lepton_mask(), one_dn);

                std::vector<std::shared_ptr<TH1>> up_variations{one_up};
                std::vector<std::shared_ptr<TH1>> dn_variations{one_dn};

                for (auto& other_ff : flavour_histos) {
                    if (other_ff == ff_h || !XAMPP::same_binning(other_ff->proc_histo(), ff_h->proc_histo()) ||
                        ff_h->associated_file() != other_ff->associated_file()) {
                        continue;
                    }
                    std::shared_ptr<TH1> syst_up = XAMPP::clone(other_ff->proc_histo());
                    std::shared_ptr<TH1> syst_dn = XAMPP::clone(other_ff->proc_histo());

                    up_variations += syst_up;
                    dn_variations += syst_dn;
                    other_ff->add_syst_up(ff_h->lepton_mask(), syst_up);
                    other_ff->add_syst_dn(ff_h->lepton_mask(), syst_dn);

                    ProcessFractionHolder::instance()->new_proc_syst_histo(ff_h->proc_histo(), syst_up);
                    ProcessFractionHolder::instance()->new_proc_syst_histo(ff_h->proc_histo(), syst_dn);
                }
                renormalize_fractions(up_variations);
                renormalize_fractions(dn_variations);
            }
        }
        /// Reset the associated file connection
        for (auto& ff : m_ff_histos) { ff->set_associated_file(std::shared_ptr<TFile>(nullptr)); }
    }

    std::vector<std::shared_ptr<TH1>> FakeWeighter::filter_variable(const std::vector<std::shared_ptr<TH1>>& in_histos,
                                                                    const std::string& h_key, unsigned int lep_flav) const {
        /// Kick all irrelevant fake-factors from the list
        std::vector<std::shared_ptr<TH1>> histos = in_histos;
        XAMPP::EraseFromVector<std::shared_ptr<TH1>>(histos, [&h_key, lep_flav](const std::shared_ptr<TH1>& H) {
            std::string h_name = H->GetName();
            return (!h_key.empty() &&
                    (h_name.rfind(h_key) == std::string::npos || h_name.substr(h_name.rfind(h_key), h_key.size()) != h_key)) ||
                   h_name.find(flavour_to_str(lep_flav)) == std::string::npos;
        });
        if (histos.empty()) {
            std::cerr << "FakeWeighter::filter_variable(): Failed to load histos " << h_key << " for " << flavour_to_str(lep_flav) << " in "
                      << std::endl;
            for (auto H : in_histos) { std::cout << " -- " << H->GetName() << std::endl; }
        }
        return histos;
    }
    bool FakeWeighter::load_histos(unsigned int lep_flav, std::shared_ptr<TFile> f, const std::string& ff_histo,
                                   const std::string& ff_region, const std::string& proc_histo, const std::string& proc_region,
                                   std::function<bool(BaseFakeTree&, size_t)> sel_func) {
        return load_histos(lep_flav, f, ff_histo, ff_region, proc_histo, proc_region, "", "", sel_func);
    }
    bool FakeWeighter::load_histos(unsigned int lep_flav, std::shared_ptr<TFile> f, const std::string& ff_histo,
                                   const std::string& ff_region, const std::string& proc_histo, const std::string& proc_region,
                                   const std::string& sf_histo, const std::string& sf_region,
                                   std::function<bool(BaseFakeTree&, size_t)> sel_func) {
        if (!f) return false;

        change_bit_mask(lepton_mask() | (lep_flav & LepClass::LepFlav));
        /// Load the fake and scale-factors from a static map to keep the used memory at minimal level
        std::vector<std::shared_ptr<TH1>> fake_factors =
            filter_variable(ProcessFractionHolder::instance()->get_fake_factors(f, ff_region), ff_histo, lep_flav);
        /// Too much has been kicked
        if (fake_factors.empty()) return false;

        std::vector<std::shared_ptr<TH1>> scale_factors;
        if (!sf_region.empty()) {
            /// Need to check with Marian the top key
            scale_factors = filter_variable(ProcessFractionHolder::instance()->get_scale_factors(f, sf_region), sf_histo, lep_flav);
            if (scale_factors.empty()) return false;
        }
        std::vector<std::shared_ptr<TH1>> proc_fractions;
        if (!proc_region.empty()) {
            proc_fractions =
                filter_variable(ProcessFractionHolder::instance()->get_process_fractions(f, proc_region), proc_histo, lep_flav);
            if (proc_fractions.empty()) return false;
        }
        int start = XAMPP::min_bit(LepClass::Fake);
        int end = XAMPP::max_bit(LepClass::Fake);
        bool added_one = false;
        for (int i = start; i <= end; ++i) {
            /// Skip ill defined bits from the beginning
            int truth_bit = 1 << i;
            if (!type_flavour_defined((lep_flav & LepClass::LepFlav) | truth_bit)) continue;

            std::shared_ptr<TH1> ff_histo = find_truth_histo(fake_factors, truth_bit);
            std::shared_ptr<TH1> proc_histo = find_truth_histo(proc_fractions, truth_bit);
            std::shared_ptr<TH1> sf_histo = find_truth_histo(scale_factors, truth_bit);
            if (!ff_histo) {
                std::cout << "FakeWeighter::load_histos(): Could not load find truth-type " << type_to_str(truth_bit);
                std::cout << " for " << flavour_to_str(lep_flav) << std::endl;
                continue;
            }
            if (!proc_region.empty() && !proc_histo) {
                std::cout << "FakeWeighter::load_histos(): Failed to find process fraction for truth-type " << type_to_str(truth_bit);
                std::cout << " and " << flavour_to_str(lep_flav) << std::endl;
                continue;
            }
            if (!sf_region.empty() && !sf_histo) {
                std::cout << "FakeWeighter::load_histos(): Failed to find scale factor for truth-type " << type_to_str(truth_bit);
                std::cout << " and " << flavour_to_str(lep_flav) << std::endl;
                return false;
            }
            m_ff_histos.push_back(std::make_shared<FakeHistoReader>(lep_flav | truth_bit, ff_histo, proc_histo, sf_histo, sel_func));
            m_ff_histos.back()->set_associated_file(f);
            added_one = true;
        }
        if (XAMPP::count<std::shared_ptr<FakeHistoReader>>(
                m_ff_histos, [](const std::shared_ptr<FakeHistoReader>& a) { return !a->associated_file(); })) {
            std::cerr << "FakeWeighter::load_histos(): Somehow the file association failed" << std::endl;
            return false;
        }
        std::sort(m_ff_histos.begin(), m_ff_histos.end(),
                  [](const std::shared_ptr<FakeHistoReader>& a, const std::shared_ptr<FakeHistoReader>& b) {
                      if (a->lepton_mask() != b->lepton_mask()) return a->lepton_mask() < b->lepton_mask();
                      return std::string(a->associated_file()->GetName()) < std::string(b->associated_file()->GetName());
                  });
        return added_one;
    }
    unsigned int FakeWeighter::get_syst_mask() const {
        unsigned int mask = (lepton_mask() & LepClass::LepFlav);
        for (auto& ff : m_ff_histos) {
            /// Truth type
            mask |= ff->lepton_mask() & LepClass::Incl;
            if (ff->ff_histo()) mask |= LepClass::SystFF;
            if (ff->sf_histo()) mask |= LepClass::SystSF;
            if (ff->proc_histo()) mask |= LepClass::SystProcFrac;
        }
        return mask;
    }
    std::shared_ptr<TH1> FakeWeighter::find_truth_histo(const std::vector<std::shared_ptr<TH1>>& vec, unsigned int truth_bit) const {
        std::vector<std::shared_ptr<TH1>>::const_iterator itr =
            std::find_if(vec.begin(), vec.end(), [truth_bit](const std::shared_ptr<TH1>& H) {
                std::string h_name = H->GetName();
                return h_name.find(type_to_str(truth_bit)) != std::string::npos;
            });
        if (itr != vec.end()) return *itr;
        return std::shared_ptr<TH1>();
    }
    //#########################################
    //            FakeClosureHisto            #
    //#########################################
    std::shared_ptr<FakeWeighter> FakeClosureHisto::CR1_weighter() const { return m_ff_weight_CR1; }
    std::shared_ptr<FakeWeighter> FakeClosureHisto::CR2_weighter() const { return m_ff_weight_CR2; }
    void FakeClosureHisto::set_CR1_weighter(std::shared_ptr<FakeWeighter> W) { m_ff_weight_CR1 = W; }
    void FakeClosureHisto::set_CR2_weighter(std::shared_ptr<FakeWeighter> W) { m_ff_weight_CR2 = W; }
    Plot<TH1D>& FakeClosureHisto::SR_plot() { return m_sr_histo; }
    Plot<TH1D>& FakeClosureHisto::CR_plot() { return m_cr_histo; }
    Plot<TH1D>& FakeClosureHisto::CR1_component() { return m_cr1_histo; }
    Plot<TH1D>& FakeClosureHisto::CR2_component() { return m_cr2_histo; }
    int FakeClosureHisto::event_selection(FakeClosureTree& t) const {
        if (m_cr1_selection(t)) return LepClass::CR1;
        if (m_cr2_selection(t)) return LepClass::CR2;
        return LepClass::SR;
    }

    FakeClosureHisto::FakeClosureHisto(std::shared_ptr<TH1> histo, unsigned int lep_mask, Sample<FakeClosureTree> sample,
                                       Selection<FakeClosureTree> sr_selection, Selection<FakeClosureTree> cr1_selection,
                                       Selection<FakeClosureTree> cr2_selection, const std::string& add_name) :
        IClosureFiller(histo, lep_mask, sr_selection, sample.getName()),
        m_cr1_selection(cr1_selection),
        m_cr2_selection(cr2_selection),
        m_ff_weight_CR1(std::make_shared<FakeWeighter>(lep_mask | LepClass::LightLep | LepClass::CR1, 1, 1, cr1_selection)),
        m_ff_weight_CR2(std::make_shared<FakeWeighter>(lep_mask | LepClass::LightLep | LepClass::CR2, 2, -1, cr2_selection)),
        m_add_name(add_name),
        m_sr_histo(sample, sr_selection, make_filler(LepClass::SR), overflow_pull, "SR", "PL",
                   PlotFormat().Color(kOrange).ExtraDrawOpts("HIST E")),
        m_cr1_histo(sample, cr1_selection, make_filler(LepClass::CR1), overflow_pull, "CR1", "PL",
                    PlotFormat().Color(kBlue).ExtraDrawOpts("HIST E")),
        m_cr2_histo(sample, cr2_selection, make_filler(LepClass::CR2 | LepClass::AbsCR2), overflow_pull, "CR2", "PL",
                    PlotFormat().Color(kRed).ExtraDrawOpts("HIST E")),
        m_cr_histo(sample, cr1_selection || cr2_selection, make_filler(LepClass::CR1 | LepClass::CR2), overflow_pull, "CR1-CR2", "PL",
                   PlotFormat().Color(kGreen + 1).ExtraDrawOpts("HIST E")) {}

    ClosureFiller1D FakeClosureHisto::make_filler(unsigned int reweight) const {
        std::function<float(FakeClosureTree&, size_t)> func = make_reader(get_histo()->GetXaxis());
        std::string axis_title = get_histo()->GetXaxis()->GetTitle();

        bool only_first = !is_lepton_variable(get_histo()->GetXaxis());
        if (reweight & (LepClass::CR1 | LepClass::CR2)) {
            if (only_first)
                return ClosureFiller1D(name() + "_" + (reweight & LepClass::AbsCR2 ? "abs" : "") + (reweight & LepClass::CR1 ? "CR1" : "") +
                                           (reweight & LepClass::CR1 ? "CR2" : ""),
                                       [reweight, func, this](TH1D* h, FakeClosureTree& t) {
                                           unsigned int n_comb = 0;
                                           if (reweight & LepClass::CR1 && m_ff_weight_CR1->get_selection()(t)) {
                                               n_comb = t.n_permutations(m_ff_weight_CR1->lepton_mask());
                                           } else if (reweight & LepClass::CR2 && m_ff_weight_CR2->get_selection()(t)) {
                                               n_comb = t.n_permutations(m_ff_weight_CR2->lepton_mask());
                                           }
                                           for (int i = n_comb - 1; i >= 0; --i) {
                                               float ff_w = 1.;
                                               if (reweight & LepClass::CR1) ff_w *= m_ff_weight_CR1->get_weight(t, i);
                                               if (reweight & LepClass::CR2) ff_w *= m_ff_weight_CR2->get_weight(t, i);
                                               if (reweight & LepClass::AbsCR2) ff_w = std::fabs(ff_w);
                                               h->Fill(func(t, i), ff_w * t.eventWeight() / n_comb);
                                           }
                                       },
                                       dynamic_cast<TH1D*>(get_histo().get()));

            return ClosureFiller1D(name() + "_" + (reweight & LepClass::AbsCR2 ? "abs" : "") + (reweight & LepClass::CR1 ? "CR1" : "") +
                                       (reweight & LepClass::CR1 ? "CR2" : ""),
                                   container_size<FakeClosureTree>(), lepton_selection<FakeClosureTree>(0),
                                   [reweight, func, this](TH1D* h, FakeClosureTree& t, size_t i) {
                                       float ff_w = 1.;
                                       if (reweight & LepClass::CR1) ff_w *= m_ff_weight_CR1->get_weight(t);
                                       if (reweight & LepClass::CR2) ff_w *= m_ff_weight_CR2->get_weight(t);
                                       if (reweight & LepClass::AbsCR2) ff_w = std::fabs(ff_w);
                                       h->Fill(func(t, i), ff_w * t.eventWeight());
                                   },
                                   dynamic_cast<TH1D*>(get_histo().get()));
        }
        if (only_first)
            return ClosureFiller1D(name() + "_SR", [func](TH1D* h, FakeClosureTree& t) { h->Fill(func(t, 0), t.eventWeight()); },
                                   dynamic_cast<TH1D*>(get_histo().get()));

        return ClosureFiller1D(name() + "_SR", container_size<FakeClosureTree>(), lepton_selection<FakeClosureTree>(LepClass::Signal),
                               [func, this](TH1D* h, FakeClosureTree& t, size_t i) { h->Fill(func(t, i), t.eventWeight()); },
                               dynamic_cast<TH1D*>(get_histo().get()));
    }
    std::string FakeClosureHisto::name() const {
        std::string sel_name = XAMPP::EraseWhiteSpaces(get_selection().getName() + m_cr1_selection.getName() + m_cr2_selection.getName());
        sel_name = XAMPP::ReplaceExpInString(sel_name, "(", "");
        sel_name = XAMPP::ReplaceExpInString(sel_name, ")", "");
        sel_name = XAMPP::ReplaceExpInString(sel_name, " ", "_");
        while (sel_name.find("_") == 0) { sel_name = sel_name.substr(1, std::string::npos); }
        while (sel_name.rfind("_") == sel_name.size() - 1) { sel_name = sel_name.substr(0, sel_name.size() - 1); }
        return m_add_name + (m_add_name.empty() ? "" : "_") + sel_name;
    }
    PlotContent<TH1D> FakeClosureHisto::get_plot(const std::vector<std::string>& legend, CanvasOptions canvas_opt) {
        return PlotContent<TH1D>({SR_plot(), CR_plot(), CR1_component(), CR2_component()},
                                 {
                                     RatioEntry(1, 0, PlotUtils::defaultErrors),
                                     RatioEntry(2, 0, PlotUtils::defaultErrors),
                                     RatioEntry(3, 0, PlotUtils::defaultErrors),
                                 },
                                 legend, "Closure_" + name(), "AllClosurePlots", canvas_opt);
    }
    void FakeClosureHisto::write_file(TFile* file) {
        if (!file) {
            std::cerr << "FakeClosureHisto::write_file() --  No TFile was given " << std::endl;
            return;
        }
        SR_plot().populate();
        CR_plot().populate();
        std::string sel_name = XAMPP::EraseWhiteSpaces(get_selection().getName());
        sel_name = XAMPP::ReplaceExpInString(sel_name, "(", "");
        sel_name = XAMPP::ReplaceExpInString(sel_name, ")", "");
        sel_name = XAMPP::ReplaceExpInString(sel_name, " ", "_");

        std::shared_ptr<TH1> closure(PlotUtils::getRatio(CR_plot().getHisto(), SR_plot().getHisto()));
        TDirectory* out_dir = XAMPP::mkdir("NonClosure/" + sel_name + "/", file);
        out_dir->WriteObject(closure.get(), m_add_name.c_str());
    }
    //###################################
    //      EffiWeighter                #
    //###################################
    EffiWeighter::EffiWeighter() : IClosureFiller(nullptr, LepClass::LepFlav), m_efficiencies() {}
    bool EffiWeighter::load_eff_weights(std::shared_ptr<TFile> f, const std::string& ff_dir_ele, const std::string& ff_dir_muo,
                                        const std::string& ff_variable) {
        if (!f) return false;

        for (auto& flav : {LepClass::Ele, LepClass::Muo}) {
            std::vector<std::string> dir_content = XAMPP::recursive_ls(f.get());
            std::string ff_dir = flav == LepClass::Ele ? ff_dir_ele : ff_dir_muo;
            XAMPP::EraseFromVector<std::string>(dir_content, [&ff_dir, &ff_variable](const std::string& dir) {
                return dir.find(ff_dir) == std::string::npos ||
                       (!ff_variable.empty() && dir.rfind(ff_variable) != dir.size() - ff_variable.size());
            });
            if (ff_dir.empty()) return false;
            for (auto& type : {LepClass::Real, LepClass::HF, LepClass::CONV, LepClass::Other, LepClass::LF}) {
                if (!type_flavour_defined(flav | type)) continue;
                std::vector<std::string>::const_iterator itr =
                    std::find_if(dir_content.begin(), dir_content.end(), [&flav, &type](const std::string& str) {
                        std::string h_name = str.substr(str.rfind("/") + 1);
                        return h_name.find(flavour_to_str(flav)) != std::string::npos &&
                               h_name.find(type_to_str(type)) != std::string::npos;
                    });
                if (itr == dir_content.end()) {
                    std::cerr << "Failed to find " << flavour_to_str(flav) << " " << type_to_str(type) << " in directory " << ff_dir
                              << std::endl;
                    std::cout << dir_content << std::endl;
                    return false;
                }
                std::shared_ptr<TH1> h = XAMPP::fetch_from_file(*itr, f.get());
                if (!h) return false;
                /// Let's first try signal... Then we
                /// need to find out what's going to be propagated to loose and what's to tight
                m_efficiencies.push_back(std::make_shared<EffiWeightHistoReader>(LepClass::Signal | flav | type, h));
            }
        }
        return true;
    }
    float EffiWeighter::get_weight(FakeClosureTree& t) const {
        float W = 1;
        for (auto& i : t.get_sorted_indices()) {
            for (auto& ww : m_efficiencies) { W *= ww->get_weight(t, i); }
        }
        return W;
    }
    //####################################################
    //      FakeClosureSystematic                        #
    //####################################################
    FakeClosureSystematic::FakeClosureSystematic(Selection<FakeClosureTree> apply_in, const std::string& non_closure_for) :
        IClosureFiller(nullptr, LepClass::Incl | LepClass::LepFlav | LepClass::Baseline, apply_in, ""),
        m_ff_histos(),
        m_branch(nullptr),
        m_label(non_closure_for) {}
    std::vector<std::shared_ptr<FakeHistoReader>> FakeClosureSystematic::get_components() const { return m_ff_histos; }
    std::shared_ptr<NtupleBranch<double>> FakeClosureSystematic::get_branch() const { return m_branch; }
    void FakeClosureSystematic::set_branch(std::shared_ptr<NtupleBranch<double>> br) { m_branch = br; }

    std::string FakeClosureSystematic::label() const { return m_label; }
    double FakeClosureSystematic::get_weight(FakeClosureTree& t) const {
        double w = 1.;
        if (!get_selection()(t)) return w;
        unsigned int n_lep = t.get_n_lep(LepClass::Incl | LepClass::LepFlav | LepClass::Baseline);
        for (unsigned n = 0; n < n_lep; ++n) {
            for (auto& ff : m_ff_histos) { w += ff->get_weight(t, n); }
        }
        return w / n_lep;
    }
}  // namespace SUSY4L
