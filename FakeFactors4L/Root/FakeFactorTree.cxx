
// Please add your package name here
#include <FakeFactors4L/FakeFactorTree.h>
#include <FourMomUtils/xAODP4Helpers.h>

namespace SUSY4L {
    FakeFactorTree::FakeFactorTree(TTree* t) :
        BaseFakeTree(t),
        Ele_isConv("Ele_isConv", t, this),
        Ele_isHF("Ele_isHF", t, this),
        Ele_isLF("Ele_isLF", t, this),
        Ele_isReal("Ele_isReal", t, this),
        Muo_isHF("Muo_isHF", t, this),
        Muo_isLF("Muo_isLF", t, this),
        Muo_isReal("Muo_isReal", t, this),
        Tau_isEle("Tau_isEle", t, this),
        Tau_isGJ("Tau_isGJ", t, this),
        Tau_isHF("Tau_isHF", t, this),
        Tau_isLF("Tau_isLF", t, this),
        Tau_isReal("Tau_isReal", t, this),
        m_n_REAL_Muo(0),
        m_n_REAL_Ele(0),
        m_n_REAL_Tau1P(0),
        m_n_REAL_Tau3P(0),
        m_n_HF_Ele(0),
        m_n_HF_Muo(0),
        m_n_HF_Tau1P(0),
        m_n_HF_Tau3P(0),
        m_n_LF_Ele(0),
        m_n_LF_Muo(0),
        m_n_LF_Tau1P(0),
        m_n_LF_Tau3P(0),
        m_n_OTHER_Ele(0),
        m_n_OTHER_Muo(0),
        m_n_OTHER_Tau1P(0),
        m_n_OTHER_Tau3P(0),
        m_n_CONV_Ele(0),
        m_n_GJ_Tau1P(0),
        m_n_GJ_Tau3P(0),
        m_n_ELEC_Tau1P(0),
        m_n_ELEC_Tau3P(0),
        m_n_REAL_Muo_Sig(0),
        m_n_REAL_Ele_Sig(0),
        m_n_REAL_Tau1P_Sig(0),
        m_n_REAL_Tau3P_Sig(0),
        m_n_HF_Ele_Sig(0),
        m_n_HF_Muo_Sig(0),
        m_n_HF_Tau1P_Sig(0),
        m_n_HF_Tau3P_Sig(0),
        m_n_LF_Ele_Sig(0),
        m_n_LF_Muo_Sig(0),
        m_n_LF_Tau1P_Sig(0),
        m_n_LF_Tau3P_Sig(0),
        m_n_OTHER_Ele_Sig(0),
        m_n_OTHER_Muo_Sig(0),
        m_n_OTHER_Tau1P_Sig(0),
        m_n_OTHER_Tau3P_Sig(0),
        m_n_CONV_Ele_Sig(0),
        m_n_GJ_Tau1P_Sig(0),
        m_n_GJ_Tau3P_Sig(0),
        m_n_ELEC_Tau1P_Sig(0),
        m_n_ELEC_Tau3P_Sig(0) {
        if (t) getMissedBranches(t);
    }
    bool FakeFactorTree::cache_numbers() {
        if (!BaseFakeTree::cache_numbers()) return false;
        if (!isMC()) return true;
        /// Reset the counter
        m_n_HF_Ele = m_n_HF_Muo = 0;
        m_n_HF_Tau1P = m_n_HF_Tau3P = 0;

        m_n_LF_Ele = m_n_LF_Muo = 0;
        m_n_LF_Tau1P = m_n_LF_Tau3P = 0;

        m_n_OTHER_Muo = m_n_OTHER_Ele = 0;
        m_n_OTHER_Tau1P = m_n_OTHER_Tau3P = 0;

        m_n_REAL_Ele = m_n_REAL_Muo = 0;
        m_n_REAL_Tau1P = m_n_REAL_Tau3P = 0;

        m_n_CONV_Ele = 0;

        m_n_GJ_Tau1P = m_n_GJ_Tau3P = 0;
        m_n_ELEC_Tau1P = m_n_ELEC_Tau3P = 0;

        m_n_HF_Ele_Sig = m_n_HF_Muo_Sig = 0;
        m_n_HF_Tau1P_Sig = m_n_HF_Tau3P_Sig = 0;

        m_n_LF_Ele_Sig = m_n_LF_Muo_Sig = 0;
        m_n_LF_Tau1P_Sig = m_n_LF_Tau3P_Sig = 0;

        m_n_REAL_Ele_Sig = m_n_REAL_Muo_Sig = 0;
        m_n_REAL_Tau1P_Sig = m_n_REAL_Tau3P_Sig = 0;

        m_n_OTHER_Muo_Sig = m_n_OTHER_Ele_Sig = 0;
        m_n_OTHER_Tau1P_Sig = m_n_OTHER_Tau3P_Sig = 0;

        m_n_CONV_Ele_Sig = 0;

        m_n_GJ_Tau1P_Sig = m_n_GJ_Tau3P_Sig = 0;
        m_n_ELEC_Tau1P_Sig = m_n_ELEC_Tau3P_Sig = 0;

        for (unsigned int m = 0; m < Muo_size(); ++m) {
            unsigned int muo_mask = get_muo_bitmask(m);
            if (muo_mask & LepClass::LF) {
                ++m_n_LF_Muo;
                if (muo_mask & LepClass::Signal) ++m_n_LF_Muo_Sig;
            } else if (muo_mask & LepClass::Real) {
                ++m_n_REAL_Muo;
                if (muo_mask & LepClass::Signal) ++m_n_REAL_Muo_Sig;
            } else if (muo_mask & LepClass::HF) {
                ++m_n_HF_Muo;
                if (muo_mask & LepClass::Signal) ++m_n_HF_Muo_Sig;
            } else {
                ++m_n_OTHER_Muo;
                if (muo_mask & LepClass::Signal) ++m_n_OTHER_Muo_Sig;
            }
        }

        for (unsigned int e = 0; e < Ele_size(); ++e) {
            unsigned int ele_mask = get_ele_bitmask(e);

            if (ele_mask & LepClass::LF) {
                ++m_n_LF_Ele;
                if (ele_mask & LepClass::Signal) ++m_n_LF_Ele_Sig;
            } else if (ele_mask & LepClass::CONV) {
                ++m_n_CONV_Ele;
                if (ele_mask & LepClass::Signal) ++m_n_CONV_Ele_Sig;
            } else if (ele_mask & LepClass::Real) {
                ++m_n_REAL_Ele;
                if (ele_mask & LepClass::Signal) ++m_n_REAL_Ele_Sig;
            } else if (ele_mask & LepClass::HF) {
                ++m_n_HF_Ele;
                if (ele_mask & LepClass::Signal) ++m_n_HF_Ele_Sig;
            } else {
                ++m_n_OTHER_Ele;
                if (ele_mask & LepClass::Signal) ++m_n_OTHER_Ele_Sig;
            }
        }

        for (unsigned int t = 0; t < Tau_size(); ++t) {
            unsigned int tau_mask = get_tau_bitmask(t);
            if (tau_mask & LepClass::HF) {
                if (Tau_prongs(t) == 1) {
                    ++m_n_HF_Tau1P;
                } else {
                    ++m_n_HF_Tau3P;
                }
                if (!(tau_mask & LepClass::Signal)) continue;
                if (Tau_prongs(t) == 1) {
                    ++m_n_HF_Tau1P_Sig;
                } else {
                    ++m_n_HF_Tau3P_Sig;
                }
            } else if (tau_mask & LepClass::LF) {
                if (Tau_prongs(t) == 1) {
                    ++m_n_LF_Tau1P;
                } else {
                    ++m_n_LF_Tau3P;
                }
                if (!(tau_mask & LepClass::Signal)) continue;
                if (Tau_prongs(t) == 1) {
                    ++m_n_LF_Tau1P_Sig;
                } else {
                    ++m_n_LF_Tau3P_Sig;
                }
            } else if (tau_mask & LepClass::GJ) {
                if (Tau_prongs(t) == 1) {
                    ++m_n_GJ_Tau1P;
                } else {
                    ++m_n_GJ_Tau3P;
                }
                if (!(tau_mask & LepClass::Signal)) continue;
                if (Tau_prongs(t) == 1) {
                    ++m_n_GJ_Tau1P_Sig;
                } else {
                    ++m_n_GJ_Tau3P_Sig;
                }
            } else if (tau_mask & LepClass::Real) {
                if (Tau_prongs(t) == 1) {
                    ++m_n_REAL_Tau1P;
                } else {
                    ++m_n_REAL_Tau3P;
                }
                if (!(tau_mask & LepClass::Signal)) continue;
                if (Tau_prongs(t) == 1) {
                    ++m_n_REAL_Tau1P_Sig;
                } else {
                    ++m_n_REAL_Tau3P_Sig;
                }
            } else if (tau_mask & LepClass::ELEC) {
                if (Tau_prongs(t) == 1) {
                    ++m_n_ELEC_Tau1P;
                } else {
                    ++m_n_ELEC_Tau3P;
                }
                if (!(tau_mask & LepClass::Signal)) continue;
                if (Tau_prongs(t) == 1) {
                    ++m_n_ELEC_Tau1P_Sig;
                } else {
                    ++m_n_ELEC_Tau3P_Sig;
                }
            } else {
                if (Tau_prongs(t) == 1) {
                    ++m_n_OTHER_Tau1P;
                } else {
                    ++m_n_OTHER_Tau3P;
                }
                if (!(tau_mask & LepClass::Signal)) continue;
                if (Tau_prongs(t) == 1) {
                    ++m_n_OTHER_Tau1P_Sig;
                } else {
                    ++m_n_OTHER_Tau3P_Sig;
                }
            }
        }
        return true;
    }

    unsigned int FakeFactorTree::get_n_ele(unsigned int multip_mask) {
        cache_numbers();
        /// Inclusive final state
        if ((multip_mask & LepClass::Incl) == LepClass::Incl) { return BaseFakeTree::get_n_ele(multip_mask); }
        /// Baseline leptons
        if ((multip_mask & LepClass::Baseline) == LepClass::Baseline) {
            /// Truth origin and type
            unsigned int n = 0;
            if (multip_mask & LepClass::HF) n += m_n_HF_Ele;
            if (multip_mask & LepClass::LF) n += m_n_LF_Ele;
            if (multip_mask & LepClass::CONV) n += m_n_CONV_Ele;
            if (multip_mask & LepClass::Real) n += m_n_REAL_Ele;
            if (multip_mask & LepClass::Other) n += m_n_OTHER_Ele;
            return n;
        } else if (multip_mask & LepClass::Signal) {
            unsigned int n = 0;
            if (multip_mask & LepClass::HF) n += m_n_HF_Ele_Sig;
            if (multip_mask & LepClass::LF) n += m_n_LF_Ele_Sig;
            if (multip_mask & LepClass::CONV) n += m_n_CONV_Ele_Sig;
            if (multip_mask & LepClass::Real) n += m_n_REAL_Ele_Sig;
            if (multip_mask & LepClass::Other) n += m_n_OTHER_Ele_Sig;
            return n;
        } else if (multip_mask & LepClass::Loose) {
            unsigned int n = 0;
            if (multip_mask & LepClass::HF) n += (m_n_HF_Ele - m_n_HF_Ele_Sig);
            if (multip_mask & LepClass::LF) n += (m_n_LF_Ele - m_n_LF_Ele_Sig);
            if (multip_mask & LepClass::CONV) n += (m_n_CONV_Ele - m_n_CONV_Ele_Sig);
            if (multip_mask & LepClass::Real) n += (m_n_REAL_Ele - m_n_REAL_Ele_Sig);
            if (multip_mask & LepClass::Other) n += (m_n_OTHER_Ele - m_n_OTHER_Ele_Sig);
            return n;
        }
        return 0;
    }
    unsigned int FakeFactorTree::get_n_muo(unsigned int multip_mask) {
        cache_numbers();
        if ((multip_mask & LepClass::Incl) == LepClass::Incl) { return BaseFakeTree::get_n_muo(multip_mask); }
        /// Baseline leptons
        if ((multip_mask & LepClass::Baseline) == LepClass::Baseline) {
            /// Truth origin and type
            unsigned int n = 0;
            if (multip_mask & LepClass::HF) n += m_n_HF_Muo;
            if (multip_mask & LepClass::LF) n += m_n_LF_Muo;
            if (multip_mask & LepClass::Real) n += m_n_REAL_Muo;
            if (multip_mask & LepClass::Other) n += m_n_OTHER_Muo;
            return n;
        } else if (multip_mask & LepClass::Signal) {
            unsigned int n = 0;
            if (multip_mask & LepClass::HF) n += m_n_HF_Muo_Sig;
            if (multip_mask & LepClass::LF) n += m_n_LF_Muo_Sig;
            if (multip_mask & LepClass::Real) n += m_n_REAL_Muo_Sig;
            if (multip_mask & LepClass::Other) n += m_n_OTHER_Muo_Sig;
            return n;
        } else if (multip_mask & LepClass::Loose) {
            unsigned int n = 0;
            if (multip_mask & LepClass::HF) n += (m_n_HF_Muo - m_n_HF_Muo_Sig);
            if (multip_mask & LepClass::LF) n += (m_n_LF_Muo - m_n_LF_Muo_Sig);
            if (multip_mask & LepClass::Real) n += (m_n_REAL_Muo - m_n_REAL_Muo_Sig);
            if (multip_mask & LepClass::Other) n += (m_n_OTHER_Muo - m_n_OTHER_Muo_Sig);
            return n;
        }

        return 0;
    }
    unsigned int FakeFactorTree::get_n_tau(unsigned int multip_mask) {
        if ((multip_mask & LepClass::Incl) == LepClass::Incl) { return BaseFakeTree::get_n_tau(multip_mask); }
        unsigned int n = 0;
        if (multip_mask & LepClass::Signal) {
            if (multip_mask & LepClass::Tau1P && multip_mask & LepClass::Real) n += m_n_REAL_Tau1P_Sig;
            if (multip_mask & LepClass::Tau3P && multip_mask & LepClass::Real) n += m_n_REAL_Tau3P_Sig;
        }

        return n;
    }

    int FakeFactorTree::get_ele_bitmask(size_t i) {
        int bit_mask = 0;
        if (!isMC()) return BaseFakeTree::get_ele_bitmask(i);
        if (i < Ele_size()) {
            bit_mask |= LepClass::Ele;
            bit_mask |= !Ele_signal(i) ? LepClass::Loose : LepClass::Signal;
            if (!Ele_Isol(i)) bit_mask |= LepClass::FailVanillaIso;

            if (Ele_isReal(i))
                bit_mask |= LepClass::Real;
            else if (Ele_isLF(i))
                bit_mask |= LepClass::LF;
            else if (Ele_isConv(i))
                bit_mask |= LepClass::CONV;
            /// Dirty hack to assign all other to HF
            else if (Ele_isHF(i))
                bit_mask |= LepClass::HF;
            else
                bit_mask |= LepClass::Other;
        }
        return bit_mask;
    }
    int FakeFactorTree::get_muo_bitmask(size_t i) {
        int bit_mask = 0;
        if (!isMC()) return BaseFakeTree::get_muo_bitmask(i);
        if (i < Muo_size()) {
            bit_mask |= LepClass::Muo;
            bit_mask |= !Muo_signal(i) ? LepClass::Loose : LepClass::Signal;
            if (!Muo_Isol(i)) bit_mask |= LepClass::FailVanillaIso;
            if (Muo_isReal(i))
                bit_mask |= LepClass::Real;
            else if (Muo_isLF(i))
                bit_mask |= LepClass::LF;
            /// Dirty hack to assign all other to HF
            else if (Muo_isHF(i))
                bit_mask |= LepClass::HF;
            else
                bit_mask |= LepClass::Other;
        }
        return bit_mask;
    }
    int FakeFactorTree::get_tau_bitmask(size_t i) {
        int bit_mask = 0;
        if (!isMC()) return BaseFakeTree::get_tau_bitmask(i);
        if (i < Tau_size()) {
            bit_mask |= Tau_prongs(i) == 1 ? LepClass::Tau1P : LepClass::Tau3P;
            bit_mask |= !Tau_signal(i) ? LepClass::Loose : LepClass::Signal;
            if (Tau_isReal(i))
                bit_mask |= LepClass::Real;
            else if (Tau_isHF(i))
                bit_mask |= LepClass::HF;
            else if (Tau_isLF(i))
                bit_mask |= LepClass::LF;
            else if (Tau_isGJ(i))
                bit_mask |= LepClass::GJ;
            else if (Tau_isEle(i))
                bit_mask |= LepClass::ELEC;
            else
                bit_mask |= LepClass::Other;
        }
        return bit_mask;
    }
}  // namespace SUSY4L
