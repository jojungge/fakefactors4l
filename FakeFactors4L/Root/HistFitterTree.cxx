#include <FakeFactors4L/HistFitterTree.h>

namespace SUSY4L {
    HistFitterTree::HistFitterTree(TTree* t) :
        NtupleBranchMgr(t),
        EventNumber("EventNumber", t, this),
        GGM0Weight("GGM0Weight", t, this),
        GGM100Weight("GGM100Weight", t, this),
        GGM25Weight("GGM25Weight", t, this),
        GGM75Weight("GGM75Weight", t, this),
        Is2L2T("Is2L2T", t, this),
        Is2L2TZ("Is2L2TZ", t, this),
        Is2L2TZ_bonly("Is2L2TZ_bonly", t, this),
        Is2L2TZ_bveto("Is2L2TZ_bveto", t, this),
        Is2L2T_bonly("Is2L2T_bonly", t, this),
        Is2L2T_bveto("Is2L2T_bveto", t, this),
        Is2L2TnoZ("Is2L2TnoZ", t, this),
        Is2L2TnoZ_bonly("Is2L2TnoZ_bonly", t, this),
        Is2L2TnoZ_bveto("Is2L2TnoZ_bveto", t, this),
        Is3L1T("Is3L1T", t, this),
        Is3L1TZ("Is3L1TZ", t, this),
        Is3L1TZ_bonly("Is3L1TZ_bonly", t, this),
        Is3L1TZ_bveto("Is3L1TZ_bveto", t, this),
        Is3L1T_bonly("Is3L1T_bonly", t, this),
        Is3L1T_bveto("Is3L1T_bveto", t, this),
        Is3L1TnoZ("Is3L1TnoZ", t, this),
        Is3L1TnoZ_bonly("Is3L1TnoZ_bonly", t, this),
        Is3L1TnoZ_bveto("Is3L1TnoZ_bveto", t, this),
        Is4L("Is4L", t, this),
        Is4LZ("Is4LZ", t, this),
        Is4LZZ("Is4LZZ", t, this),
        Is4LZZ_bveto("Is4LZZ_bveto", t, this),
        Is4LZ_bonly("Is4LZ_bonly", t, this),
        Is4LZ_bveto("Is4LZ_bveto", t, this),
        Is4L_bveto("Is4L_bveto", t, this),
        Is4LnoZ("Is4LnoZ", t, this),
        Is4LnoZ_bonly("Is4LnoZ_bonly", t, this),
        Is4LnoZ_bveto("Is4LnoZ_bveto", t, this),
        Is5L("Is5L", t, this),
        IsCR_ZZ("IsCR_ZZ", t, this),
        IsCR_ttZ("IsCR_ttZ", t, this),
        IsReal2L2T("IsReal2L2T", t, this),
        IsReal2L2TZ("IsReal2L2TZ", t, this),
        IsReal2L2TZ_bonly("IsReal2L2TZ_bonly", t, this),
        IsReal2L2TZ_bveto("IsReal2L2TZ_bveto", t, this),
        IsReal2L2T_bonly("IsReal2L2T_bonly", t, this),
        IsReal2L2T_bveto("IsReal2L2T_bveto", t, this),
        IsReal2L2TnoZ("IsReal2L2TnoZ", t, this),
        IsReal2L2TnoZ_bonly("IsReal2L2TnoZ_bonly", t, this),
        IsReal2L2TnoZ_bveto("IsReal2L2TnoZ_bveto", t, this),
        IsReal3L1T("IsReal3L1T", t, this),
        IsReal3L1TZ("IsReal3L1TZ", t, this),
        IsReal3L1TZ_bonly("IsReal3L1TZ_bonly", t, this),
        IsReal3L1TZ_bveto("IsReal3L1TZ_bveto", t, this),
        IsReal3L1T_bonly("IsReal3L1T_bonly", t, this),
        IsReal3L1T_bveto("IsReal3L1T_bveto", t, this),
        IsReal3L1TnoZ("IsReal3L1TnoZ", t, this),
        IsReal3L1TnoZ_bonly("IsReal3L1TnoZ_bonly", t, this),
        IsReal3L1TnoZ_bveto("IsReal3L1TnoZ_bveto", t, this),
        IsReal4L("IsReal4L", t, this),
        IsReal4LZ("IsReal4LZ", t, this),
        IsReal4LZZ("IsReal4LZZ", t, this),
        IsReal4LZZ_bveto("IsReal4LZZ_bveto", t, this),
        IsReal4LZ_bonly("IsReal4LZ_bonly", t, this),
        IsReal4LZ_bveto("IsReal4LZ_bveto", t, this),
        IsReal4L_bveto("IsReal4L_bveto", t, this),
        IsReal4LnoZ("IsReal4LnoZ", t, this),
        IsReal4LnoZ_bonly("IsReal4LnoZ_bonly", t, this),
        IsReal4LnoZ_bveto("IsReal4LnoZ_bveto", t, this),
        IsReal5L("IsReal5L", t, this),
        IsRealCR_ZZ("IsRealCR_ZZ", t, this),
        IsRealCR_ttZ("IsRealCR_ttZ", t, this),
        IsRealSR0A("IsRealSR0A", t, this),
        IsRealSR0A_bincl("IsRealSR0A_bincl", t, this),
        IsRealSR0B("IsRealSR0B", t, this),
        IsRealSR0B_bincl("IsRealSR0B_bincl", t, this),
        IsRealSR0C("IsRealSR0C", t, this),
        IsRealSR0C_bveto("IsRealSR0C_bveto", t, this),
        IsRealSR0D("IsRealSR0D", t, this),
        IsRealSR0E("IsRealSR0E", t, this),
        IsRealSR0F("IsRealSR0F", t, this),
        IsRealSR0G("IsRealSR0G", t, this),
        IsRealSR1A("IsRealSR1A", t, this),
        IsRealSR1A_bincl("IsRealSR1A_bincl", t, this),
        IsRealSR1A_bonly("IsRealSR1A_bonly", t, this),
        IsRealSR1B("IsRealSR1B", t, this),
        IsRealSR1C("IsRealSR1C", t, this),
        IsRealSR2A("IsRealSR2A", t, this),
        IsRealSR2A_bincl("IsRealSR2A_bincl", t, this),
        IsRealSR2B("IsRealSR2B", t, this),
        IsRealSR2C("IsRealSR2C", t, this),
        IsRealVR0("IsRealVR0", t, this),
        IsRealVR0ZZ("IsRealVR0ZZ", t, this),
        // IsRealVR0Z_bincl                                                ("IsRealVR0Z_bincl", t, this),
        IsRealVR0_bincl("IsRealVR0_bincl", t, this),
        IsRealVR0_ttZ("IsRealVR0_ttZ", t, this),
        IsRealVR1("IsRealVR1", t, this),
        IsRealVR1Z("IsRealVR1Z", t, this),
        IsRealVR1Z_bincl("IsRealVR1Z_bincl", t, this),
        IsRealVR1_bincl("IsRealVR1_bincl", t, this),
        IsRealVR2("IsRealVR2", t, this),
        IsRealVR2Z("IsRealVR2Z", t, this),
        IsRealVR2Z_bincl("IsRealVR2Z_bincl", t, this),
        IsRealVR2_bincl("IsRealVR2_bincl", t, this),
        IsSR0A("IsSR0A", t, this),
        IsSR0A_bincl("IsSR0A_bincl", t, this),
        IsSR0B("IsSR0B", t, this),
        IsSR0B_bincl("IsSR0B_bincl", t, this),
        IsSR0C("IsSR0C", t, this),
        IsSR0C_bveto("IsSR0C_bveto", t, this),
        IsSR0D("IsSR0D", t, this),
        IsSR0E("IsSR0E", t, this),
        IsSR0F("IsSR0F", t, this),
        IsSR0G("IsSR0G", t, this),
        IsSR1A("IsSR1A", t, this),
        IsSR1A_bincl("IsSR1A_bincl", t, this),
        IsSR1A_bonly("IsSR1A_bonly", t, this),
        IsSR1B("IsSR1B", t, this),
        IsSR1C("IsSR1C", t, this),
        IsSR2A("IsSR2A", t, this),
        IsSR2A_bincl("IsSR2A_bincl", t, this),
        IsSR2B("IsSR2B", t, this),
        IsSR2C("IsSR2C", t, this),
        IsVR0("IsVR0", t, this),
        IsVR0ZZ("IsVR0ZZ", t, this),
        IsVR0ZZ_bincl("IsVR0ZZ_bincl", t, this),
        IsVR0_bincl("IsVR0_bincl", t, this),
        IsVR0_ttZ("IsVR0_ttZ", t, this),
        IsVR1("IsVR1", t, this),
        IsVR1Z("IsVR1Z", t, this),
        IsVR1Z_bincl("IsVR1Z_bincl", t, this),
        IsVR1_bincl("IsVR1_bincl", t, this),
        IsVR2("IsVR2", t, this),
        IsVR2Z("IsVR2Z", t, this),
        IsVR2Z_bincl("IsVR2Z_bincl", t, this),
        IsVR2_bincl("IsVR2_bincl", t, this),

        LumiBlock("LumiBlock", t, this),
        Meff("Meff", t, this),
        Met("Met", t, this),
        Met_phi("Met_phi", t, this),
        Mll_Z1("Mll_Z1", t, this),
        Mll_Z2("Mll_Z2", t, this),
        Mt_LepMax("Mt_LepMax", t, this),
        Mt_LepMin("Mt_LepMin", t, this),
        NJets("NJets", t, this),
        NBJets("NBJets", t, this),
        NLooseElectrons("NLooseElectrons", t, this),
        NLooseLep("NLooseLep", t, this),
        NLooseMuons("NLooseMuons", t, this),
        NLooseTaus("NLooseTaus", t, this),
        NSignalElec("NSignalElec", t, this),
        NSignalLep("NSignalLep", t, this),
        NSignalMuon("NSignalMuon", t, this),
        NSignalTaus("NSignalTaus", t, this),
        NormWeight("NormWeight", t, this),
        SigEle_IFFType("SigEle_IFFType", t, this),
        SigEle_d0Sig("SigEle_d0Sig", t, this),
        SigEle_eta("SigEle_eta", t, this),
        SigEle_phi("SigEle_phi", t, this),
        SigEle_pt("SigEle_pt", t, this),
        SigEle_q("SigEle_q", t, this),
        SigEle_z0Sin("SigEle_z0Sin", t, this),
        SigJet_bjet("SigJet_bjet", t, this),
        SigJet_eta("SigJet_eta", t, this),
        SigJet_phi("SigJet_phi", t, this),
        SigJet_pt("SigJet_pt", t, this),
        SigMuo_IFFType("SigMuo_IFFType", t, this),
        SigMuo_d0Sig("SigMuo_d0Sig", t, this),
        SigMuo_eta("SigMuo_eta", t, this),
        SigMuo_phi("SigMuo_phi", t, this),
        SigMuo_pt("SigMuo_pt", t, this),
        SigMuo_q("SigMuo_q", t, this),
        SigMuo_z0Sin("SigMuo_z0Sin", t, this),
        SigTau_eta("SigTau_eta", t, this),
        SigTau_phi("SigTau_phi", t, this),
        SigTau_prongs("SigTau_prongs", t, this),
        SigTau_pt("SigTau_pt", t, this),
        SigTau_q("SigTau_q", t, this),
        runNumber("runNumber", t, this),
        xSection_Uncertainty("xSection_Uncertainty", t, this),
        isMC("isMC", t, this),
        isCR1("isCR1", t, this),
        m_FFWeight("FakeWeight", t, this),
        m_ff_syst(),
        m_ff_syst_closure() {
        if (t) getMissedBranches(t);
    }
    void HistFitterTree::register_systematics(std::shared_ptr<FakeWeighter> weighter) {
        unsigned int syst_mask = weighter->get_syst_mask();
        const unsigned int first_sys_bit = XAMPP::min_bit(syst_mask & LepClass::SystComp);
        const unsigned int last_sys_bit = XAMPP::max_bit(syst_mask & LepClass::SystComp);
        /// Fake-factor component
        for (unsigned int sys_comp = first_sys_bit; sys_comp <= last_sys_bit; ++sys_comp) {
            /// Somehow the SF/ ProcFrac are not defined
            if (!(syst_mask & 1 << sys_comp)) continue;
            /// Now we need to ask for the truth_component
            const unsigned int first_truth_bit = XAMPP::min_bit(syst_mask & LepClass::Fake);
            const unsigned int last_truth_bit = XAMPP::max_bit(syst_mask & LepClass::Fake);
            for (unsigned int truth_bit = first_truth_bit; truth_bit <= last_truth_bit; ++truth_bit) {
                if (!(syst_mask & 1 << truth_bit)) continue;
                for (int var : std::vector<int>{0, LepClass::SystUp}) {
                    /// Define the systematic one up and one down
                    int syst = var | 1 << truth_bit | 1 << sys_comp;
                    if (m_ff_syst.find(syst) == m_ff_syst.end()) {
                        std::string sys_name = syst_to_str(syst) + "_" + type_to_str(syst) + (var ? "__1UP" : "__1DN");
                        std::cout << "HistFitterTree::register_systematics() -- INFO:  New systematic found: " << sys_name << std::endl;
                        m_ff_syst.insert(std::pair<unsigned int, NtupleBranch<Double_t>>(
                            syst, NtupleBranch<Double_t>("FakeWeight__" + sys_name, getTree(), this)));
                    }
                }
            }
        }
    }
    void HistFitterTree::fill_ff_weight(std::shared_ptr<FakeWeighter> weighter, FakeTreeWriter& t, unsigned int permut) {
        if (weighter) {
            double nominal_w = weighter->get_weight(t, permut);
            if (std::fabs(nominal_w) > 5.) {
                XAMPP::PrintHeadLine("HistFitterTree::dump_event: " + std::to_string(t.currentEntry()));
                for (auto& l : t.write_particles(LepClass::LepFlav, permut)) {
                    unsigned int l_th_mask = t.get_lepton_bitmask(l);
                    std::cout << " -- " << l << " " << std::setw(15) << flavour_to_str(l_th_mask);
                    std::cout << " pT: " << std::setw(6) << std::setprecision(4) << t.get_pt(l, LepClass::LepFlav) << " GeV";
                    std::cout << " eta: " << std::setw(6) << std::setprecision(3) << t.get_eta(l, LepClass::LepFlav);
                    std::cout << " phi: " << std::setw(6) << std::setprecision(3) << t.get_phi(l, LepClass::LepFlav);
                    if (l_th_mask & LepClass::LightLep) {
                        std::cout << " dR:  " << std::setw(4) << std::setprecision(3) << t.get_dR_minLep(l);
                    } else {
                        std::cout << " dR: " << std::setw(4) << "--";
                    }
                    if (l_th_mask & LepClass::Loose) {
                        std::cout << "         FFs: ";
                        for (auto& comp : weighter->get_components()) {
                            double comp_w = comp->get_weight(t, l);
                            std::cout << comp->lepton_type() << " (" << std::setprecision(6) << comp_w << ") ";
                        }
                    }
                    std::cout << std::endl;
                }
                std::cout << std::endl;
                for (auto& j : t.write_particles(LepClass::Jets, permut)) {
                    std::cout << " -- " << j << std::setw(15) << (t.SigJet_bjet(j) ? "b-jet " : "jet");
                    std::cout << " pT: " << std::setw(6) << std::setprecision(4) << t.SigJet_pt(j) << " GeV";
                    std::cout << " eta: " << std::setw(6) << std::setprecision(3) << t.SigJet_eta(j);
                    std::cout << " phi: " << std::setw(6) << std::setprecision(3) << t.SigJet_phi(j);
                    std::cout << std::endl;
                }
                std::cout << std::endl << std::setprecision(5);
                std::cout << "Met: " << t.Met() << " Meff: " << t.baseline_meff(permut);
                std::cout << " Mll_Z1: " << t.Mll_Z1() << "  Mll_Z2: " << t.Mll_Z2();
                std::cout << " n-bjets: " << t.num_bjets(permut) << "\t CR1: " << (t.get_selection() & LepClass::CR1 ? "si" : "no");
                std::cout << " b-pick-up "
                          << (t.b_selection_mode() & FakeTreeWriter::BVeto
                                  ? " b-veto"
                                  : (t.b_selection_mode() & FakeTreeWriter::BSelection ? " b-selection" : "inclusive"));
                std::cout << std::endl;

                std::cout << "  WARNING: Stupid large fake weight found " << nominal_w << std::endl << std::endl;

            } else if (nominal_w == 0.) {
                // t.dump_event();
                // std::cout << "  WARNING: The event is killed by the fake-factor method " << nominal_w << std::endl;
            } else if (std::fabs(nominal_w) == 1.) {
                t.dump_event();
                std::cout << "stonjek no weight applied" << std::endl;
            }
            m_FFWeight.set(nominal_w * (t.isMC() ? -1. : 1.));
            for (auto& syst_pair : m_ff_syst) {
                syst_pair.second.set(weighter->get_syst_weight(t, syst_pair.first, permut) * (t.isMC() ? -1 : 1));
            }
        } else {
            t.dump_event();
            throw std::runtime_error("No fake weighter has been assigned");
        }
    }
    void HistFitterTree::fill_non_closure(FakeTreeWriter& t) {
        std::vector<std::shared_ptr<FakeClosureSystematic>>::iterator first = m_ff_syst_closure.begin();
        while (first != m_ff_syst_closure.end()) {
            /// Find first the next element in the row. It could be either the non closure systematic passing
            /// the selection or the next branch in the field.
            std::vector<std::shared_ptr<FakeClosureSystematic>>::iterator next =
                std::find_if(first + 1, m_ff_syst_closure.end(), [&first, &t](const std::shared_ptr<FakeClosureSystematic>& candidate) {
                    return candidate->get_selection()(t) || candidate->get_branch() != (*first)->get_branch();
                });
            /// Check that the closure systematic is assigned uniquely foreach selection

            if ((*first)->get_selection()(t) && next != m_ff_syst_closure.end() && (*next)->get_selection()(t) &&
                (*first)->get_branch() == (*next)->get_branch()) {
                throw std::runtime_error(Form(
                    " Two times the same non-closure systematic %s is assigned from %s and %s... What happened here?",
                    (*first)->label().c_str(), (*first)->get_selection().getName().c_str(), (*next)->get_selection().getName().c_str()));
            }

            /// Fill the number in
            (*first)->get_branch()->set((*first)->get_weight(t));
            first = next;
        }
    }
    void HistFitterTree::add_closure_syst(std::shared_ptr<FakeClosureSystematic> syst) {
        std::vector<std::shared_ptr<FakeClosureSystematic>>::const_iterator itr =
            std::find_if(m_ff_syst_closure.begin(), m_ff_syst_closure.end(),
                         [syst](const std::shared_ptr<FakeClosureSystematic> known) { return known->label() == syst->label(); });
        if (itr != m_ff_syst_closure.end()) {
            syst->set_branch((*itr)->get_branch());
        } else {
            std::cout << " HistFitterTree::add_closure_syst() -- new non-closure systematic found targeting at " << syst->label() << "."
                      << std::endl;
            syst->set_branch(std::make_shared<NtupleBranch<double>>("FakeWeight_CLOSURE_" + syst->label(), getTree(), this));
        }
        m_ff_syst_closure += syst;
        std::sort(m_ff_syst_closure.begin(), m_ff_syst_closure.end(),
                  [](const std::shared_ptr<FakeClosureSystematic>& a, const std::shared_ptr<FakeClosureSystematic>& b) {
                      return a->label() < b->label();
                  });
    }

}  // namespace SUSY4L
