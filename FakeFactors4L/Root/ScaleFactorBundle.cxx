// Please add your package name here
#include <FakeFactors4L/ScaleFactorBundle.h>
using namespace XAMPP;
namespace SUSY4L {
    namespace {
        PlotPostProcessor<TH1D, TH1D> overflow_pull("PullOverFlow", [](std::shared_ptr<TH1D> H) {
            pull_overflow(H);
            return H;
        });
        static const std::vector<int> colors = {kBlack, kCyan - 2, kRed + 1, kGreen + 1, kOrange + 1, kViolet + 1, kBlue};
        static const std::vector<int> styles = {kFullTriangleUp,   kFullDiamond,        kFullFourTrianglesPlus, kFullStar,
                                                kFullTriangleDown, kFullThreeTriangles, kFullFourTrianglesX};
    }  // namespace
    //####################################
    //      IScaleFactorBundle
    //####################################
    unsigned int IScaleFactorBundle::m_non_signal_mc_types = LepClass::Incl;
    void IScaleFactorBundle::set_non_signal_mc(unsigned int flav_mask) { m_non_signal_mc_types = flav_mask & LepClass::Incl; }
    unsigned int IScaleFactorBundle::non_signal_mc() { return m_non_signal_mc_types; }

    IScaleFactorBundle::IScaleFactorBundle(std::shared_ptr<TH1> h_ref, SFCut selection, unsigned int lep_mask, const std::string& p_name,
                                           size_t tagged_lepton) :
        IBaseFakeTreeFiller(h_ref, lep_mask, p_name),
        m_selection(selection),
        m_tagged_lepton(tagged_lepton),
        m_disableTagging(tagged_lepton == dummy_idx) {}

    std::string IScaleFactorBundle::selection_str() const {
        std::string sel_name = XAMPP::EraseWhiteSpaces(m_selection.getName());
        for (std::string c : {"(", ")", "{", "}", "[", "]", "#"}) { sel_name = XAMPP::ReplaceExpInString(sel_name, c, ""); }
        sel_name = XAMPP::ReplaceExpInString(sel_name, " ", "__");
        while (sel_name.find("_") == 0) sel_name = sel_name.substr(1, std::string::npos);
        while (sel_name.rfind("_") == sel_name.size() - 1) sel_name = sel_name.substr(0, sel_name.size() - 1);
        return sel_name;
    }
    size_t IScaleFactorBundle::tagged_lepton() const { return m_tagged_lepton; }
    bool IScaleFactorBundle::tagging_disabled() const { return m_disableTagging; }

    const SFCut& IScaleFactorBundle::get_selection() const { return m_selection; }
    SFCut& IScaleFactorBundle::get_selection() { return m_selection; }
    void IScaleFactorBundle::write_file(TFile*) {}
    std::string IScaleFactorBundle::name() const {
        return lepton_flavour() + "_" + XAMPP::ReplaceExpInString(lepton_type(), ", ", "__") +
               (tagging_disabled() ? "_" : "_" + std::to_string(tagged_lepton()) + "_") + (IBaseFakeTreeFiller::name().empty() ? "" : "_") +
               IBaseFakeTreeFiller::name();
    }
    std::function<float(ScaleFactorTree&, size_t)> IScaleFactorBundle::make_sf_reader(TAxis* A) const {
        if (A) {
            std::string axis_title = A->GetTitle();
            if (axis_title == "#Sigmacos(#Delta#phi)") {
                return [this](ScaleFactorTree& t, size_t i) {
                    return std::cos(t.get_dphi_met(i, lepton_mask())) + std::cos(t.get_dphi_met(0, LepClass::Muo));
                };
            } else if (axis_title == "q_{tag}#timesq_{probe}") {
                return [](ScaleFactorTree& t, size_t) { return t.get_charge(0, LepClass::LepFlav) * t.get_charge(1, LepClass::LepFlav); };
            }
        }
        return make_reader<ScaleFactorTree>(A);
    }
    //####################################
    //      TagProbeProcFracBundle
    //####################################
    TagProbeProcFracBundle::TagProbeProcFracBundle(std::shared_ptr<TH1> h_ref, Sample<ScaleFactorTree> MCsample, SFCut selection,
                                                   unsigned int lep_mask, const std::string& p_name, size_t tagged_lepton) :
        IScaleFactorBundle(h_ref, selection, lep_mask, p_name, tagged_lepton),
        m_break_down() {
        lep_mask &= ~LepClass::Baseline;

        int start = min_bit(lep_mask & LepClass::Incl);
        int end = max_bit(lep_mask & LepClass::Incl);
        m_break_down.reserve(end - start + 1);
        m_break_down.emplace_back(h_ref, MCsample, selection, lep_mask, p_name, tagged_lepton, overflow_pull);

        for (int i = start; i <= end; ++i) {
            /// Skip conversions
            int truth_bit = 1 << i;
            if (!type_flavour_defined((lep_mask & LepClass::LepFlav) | truth_bit) || !(lep_mask & truth_bit)) continue;
            m_break_down.emplace_back(h_ref, MCsample, selection, (lep_mask & ~LepClass::Incl) | truth_bit, p_name, tagged_lepton,
                                      overflow_pull);
        }
    }
    PlotContent<TH1D> TagProbeProcFracBundle::get_plot(CanvasOptions canvas_opt) {
        std::vector<RatioEntry> ratios;
        std::vector<Plot<TH1D>> plots;
        for (unsigned int i = 0; i < m_break_down.size(); ++i) {
            if ((lepton_mask() & LepClass::Baseline) == LepClass::Baseline) {
                plots.emplace_back(m_break_down[i].get_baseline());
            } else if (lepton_mask() & LepClass::Signal) {
                plots.emplace_back(m_break_down[i].get_signal());
            } else if (lepton_mask() & LepClass::Loose) {
                plots.emplace_back(m_break_down[i].get_loose());
            } else
                continue;
            plots[i].setLegendTitle(i > 0 ? m_break_down[i].lepton_type() : "All");
            plots[i].plotFormat().Color(colors[i]).MarkerStyle(styles[i]).MarkerSize(1.5);
        }
        for (unsigned int i = 1; i < m_break_down.size(); ++i) { ratios.emplace_back(i, 0, PlotUtils::efficiencyErrors); }

        return PlotContent<TH1D>(plots, ratios, {lepton_flavour() + ", " + lepton_quality(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "proc_frac_" + lepton_quality(), "AllScaleFactors", canvas_opt);
    }
    //####################################
    //      ScaleFactorBundle
    //####################################
    ScaleFactorBundle::ScaleFactorBundle(std::shared_ptr<TH1> h_ref, Sample<ScaleFactorTree> MCsample, Sample<ScaleFactorTree> Datasample,
                                         SFCut selection, unsigned int lep_mask, const std::string& p_name, size_t tagged_lepton) :
        IScaleFactorBundle(h_ref, selection, lep_mask, p_name, tagged_lepton),
        m_mc_ff_interest(h_ref, MCsample, selection, lep_mask, p_name, tagged_lepton, overflow_pull),
        m_mc_ff_other(h_ref, MCsample, selection, lep_mask ^ non_signal_mc(), p_name, tagged_lepton, overflow_pull),
        m_data_raw(h_ref, Datasample, selection, lep_mask | LepClass::Incl, p_name, tagged_lepton, overflow_pull),
        m_data(m_data_raw, m_mc_ff_other) {}

    TagProbeFakeFactor<TH1D>& ScaleFactorBundle::get_TP_mc() { return m_mc_ff_interest; }
    TagProbeFakeFactor<TH1D>& ScaleFactorBundle::get_TP_mc_other() { return m_mc_ff_other; }
    TagProbeFakeFactor<TH1D>& ScaleFactorBundle::get_TP_data() { return m_data; }
    TagProbeFakeFactor<TH1D>& ScaleFactorBundle::get_TP_data_raw() { return m_data_raw; }

    void ScaleFactorBundle::write_file(TFile* f) {
        if (!f) return;
        TDirectory* out_dir_sf = mkdir("SF/ScaleFactors/", f);
        TDirectory* out_dir_ff_mc = mkdir("SF/FakeFactorsMC/", f);
        TDirectory* out_dir_ff_data = mkdir("SF/FakeFactorsData/", f);

        Plot<TH1D> data_ff = m_data.make_ff();
        Plot<TH1D> mc_ff = m_mc_ff_interest.make_ff();
        data_ff.populate();
        mc_ff.populate();
        Plot<TH1> SF = PlotUtils::getRatio(data_ff, mc_ff);
        out_dir_sf->WriteObject(SF.getHisto(), name().c_str());
        out_dir_ff_mc->WriteObject(data_ff.getHisto(), name().c_str());
        out_dir_ff_data->WriteObject(mc_ff.getHisto(), name().c_str());
    }

    PlotContent<TH1D> ScaleFactorBundle::CreateSFPlot(CanvasOptions canvas_opt) {
        Plot<TH1D> data_FF = m_data.make_ff();
        Plot<TH1D> mc_FF = m_mc_ff_interest.make_ff();

        data_FF.setLegendTitle("Data");
        data_FF.setPlotFormat(PlotFormat().MarkerStyle(kFullCircle).MarkerSize(1.3).LineWidth(2.5).Color(kBlack).ExtraDrawOpts("E"));

        mc_FF.setLegendTitle("MC");
        mc_FF.setPlotFormat(PlotFormat().FillColor(kOrange + 1).FillStyle(1001).LineColor(kBlack).ExtraDrawOpts("HIST").MarkerStyle(kDot));
        mc_FF.setLegendOption("FL");
        return PlotContent<TH1D>({mc_FF, data_FF},
                                 {
                                     RatioEntry(1, 0, PlotUtils::defaultErrors),
                                 },
                                 {"SF, " + lepton_flavour() + ", " + lepton_type(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_sf", "AllScaleFactors", canvas_opt);
    }

    PlotContent<TH1D> ScaleFactorBundle::get_plot_mc(CanvasOptions canvas_opt) {
        return PlotContent<TH1D>({m_mc_ff_interest.get_baseline(), m_mc_ff_interest.get_signal(), m_mc_ff_interest.get_loose()},
                                 {
                                     RatioEntry(1, 2, PlotUtils::defaultErrors),
                                 },
                                 {"MC, " + lepton_flavour() + ", " + lepton_type(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_mc", "AllScaleFactors", canvas_opt);
    }
    PlotContent<TH1D> ScaleFactorBundle::get_plot_mc_bkg(CanvasOptions canvas_opt) {
        return PlotContent<TH1D>({m_mc_ff_other.get_baseline(), m_mc_ff_other.get_signal(), m_mc_ff_other.get_loose()},
                                 {
                                     RatioEntry(1, 2, PlotUtils::defaultErrors),
                                 },
                                 {"MC, " + lepton_flavour() + ",  not " + lepton_type(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_mc_bkg", "AllScaleFactors", canvas_opt);
    }

    PlotContent<TH1D> ScaleFactorBundle::get_plot_data(CanvasOptions canvas_opt) {
        return PlotContent<TH1D>({m_data.get_baseline(), m_data.get_signal(), m_data.get_loose()},
                                 {
                                     RatioEntry(1, 2, PlotUtils::defaultErrors),
                                 },
                                 {"Data, " + lepton_flavour() + ", " + lepton_type(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_data", "AllScaleFactors", canvas_opt);
    }
    PlotContent<TH1D> ScaleFactorBundle::get_data_mc_plot(CanvasOptions canvas_opt, unsigned int sel_mask) {
        ZllFakeFactor<TH1D> combined_mc(m_mc_ff_interest, m_mc_ff_other);
        std::vector<Plot<TH1D>> plots;
        if (sel_mask == LepClass::Baseline) {
            plots += combined_mc.get_baseline();
            plots += m_mc_ff_other.get_baseline();
            plots += m_data_raw.get_baseline();
        } else if (sel_mask == LepClass::Signal) {
            plots += combined_mc.get_signal();
            plots += m_mc_ff_other.get_signal();
            plots += m_data_raw.get_signal();
        } else if (sel_mask == LepClass::Loose) {
            plots += combined_mc.get_loose();
            plots += m_mc_ff_other.get_loose();
            plots += m_data_raw.get_loose();
        }
        plots[0].setLegendTitle("MC, " + lepton_type());
        plots[0].setPlotFormat(
            PlotFormat().FillColor(kOrange + 1).FillStyle(1001).LineColor(kBlack).ExtraDrawOpts("HIST").MarkerStyle(kDot));
        plots[0].setLegendOption("F");

        plots[1].setLegendTitle("MC, background");
        plots[1].setPlotFormat(PlotFormat().FillColor(kBlue - 6).FillStyle(1001).ExtraDrawOpts("HIST").MarkerStyle(kDot));
        plots[1].setLegendOption("F");

        plots[2].setLegendTitle("Data");
        plots[2].setPlotFormat(PlotFormat().MarkerStyle(kFullCircle).MarkerSize(1.3).LineWidth(2.5).Color(kBlack).ExtraDrawOpts("E"));
        return PlotContent<TH1D>(plots,
                                 {
                                     RatioEntry(2, 0, PlotUtils::defaultErrors),
                                 },
                                 {lepton_flavour() + ", " + selection_to_str(sel_mask), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_data_vs_mc_" + selection_to_str(sel_mask), "AllScaleFactors",
                                 canvas_opt);
    }
    //####################################
    //      ZllScaleFactorBundle
    //####################################

    ZllScaleFactorBundle::ZllScaleFactorBundle(std::shared_ptr<ScaleFactorBundle> Zee, std::shared_ptr<ScaleFactorBundle> Zmumu) :
        IScaleFactorBundle(Zee->get_histo(), Zee->get_selection() || Zmumu->get_selection(), Zee->lepton_mask() | Zmumu->lepton_mask(),
                           Zee->IBaseFakeTreeFiller::name()),
        m_Zee(Zee),
        m_Zmumu(Zmumu),
        m_mc_ff_interest(Zee->get_TP_mc(), Zmumu->get_TP_mc()),
        m_mc_ff_other(Zee->get_TP_mc_other(), Zmumu->get_TP_mc_other()),
        m_data(Zee->get_TP_data(), Zmumu->get_TP_data()) {}

    void ZllScaleFactorBundle::write_file(TFile* f) {
        if (!f) return;
        TDirectory* out_dir_sf = mkdir("SF/ScaleFactors/", f);
        TDirectory* out_dir_ff_mc = mkdir("SF/FakeFactorsMC/", f);
        TDirectory* out_dir_ff_data = mkdir("SF/FakeFactorsData/", f);

        Plot<TH1D> data_ff = m_data.make_ff();
        Plot<TH1D> mc_ff = m_mc_ff_interest.make_ff();
        data_ff.populate();
        mc_ff.populate();
        Plot<TH1> SF = PlotUtils::getRatio(data_ff, mc_ff);
        out_dir_sf->WriteObject(SF.getHisto(), name().c_str());
        out_dir_ff_mc->WriteObject(data_ff.getHisto(), name().c_str());
        out_dir_ff_data->WriteObject(mc_ff.getHisto(), name().c_str());
    }

    PlotContent<TH1D> ZllScaleFactorBundle::CreateSFPlot(CanvasOptions canvas_opt) {
        Plot<TH1D> data_FF = m_data.make_ff();
        Plot<TH1D> mc_FF = m_mc_ff_interest.make_ff();

        data_FF.setLegendTitle("Data");
        data_FF.setPlotFormat(PlotFormat().MarkerStyle(kFullCircle).MarkerSize(1.3).LineWidth(2.5).Color(kBlack).ExtraDrawOpts("E"));

        mc_FF.setLegendTitle("MC");
        mc_FF.setPlotFormat(PlotFormat().FillColor(kOrange + 1).FillStyle(1001).LineColor(kBlack).ExtraDrawOpts("HIST").MarkerStyle(kDot));
        mc_FF.setLegendOption("FL");
        return PlotContent<TH1D>({mc_FF, data_FF},
                                 {
                                     RatioEntry(1, 0, PlotUtils::defaultErrors),
                                 },
                                 {"SF, " + lepton_flavour() + ", " + lepton_type(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_sf", "AllScaleFactors", canvas_opt);
    }

    PlotContent<TH1D> ZllScaleFactorBundle::get_plot_mc(CanvasOptions canvas_opt) {
        return PlotContent<TH1D>({m_mc_ff_interest.get_baseline(), m_mc_ff_interest.get_signal(), m_mc_ff_interest.get_loose()},
                                 {
                                     RatioEntry(1, 2, PlotUtils::defaultErrors),
                                 },
                                 {"MC, " + lepton_flavour() + ", " + lepton_type(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_mc", "AllScaleFactors", canvas_opt);
    }
    PlotContent<TH1D> ZllScaleFactorBundle::get_plot_mc_bkg(CanvasOptions canvas_opt) {
        return PlotContent<TH1D>({m_mc_ff_other.get_baseline(), m_mc_ff_other.get_signal(), m_mc_ff_other.get_loose()},
                                 {
                                     RatioEntry(1, 2, PlotUtils::defaultErrors),
                                 },
                                 {"MC, " + lepton_flavour() + ",  not " + lepton_type(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_mc_bkg", "AllScaleFactors", canvas_opt);
    }

    PlotContent<TH1D> ZllScaleFactorBundle::get_plot_data(CanvasOptions canvas_opt) {
        return PlotContent<TH1D>({m_data.get_baseline(), m_data.get_signal(), m_data.get_loose()},
                                 {
                                     RatioEntry(1, 2, PlotUtils::defaultErrors),
                                 },
                                 {"Data, " + lepton_flavour() + ", " + lepton_type(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_data", "AllScaleFactors", canvas_opt);
    }

    //####################################
    //      ScaleFactorBundle2D
    //####################################
    ScaleFactorBundle2D::ScaleFactorBundle2D(std::shared_ptr<TH1> h_ref, Sample<ScaleFactorTree> MCsample,
                                             Sample<ScaleFactorTree> Datasample, SFCut selection, unsigned int lep_mask,
                                             const std::string& p_name, size_t tagged_lepton, PlotPostProcessor<TH2D, TH2D> post_proc) :
        IScaleFactorBundle(h_ref, selection, lep_mask, p_name, tagged_lepton),
        m_mc_ff_interest(h_ref, MCsample, selection, lep_mask, p_name, tagged_lepton, post_proc),
        m_mc_ff_other(h_ref, MCsample, selection, lep_mask ^ non_signal_mc(), p_name, tagged_lepton, post_proc),
        m_data_raw(h_ref, Datasample, selection, lep_mask | LepClass::Incl, p_name, tagged_lepton, post_proc),
        m_data(m_data_raw, m_mc_ff_other) {}

    void ScaleFactorBundle2D::write_file(TFile* f) {
        if (!f) return;
        TDirectory* out_dir_sf = mkdir("SF/ScaleFactors/", f);
        TDirectory* out_dir_ff_mc = mkdir("SF/FakeFactorsMC/", f);
        TDirectory* out_dir_ff_data = mkdir("SF/FakeFactorsData/", f);

        Plot<TH2D> data_ff = m_data.make_ff();
        Plot<TH2D> mc_ff = m_mc_ff_interest.make_ff();
        data_ff.populate();
        mc_ff.populate();
        Plot<TH1> SF = PlotUtils::getRatio(data_ff, mc_ff);
        out_dir_sf->WriteObject(SF.getHisto(), name().c_str());
        out_dir_ff_mc->WriteObject(data_ff.getHisto(), name().c_str());
        out_dir_ff_data->WriteObject(mc_ff.getHisto(), name().c_str());
    }

    PlotContent<TH2D> ScaleFactorBundle2D::CreateSFPlot(CanvasOptions canvas_opt) {
        Plot<TH2D> data_FF = m_data.make_ff();
        Plot<TH2D> mc_FF = m_mc_ff_interest.make_ff();

        data_FF.setLegendTitle("Data");
        mc_FF.setLegendTitle("MC");
        return PlotContent<TH2D>({mc_FF, data_FF},
                                 {
                                     RatioEntry(1, 0, PlotUtils::defaultErrors),
                                 },
                                 {"SF, " + lepton_flavour() + ", " + lepton_type(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_sf", "AllScaleFactors", canvas_opt);
    }

    PlotContent<TH2D> ScaleFactorBundle2D::get_plot_mc(CanvasOptions canvas_opt) {
        return PlotContent<TH2D>({m_mc_ff_interest.get_baseline(), m_mc_ff_interest.get_signal(), m_mc_ff_interest.get_loose()},
                                 {
                                     RatioEntry(1, 2, PlotUtils::defaultErrors),
                                 },
                                 {"MC, " + lepton_flavour() + ", " + lepton_type(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_mc", "AllScaleFactors", canvas_opt);
    }
    PlotContent<TH2D> ScaleFactorBundle2D::get_plot_data(CanvasOptions canvas_opt) {
        return PlotContent<TH2D>({m_data.get_baseline(), m_data.get_signal(), m_data.get_loose()},
                                 {
                                     RatioEntry(1, 2, PlotUtils::defaultErrors),
                                 },
                                 {"Data, " + lepton_flavour() + ", " + lepton_type(), get_selection().getTitle()},
                                 name() + "_" + selection_str() + "_data", "AllScaleFactors", canvas_opt);
    }

}  // namespace SUSY4L
