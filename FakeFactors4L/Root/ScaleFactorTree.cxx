#include <FakeFactors4L/ScaleFactorTree.h>
namespace SUSY4L {

    double ScaleFactorTree::m_lumi = 139.;
    void ScaleFactorTree::set_lumi(double l) { m_lumi = l; }
    ScaleFactorTree::ScaleFactorTree(TTree* t) :
        FakeFactorTree(t),
        EleMuTriggerSF("EleMuTriggerSF", t, this),
        EleTriggerSF("EleTriggerSF", t, this),
        MuTriggerSF("MuTriggerSF", t, this),

        EleWeight("EleWeight", t, this),
        MuoWeight("MuoWeight", t, this),
        TauWeight("TauWeight", t, this),
        JetWeight("JetWeight", t, this),
        IsCRttbar("IsCRttbar", t, this),
        IsCRttbar_tau("IsCRttbar_tau", t, this),
        IsZee("IsZee", t, this),
        IsZee_tau("IsZee_tau", t, this),
        IsZmumu("IsZmumu", t, this),
        IsZmumu_tau("IsZmumu_tau", t, this),
        IsWmunu("IsWmunu", t, this),
        IsWenu("IsWenu", t, this),
        Ele_isProbe("Ele_isProbe", t, this),
        Muo_isProbe("Muo_isProbe", t, this),
        Ele_q("Ele_q", t, this),
        Muo_q("Muo_q", t, this) {
        if (t) getMissedBranches(t);
    }
    double ScaleFactorTree::eventWeight() {
        return isMC() ? m_lumi * NormWeight() * prwWeight() * EleWeight() * MuoWeight() * TauWeight() * JetWeight() * EleMuTriggerSF() *
                            EleTriggerSF() * MuTriggerSF()
                      : 1.;
    }
    bool ScaleFactorTree::is_probe(size_t i, unsigned int multip_mask) {
        global_to_local(i, multip_mask);
        if ((multip_mask & LepClass::Ele) && i < Ele_size()) return Ele_isProbe(i);
        if ((multip_mask & LepClass::Muo) && i < Muo_size()) return Muo_isProbe(i);
        if ((multip_mask & LepClass::Tau) && i < Tau_size()) return true;
        std::cerr << "ScaleFactorTree::is_probe() --- WARNING: Index i " << i << " exceeds range or flavour bit not set "
                  << flavour_to_str(multip_mask) << " " << selection_to_str(multip_mask) << " " << type_to_str(multip_mask) << std::endl;
        return false;
    }
    int ScaleFactorTree::get_charge(size_t i, unsigned int multip_mask) {
        global_to_local(i, multip_mask);
        if ((multip_mask & LepClass::Ele) && i < Ele_size()) return Ele_q(i);
        if ((multip_mask & LepClass::Muo) && i < Muo_size()) return Muo_q(i);
        std::cerr << "ScaleFactorTree::get_charge() --- WARNING: Index i " << i << " exceeds range or flavour bit not set "
                  << flavour_to_str(multip_mask) << " " << selection_to_str(multip_mask) << " " << type_to_str(multip_mask) << std::endl;
        return 0;
    }

    size_t ScaleFactorTree::get_probe() {
        size_t probe = dummy_idx;
        for (size_t e = 0; e < Ele_size(); ++e) {
            if (Ele_isProbe(e)) {
                if (probe != dummy_idx) {
                    dump_event();
                    throw std::runtime_error("Stonjek.... Why are there two probes in the event?");
                }
                probe = e;
            }
        }
        for (size_t m = 0; m < Muo_size(); ++m) {
            if (Muo_isProbe(m)) {
                if (probe != dummy_idx) {
                    dump_event();
                    throw std::runtime_error("Stonjek.... Why are there two probes in the event?");
                }
                probe = m + Ele_size();
            }
        }

        return probe;
    }

}  // namespace SUSY4L
