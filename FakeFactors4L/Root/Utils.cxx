#include "FakeFactors4L/Utils.h"

namespace SUSY4L {
    bool type_flavour_defined(unsigned int bit_mask) {
        /// Make sure that the Conv muons do not enter the selection
        if ((bit_mask & LepClass::Muo) == (bit_mask & LepClass::LepFlav)) {
            /// It's exclusively asked for convsersion muons
            if ((bit_mask & LepClass::CONV) == (bit_mask & LepClass::Incl)) return false;
            if ((bit_mask & LepClass::GJ) == (bit_mask & LepClass::Incl)) return false;
            if ((bit_mask & LepClass::ELEC) == (bit_mask & LepClass::Incl)) return false;
        }
        if ((bit_mask & LepClass::Ele) == (bit_mask & LepClass::LepFlav)) {
            if ((bit_mask & LepClass::GJ) == (bit_mask & LepClass::Incl)) return false;
            if ((bit_mask & LepClass::ELEC) == (bit_mask & LepClass::Incl)) return false;
        }
        if ((bit_mask & LepClass::Tau) == (bit_mask & LepClass::LepFlav)) {
            if ((bit_mask & LepClass::CONV) == (bit_mask & LepClass::Incl)) return false;
        }
        return true;
    }
    int count_non_empty(PlotContent<TH1D>& h) {
        h.populateAll();
        unsigned int n = 0;

        for (auto& pl : h.getPlots())
            if (pl->GetEntries() > 0) ++n;
        return n;
    }

    std::string flavour_to_str(unsigned int bit_mask) {
        if ((bit_mask & LepClass::LepFlav) == LepClass::LepFlav) return "AnyLepton";
        if ((bit_mask & LepClass::LightLep) == LepClass::LightLep) return "LightLep";
        if ((bit_mask & LepClass::Tau) == LepClass::Tau) return "Tau";
        std::string flav;
        if (bit_mask & LepClass::Ele) flav += (flav.empty() ? "" : ", ") + std::string("Electron");
        if (bit_mask & LepClass::Muo) flav += (flav.empty() ? "" : ", ") + std::string("Muon");
        if (bit_mask & LepClass::Tau1P) flav += (flav.empty() ? "" : ", ") + std::string("Tau1P");
        if (bit_mask & LepClass::Tau3P) flav += (flav.empty() ? "" : ", ") + std::string("Tau3P");
        if (flav.empty()) return "UNKOWN_FLAVOUR";
        return flav;
    }
    std::string selection_to_str(unsigned int bit_mask) {
        if ((bit_mask & LepClass::Baseline) == LepClass::Baseline) return "Baseline";
        if (bit_mask & LepClass::FailVanillaIso) {
            if (bit_mask & LepClass::Signal) return "Vanilla Signal";
            if (bit_mask & LepClass::Loose) return "Vanilla Loose";
        }
        if (bit_mask & LepClass::Signal) return "Signal";
        if (bit_mask & LepClass::Loose) return "Loose";
        return "UNKOWN_SELECTION";
    }
    std::string syst_to_str(unsigned int bit_mask) {
        if (bit_mask & LepClass::SystFF) return "FAKE_STAT";
        if (bit_mask & LepClass::SystSF) return "SCALE_STAT";
        if (bit_mask & LepClass::SystProcFrac) return "PROC_STAT";
        return "UNKOWN_COMPONENT";
    }
    std::string type_to_str(unsigned int bit_mask) {
        if ((bit_mask & LepClass::Incl) == LepClass::Incl) return "Inclusive";
        if ((bit_mask & LepClass::Fake) == LepClass::Fake) return "Fake";
        std::string truth_type;
        if (bit_mask & LepClass::Real) truth_type += (truth_type.empty() ? "" : ", ") + std::string("Real");
        if (bit_mask & LepClass::HF) truth_type += (truth_type.empty() ? "" : ", ") + std::string("HF");
        if (bit_mask & LepClass::LF) truth_type += (truth_type.empty() ? "" : ", ") + std::string("LF");
        if (bit_mask & LepClass::CONV) truth_type += (truth_type.empty() ? "" : ", ") + std::string("CONV");
        if (bit_mask & LepClass::ELEC) truth_type += (truth_type.empty() ? "" : ", ") + std::string("ELEC");
        if (bit_mask & LepClass::GJ) truth_type += (truth_type.empty() ? "" : ", ") + std::string("GJ");
        if (bit_mask & LepClass::Other) truth_type += (truth_type.empty() ? "" : ", ") + std::string("Other");
        if (truth_type.empty()) return "UNKOWN_TYPE";
        return truth_type;
    }
    bool isElementSubstr(const std::vector<std::string>& vec, const std::string& str) {
        return std::find_if(vec.begin(), vec.end(), [&str](const std::string& ele) { return str.find(ele) != std::string::npos; }) !=
               vec.end();
    }
    void saveProcessFractions(PlotContent<TH2D>& pc) {
        using namespace PlotUtils;
        pc.populateAll();
        SetAtlasStyle();
        gStyle->SetPalette(pc.getCanvasOptions().colorPalette());
        CanvasOptions opt = pc.getCanvasOptions();

        std::vector<Plot<TH1>> ratios = pc.getRatios();
        int i = 1;
        for (auto& rat : ratios) {
            std::shared_ptr<TCanvas> can = std::make_shared<TCanvas>(RandomString(50).c_str(), "can", 800, 600);
            can->SetTopMargin(0.15);
            can->SetRightMargin(0.18);
            can->SetLeftMargin(0.12);

            can->cd();
            std::shared_ptr<TH1> h = rat.getSharedHisto();
            if (XAMPP::GetMaximumBin(h) == XAMPP::GetMinimumBin(h)) continue;
            /// Scale by 100
            h->Scale(100);
            /// Set maximuum to 100
            h->SetMinimum(0);
            h->SetMaximum(100);
            h->GetZaxis()->SetTitle(Form("Process fraction %s [%%]", pc.getPlot(i).getLegendTitle().c_str()));
            h->GetYaxis()->SetTitleOffset(0.9 * h->GetYaxis()->GetTitleOffset());
            h->Draw("colz");
            // h->Draw("same text");

            std::vector<TLatex*> labels;
            if (opt.doAtlasLabel()) { labels += drawAtlas(can->GetLeftMargin(), 0.92, opt.labelStatusTag(), opt.fontSize()); }
            labels += drawLumiSqrtS(can->GetLeftMargin(), 0.87, opt.labelLumiTag(), opt.labelSqrtsTag(), opt.fontSize());
            double y = 0.92;
            double dy = opt.otherLabelStepY();
            for (auto& lbl : pc.getLabels()) {
                labels += drawTLatex(0.5, y, lbl, opt.fontSize());
                y -= dy;
            }
            opt.SaveCanvas(can, pc.getFileName() + "_" + pc.getPlot(i).getLegendTitle(), pc.getMultiPageFileName());
            ++i;
        }
    }
    void saveFakeFactor(PlotContent<TH2D>& pc) {
        using namespace PlotUtils;
        pc.populateAll();
        SetAtlasStyle();
        gStyle->SetPalette(pc.getCanvasOptions().colorPalette());
        CanvasOptions opt = pc.getCanvasOptions();

        std::vector<Plot<TH2D>> dist = pc.getPlots();
        for (auto& d : dist) {
            std::shared_ptr<TCanvas> can = std::make_shared<TCanvas>(RandomString(50).c_str(), "can", 800, 600);
            can->SetTopMargin(0.15);
            can->SetRightMargin(0.18);
            can->SetLeftMargin(0.12);

            can->cd();
            std::shared_ptr<TH1> h = d.getSharedHisto();
            if (XAMPP::GetMaximumBin(h) == XAMPP::GetMinimumBin(h)) continue;
            h->SetMinimum(0);
            h->GetZaxis()->SetTitle(Form("Entries %s", d.getLegendTitle().c_str()));
            h->GetYaxis()->SetTitleOffset(0.9 * h->GetYaxis()->GetTitleOffset());
            h->Draw("colz");

            std::vector<TLatex*> labels;
            if (opt.doAtlasLabel()) { labels += drawAtlas(can->GetLeftMargin(), 0.92, opt.labelStatusTag(), opt.fontSize()); }
            labels += drawLumiSqrtS(can->GetLeftMargin(), 0.87, opt.labelLumiTag(), opt.labelSqrtsTag(), opt.fontSize());
            double y = 0.92;
            double dy = opt.otherLabelStepY();
            for (auto& lbl : pc.getLabels()) {
                labels += drawTLatex(0.5, y, lbl, opt.fontSize());
                y -= dy;
            }
            opt.SaveCanvas(can, pc.getFileName() + "_" + d.getLegendTitle(), pc.getMultiPageFileName());
        }
        std::vector<Plot<TH1>> ratios = pc.getRatios();
        for (auto& rat : ratios) {
            std::shared_ptr<TCanvas> can = std::make_shared<TCanvas>(RandomString(50).c_str(), "can", 800, 600);
            can->SetTopMargin(0.15);
            can->SetRightMargin(0.18);
            can->SetLeftMargin(0.12);

            can->cd();
            std::shared_ptr<TH1> h = rat.getSharedHisto();
            if (XAMPP::GetMaximumBin(h) == XAMPP::GetMinimumBin(h)) continue;
            h->SetMinimum(0);
            h->GetZaxis()->SetTitle("fake factor");
            h->GetYaxis()->SetTitleOffset(0.9 * h->GetYaxis()->GetTitleOffset());
            h->Draw("colz");
            std::vector<TLatex*> labels;
            if (opt.doAtlasLabel()) { labels += drawAtlas(can->GetLeftMargin(), 0.92, opt.labelStatusTag(), opt.fontSize()); }
            labels += drawLumiSqrtS(can->GetLeftMargin(), 0.87, opt.labelLumiTag(), opt.labelSqrtsTag(), opt.fontSize());
            double y = 0.92;
            double dy = opt.otherLabelStepY();
            for (auto& lbl : pc.getLabels()) {
                labels += drawTLatex(0.5, y, lbl, opt.fontSize());
                y -= dy;
            }
            opt.SaveCanvas(can, pc.getFileName(), pc.getMultiPageFileName());
        }
    }
    std::shared_ptr<TH1> make2DSlice(std::shared_ptr<TH1> H3, unsigned int bin_z) {
        if (!H3 || H3->GetDimension() != 3) return std::shared_ptr<TH1>();
        std::shared_ptr<TH1> H2 = XAMPP::MakeTH1(XAMPP::ExtractBinning(H3->GetXaxis()), XAMPP::ExtractBinning(H3->GetYaxis()));
        H2->GetXaxis()->SetTitle(H3->GetXaxis()->GetTitle());
        H2->GetYaxis()->SetTitle(H3->GetYaxis()->GetTitle());
        for (int x = 1; x <= H2->GetNbinsX(); ++x) {
            for (int y = 1; y <= H2->GetNbinsY(); ++y) {
                H2->SetBinContent(x, y, H3->GetBinContent(x, y, bin_z));
                H2->SetBinError(x, y, H3->GetBinError(x, y, bin_z));
            }
        }
        return H2;
    }

    // for 2d histos
    void saveScaleFactor(PlotContent<TH1>& pc) {
        using namespace PlotUtils;
        SetAtlasStyle();
        gStyle->SetPalette(pc.getCanvasOptions().colorPalette());
        CanvasOptions opt = pc.getCanvasOptions();
        Plot<TH1> scalefactor = pc.getRatios().at(0);
        std::shared_ptr<TCanvas> can = std::make_shared<TCanvas>(RandomString(50).c_str(), "can", 800, 600);
        can->SetTopMargin(0.15);
        can->SetRightMargin(0.18);
        can->SetLeftMargin(0.12);

        can->cd();
        std::shared_ptr<TH1> h = scalefactor.getSharedHisto();

        h->SetMinimum(0);
        h->GetZaxis()->SetTitle("fake factor");
        h->GetYaxis()->SetTitleOffset(0.9 * h->GetYaxis()->GetTitleOffset());
        h->Draw("colz");
        std::vector<TLatex*> labels;
        if (opt.doAtlasLabel()) { labels += drawAtlas(can->GetLeftMargin(), 0.92, opt.labelStatusTag(), opt.fontSize()); }
        labels += drawLumiSqrtS(can->GetLeftMargin(), 0.87, opt.labelLumiTag(), opt.labelSqrtsTag(), opt.fontSize());
        double y = 0.92;
        double dy = opt.otherLabelStepY();
        for (auto& lbl : pc.getLabels()) {
            labels += drawTLatex(0.5, y, lbl, opt.fontSize());
            y -= dy;
        }
        opt.SaveCanvas(can, pc.getFileName(), pc.getMultiPageFileName());
    }

    void saveProcessFractions(PlotContent<TH3D>& pc) {
        using namespace PlotUtils;

        pc.populateAll();
        SetAtlasStyle();
        gStyle->SetPalette(pc.getCanvasOptions().colorPalette());
        CanvasOptions opt = pc.getCanvasOptions();

        std::vector<Plot<TH1>> ratios = pc.getRatios();
        int i = 1;
        for (auto& rat : ratios) {
            std::shared_ptr<TCanvas> can = std::make_shared<TCanvas>(RandomString(50).c_str(), "can", 800, 600);
            can->SetTopMargin(0.15);
            can->SetRightMargin(0.18);
            can->SetLeftMargin(0.12);

            can->cd();
            std::shared_ptr<TH1> pf_histo = rat.getSharedHisto();
            if (XAMPP::GetMaximum(pf_histo) <= 0. || pc.getPlot(i)->GetEntries() == 0) continue;
            for (int z = 1; z <= pf_histo->GetNbinsZ(); ++z) {
                std::shared_ptr<TH1> h = make2DSlice(pf_histo, z);
                if (XAMPP::GetMaximumBin(h) == XAMPP::GetMinimumBin(h)) continue;
                /// Scale by 100
                h->Scale(100);
                /// Set maximuum to 100
                h->SetMinimum(0);
                h->SetMaximum(100);
                h->GetZaxis()->SetTitle(Form("Process fraction %s (%.2f<%s<%.2f)[%%]", pc.getPlot(i).getLegendTitle().c_str(),
                                             pf_histo->GetZaxis()->GetBinLowEdge(z), pf_histo->GetZaxis()->GetTitle(),
                                             pf_histo->GetZaxis()->GetBinUpEdge(z)));

                h->GetYaxis()->SetTitleOffset(0.9 * h->GetYaxis()->GetTitleOffset());
                h->Draw("colz");

                std::vector<TLatex*> labels;
                if (opt.doAtlasLabel()) { labels += drawAtlas(can->GetLeftMargin(), 0.92, opt.labelStatusTag(), opt.fontSize()); }
                labels += drawLumiSqrtS(can->GetLeftMargin(), 0.87, opt.labelLumiTag(), opt.labelSqrtsTag(), opt.fontSize());
                double y = 0.92;
                double dy = opt.otherLabelStepY();
                for (auto& lbl : pc.getLabels()) {
                    labels += drawTLatex(0.5, y, lbl, opt.fontSize());
                    y -= dy;
                }
                opt.SaveCanvas(can, pc.getFileName() + "_" + pc.getPlot(i).getLegendTitle() + "_" + std::to_string(z),
                               pc.getMultiPageFileName());
            }
            ++i;
        }
    }
    void saveFakeFactor(PlotContent<TH3D>& pc) {
        using namespace PlotUtils;
        pc.populateAll();
        SetAtlasStyle();
        gStyle->SetPalette(pc.getCanvasOptions().colorPalette());
        CanvasOptions opt = pc.getCanvasOptions();

        std::vector<Plot<TH1>> ratios = pc.getRatios();
        for (auto& rat : ratios) {
            std::shared_ptr<TH1> ff_histo = rat.getSharedHisto();
            if (XAMPP::GetMaximum(ff_histo) == 0) continue;
            for (int z = 1; z <= ff_histo->GetNbinsZ(); ++z) {
                std::shared_ptr<TCanvas> can = std::make_shared<TCanvas>(RandomString(50).c_str(), "can", 800, 600);
                can->SetTopMargin(0.15);
                can->SetRightMargin(0.18);
                can->SetLeftMargin(0.12);
                can->cd();
                std::shared_ptr<TH1> h = make2DSlice(ff_histo, z);
                if (XAMPP::GetMaximumBin(h) == XAMPP::GetMinimumBin(h)) continue;
                h->SetMinimum(0);
                h->GetZaxis()->SetTitle(Form("fake factor (%.2f<%s<%.2f)", ff_histo->GetZaxis()->GetBinLowEdge(z),
                                             ff_histo->GetZaxis()->GetTitle(), ff_histo->GetZaxis()->GetBinUpEdge(z)));
                h->GetYaxis()->SetTitleOffset(0.9 * h->GetYaxis()->GetTitleOffset());
                h->Draw("colz");
                std::vector<TLatex*> labels;
                if (opt.doAtlasLabel()) { labels += drawAtlas(can->GetLeftMargin(), 0.92, opt.labelStatusTag(), opt.fontSize()); }
                labels += drawLumiSqrtS(can->GetLeftMargin(), 0.87, opt.labelLumiTag(), opt.labelSqrtsTag(), opt.fontSize());
                double y = 0.92;
                double dy = opt.otherLabelStepY();
                for (auto& lbl : pc.getLabels()) {
                    labels += drawTLatex(0.5, y, lbl, opt.fontSize());
                    y -= dy;
                }
                opt.SaveCanvas(can, Form("%s_%d", pc.getFileName().c_str(), z), pc.getMultiPageFileName());
            }
        }
    }

    std::shared_ptr<TH1> pullErrors(const std::shared_ptr<TH1>& H) {
        std::shared_ptr<TH1> out = XAMPP::clone(H, true);
        for (unsigned int i = 0; i <= XAMPP::GetNbins(H); ++i) { out->SetBinContent(i, H->GetBinError(i)); }
        return out;
    }
    std::shared_ptr<TH1> makeConstant(const std::shared_ptr<TH1>& H, double value) {
        std::shared_ptr<TH1> out = XAMPP::clone(H, true);
        for (unsigned int i = 0; i <= XAMPP::GetNbins(H); ++i) { out->SetBinContent(i, value); }
        return out;
    }
    void truncate(std::shared_ptr<TH1> H, double lower_value, double upper_value) {
        for (unsigned int i = 0; i <= XAMPP::GetNbins(H); ++i) {
            if (H->GetBinContent(i) < lower_value)
                H->SetBinContent(i, lower_value);
            else if (H->GetBinContent(i) > upper_value)
                H->SetBinContent(i, upper_value);
        }
    }
    void pull_overflow(std::shared_ptr<TH1> H) {
        std::function<int(TAxis*, int)> shift_bin = [](TAxis* A, int bin) {
            if (bin == 0) return 1;
            if (XAMPP::isOverflowBin(A, bin)) return bin - 1;
            return bin;
        };
        for (int b = XAMPP::GetNbins(H) - 1; b >= 0; --b) {
            if (!XAMPP::isOverflowBin(H, b)) continue;
            int x(0), y(0), z(0);
            H->GetBinXYZ(b, x, y, z);
            /// Determine the target X
            int t_x(shift_bin(H->GetXaxis(), x)), t_y(y), t_z(z);
            if (H->GetDimension() > 1) t_y = shift_bin(H->GetYaxis(), y);
            if (H->GetDimension() > 2) t_z = shift_bin(H->GetZaxis(), z);
            int target_bin = H->GetBin(t_x, t_y, t_z);
            H->SetBinContent(target_bin, H->GetBinContent(target_bin) + H->GetBinContent(b));
            H->SetBinError(target_bin, std::hypot(H->GetBinError(target_bin), H->GetBinError(b)));
            H->SetBinContent(b, 0);
            H->SetBinError(b, 0);
        }
    }
    std::shared_ptr<TFile> make_out_file(const std::string& path) {
        if (path.rfind("/") != std::string::npos) {
            std::string OutDir = path.substr(0, path.rfind("/"));
            if (!XAMPP::DoesDirectoryExist(OutDir)) {
                std::cout << "make_out_file()  -- Create directory " << path << " to store the file there." << std::endl;
                gSystem->mkdir(OutDir.c_str(), true);
            } else
                std::cout << "make_out_file()  -- Output file will be stored in " << OutDir << std::endl;
        }
        if (path.find(".root") == std::string::npos)
            std::cout << "make_out_file() -- The File " << path << " has no .root suffix." << std::endl;
        std::cout << "make_out_file()  -- Create new file " << path << " for writing job output " << std::endl;
        std::shared_ptr<TFile> out_file = std::make_shared<TFile>(path.c_str(), "RECREATE");
        if (!out_file->IsOpen()) out_file.reset();
        return out_file;
    }
    FileCheck check_file(const std::string& file_name, const std::string& tree_name) {
        std::shared_ptr<TFile> f = XAMPP::Open(file_name);
        if (!f) return FileCheck::Error;
        TTree* t = nullptr;
        f->GetObject(tree_name.c_str(), t);
        if (!t) {
            std::cerr << "check_file() --- file " << file_name << " does not contain tree: " << tree_name << std::endl;
            return FileCheck::Error;
        }
        Long64_t e = t->GetEntries();
        delete t;
        return e > 0 ? FileCheck::Okay : FileCheck::Empty;
    }
}  // namespace SUSY4L
