from ClusterSubmission.Utils import ResolvePath, prettyPrint
from XAMPPplotting.Utils import GetPlotRangeX, GetPlotRangeY, PrintHistogram, SameAxisBinning, mkdir, GetNbins, GetBinArea, isOverFlowBin, GlobalBinToLocalBin
from XAMPPplotting.QuickPlots import GetHistoPaths
from compareFakeFiles import log
import argparse, os, ROOT, logging, math


def search_large_ff(histo, log_file, h_path):
    pathological_ff = []

    mean = [
        histo.GetBinContent(b) * GetBinArea(histo, b) for b in range(1, GetNbins(histo))
        if not isOverFlowBin(histo, b) and (histo.GetBinContent(b) != 0. or histo.GetBinError(b) != 0.)
    ]
    area = sum([
        GetBinArea(histo, b) for b in range(1, GetNbins(histo))
        if not isOverFlowBin(histo, b) and (histo.GetBinContent(b) != 0. or histo.GetBinError(b) != 0.)
    ])
    mean = sum(mean) / area
    sigma = [((histo.GetBinContent(b) - mean) * GetBinArea(histo, b))**2 for b in range(1, GetNbins(histo))
             if not isOverFlowBin(histo, b) and (histo.GetBinContent(b) != 0. or histo.GetBinError(b) != 0.)]
    sigma = math.sqrt(sum(sigma)) / area
    for b in range(1, GetNbins(histo)):
        if isOverFlowBin(histo, b): continue
        elif histo.GetBinContent(b) < 0.:
            pathological_ff += [b]
        elif histo.GetBinContent(b) == 0 and histo.GetBinError(b) == 0:
            continue
            ### The fake factor has a large uncertainty
        elif histo.GetBinError(b) > histo.GetBinContent(b):
            pathological_ff += [b]
        #elif math.fabs(histo.GetBinContent(b)-mean) >= 2.*mean:
        #    pathological_ff +=[b]

    if len(pathological_ff) > 0:
        log(
            "Found %d/%d pathological fake-factors in %s with %.2f as mean and %.2f as sigma" %
            (len(pathological_ff), GetNbins(histo), h_path, mean, sigma), log_file)
        for b in pathological_ff:
            if histo.GetDimension() == 1:
                log(
                    "%s %.2f -- %.2f (%d): %.3f pm %.3f" %
                    (histo.GetXaxis().GetTitle(), histo.GetXaxis().GetBinLowEdge(b), histo.GetXaxis().GetBinUpEdge(b), b,
                     histo.GetBinContent(b), histo.GetBinError(b)), log_file)
            elif histo.GetDimension() == 2:
                x, y, z = GlobalBinToLocalBin(histo, b)
                log(
                    "%s vs. %s %.2f -- %.2f vs. %.2f -- %.2f (%d): %.3f pm %.3f" %
                    (histo.GetXaxis().GetTitle(), histo.GetYaxis().GetTitle(), histo.GetXaxis().GetBinLowEdge(x),
                     histo.GetXaxis().GetBinUpEdge(x), histo.GetYaxis().GetBinLowEdge(y), histo.GetYaxis().GetBinUpEdge(y), b,
                     histo.GetBinContent(b), histo.GetBinError(b)), log_file)
            elif histo.GetDimension() == 3:
                x, y, z = GlobalBinToLocalBin(histo, b)
                log(
                    "%s vs. %s vs. %s %.2f -- %.2f vs. %.2f -- %.2f vs. %.2f -- %.2f (%d): %.3f pm %.3f" %
                    (histo.GetXaxis().GetTitle(), histo.GetYaxis().GetTitle(), histo.GetZaxis().GetTitle(),
                     histo.GetXaxis().GetBinLowEdge(x), histo.GetXaxis().GetBinUpEdge(x), histo.GetYaxis().GetBinLowEdge(y),
                     histo.GetYaxis().GetBinUpEdge(y), histo.GetZaxis().GetBinLowEdge(z), histo.GetZaxis().GetBinUpEdge(z), b,
                     histo.GetBinContent(b), histo.GetBinError(b)), log_file)


if __name__ == "__main__":

    ref_directory = "FakeFactors4L/data/"

    for ff_file in ["fake_results_ttbar.root", "fake_results_Zjets.root"]:
        ref_file = ROOT.TFile.Open(ref_directory + ff_file)

        ### Obtain the list of all histograms in each file
        ref_paths = [p for p in GetHistoPaths(ref_file) if "Other" not in p and not "Real" in p]

        with open("file_check_%s.txt" % (ff_file[:ff_file.rfind(".")]), "w") as log_file:
            for h_path in ref_paths:
                ref_histo = ref_file.Get(h_path)
                ####
                category = h_path[:h_path.find("/")]
                if category == "FF":
                    search_large_ff(ref_histo, log_file, h_path)
