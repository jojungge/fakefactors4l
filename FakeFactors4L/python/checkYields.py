from ClusterSubmission.Utils import ResolvePath, prettyPrint
import argparse, os, ROOT, logging, math


class FakeYield(object):
    def __init__(self, tree, region, syst=None, lumi=139.):
        self.__tree = tree
        self.__region = tree.GetLeaf(region)
        self.__weight = tree.GetLeaf("FakeWeight" if not syst else syst)
        self.__norm = tree.GetLeaf("NormWeight")
        self.__name = syst

        self.__systematic = syst[syst.find("_"):] if syst else "Nominal"
        while self.__systematic.find("_") == 0:
            self.__systematic = self.__systematic[1:]
        self.__lumi = lumi
        self.__yield = 0.
        self.__error = 0.

    def new_event(self):
        if not self.__region.GetValue(): return
        w = self.__weight.GetValue() * self.__norm.GetValue() * self.__lumi
        self.__yield += w
        self.__error += w**2
        return

    def get_yield(self):
        return self.__yield

    def get_error(self):
        return math.sqrt(self.__error)

    def get_region(self):
        return self.__region.GetName()

    def is_nominal(self):
        return self.__systematic == "Nominal"

    def is_up_variation(self):
        return self.__systematic.find("1UP") != -1

    def getSystName(self):
        return self.__name


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='This script checks the final reducible tree and calculates the yields for each region',
                                     prog='Run',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--in_file", type=str, required=True)
    parser.add_argument("--lumi", type=float, default=139.)
    parser.add_argument("--noSyst", action='store_true', default=False)
    parser.add_argument("--printAllSysts", action='store_true', default=False)
    options = parser.parse_args()

    r_file = ResolvePath(options.in_file)
    if not r_file: exit(1)

    in_file = ROOT.TFile.Open(r_file, "READ")
    tree = in_file.Get("Reducible_Nominal")
    if not tree:
        logging.error("File %s does not contain the Reducible_Nominal tree" % (r_file))
        exit(1)

    regions = sorted([b.GetName() for b in tree.GetListOfBranches() if b.GetName().startswith("Is") and b.GetName().find("Real") == -1])
    fake_weights = sorted([b.GetName() for b in tree.GetListOfBranches() if b.GetName().startswith("FakeWeight_")])

    yield_dict = []
    for r in regions:
        for syst in [None] + fake_weights:
            yield_dict += [FakeYield(tree=tree, region=r, syst=syst, lumi=options.lumi)]
            if options.noSyst: break

    for i in range(tree.GetEntries()):
        tree.GetEntry(i)
        for y in yield_dict:
            y.new_event()
        if i % 1000 == 0: print "Stonjek %d/%d" % (i + 1, tree.GetEntries())
    for y in [x for x in yield_dict if x.is_nominal()]:
        sys_up = math.sqrt(
            sum([(s.get_yield() - y.get_yield())**2 for s in yield_dict if s.get_region() == y.get_region() and s.is_up_variation()]))
        sys_dn = math.sqrt(
            sum([(s.get_yield() - y.get_yield())**2 for s in yield_dict if s.get_region() == y.get_region() and not s.is_up_variation()]))

        prettyPrint(y.get_region(), "%.2f  pm  %.2f + %.2f - %.2f" % (y.get_yield(), y.get_error(), sys_up, sys_dn))

        if options.printAllSysts:
            for sys in [x for x in yield_dict if not x.is_nominal() and x.get_region() == y.get_region()]:
                syst = abs(sys.get_yield() - y.get_yield())
                prettyPrint(sys.getSystName(), "%2f" % (syst))
