from ClusterSubmission.Utils import ResolvePath, prettyPrint
from XAMPPplotting.Utils import GetPlotRangeX, GetPlotRangeY, PrintHistogram, SameAxisBinning, mkdir, GetNbins
from XAMPPplotting.QuickPlots import GetHistoPaths
import argparse, os, ROOT, logging, math


def log(line, log_file):
    logging.warning(line)
    log_file.write(line + "\n")


if __name__ == "__main__":

    #parser = argparse.ArgumentParser(description='This script checks the final reducible tree and calculates the yields for each region',
    #                                 prog='Run',
    #                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    #parser.add_argument("--in_file", type=str, required=True)
    #parser.add_argument("--noSyst", action='store_true', default=False)
    #options = parser.parse_args()

    ref_directory = "FakeFactors4L/data_old/"
    new_directory = "FakeFactors4L/data/"

    for ff_file in ["fake_results_ttbar.root", "fake_results_Zjets.root"]:
        ref_file = ROOT.TFile.Open(ref_directory + ff_file)
        new_file = ROOT.TFile.Open(new_directory + ff_file)

        ### Obtain the list of all histograms in each file
        ref_paths = GetHistoPaths(ref_file)
        new_paths = GetHistoPaths(new_file)

        ### Prune the histograms to the minimal common set
        ref_paths = [p for p in ref_paths if p in new_paths]
        #new_paths = [ p for p in new_paths if p in ref_paths]
        n_good = 0
        with open("changelog_%s.txt" % (ff_file[:ff_file.rfind(".")]), "w") as log_file:
            for h_path in ref_paths:
                ref_histo = ref_file.Get(h_path)
                new_histo = new_file.Get(h_path)
                if not ref_histo or not new_histo:
                    log("Comparison of " + h_path + " does not seem to be sensible", log_file)
                    continue
                if ref_histo.GetDimension() != new_histo.GetDimension():
                    log(
                        "The dimension of the histograms %s changed in the meantime %d vs. %d" %
                        (h_path, ref_histo.GetDimension(), new_histo.GetDimension()), log_file)
                    continue
                axis_test = True
                if not SameAxisBinning(ref_histo.GetXaxis(), new_histo.GetXaxis()):
                    log("The x-axis binning of the histograms %s changed in the meantime " % (h_path), log_file)
                    log(
                        " **** %s" % (" ".join(["%.2f," % (b)
                                                for b in ROOT.XAMPP.SampleHisto.ExtractBinningFromAxis(ref_histo.GetXaxis())])), log_file)
                    log(
                        " ++++ %s" % (" ".join(["%.2f," % (b)
                                                for b in ROOT.XAMPP.SampleHisto.ExtractBinningFromAxis(new_histo.GetXaxis())])), log_file)

                    axis_test = False
                if ref_histo.GetDimension() > 1 and not SameAxisBinning(ref_histo.GetYaxis(), new_histo.GetYaxis()):
                    log("The y-axis binning of the histograms %s changed in the meantime " % (h_path), log_file)
                    log(
                        " **** %s" % (" ".join(["%.2f," % (b)
                                                for b in ROOT.XAMPP.SampleHisto.ExtractBinningFromAxis(ref_histo.GetYaxis())])), log_file)
                    log(
                        " ++++ %s" % (" ".join(["%.2f," % (b)
                                                for b in ROOT.XAMPP.SampleHisto.ExtractBinningFromAxis(new_histo.GetYaxis())])), log_file)
                    axis_test = False
                if ref_histo.GetDimension() == 3 and not SameAxisBinning(ref_histo.GetZaxis(), new_histo.GetZaxis()):
                    log("The y-axis binning of the histograms %s changed in the meantime " % (h_path), log_file)
                    log(
                        " **** %s" % (" ".join(["%.2f," % (b)
                                                for b in ROOT.XAMPP.SampleHisto.ExtractBinningFromAxis(ref_histo.GetZaxis())])), log_file)
                    log(
                        " ++++ %s" % (" ".join(["%.2f," % (b)
                                                for b in ROOT.XAMPP.SampleHisto.ExtractBinningFromAxis(new_histo.GetZaxis())])), log_file)
                    axis_test = False
                if not axis_test:
                    print("\n\n\n")
                    continue
                different_contents = []
                for b in range(1, GetNbins(ref_histo)):
                    if "%.3f" % (ref_histo.GetBinContent(b)) != "%.3f" % (new_histo.GetBinContent(b)):
                        different_contents += [(b, ref_histo.GetBinContent(b), new_histo.GetBinContent(b))]
                if len(different_contents):
                    log("The histograms %s have different bin-contents" % (h_path), log_file)
                    for b, old, new in different_contents:
                        log(" -- %d   %.2f vs. %.2f" % (b, old, new), log_file)
