from ClusterSubmission.Utils import ExecuteThreads, CmdLineThread, CreateDirectory, ExecuteCommands
import threading, argparse, os, logging


def is_file_sample(file_name, sample_name):
    return file_name[:file_name.rfind(".")] == sample_name or (file_name[:file_name.rfind("_")] == sample_name
                                                               and file_name[file_name.rfind("_") + 1:file_name.rfind(".")].isdigit())


def make_list_arg(arg_list, arg):
    if len(arg_list) == 0: return ""
    arg_w_space = " " + arg + " "
    return arg_w_space + " " + arg_w_space.join(arg_list)


class ClosureTest(threading.Thread):
    def __init__(self, inputsamples, outputdir, sample_name, no_atlas=False, is_zjets=False):
        threading.Thread.__init__(self)

        self.__inputsamples = sorted(inputsamples)
        self.__outputdir = outputdir
        self.__sample_name = sample_name
        self.__no_atlas = no_atlas
        self.__is_zjets = is_zjets
        CreateDirectory(self.__outputdir + "/TMP", True)

    def run(self):
        if len(self.__inputsamples) == 0:
            logging.error("No samples given to " + self.__sample_name)
            return
        run_cmd = "CalculateFF --outDir %s/Fakefactor/  --sampleName '%s' --outFile %s/TMP/fake_results.root %s %s " % (
            self.__outputdir, self.__sample_name, self.__outputdir, make_list_arg(
                arg_list=self.__inputsamples, arg="--inFile"), "" if not self.__no_atlas else "--noATLAS")

        closure_dir = self.__outputdir + "/ClosureTest/"
        closure_cmd = "RunClosureTest --outDir %s --sampleName '%s'  --fakeFactorFile %s/TMP/fake_results.root  %s %s %s --nonClosureFile %s/TMP/non_closure.root" % (
            closure_dir, self.__sample_name, self.__outputdir, make_list_arg(arg_list=self.__inputsamples, arg="--inFile"),
            "" if not self.__no_atlas else "--noATLAS", "" if not self.__is_zjets else "--isZJets", self.__outputdir)
        if os.system(run_cmd):
            os.system("rm -rf " + self.__outputdir)
            return False
        CreateDirectory(closure_dir, True)
        if os.system(closure_cmd):
            os.system("rm -rf " + closure_dir)
            os.system("rm -rf %s/TMP" % (self.__outputdir))
            return False
        if os.path.exists("%s/fake_results.root" % (self.__outputdir)):
            logging.info("Remove previous version of %s/fake_results.root" % (self.__outputdir))
            os.system("rm -f %s/fake_results.root" % (self.__outputdir))
        hadd_cmd = "hadd  %s/fake_results.root %s/TMP/fake_results.root %s/TMP/non_closure.root" % (self.__outputdir, self.__outputdir,
                                                                                                    self.__outputdir)
        if os.system(hadd_cmd):
            os.system("rm -rf " + self.__outputdir)
            return False
        os.system("rm -rf %s/TMP" % (self.__outputdir))
        return True


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='This script calculates the Fakefactor and processfractions and runs the Closuretest on ttbar and Z+jets',
        prog='Run',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--inputPath", type=str, default="/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-05-13/SF_Trees/")

    parser.add_argument("--outDir", type=str, default="FakeCalculation/")
    parser.add_argument("--noATLAS", action='store_true', default=False)
    parser.add_argument("--noOther", action='store_true', default=False)
    parser.add_argument("--nThreadsPerJob", default=14, type=int)

    Options = parser.parse_args()

    z_jets = [
        "PowHegPy8_Zee",
        "PowHegPy8_Zmumu",
        "PowHegPy8_Ztautau",
    ]
    ttbar = ["PowHegPy8_ttbar_diL"]
    sf_bkgs = [
        "PowHegPy8_Wenu",
        "PowHegPy8_Wmunu",
        "PowHegPy8_Wtaunu",
        "PowHegPy8_ttbar_diL",
        "PowHegPy8_Zee",
        "PowHegPy8_Zmumu",
        "PowHegPy8_Ztautau",
        "PowhegPy_top",
        "Sherpa221_VV",
        "Sherpa221_VVV",
        "Sherpa222_VV",
        "Sherpa222_ggZZ",
    ]
    os.environ["NTAU_NTHREADS"] = str(Options.nThreadsPerJob)
    sf_mc_files = sorted([
        Options.inputPath + x for x in os.listdir(Options.inputPath)
        if len([y for y in sf_bkgs if is_file_sample(file_name=x, sample_name=y)]) > 0
    ])
    sf_data_files = sorted([Options.inputPath + x for x in os.listdir(Options.inputPath) if x.startswith("data")])

    CreateDirectory(Options.outDir + "/ScaleFactors/", True)

    Commands = [
        #ClosureTest(inputsamples=[
        #    "%s/%s" % (Options.inputPath, x) for x in os.listdir(Options.inputPath)
        #    if len([y for y in ttbar if is_file_sample(file_name=x, sample_name=y)]) > 0
        #],
        #            outputdir=Options.outDir + "/ttbar/",
        #            sample_name="t#bar{t}",
        #            no_atlas=Options.noATLAS),
        #ClosureTest(inputsamples=[
        #    "%s/%s" % (Options.inputPath, x) for x in os.listdir(Options.inputPath)
        #    if len([y for y in z_jets if is_file_sample(file_name=x, sample_name=y)]) > 0
        #],
        #            outputdir=Options.outDir + "/Zjets/",
        #            sample_name="Z+jets",
        #            no_atlas=Options.noATLAS,
        #            is_zjets=True),
        #ClosureTest(inputsamples=[
        #    "%s/%s" % (Options.inputPath, x) for x in os.listdir(Options.inputPath)
        #    if len([y for y in z_jets if is_file_sample(file_name=x, sample_name="PowHegPy8_Zee")]) > 0
        #],
        #            outputdir=Options.outDir + "/Zee/",
        #            sample_name="Z+ee",
        #            no_atlas=Options.noATLAS),
        #ClosureTest(inputsamples=[
        #    "%s/%s" % (Options.inputPath, x) for x in os.listdir(Options.inputPath)
        #    if len([y for y in z_jets if is_file_sample(file_name=x, sample_name="PowHegPy8_Zmumu")]) > 0
        #],
        #            outputdir=Options.outDir + "/Zmumu/",
        #            sample_name="Z+mumu",
        #            no_atlas=Options.noATLAS),
        #ClosureTest(inputsamples=[
        #    "%s/%s" % (Options.inputPath, x) for x in os.listdir(Options.inputPath)
        #    if len([y for y in z_jets + ttbar if is_file_sample(file_name=x, sample_name=y)]) > 0
        #],
        #            outputdir=Options.outDir + "/Combined/",
        #            sample_name="t#bar{t}, Z+jets",
        #            no_atlas=Options.noATLAS),
        CmdLineThread(
            cmd="CalculateSF",
            args=" ".join([
                #"--noOther",
                make_list_arg(arg_list=sf_mc_files, arg="--inFileMC"),
                make_list_arg(arg_list=sf_data_files, arg="--inFileData"),
                "--outDir " + Options.outDir + "/ScaleFactors/Plots/",
                "--outFile " + Options.outDir + "/ScaleFactors/sf_file.root",
                "" if not Options.noATLAS else "--noATLAS",
                "" if not Options.noOther else "--noOther",
            ])),
    ]
    ExecuteThreads(Commands, verbose=False, MaxCurrent=3)
    CreateDirectory(Options.outDir + "/FinalFiles/", True)
    Commands = [
        "hadd " + Options.outDir + "/FinalFiles/fake_results_ttbar.root " + Options.outDir + "/ScaleFactors/sf_file.root " +
        Options.outDir + "/ttbar/fake_results.root",
        "hadd " + Options.outDir + "/FinalFiles/fake_results_Zjets.root " + Options.outDir + "/ScaleFactors/sf_file.root " +
        Options.outDir + "/Zjets/fake_results.root",
    ]
    ExecuteCommands(Commands)
