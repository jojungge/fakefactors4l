from ClusterSubmission.Utils import ExecuteCommands, CreateDirectory
from FakeFactors4L.runFFCalculation import is_file_sample, make_list_arg
import argparse, os, logging

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='This script calculates the Fakefactor and processfractions and runs the Closuretest on ttbar and Z+jets',
        prog='Run',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--inputPath", type=str, default="/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-14/HF_Reducible/")
    parser.add_argument("--outDir", type=str, default="/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-05-13/Reducible_Fakes/")
    parser.add_argument("--sm_backgrounds",
                        nargs="+",
                        default=[
                            "Sherpa222_VV",
                            "Sherpa222_ggZZ",
                            "aMCatNLOPy8_ttW",
                            "aMCatNLOPy8_ttZ",
                            "PowHegPy8_ttH",
                            "aMcAtNlo_tWZ",
                            "MG5Py8_ttWZ",
                            "MG5Py8_ttWW",
                            "MG5Py8_4t",
                            "PowHegPy8_WH",
                            "PowHegPy8_ZH",
                            "Sherpa221_VVV",
                        ])
    options = parser.parse_args()
    primary_files = []
    data_files = []
    other_files = []
    for x in sorted(os.listdir(options.inputPath)):
        #### Reject all ROOT files
        if x[x.rfind(".") + 1:] != "root": continue
        ### Taylor the needed SM backgrounds together

        if len([y for y in options.sm_backgrounds if is_file_sample(file_name=x, sample_name=y)]):
            primary_files += [options.inputPath + x]
        #### Data in a separate list to check later whether all background are complete
        elif "data" in x and not "debug" in x:
            data_files += [options.inputPath + x]
            continue
            #### We do not need the PowHeg, MG5Py8, etc files as they are backgrounds
        elif len([y for y in ["PowHeg", "MG5Py8", "Sherpa", "aMCAtNLO", "Powheg", "aMcAtNlo"] if y in x]) == 0:
            other_files += [options.inputPath + x]

    if len(primary_files) != len(options.sm_backgrounds):
        logging.error("Some of the required SM backgrounds are missing")
        for p in primary_files:
            logging.info(" -- %s " % (p[p.rfind("/") + 1:]))
        for s in sorted(options.sm_backgrounds):
            logging.info(" ** %s " % (s))

        exit(1)
    CreateDirectory(options.outDir + "/Signals/", False)
    Commands = [
        "WriteFakeTree --outFile " + options.outDir + "ff_estimate.root" +
        make_list_arg(arg_list=primary_files + data_files, arg="--inFile")
    ] + ["WriteFakeTree --outFile " + options.outDir + "/Signals/" + x[x.rfind("/") + 1:] + " --inFile " + x for x in other_files]

    ExecuteCommands(Commands, verbose=False, MaxCurrent=8, MaxExec=-1)
