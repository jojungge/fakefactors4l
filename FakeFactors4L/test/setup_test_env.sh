#!/bin/bash
source /home/atlas/release_setup.sh
##############################
# Setup                      #
##############################
#prepare AthAnalysis or build if not already done so

if [ -f /xampp/build/${AthAnalysis_PLATFORM}/setup.sh ]; then
    if [ -z "${TestArea}" ]; then
        export TestArea=/xampp/fakefactors4l
    fi
    source /xampp/build/${AthAnalysis_PLATFORM}/setup.sh
else
    asetup AthAnalysis,latest,here
    if [ -f ${TestArea}/build/${AthAnalysis_PLATFORM}/setup.sh ]; then
        source ${TestArea}/build/${AthAnalysis_PLATFORM}/setup.sh
    else
        mkdir -p ${TestArea}/build && cd ${TestArea}/build
        cmake ..
        cmake --build .
        cd .. && source build/${AthAnalysis_PLATFORM}/setup.sh
    fi
fi
