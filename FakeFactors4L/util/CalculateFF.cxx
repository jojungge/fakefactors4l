#include <FakeFactors4L/FakeBundle.h>
#include <FakeFactors4L/Utils.h>

///

#include <TFile.h>
#include <TH2D.h>
#include <TSystem.h>
#include <TTree.h>
#include <cstring>
#include <iostream>
#include <memory>
#include <string>

using namespace XAMPP;
using namespace SUSY4L;

int main(int argc, char* argv[]) {
    SetAtlasStyle();
    /// I'm too lazy to call SumW for each histo type
    XAMPP::HistoTemplates::getHistoTemplater();

    //     HistoFiller::getDefaultFiller()->setFillMode(HistoFillMode::singleThread);

    std::vector<std::string> in_files{
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_1.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_2.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_3.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_4.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_5.root",
    };

    bool in_file_reset = false;
    /// Plot styling
    bool do_atlas = true;

    std::string tree_name = "DebugTree_Nominal";
    std::string out_root_file = "fake_results.root";
    std::string sample_name = "t#bar{t}";
    std::string out_dir = "Plots/FakeFactors/";  // fallback to NtupleAnalysisUtils default scheme if not specified
    for (int k = 1; k < argc; ++k) {
        std::string current_arg(argv[k]);
        if (current_arg == "--inFile" && k + 1 < argc) {
            if (!in_file_reset) in_files.clear();
            in_file_reset = true;
            in_files.push_back(argv[k + 1]);

        } else if (current_arg == "--outDir" && k + 1 < argc) {
            out_dir = argv[k + 1];
        } else if (current_arg == "--outFile" && k + 1 < argc) {
            out_root_file = argv[k + 1];
        } else if (current_arg == "--treeName" && k + 1 < argc) {
            tree_name = argv[k + 1];
        } else if (current_arg == "--sampleName" && k + 1 < argc) {
            sample_name = argv[k + 1];
        } else if (current_arg == "--noATLAS") {
            do_atlas = false;
        }
    }

    Sample<FakeFactorTree> mySample(sample_name);
    for (auto& in_file : in_files) { mySample.addFile(in_file, tree_name); }
    ///////////////////////////////////////////
    //     Define some selection cuts
    //////////////////////////////////////////

    Cut trig("NoTrigger", "no trigger", [](FakeFactorTree&) { return true; });
    Cut lep_mult("LepIncl", "N_{#it{l}}^{baseline}#geq1", [](FakeFactorTree& t) { return t.get_n_lep(LepClass::AllLep) >= 1; });

    Selection<SUSY4L::FakeFactorTree> ff_selection = trig && lep_mult;
    std::string ff_sel_label = lep_mult.getTitle();
    std::vector<Cut> Proc_FracCuts{
        //
        //          Standard requirements
        //          without any dR
        // Cut("CR1_LLLl", "LLLl",
        //    [](FakeFactorTree& t) { return t.get_n_lep(LepClass::InclSignal) == 3 && t.get_n_lep(LepClass::InclLoose) >= 1; }),
        Cut("CR2_LLll", "LLll",
            [](FakeFactorTree& t) { return t.get_n_lep(LepClass::InclSignal) == 2 && t.get_n_lep(LepClass::InclLoose) >= 2; }),
        Cut("SR_LLLL", "LLLL", [](FakeFactorTree& t) { return t.get_n_lep(LepClass::InclSignal) >= 4; }),
        // Cut("CR1_LLl", "LLl",
        //    [](FakeFactorTree& t) { return t.get_n_lep(LepClass::InclSignal) == 2 && t.get_n_lep(LepClass::InclLoose) == 1; }),
        Cut("CR2_Lll", "Lll",
            [](FakeFactorTree& t) { return t.get_n_lep(LepClass::InclSignal) == 1 && t.get_n_lep(LepClass::InclLoose) == 2; }),
        Cut("SR_LLL", "LLL",
            [](FakeFactorTree& t) { return t.get_n_lep(LepClass::InclSignal) == 3 && t.get_n_lep(LepClass::InclLoose) == 0; }),

        ///         Tau CR

        Cut("SR_LLLT", "LLLT",
            [](FakeFactorTree& t) {
                /// 3 light leptons
                /// 1 fake loose tau
                return (t.get_n_lep(LepClass::InclSignal) == 3 && t.get_n_lep(LepClass::InclLoose) == 0 &&
                        t.get_n_lep(LepClass::Tau | LepClass::Incl | LepClass::Signal) >= 1);
            }),

        // Cut("CR1_LLTl", "LLTl",
        //    [](FakeFactorTree& t) {
        //        /// 3 light leptons
        //        /// 1 fake loose tau
        //        return (t.get_n_lep(LepClass::InclSignal) == 2 && t.get_n_lep(LepClass::InclLoose) == 1 &&
        //                t.get_n_lep(LepClass::Tau | LepClass::Incl | LepClass::Signal) == 1);
        //    }),
        // Cut("CR1_LLLt", "LLLt",
        //    [](FakeFactorTree& t) {
        //        /// 3 light leptons
        //        /// 1 fake loose tau
        //        return (t.get_n_lep(LepClass::InclSignal) == 3 && t.get_n_lep(LepClass::InclLoose) == 0 &&
        //                t.get_n_lep(LepClass::Tau | LepClass::Incl | LepClass::Signal) == 0 &&
        //                t.get_n_lep(LepClass::Tau | LepClass::Incl | LepClass::Loose) >= 1);
        //    }),

        Cut("CR2_LLlt", "LLlt",
            [](FakeFactorTree& t) {
                /// 3 light leptons
                /// 1 fake loose tau
                return t.get_n_lep(LepClass::InclSignal) == 2 && t.get_n_lep(LepClass::InclLoose) == 1 &&
                       t.get_n_lep(LepClass::Tau | LepClass::Incl | LepClass::Signal) == 0 &&
                       t.get_n_lep(LepClass::Tau | LepClass::Incl | LepClass::Loose) >= 1;
            }),

        // 2L2T

        Cut("SR_LLTT", "LLTT",
            [](FakeFactorTree& t) {
                /// 3 light leptons
                /// 1 fake loose tau
                return (t.get_n_lep(LepClass::InclSignal) == 2 && t.get_n_lep(LepClass::InclLoose) == 0 &&
                        t.get_n_lep(LepClass::Tau | LepClass::Incl | LepClass::Signal) >= 2);
            }),

        // Cut("CR1_LLTt", "LLTt",
        //    [](FakeFactorTree& t) {
        //        /// 2 light leptons
        //        /// 1 fake loose tau and one signal tau
        //        return t.get_n_lep(LepClass::InclSignal) == 2 && t.get_n_lep(LepClass::InclLoose) == 0 &&
        //               t.get_n_lep(LepClass::Tau | LepClass::Incl | LepClass::Signal) == 1 &&
        //               t.get_n_lep(LepClass::Tau | LepClass::Incl | LepClass::Loose) >= 1;
        //    }),

        Cut("CR2_LLtt", "LLtt",
            [](FakeFactorTree& t) {
                /// 2 light leptons
                /// 2 fake loose tau
                return t.get_n_lep(LepClass::InclSignal) == 2 && t.get_n_lep(LepClass::InclLoose) == 0 &&
                       t.get_n_lep(LepClass::Tau | LepClass::Incl | LepClass::Signal) == 0 &&
                       t.get_n_lep(LepClass::Tau | LepClass::Incl | LepClass::Loose) >= 2;
            }),

    };
    std::vector<Cut> closure_regions{
        Cut("noZ", "Z_{veto}", [](FakeFactorTree& t) { return t.IsBaseZVeto(); }),
        Cut("noB", "N_{bjets}=0", [](FakeFactorTree& t) { return t.NumBJets() == 0; }),
        Cut("B", "N_{bjets}#geq1", [](FakeFactorTree& t) { return t.NumBJets() >= 1; }),
        Cut("TwoB", "N_{bjets}#geq2", [](FakeFactorTree& t) { return t.NumBJets() >= 2; }),

        Cut("noZ", "Z-veto", [](FakeFactorTree& t) { return t.IsBaseZVeto(); }),
        Cut("noZB", "Z-veto, N_{bjets}=0", [](FakeFactorTree& t) { return t.IsBaseZVeto() && t.NumBJets() == 0; }),
        Cut("noZ1B", "Z-veto, N_{bjets}#geq1", [](FakeFactorTree& t) { return t.IsBaseZVeto() && t.NumBJets() > 0; }),

        Cut("Z", "Z-selection", [](FakeFactorTree& t) { return t.IsBaseZ(); }),
        Cut("ZnoB", "Z-selection, N_{bjets}=0", [](FakeFactorTree& t) { return t.IsBaseZ() && t.NumBJets() == 0; }),
        Cut("Z1B", "Z-selection, N_{bjets}=1", [](FakeFactorTree& t) { return t.IsBaseZ() && t.NumBJets() > 0; }),

        Cut("ZZ", "ZZ-selection", [](FakeFactorTree& t) { return t.IsBaseZZ(); }),
        Cut("ZZnoB", "ZZ-selection, N_{bjets}=0", [](FakeFactorTree& t) { return t.IsBaseZZ() && t.NumBJets() == 0; }),
        Cut("ZZ1B", "ZZ-selection, N_{bjets}=1", [](FakeFactorTree& t) { return t.IsBaseZZ() && t.NumBJets() > 0; }),

    };
    // closure_regions.clear();

    {
        std::vector<Cut> Proc_FracCuts_copy = Proc_FracCuts;
        for (auto R : Proc_FracCuts_copy) {
            for (auto S : closure_regions) {
                if (S.getName().find("ZZ") != std::string::npos &&
                    (R.getName().find("T") != std::string::npos || R.getName().find("t") != std::string::npos))
                    continue;
                Proc_FracCuts.push_back(R && S);
            }
        }
    }
    ///     Define the histograms for the fake calculation here
    ///
    ///
    std::shared_ptr<TH1> lep_pt = XAMPP::MakeTH1({5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 70, 80, 100});
    lep_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
    lep_pt->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> tau_pt = std::make_shared<TH1D>("TauPt", "dummy;p_{T} [GeV];a.u.", 16, 20, 100);
    tau_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
    tau_pt->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> lep_eta = std::make_shared<TH1D>("Eta", "dummy;p_{T} [GeV];a.u.", 27, 0, 2.7);
    lep_eta->GetXaxis()->SetTitle("|#eta|");
    lep_eta->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> ele_pt_eta = XAMPP::MakeTH1({5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 100}, {0, 1.37, 1.52, 2.5});
    ele_pt_eta->GetXaxis()->SetTitle("p_{T} [GeV]");
    ele_pt_eta->GetYaxis()->SetTitle("|#eta|");

    std::shared_ptr<TH1> muo_pt_eta = XAMPP::MakeTH1({5, 8, 10, 12, 15, 20, 30, 40, 50, 60, 70, 80, 100}, {0, 1.05, 1.3, 2.0, 2.5, 2.7});
    muo_pt_eta->GetXaxis()->SetTitle("p_{T} [GeV]");
    muo_pt_eta->GetYaxis()->SetTitle("|#eta|");
    /// Template for the fake factors
    std::shared_ptr<TH1> tau_pt_eta = XAMPP::MakeTH1({20, 25, 30, 35, 40, 50, 60, 80, 100}, {0, 1.37, 1.52, 2.5});
    tau_pt_eta->GetXaxis()->SetTitle("p_{T} [GeV]");
    tau_pt_eta->GetYaxis()->SetTitle("|#eta|");

    std::shared_ptr<TH1> tau_pt_eta_frac = XAMPP::MakeTH1({20, 25, 30, 35, 40, 50}, {0, 1.37, 1.52, 2, 2.25, 2.5});
    tau_pt_eta_frac->GetXaxis()->SetTitle("p_{T} [GeV]");
    tau_pt_eta_frac->GetYaxis()->SetTitle("|#eta|");

    std::shared_ptr<TH1> tau_eta_frac = XAMPP::MakeTH1({0, 1.37, 1.52, 2, 2.25, 2.5});
    tau_eta_frac->GetXaxis()->SetTitle("|#eta|");
    tau_eta_frac->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> ele_pt_eta_dR =
        XAMPP::MakeTH1({5, 10, 15, 20, 30, 40, 50, 60, 80, 100}, {0, 1.37, 1.52, 2.5}, {0, 0.6, 2.5, max_dR});
    ele_pt_eta_dR->GetXaxis()->SetTitle("p_{T} [GeV]");
    ele_pt_eta_dR->GetYaxis()->SetTitle("|#eta|");
    ele_pt_eta_dR->GetZaxis()->SetTitle("#DeltaR(lep,lep)");

    std::shared_ptr<TH1> ele_pt_eta_M =
        XAMPP::MakeTH1({5, 10, 15, 20, 30, 40, 50, 60, 80, 100}, {0, 1.37, 1.52, 2.5}, {0, 45, 100, 300, 400});
    ele_pt_eta_M->GetXaxis()->SetTitle("p_{T} [GeV]");
    ele_pt_eta_M->GetYaxis()->SetTitle("|#eta|");
    ele_pt_eta_M->GetZaxis()->SetTitle("H_{T}^{lep} [GeV]");

    std::shared_ptr<TH1> muo_pt_eta_dR =
        XAMPP::MakeTH1({5, 7.5, 10, 15, 20, 25, 30, 35, 40, 50, 60, 80, 100}, {0, 0.1, 1.05, 1.3, 2.0, 2.5, 2.7}, {0, 0.6, max_dR});
    muo_pt_eta_dR->GetXaxis()->SetTitle("p_{T} [GeV]");
    muo_pt_eta_dR->GetYaxis()->SetTitle("|#eta|");
    muo_pt_eta_dR->GetZaxis()->SetTitle("#DeltaR(lep,lep)");

    std::shared_ptr<TH1> muo_pt_eta_M =
        XAMPP::MakeTH1({5, 7.5, 10, 15, 20, 25, 30, 35, 40, 50, 60, 80, 100}, {0, 0.1, 1.05, 1.3, 2.0, 2.5, 2.7}, {0, 45, 100, 300, 400});
    muo_pt_eta_M->GetXaxis()->SetTitle("p_{T} [GeV]");
    muo_pt_eta_M->GetYaxis()->SetTitle("|#eta|");
    muo_pt_eta_M->GetZaxis()->SetTitle("H_{T}^{lep} [GeV]");

    std::shared_ptr<TH1> deltaR = std::make_shared<TH1D>(RandomString(60).c_str(), "dummy.;#DeltaR(lep,lep);a.u.", 20, 0, 5.);

    std::shared_ptr<TH1> deltaRFlav = std::make_shared<TH1D>(RandomString(60).c_str(), "dummy.", 40, 0, 10);
    deltaRFlav->GetXaxis()->SetTitle("#DeltaR_{e/#mu}");
    deltaRFlav->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> deltaR_flav_selec = std::make_shared<TH1D>(RandomString(60).c_str(), "dummy.", 80, 0, 20);
    deltaR_flav_selec->GetXaxis()->SetTitle("#DeltaR^{e/#mu}_{signal/loose}");
    deltaR_flav_selec->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> lowPtdR = XAMPP::MakeTH1({5, 7.5, 10, 12.5, 15, 20, 25, 30, 40, 50, 100},
                                                  {0, 0.1, 0.2, 0.3, 0.6, max_dR, max_dR + 0.6, max_dR + 0.3, max_dR + 0.2, max_dR + 0.1});

    lowPtdR->GetXaxis()->SetTitle("p_{T} [GeV]");
    lowPtdR->GetYaxis()->SetTitle("#DeltaR_{signal/loose}");
    // lowPtdR->GetYaxis()->SetTitle("#pm#DeltaR_{e/#mu}");

    std::shared_ptr<TH1> lowPtProcFracDR =
        XAMPP::MakeTH1({0, 5, 10, 15, 20, 30, 40, 50, 60, 80, 100},
                       {0, 0.1, 0.2, 0.3, 0.6, max_dR, max_dR + 0.6, max_dR + 0.3, max_dR + 0.2, max_dR + 0.1});

    lowPtProcFracDR->GetXaxis()->SetTitle("p_{T} [GeV]");
    lowPtProcFracDR->GetYaxis()->SetTitle("#DeltaR_{signal/loose}");

    std::shared_ptr<TH1> deltaR_jet = std::make_shared<TH1D>(RandomString(60).c_str(), "dummy;#DeltaR(lep,jet);a.u.", 26, 0.4, 3);

    std::shared_ptr<TH1> HtLep_template = XAMPP::MakeTH1({0, 25, 50, 75, 100, 150, 200, 250, 300});
    HtLep_template = std::make_shared<TH1D>("HtLep", "Dummy;m_{eff} [GeV];a.u.", 20, 0, 500);
    HtLep_template->GetXaxis()->SetTitle("H_{T}^{lep} [GeV]");
    HtLep_template->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> HtJet_template = XAMPP::MakeTH1({0, 25, 50, 75, 100, 150, 200, 250, 300});
    HtJet_template->GetXaxis()->SetTitle("H_{T}^{jets} [GeV]");
    HtJet_template->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> Met_template = XAMPP::MakeTH1({0, 25, 50, 75, 100, 150, 200, 250, 300, 400});
    Met_template->GetXaxis()->SetTitle("E^{miss}_{T} [GeV]");
    Met_template->GetYaxis()->SetTitle("a.u.");

    ///
    ///
    ///
    // std::shared_ptr<TH1> meff_histo = XAMPP::MakeTH1({0, 50, 100, 200, 300, 400, 500, 600, 1000, 1500, 2000, 3000});
    // meff_histo->GetXaxis()->SetTitle("m_{eff} [GeV]");
    // meff_histo->GetYaxis()->SetTitle("a.u.");
    std::shared_ptr<TH1> meff_histo = std::make_shared<TH1D>("LowMeff", "Dummy;m_{eff} [GeV];a.u.", 40, 0, 1000);

    std::shared_ptr<TH1> n_jet_histo = std::make_shared<TH1D>(RandomString(60).c_str(), "dummy;N_{jets};a.u.", 11, 0, 11);
    std::shared_ptr<TH1> n_b_jet_histo = std::make_shared<TH1D>(RandomString(60).c_str(), "dummy;N_{b-jets};a.u.", 5, 0, 5);
    std::shared_ptr<TH1> mass_histo = std::make_shared<TH1D>(RandomString(60).c_str(), "dummy;#pmm_{#it{ee/#mu#mu}};a.u.", 80, -40, 40);

    std::shared_ptr<TH1> mass_pt_histo =
        XAMPP::MakeTH1({-20, -10, -7.5, -5, 0, 5, 7.5, 10, 20}, {5, 7.5, 10, 12.5, 15, 20, 25, 30, 35, 40, 50, 60, 70, 80, 100});
    mass_pt_histo->GetXaxis()->SetTitle("m_{#it{ll}} [GeV]");
    // mass_pt_histo->GetXaxis()->SetTitle("#pmm_{#it{ee/#mu#mu}} [GeV]");
    mass_pt_histo->GetYaxis()->SetTitle("p_{T} [GeV]");

    std::shared_ptr<TH1> lep_mass = XAMPP::MakeTH1({-20, -10, -7.5, -5, 0, 5, 7.5, 10, 20});
    lep_mass = std::make_shared<TH1D>("Mass", "dummy", 50, 0, 250);

    lep_mass->GetXaxis()->SetTitle("m_{#it{ll}} [GeV]");
    lep_mass->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> eta_dr = std::make_shared<TH2D>("Eta_dR", "dummy;|#eta|;#DeltaR(lep,lep)", 10, 0, 2.5, 16, 0., 4.);
    std::shared_ptr<TH1> eta_deta = std::make_shared<TH2D>("Eta_dEta", "dummy;|#eta|;|#Delta#eta|", 10, 0, 2.5, 13, 0, 2.5);
    std::shared_ptr<TH1> eta_dphi = std::make_shared<TH2D>("Eta_dPhi", "dummy;|#eta|;|#Delta#phi|", 10, 0, 2.5, 13., 0., std::acos(-1));

    // pt_pt_dR_histo->GetZaxis()->SetTitle("#pm#DeltaR_{e/#mu}");

    CanvasOptions canvas_opt = CanvasOptions()
                                   .ratioAxisTitle("Signal/Loose")
                                   .yAxisTitle("a.u.")
                                   .extraXtitleOffset(1.3)
                                   .extraYtitleOffset(0.4)
                                   .labelStatusTag("Simulation Internal")
                                   .legendStartX(-0.45)
                                   .doAtlasLabel(do_atlas)
                                   .logY(true)
                                   .ratioRangeConfig(PlotUtils::axisRangeConfig{PlotUtils::AxisRangeStrategy::asymmetric})
                                   .yMaxExtraPadding(0.7)
                                   .ratioExtraPadding(0.15)

                                   .overrideOutputDir(out_dir + "/FakeFactors/");

    CanvasOptions canvas_opt_proc_frac = CanvasOptions()
                                             .ratioAxisTitle("process fraction")
                                             .yAxisTitle("a.u.")
                                             .extraXtitleOffset(1.3)
                                             .extraYtitleOffset(0.4)
                                             .labelStatusTag("Simulation Internal")
                                             .ratioMax(1.1)
                                             .ratioMin(0.0)
                                             .legendStartX(-0.45)
                                             .doAtlasLabel(do_atlas)
                                             .logY(true)
                                             .yMaxExtraPadding(0.7)
                                             .overrideOutputDir(out_dir + "/ProcessFractions/");

    std::vector<std::shared_ptr<IFakeBundle>> fake_bundles;

    std::vector<PlotContent<TH1D>> fakefactors1D;
    std::vector<PlotContent<TH2D>> fakefactors2D;
    std::vector<PlotContent<TH3D>> fakefactors3D;

    std::vector<PlotContent<TH1D>> process_fractions;
    std::vector<PlotContent<TH2D>> process_fractions2D;
    std::vector<PlotContent<TH3D>> process_fractions3D;

    for (auto& CR : Proc_FracCuts) {
        int fake_type = LepClass::Fake & ~LepClass::Other;

        fake_type |= (CR.getName().find("SR") == std::string::npos ? LepClass::Loose : LepClass::Signal);

        for (int flavour : std::vector<int>{LepClass::Ele, LepClass::Muo}) {
            std::shared_ptr<ProcessFractionBundle> proc_frac;
            std::shared_ptr<ProcessFractionBundle2D> proc_frac2D;
            std::shared_ptr<ProcessFractionBundle3D> proc_frac3D;

            // proc_frac = std::make_shared<ProcessFractionBundle>(mass_histo, mySample, CR, flavour | fake_type | LepClass::Signal,
            // "Mass"); process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            // fake_bundles.push_back(proc_frac);

            proc_frac = std::make_shared<ProcessFractionBundle>(deltaR, mySample, CR, flavour | fake_type, "LooseSigLepDR");
            process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            fake_bundles.push_back(proc_frac);

            proc_frac = std::make_shared<ProcessFractionBundle>(deltaRFlav, mySample, CR, flavour | fake_type, "SFvsDFLepDR");
            process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            fake_bundles.push_back(proc_frac);

            proc_frac = std::make_shared<ProcessFractionBundle>(deltaR_flav_selec, mySample, CR, flavour | fake_type, "LooseSigFlavDR");
            process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            fake_bundles.push_back(proc_frac);

            /// Fraction against meff
            proc_frac = std::make_shared<ProcessFractionBundle>(meff_histo, mySample, CR, flavour | fake_type, "Meff");
            process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            fake_bundles.push_back(proc_frac);

            /// against Ht leptons
            proc_frac = std::make_shared<ProcessFractionBundle>(HtLep_template, mySample, CR, flavour | fake_type, "HtLep");
            process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            fake_bundles.push_back(proc_frac);

            /// against Ht jets
            proc_frac = std::make_shared<ProcessFractionBundle>(HtJet_template, mySample, CR, flavour | fake_type, "HtJet");
            process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            fake_bundles.push_back(proc_frac);
            /// against MET

            proc_frac = std::make_shared<ProcessFractionBundle>(Met_template, mySample, CR, flavour | fake_type, "Met");
            process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            fake_bundles.push_back(proc_frac);

            proc_frac = std::make_shared<ProcessFractionBundle>(n_jet_histo, mySample, CR, flavour | fake_type, "NJets");
            process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            fake_bundles.push_back(proc_frac);

            // if (CR.getName().find("B") == std::string::npos) {
            //     proc_frac = std::make_shared<ProcessFractionBundle>(n_b_jet_histo, mySample, CR, flavour | fake_type, "NBJets");
            //     process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            //     fake_bundles.push_back(proc_frac);
            // }
            //#################################
            //      Lepton kinematics
            //#################################

            // Pt
            proc_frac = std::make_shared<ProcessFractionBundle>(lep_pt, mySample, CR, flavour | fake_type, "Pt");
            process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            fake_bundles.push_back(proc_frac);

            proc_frac = std::make_shared<ProcessFractionBundle>(lep_eta, mySample, CR, flavour | fake_type, "AbsEta");
            process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            fake_bundles.push_back(proc_frac);

            proc_frac = std::make_shared<ProcessFractionBundle>(lep_mass, mySample, CR, flavour | fake_type, "Mass");
            process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            fake_bundles.push_back(proc_frac);

            // Pt -- abs(eta)
            proc_frac2D = std::make_shared<ProcessFractionBundle2D>(flavour == LepClass::Ele ? ele_pt_eta : muo_pt_eta, mySample, CR,
                                                                    flavour | fake_type, "Pt_vs_AbsEta");
            fake_bundles.push_back(proc_frac2D);
            process_fractions2D += proc_frac2D->get_plot(canvas_opt_proc_frac, CR.getTitle());

            proc_frac2D = std::make_shared<ProcessFractionBundle2D>(mass_pt_histo, mySample, CR, flavour | fake_type, "Mass_vs_Pt");
            fake_bundles.push_back(proc_frac2D);
            process_fractions2D += proc_frac2D->get_plot(canvas_opt_proc_frac, CR.getTitle());

            // proc_frac = std::make_shared<ProcessFractionBundle>(deltaR_jet, mySample, CR, flavour | fake_type, "JetDR");
            // process_fractions.push_back(proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle()));
            // fake_bundles.push_back(proc_frac);

            proc_frac2D = std::make_shared<ProcessFractionBundle2D>(lowPtProcFracDR, mySample, CR, flavour | fake_type,
                                                                    "Pt_vs_LooseSigLepDR_lowDRComplement");
            fake_bundles.push_back(proc_frac2D);
            process_fractions2D += proc_frac2D->get_plot(canvas_opt_proc_frac, CR.getTitle());

            // proc_frac3D = std::make_shared<ProcessFractionBundle3D>(pt_pt_dR_histo, mySample, CR, flavour | fake_type, "PtCloseByPtDR");
            // fake_bundles.push_back(proc_frac3D);
            // process_fractions3D += proc_frac3D->get_plot(canvas_opt_proc_frac, CR.getTitle());

            proc_frac3D = std::make_shared<ProcessFractionBundle3D>(flavour == LepClass::Ele ? ele_pt_eta_dR : muo_pt_eta_dR, mySample, CR,
                                                                    flavour | fake_type, "Pt_vs_AbsEta_vs_LooseSigLepDR");
            fake_bundles.push_back(proc_frac3D);
            process_fractions3D += proc_frac3D->get_plot(canvas_opt_proc_frac, CR.getTitle());

            proc_frac3D = std::make_shared<ProcessFractionBundle3D>(flavour == LepClass::Ele ? ele_pt_eta_M : muo_pt_eta_M, mySample, CR,
                                                                    flavour | fake_type, "Pt_vs_AbsEta_vs_Mll");
            fake_bundles.push_back(proc_frac3D);
            process_fractions3D += proc_frac3D->get_plot(canvas_opt_proc_frac, CR.getTitle());
        }
        if (CR.getName().find("T") != std::string::npos || CR.getName().find("t") != std::string::npos) {
            fake_type =
                (CR.getName().find("SR") == std::string::npos ? LepClass::Loose : LepClass::Signal) | (LepClass::Fake & ~LepClass::Other);

            for (int tau_multip : {LepClass::Tau1P, LepClass::Tau3P}) {
                /// Fraction against meff
                std::shared_ptr<ProcessFractionBundle> proc_frac =
                    std::make_shared<ProcessFractionBundle>(meff_histo, mySample, CR, tau_multip | fake_type, "Meff");
                process_fractions += proc_frac->get_plot(canvas_opt_proc_frac, CR.getTitle());
                fake_bundles.push_back(proc_frac);

                /// Fraction against pt
                std::shared_ptr<ProcessFractionBundle> pt_frac =
                    std::make_shared<ProcessFractionBundle>(tau_pt, mySample, CR, tau_multip | fake_type, "Pt");
                process_fractions += pt_frac->get_plot(canvas_opt_proc_frac, CR.getTitle());
                fake_bundles.push_back(pt_frac);

                std::shared_ptr<ProcessFractionBundle> eta_frac =
                    std::make_shared<ProcessFractionBundle>(tau_eta_frac, mySample, CR, tau_multip | fake_type, "AbsEta");
                process_fractions += eta_frac->get_plot(canvas_opt_proc_frac, CR.getTitle());
                fake_bundles.push_back(eta_frac);

                std::shared_ptr<ProcessFractionBundle2D> pt_eta_frac =
                    std::make_shared<ProcessFractionBundle2D>(tau_pt_eta_frac, mySample, CR, tau_multip | fake_type, "Pt_vs_AbsEta");
                process_fractions2D += pt_eta_frac->get_plot(canvas_opt_proc_frac, CR.getTitle());
                fake_bundles.push_back(pt_eta_frac);
            }
        }
    }
    for (int flavour : {LepClass::Ele, LepClass::Muo}) {
        for (int lep_type : std::vector<int>{LepClass::HF, LepClass::LF, LepClass::CONV, LepClass::Real, LepClass::Other}) {
            if (!type_flavour_defined(lep_type | flavour)) continue;
            ///

            std::shared_ptr<FakeBundle> fake_factor;
            std::shared_ptr<FakeBundle2D> fake_factor2D;
            std::shared_ptr<FakeBundle3D> fake_factor3D;

            /// Fake factor against lep-dR
            fake_factor = std::make_shared<FakeBundle>(deltaR, mySample, ff_selection, lep_type | flavour, "looseSigDR");
            fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            fake_bundles.push_back(fake_factor);

            fake_factor =
                std::make_shared<FakeBundleVanilla>(deltaR_flav_selec, mySample, ff_selection, lep_type | flavour, "looseSigFlavDR");
            fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            fake_bundles.push_back(fake_factor);

            fake_factor = std::make_shared<FakeBundleVanilla>(deltaRFlav, mySample, ff_selection, lep_type | flavour, "SFvsDFLepDR");
            fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            fake_bundles.push_back(fake_factor);

            fake_factor = std::make_shared<FakeBundle>(lep_mass, mySample, ff_selection, lep_type | flavour, "Mass");
            fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            fake_bundles.push_back(fake_factor);

            fake_factor = std::make_shared<FakeBundle>(meff_histo, mySample, ff_selection, lep_type | flavour, "Meff");
            fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            fake_bundles.push_back(fake_factor);

            fake_factor = std::make_shared<FakeBundle>(HtLep_template, mySample, ff_selection, lep_type | flavour, "HtLep");
            fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            fake_bundles.push_back(fake_factor);

            // fake_factor = std::make_shared<FakeBundle>(n_jet_histo, mySample, ff_selection, lep_type | flavour, "Njets");
            // fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            // fake_bundles.push_back(fake_factor);

            /// Fake factor against pt
            fake_factor = std::make_shared<FakeBundle>(lep_pt, mySample, ff_selection, lep_type | flavour, "Pt");
            fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            fake_bundles.push_back(fake_factor);

            /// Fake factor against eta
            fake_factor = std::make_shared<FakeBundle>(lep_eta, mySample, ff_selection, lep_type | flavour, "AbsEta");
            fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            fake_bundles.push_back(fake_factor);

            fake_factor2D =
                std::make_shared<FakeBundle2D>(lowPtdR, mySample, ff_selection, lep_type | flavour, "Pt_vs_FlavLepDR_lowDRComplement");
            fake_bundles.push_back(fake_factor2D);
            fakefactors2D += fake_factor2D->get_plot(canvas_opt, ff_sel_label);

            fake_factor2D = std::make_shared<FakeBundle2D>(mass_pt_histo, mySample, ff_selection, lep_type | flavour, "Mass_vs_Pt");
            fake_bundles.push_back(fake_factor2D);
            fakefactors2D += fake_factor2D->get_plot(canvas_opt, ff_sel_label);

            fake_factor3D = std::make_shared<FakeBundle3D>(flavour == LepClass::Ele ? ele_pt_eta_dR : muo_pt_eta_dR, mySample, ff_selection,
                                                           lep_type | flavour, "Pt_vs_AbsEta_LooseSigLepDR");
            fake_bundles.push_back(fake_factor3D);
            fakefactors3D += fake_factor3D->get_plot(canvas_opt, ff_sel_label);

            fake_factor3D = std::make_shared<FakeBundle3D>(flavour == LepClass::Ele ? ele_pt_eta_M : muo_pt_eta_M, mySample, ff_selection,
                                                           lep_type | flavour, "Pt_vs_AbsEta_Mll");
            fake_bundles.push_back(fake_factor3D);
            fakefactors3D += fake_factor3D->get_plot(canvas_opt, ff_sel_label);

            if (flavour == LepClass::Ele && lep_type == LepClass::LF) {
                fake_factor2D = std::make_shared<FakeBundle2D>(eta_dr, mySample, ff_selection, lep_type | flavour, "Eta_vs_FlavDR");
                fake_bundles.push_back(fake_factor2D);
                fakefactors2D += fake_factor2D->get_plot(canvas_opt, ff_sel_label);

                fake_factor2D = std::make_shared<FakeBundle2D>(eta_deta, mySample, ff_selection, lep_type | flavour, "Eta_vs_dEta");
                fake_bundles.push_back(fake_factor2D);
                fakefactors2D += fake_factor2D->get_plot(canvas_opt, ff_sel_label);

                fake_factor2D = std::make_shared<FakeBundle2D>(eta_dphi, mySample, ff_selection, lep_type | flavour, "Eta_vs_dPhi");
                fake_bundles.push_back(fake_factor2D);
                fakefactors2D += fake_factor2D->get_plot(canvas_opt, ff_sel_label);
            }
            /// Fake factor against pt and abs eta
            fake_factor2D = std::make_shared<FakeBundle2D>(flavour == LepClass::Ele ? ele_pt_eta : muo_pt_eta, mySample, ff_selection,
                                                           lep_type | flavour, "Pt_vs_AbsEta");
            fake_bundles.push_back(fake_factor2D);
            fakefactors2D += fake_factor2D->get_plot(canvas_opt, ff_sel_label);

            /// Fake factor against jet-dR
            // fake_factor = std::make_shared<FakeBundle>(deltaR_jet, mySample, ff_selection, lep_type | flavour, "jetDR");
            // fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            // fake_bundles.push_back(fake_factor);
        }
    }

    ///
    ///     Add the taus to the
    ///     fake-factor calculations

    for (int tau_multip : {LepClass::Tau1P, LepClass::Tau3P}) {
        for (int lep_type : {LepClass::HF, LepClass::LF, LepClass::Other, LepClass::Real, LepClass::GJ, LepClass::ELEC}) {
            if (!type_flavour_defined(lep_type | tau_multip)) continue;

            //
            //  Fake factor against p_{T}
            //
            std::shared_ptr<FakeBundle> fake_factor =
                std::make_shared<FakeBundle>(tau_pt, mySample, ff_selection, lep_type | tau_multip, "Pt");
            fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            fake_bundles.push_back(fake_factor);
            //
            //  Fake factor against eta for illustration purposes
            //
            fake_factor = std::make_shared<FakeBundle>(lep_eta, mySample, ff_selection, lep_type | tau_multip, "AbsEta");
            fakefactors1D += fake_factor->get_plot(canvas_opt, ff_sel_label);
            fake_bundles.push_back(fake_factor);

            std::shared_ptr<FakeBundle2D> pt_vs_eta =
                std::make_shared<FakeBundle2D>(tau_pt_eta, mySample, ff_selection, lep_type | tau_multip, "Pt_vs_AbsEta");
            fake_bundles.push_back(pt_vs_eta);
            fakefactors2D += pt_vs_eta->get_plot(canvas_opt, ff_sel_label);
        }
    }

    std::cout << "Will make " << fake_bundles.size() << " plots" << std::endl;
    /// Draw the histograms
    if (fakefactors1D.size() + fakefactors2D.size() + fakefactors3D.size()) {
        PlotUtils::startMultiPagePdfFile("AllFakeFactors", out_dir + "/FakeFactors/");
        std::sort(fakefactors1D.begin(), fakefactors1D.end(),
                  [](const PlotContent<TH1D>& a, const PlotContent<TH1D>& b) { return a.getFileName() < b.getFileName(); });

        std::sort(fakefactors2D.begin(), fakefactors2D.end(),
                  [](const PlotContent<TH2D>& a, const PlotContent<TH2D>& b) { return a.getFileName() < b.getFileName(); });

        std::sort(fakefactors3D.begin(), fakefactors3D.end(),
                  [](const PlotContent<TH3D>& a, const PlotContent<TH3D>& b) { return a.getFileName() < b.getFileName(); });

        for (auto& pc : fakefactors1D) {
            if (count_non_empty(pc)) DefaultPlotting::draw1D(pc);
        }
        for (auto& pc : fakefactors2D) { saveFakeFactor(pc); }
        for (auto& pc : fakefactors3D) { saveFakeFactor(pc); }

        PlotUtils::endMultiPagePdfFile("AllFakeFactors", out_dir + "/FakeFactors/");
    }
    if (process_fractions.size() + process_fractions2D.size() + process_fractions3D.size()) {
        std::sort(process_fractions.begin(), process_fractions.end(),
                  [](const PlotContent<TH1D>& a, const PlotContent<TH1D>& b) { return a.getFileName() < b.getFileName(); });

        std::sort(process_fractions2D.begin(), process_fractions2D.end(),
                  [](const PlotContent<TH2D>& a, const PlotContent<TH2D>& b) { return a.getFileName() < b.getFileName(); });
        std::sort(process_fractions3D.begin(), process_fractions3D.end(),
                  [](const PlotContent<TH3D>& a, const PlotContent<TH3D>& b) { return a.getFileName() < b.getFileName(); });

        PlotUtils::startMultiPagePdfFile("AllProcessFractions", out_dir + "/ProcessFractions/");
        for (auto& pc : process_fractions) {
            if (count_non_empty(pc)) { DefaultPlotting::draw1D(pc); }
        }
        for (auto& pc : process_fractions2D) { saveProcessFractions(pc); }
        for (auto& pc : process_fractions3D) { saveProcessFractions(pc); }
        PlotUtils::endMultiPagePdfFile("AllProcessFractions", out_dir + "/ProcessFractions/");
    }

    std::shared_ptr<TFile> out_file = make_out_file(out_root_file);

    for (auto& ff : fake_bundles) { ff->write_file(out_file.get()); }
}
