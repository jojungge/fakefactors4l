#include <FakeFactors4L/ScaleFactorBundle.h>
#include <FakeFactors4L/ScaleFactorTree.h>
#include <FakeFactors4L/Utils.h>

///
#include <TFile.h>
#include <TH2D.h>
#include <TSystem.h>
#include <TTree.h>
#include <cstring>
#include <iostream>
#include <memory>
#include <string>

using namespace SUSY4L;

/// used for faketypes that have no own scalefactor
std::shared_ptr<TH1> CreateDummyHisto() {
    std::shared_ptr<TH1> histo = std::make_shared<TH1D>("dummy", "dummy;p_{T} [GeV];a.u.", 1, 0, 5000);
    histo->SetBinContent(1, 1);
    histo->SetBinError(1, 0.1);
    return histo;
}

int main(int argc, char* argv[]) {
    SetAtlasStyle();
    std::vector<std::string> in_files_MC;
    std::vector<std::string> in_files_data;

    /// I'm too lazy to call SumW for each histo type
    XAMPP::HistoTemplates::getHistoTemplater();

    /// Flags to check if the data files have been resettet
    bool in_file_MC_reset = false;
    bool in_file_data_reset = false;
    /// Draw the atlas label
    bool do_atlas = true;
    /// Include the other contributions
    bool include_other = true;

    double lumi = 139.;
    std::string tree_name = "DebugTree_Nominal";
    std::string out_dir = "Plots/ScalefactorPlots/";
    std::string out_root_file = "sf_results.root";

    for (int k = 1; k < argc; ++k) {
        std::string current_arg(argv[k]);
        if (current_arg == "--inFileMC" && k + 1 < argc) {
            if (!in_file_MC_reset) in_files_MC.clear();
            in_file_MC_reset = true;
            in_files_MC.push_back(argv[k + 1]);
        } else if (current_arg == "--inFileData" && k + 1 < argc) {
            if (!in_file_data_reset) in_files_data.clear();
            in_file_data_reset = true;
            in_files_data.push_back(argv[k + 1]);
        } else if (current_arg == "--outDir" && k + 1 < argc) {
            out_dir = argv[k + 1];
        } else if (current_arg == "--treeName" && k + 1 < argc) {
            tree_name = argv[k + 1];
        } else if (current_arg == "--outFile" && k + 1 < argc) {
            out_root_file = argv[k + 1];
        } else if (current_arg == "--lumi" && k + 1 < argc) {
            lumi = atof(argv[k + 1]);
        } else if (current_arg == "--noATLAS") {
            do_atlas = false;
        } else if (current_arg == "--noOther") {
            include_other = false;
        }
    }

    ScaleFactorTree::set_lumi(lumi);

    unsigned int truth_types = LepClass::Incl;
    if (!include_other) truth_types &= ~LepClass::Other;
    //     IScaleFactorBundle::set_non_signal_mc(truth_types);

    // Make the two sample lists for MC and data
    Sample<ScaleFactorTree> MCSample("MC");
    for (auto& in_file : in_files_MC) { MCSample.addFile(in_file, tree_name); }

    Sample<ScaleFactorTree> dataSample("data");
    for (auto& in_file : in_files_data) { dataSample.addFile(in_file, tree_name); }

    /// Ttbar CR for light leptons
    SFCut CRttbar("CRttbar", "t#bar{t}+#it{l}", [](ScaleFactorTree& t) { return (t.IsCRttbar()); });
    /// TTbar CR for taus
    SFCut CRttbar_tau("CRttbar_tau", "t#bar{t}+#tau", [](ScaleFactorTree& t) { return t.IsCRttbar_tau(); });

    /// LF scale-factors for taus
    SFCut Zmumu_tau("Zmumu_tau", "Z#rightarrow#mu#mu+#tau", [](ScaleFactorTree& t) { return t.IsZmumu_tau(); });

    // LF scale-factors for electrons
    SFCut WmunuSC("WmunuSC", "W#rightarrow#mu#nu (SC)", [](ScaleFactorTree& t) { return t.IsWmunu(); });

    SFCut Wmunu_dEta("WmunuSC_dEta", "W#rightarrow#mu#nu (SC), no #Delta#eta", [](ScaleFactorTree& t) {
        if (t.Met() < 30) return false;
        if (t.NumBJets() || t.NumJets() > 3) return false;
        if (t.get_n_lep(LepClass::LightLep | LepClass::Baseline | LepClass::Incl) != 2) return false;
        if (t.get_n_lep(LepClass::Muo | LepClass::Signal | LepClass::Incl) != 1) return false;
        if (t.get_n_lep(LepClass::Ele | LepClass::Baseline | LepClass::Incl) != 1) return false;
        if (t.Ele_q(0) + t.Muo_q(0) == 0) return false;
        if (t.Muo_pt(0) < 28) return false;
        if (t.get_mt_met(0, LepClass::Muo) < 50) return false;
        return true;
    });

    SFCut Wmunu_Mll("WmunuSC_Mll", "W#rightarrow#mu#nu (SC), no m_{#it{l}#it{l}}", [](ScaleFactorTree& t) {
        if (t.Met() < 30) return false;
        if (t.NumBJets() || t.NumJets() > 3) return false;
        if (t.get_n_lep(LepClass::LightLep | LepClass::Baseline | LepClass::Incl) != 2) return false;
        if (t.get_n_lep(LepClass::Muo | LepClass::Signal | LepClass::Incl) != 1) return false;
        if (t.get_n_lep(LepClass::Ele | LepClass::Baseline | LepClass::Incl) != 1) return false;
        if (t.Ele_q(0) + t.Muo_q(0) == 0) return false;
        if (t.Muo_pt(0) < 28) return false;
        if (t.get_mt_met(0, LepClass::Muo) < 50) return false;
        if (std::fabs(t.get_delta_eta(0, 1)) < 0.2) return false;
        return true;
    });

    SFCut Wmunu_no_q("WmunuSC_qTP", "W#rightarrow#mu#nu", [](ScaleFactorTree& t) {
        if (t.Met() < 30) return false;
        if (t.NumBJets() || t.NumJets() > 3) return false;
        if (t.get_n_lep(LepClass::LightLep | LepClass::Baseline | LepClass::Incl) != 2) return false;
        if (t.get_n_lep(LepClass::Muo | LepClass::Signal | LepClass::Incl) != 1) return false;
        if (t.get_n_lep(LepClass::Ele | LepClass::Baseline | LepClass::Incl) != 1) return false;
        if (t.Muo_pt(0) < 28) return false;
        if (t.get_mt_met(0, LepClass::Muo) < 50) return false;
        if (std::fabs(t.get_delta_eta(0, 1)) < 0.2) return false;
        if ((t.get_p4(0, LepClass::LepFlav) + t.get_p4(1, LepClass::LepFlav)).M() < 20) return false;

        return true;
    });

    SFCut Wmunu_manual("WmunuSC_manual", "W#rightarrow#mu#nu (SC)", [](ScaleFactorTree& t) {
        if (t.Met() < 30) return false;
        if (t.NumBJets() || t.NumJets() > 3 || !t.MuonTrigger()) return false;
        if (t.get_n_lep(LepClass::LightLep | LepClass::Baseline | LepClass::Incl) != 2) return false;
        if (t.get_n_lep(LepClass::Muo | LepClass::Signal | LepClass::Incl) != 1) return false;
        if (t.get_n_lep(LepClass::Ele | LepClass::Baseline | LepClass::Incl) != 1) return false;
        if (t.Muo_pt(0) < 28) return false;
        if (t.Ele_q(0) + t.Muo_q(0) == 0) return false;

        if (t.get_mt_met(0, LepClass::Muo) < 50) return false;
        if (std::fabs(t.get_delta_eta(0, 1)) < 0.2) return false;
        if ((t.get_p4(0, LepClass::LepFlav) + t.get_p4(1, LepClass::LepFlav)).M() < 20) return false;

        return true;
    });
    std::shared_ptr<TH1> ele_pt = XAMPP::MakeTH1({5, 8, 10, 12, 15, 20, 30, 60, 100});
    ele_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
    ele_pt->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> ele_pt_lf = XAMPP::MakeTH1({5, 8, 10, 12, 15, 20, 60, 100});
    ele_pt_lf->GetXaxis()->SetTitle("p_{T} [GeV]");
    ele_pt_lf->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> muo_pt = XAMPP::MakeTH1({5, 8, 10, 12, 15, 20, 30, 100});
    muo_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
    muo_pt->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> ele_eta = XAMPP::MakeTH1({0, 1.37, 1.52, 2.5});
    ele_eta->GetXaxis()->SetTitle("|#eta|");
    ele_eta->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> muo_eta = XAMPP::MakeTH1({0, 1.05, 1.3, 2.0, 2.5, 2.7});
    muo_eta->GetXaxis()->SetTitle("|#eta|");
    muo_eta->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> tau_lf_pt = XAMPP::MakeTH1({20, 30, 40, 50, 100});
    tau_lf_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
    tau_lf_pt->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> tau_hf_pt = XAMPP::MakeTH1({20, 30, 40, 50});
    tau_hf_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
    tau_hf_pt->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> tau_eta = XAMPP::MakeTH1({0, 1.37, 1.52, 2.5});
    tau_eta->GetXaxis()->SetTitle("|#eta|");
    tau_eta->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> deltaR = XAMPP::MakeTH1({10, 8, 6, 5.5, 5.25, 0, 0.25, 0.5, 1, 3, 5});
    deltaR->GetXaxis()->SetTitle("#DeltaR_{signal/loose}");
    deltaR->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> deltaRflav = XAMPP::MakeTH1({10, 8, 6, 5.5, 5.25, 0, 0.25, 0.5, 1, 3, 5});
    deltaRflav->GetXaxis()->SetTitle("#DeltaR_{e/#mu}");
    deltaRflav->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> n_jets = std::make_shared<TH1D>("NJets", "Dummy;N_{jets};a.u.", 10, 0, 10);
    std::shared_ptr<TH1> Mt = std::make_shared<TH1D>("MT", "Dummy;m_{T} [GeV];a.u.", 50, 0, 150);
    std::shared_ptr<TH1> Mll = std::make_shared<TH1D>("Mll", "Dummy;m_{#it{ll}} [GeV];a.u.", 50, 0, 150);
    std::shared_ptr<TH1> Met = std::make_shared<TH1D>("Met", "Dummy;E^{miss}_{T} [GeV];a.u.", 50, 0, 150);

    std::shared_ptr<TH1> d_phi_met = std::make_shared<TH1D>("dphi_met", "Dummy;|#Delta#phi(E^{miss}_{T},lep)|;a.u.", 20, 0, std::acos(-1));
    std::shared_ptr<TH1> d_eta = std::make_shared<TH1D>("deta", "Dummy;|#Delta#eta|;a.u.", 25, 0, 5.);
    std::shared_ptr<TH1> d_phi = std::make_shared<TH1D>("dphi", "Dummy;|#Delta#phi|;a.u.", 20, 0, std::acos(-1));
    std::shared_ptr<TH1> d_cos = std::make_shared<TH1D>("cosDphi", "Dummy;#Sigmacos(#Delta#phi);a.u.", 20, -2, 2.);
    std::shared_ptr<TH1> d_rj = std::make_shared<TH1D>("dRJ", "Dummy;#DeltaR(lep,jet);a.u.", 20, -0.1, 5.);
    std::shared_ptr<TH1> d_rl = std::make_shared<TH1D>("dRL", "Dummy;#DeltaR(lep,lep);a.u.", 20, -0.1, 5.);
    std::shared_ptr<TH1> q_tp = std::make_shared<TH1D>("qTP", "Dummy;q_{tag}#timesq_{probe}", 2, -1, 1);
    CanvasOptions canvas_opt_sf = CanvasOptions()
                                      .ratioAxisTitle("Data / MC")
                                      .yAxisTitle("fake factor")
                                      .extraXtitleOffset(1.3)
                                      .extraYtitleOffset(0.4)
                                      .labelStatusTag("Internal")
                                      .legendStartX(-0.45)
                                      .doAtlasLabel(do_atlas)
                                      .yMin(0)
                                      .ratioRangeConfig(PlotUtils::axisRangeConfig{PlotUtils::AxisRangeStrategy::asymmetric})
                                      .yMaxExtraPadding(1.15)
                                      .ratioExtraPadding(0.1)
                                      .overrideOutputDir(out_dir);

    CanvasOptions canvas_opt_ff = CanvasOptions()
                                      .ratioAxisTitle("Signal / Loose")
                                      .yAxisTitle("a.u.")
                                      .extraXtitleOffset(1.3)
                                      .extraYtitleOffset(0.4)
                                      .labelStatusTag("Internal")
                                      .legendStartX(-0.45)
                                      .doAtlasLabel(do_atlas)
                                      .yMaxExtraPadding(0.85)
                                      .ratioRangeConfig(PlotUtils::axisRangeConfig{PlotUtils::AxisRangeStrategy::asymmetric})
                                      .ratioExtraPadding(0.05)
                                      .overrideOutputDir(out_dir);

    CanvasOptions canvas_opt_proc_frac = CanvasOptions()
                                             .ratioAxisTitle("Process fraction")
                                             .yAxisTitle("a.u.")
                                             .extraXtitleOffset(1.3)
                                             .extraYtitleOffset(0.4)
                                             .labelStatusTag("Simulation Internal")
                                             .legendStartX(-0.45)
                                             .doAtlasLabel(do_atlas)
                                             .yMaxExtraPadding(0.7)
                                             .ratioMax(1.1)
                                             .ratioMin(0.0)
                                             .overrideOutputDir(out_dir);

    CanvasOptions canvas_opt_2d = CanvasOptions()
                                      .ratioAxisTitle("Data/MC")
                                      .yAxisTitle("Data/MC")
                                      .extraXtitleOffset(1.3)
                                      .extraYtitleOffset(0.4)
                                      .labelStatusTag("Simulation Internal")
                                      .legendStartX(-0.45)
                                      .doAtlasLabel(do_atlas)
                                      .yMaxExtraPadding(0.7)
                                      .overrideOutputDir(out_dir);

    std::vector<std::shared_ptr<IScaleFactorBundle>> sf_bundles;
    std::vector<PlotContent<TH1D>> scalefactors1D;
    std::vector<PlotContent<TH2D>> scalefactors2D;

    std::function<void(std::shared_ptr<ScaleFactorBundle>)> add_sf = [&scalefactors1D, &sf_bundles, &canvas_opt_ff,
                                                                      &canvas_opt_sf](std::shared_ptr<ScaleFactorBundle> sf) {
        sf_bundles.push_back(sf);
        scalefactors1D += sf->get_plot_mc(canvas_opt_ff);
        // scalefactors1D += sf->get_plot_mc_bkg(canvas_opt_ff);
        scalefactors1D += sf->get_plot_data(canvas_opt_ff);
        scalefactors1D += sf->CreateSFPlot(canvas_opt_sf);

        scalefactors1D += sf->get_data_mc_plot(canvas_opt_sf, LepClass::Baseline);
        scalefactors1D += sf->get_data_mc_plot(canvas_opt_sf, LepClass::Signal);
        scalefactors1D += sf->get_data_mc_plot(canvas_opt_sf, LepClass::Loose);
    };
    std::function<void(std::shared_ptr<TagProbeProcFracBundle>)> add_proc_frac =
        [&scalefactors1D, &sf_bundles, &canvas_opt_proc_frac, &canvas_opt_sf](std::shared_ptr<TagProbeProcFracBundle> sf) {
            sf_bundles.push_back(sf);
            scalefactors1D += sf->get_plot(canvas_opt_proc_frac);
        };

    std::function<void(std::shared_ptr<ScaleFactorBundle2D>)> add_sf2D = [&scalefactors2D, &sf_bundles, &canvas_opt_ff,
                                                                          &canvas_opt_sf](std::shared_ptr<ScaleFactorBundle2D> sf) {
        sf_bundles.push_back(sf);
        scalefactors2D += sf->get_plot_mc(canvas_opt_ff);
        scalefactors2D += sf->get_plot_data(canvas_opt_ff);
        scalefactors2D += sf->CreateSFPlot(canvas_opt_sf);
    };

    for (int lep : {LepClass::Ele, LepClass::Muo}) {
        std::shared_ptr<TH1> lep_pt = lep == LepClass::Ele ? ele_pt : muo_pt;
        std::shared_ptr<TH1> lep_eta = lep == LepClass::Ele ? ele_eta : muo_eta;
        SFCut& LF_Cut = WmunuSC;
        for (int s : {LepClass::Baseline, LepClass::Signal, LepClass::Loose}) {
            add_proc_frac(std::make_shared<TagProbeProcFracBundle>(lep_pt, MCSample, CRttbar, lep | truth_types | s, "Pt"));
            add_proc_frac(std::make_shared<TagProbeProcFracBundle>(lep_eta, MCSample, CRttbar, lep | truth_types | s, "AbsEta"));
            if (LepClass::Ele == lep) {
                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(lep_pt, MCSample, LF_Cut, lep | truth_types | s, "Pt"));
                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(lep_eta, MCSample, LF_Cut, lep | truth_types | s, "AbsEta"));

                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(Mll, MCSample, LF_Cut, lep | truth_types | s, "Mll"));
                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(d_phi, MCSample, LF_Cut, lep | truth_types | s, "dPhi"));
                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(d_eta, MCSample, LF_Cut, lep | truth_types | s, "dEta"));
                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(d_rl, MCSample, LF_Cut, lep | truth_types | s, "dRLep"));
                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(d_cos, MCSample, LF_Cut, lep | truth_types | s, "CosDphi"));

                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(d_phi, MCSample, Wmunu_dEta, lep | truth_types | s, "dPhi", 0));
                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(d_eta, MCSample, Wmunu_dEta, lep | truth_types | s, "dEta", 0));
                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(d_rl, MCSample, Wmunu_dEta, lep | truth_types | s, "dRLep", 0));
                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(Mll, MCSample, Wmunu_Mll, lep | truth_types | s, "Mll", 0));
                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(q_tp, MCSample, Wmunu_no_q, lep | truth_types | s, "QTP", 0));

                add_proc_frac(std::make_shared<TagProbeProcFracBundle>(lep_pt, MCSample, Wmunu_manual, lep | truth_types | s, "Pt"));
            }
        }

        if (LepClass::Ele == lep) {
            add_sf(std::make_shared<ScaleFactorBundle>(ele_pt_lf, MCSample, dataSample, WmunuSC, lep | LepClass::LF, "Pt"));
            add_sf(std::make_shared<ScaleFactorBundle>(lep_eta, MCSample, dataSample, WmunuSC, lep | LepClass::LF, "AbsEta"));
        }
        add_sf(std::make_shared<ScaleFactorBundle>(lep_pt, MCSample, dataSample, CRttbar, lep | LepClass::HF, "Pt"));
        add_sf(std::make_shared<ScaleFactorBundle>(lep_eta, MCSample, dataSample, CRttbar, lep | LepClass::HF, "AbsEta"));
        add_sf(std::make_shared<ScaleFactorBundle>(deltaR, MCSample, dataSample, CRttbar, lep | LepClass::HF, "SigLoose_dR"));
        add_sf(std::make_shared<ScaleFactorBundle>(deltaRflav, MCSample, dataSample, CRttbar, lep | LepClass::HF, "Flav_dR"));
    }

    for (int prong : {LepClass::Tau1P, LepClass::Tau3P}) {
        /// Add the process fraction plots
        for (int s : {LepClass::Baseline, LepClass::Signal, LepClass::Loose}) {
            add_proc_frac(std::make_shared<TagProbeProcFracBundle>(tau_hf_pt, MCSample, CRttbar_tau, prong | truth_types | s, "Pt"));
            add_proc_frac(std::make_shared<TagProbeProcFracBundle>(tau_eta, MCSample, CRttbar_tau, prong | truth_types | s, "AbsEta"));
            add_proc_frac(std::make_shared<TagProbeProcFracBundle>(tau_lf_pt, MCSample, Zmumu_tau, prong | truth_types | s, "Pt"));
            add_proc_frac(std::make_shared<TagProbeProcFracBundle>(tau_eta, MCSample, Zmumu_tau, prong | truth_types | s, "AbsEta"));
        }
        add_sf(std::make_shared<ScaleFactorBundle>(tau_hf_pt, MCSample, dataSample, CRttbar_tau, prong | LepClass::HF, "Pt"));
        add_sf(std::make_shared<ScaleFactorBundle>(tau_lf_pt, MCSample, dataSample, Zmumu_tau, prong | LepClass::LF, "Pt"));

        add_sf(std::make_shared<ScaleFactorBundle>(tau_eta, MCSample, dataSample, CRttbar_tau, prong | LepClass::HF, "AbsEta"));
        add_sf(std::make_shared<ScaleFactorBundle>(tau_eta, MCSample, dataSample, Zmumu_tau, prong | LepClass::HF, "AbsEta"));
    }
    std::sort(scalefactors1D.begin(), scalefactors1D.end(),
              [](const PlotContent<TH1D>& a, const PlotContent<TH1D>& b) { return a.getFileName() < b.getFileName(); });

    std::sort(scalefactors2D.begin(), scalefactors2D.end(),
              [](const PlotContent<TH2D>& a, const PlotContent<TH2D>& b) { return a.getFileName() < b.getFileName(); });

    PlotUtils::startMultiPagePdfFile("AllScaleFactors", out_dir);

    for (auto& pc : scalefactors1D) {
        if (count_non_empty(pc)) { DefaultPlotting::draw1D(pc); }
    }

    PlotUtils::endMultiPagePdfFile("AllScaleFactors", out_dir);

    std::shared_ptr<TFile> out_file = make_out_file(out_root_file);
    for (auto& sf : sf_bundles) { sf->write_file(out_file.get()); }

    /// Write the dummy scale-factor histograms
    TDirectory* outdir = XAMPP::mkdir("SF/ScaleFactors/", out_file.get());

    for (std::string sf : {
             //"Electron_LF_Pt",
             "Electron_CONV_Pt",
             "Electron_Other_Pt",
             "Muon_LF_Pt",
             "Muon_Other_Pt",
             "Tau1P_ELEC_Pt",
             "Tau3P_ELEC_Pt",
             "Tau1P_GJ_Pt",
             "Tau3P_GJ_Pt",
             "Tau1P_Other_Pt",
             "Tau3P_Other_Pt",
         }) {
        outdir->WriteObject(CreateDummyHisto().get(), sf.c_str());
    }

    return EXIT_SUCCESS;
}
