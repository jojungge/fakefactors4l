#include <FakeFactors4L/FakeBundle.h>
#include <FakeFactors4L/FakeClosureTree.h>
#include <FakeFactors4L/Utils.h>

///

#include <TFile.h>
#include <TH2D.h>
#include <TSystem.h>
#include <TTree.h>
#include <cstring>
#include <iostream>
#include <memory>
#include <string>

// using namespace XAMPP;
using namespace SUSY4L;

bool load_ff(const std::shared_ptr<TFile>& ff_file, std::shared_ptr<FakeWeighter> weighter, const std::string& CR, bool load_zjets_ff) {
    if (CR.find("l") != std::string::npos) {
        if (!weighter->load_histos(LepClass::Ele, ff_file, load_zjets_ff ? "Pt_vs_AbsEta_Mll" : "Pt_vs_AbsEta_LooseSigLepDR",
                                   "NoTrigger__AND__LepIncl", "Pt_vs_AbsEta_vs_LooseSigLepDR", CR, [](BaseFakeTree& t, size_t i) {
                                       return t.is_Lep(i, LepClass::LightLep | LepClass::Incl | LepClass::Baseline) &&
                                              t.get_dR_minLep(i) > 0.6;
                                   }))
            return false;

        if (!weighter->load_histos(LepClass::Muo, ff_file, load_zjets_ff ? "Pt_vs_AbsEta_Mll" : "Pt_vs_AbsEta_LooseSigLepDR",
                                   "NoTrigger__AND__LepIncl", "Pt_vs_AbsEta_vs_LooseSigLepDR", CR, [](BaseFakeTree& t, size_t i) {
                                       return t.is_Lep(i, LepClass::LightLep | LepClass::Incl | LepClass::Baseline) &&
                                              t.get_dR_minLep(i) > 0.6;
                                   }))
            return false;

        if (!weighter->load_histos(LepClass::Ele, ff_file, "Pt_vs_FlavLepDR_lowDRComplement", "NoTrigger__AND__LepIncl", "Mass_vs_Pt", CR,
                                   [](BaseFakeTree& t, size_t i) {
                                       return t.is_Lep(i, LepClass::LightLep | LepClass::Incl | LepClass::Baseline) &&
                                              t.get_dR_minLep(i) < 0.6;
                                   }))
            return false;

        if (!weighter->load_histos(LepClass::Muo, ff_file, "Pt_vs_FlavLepDR_lowDRComplement", "NoTrigger__AND__LepIncl", "Mass_vs_Pt", CR,
                                   [](BaseFakeTree& t, size_t i) {
                                       return t.is_Lep(i, LepClass::LightLep | LepClass::Incl | LepClass::Baseline) &&
                                              t.get_dR_minLep(i) < 0.6;
                                   }))
            return false;
    }

    if (CR.find("t") != std::string::npos || CR.find("T") != std::string::npos) {
        for (int tau_multip : {LepClass::Tau1P, LepClass::Tau3P}) {
            if (!weighter->load_histos(tau_multip, ff_file, "Pt_vs_AbsEta", "NoTrigger__AND__LepIncl", "Pt_vs_AbsEta", CR)) return false;
        }
    }
    if (weighter->n_weigthers() == 0) {
        std::cerr << "No weight has been added for CR " << CR << std::endl;
        return false;
    }
    return true;
}

bool setup_closure(std::shared_ptr<TH1> histo, const std::shared_ptr<TFile>& ff_file, Sample<FakeClosureTree>& smp,
                   std::vector<std::shared_ptr<FakeClosureHisto>>& closure_tests, std::vector<PlotContent<TH1D>>& plots,
                   const std::string& h_name, ClosureCut extra_cut, unsigned int lep_bits, CanvasOptions canvas_opt,
                   const std::string& cr_sub_sel = "", bool load_zjets_ff = false) {
    const int emu_bits_signal = LepClass::LightLep | LepClass::Incl | LepClass::Signal;
    const int emu_bits_loose = LepClass::LightLep | LepClass::Incl | LepClass::Loose;

    const int tau_bits_signal = LepClass::Tau | LepClass::Incl | LepClass::Signal;
    const int tau_bits_loose = LepClass::Tau | LepClass::Incl | LepClass::Loose;

    /// Light lepton Closure
    ClosureCut SR_LLLL("LLLL", "N_{signal}=4", [emu_bits_signal](FakeClosureTree& t) {
        return (t.ElecTrigger() || t.MuonTrigger()) && t.get_n_lep(emu_bits_signal) >= 4;
    });
    ClosureCut CR_LLLl("LLLl", "N_{signal}=3,N_{loose}#geq1", [emu_bits_signal, emu_bits_loose](FakeClosureTree& t) {
        return (t.ElecTrigger() || t.MuonTrigger()) && t.get_n_lep(emu_bits_signal) == 3 && t.get_n_lep(emu_bits_loose) >= 1;
    });
    ClosureCut CR_LLll("LLll", "N_{signal}=2,N_{loose}#geq2", [emu_bits_signal, emu_bits_loose](FakeClosureTree& t) {
        return (t.ElecTrigger() || t.MuonTrigger()) && t.get_n_lep(emu_bits_signal) == 2 && t.get_n_lep(emu_bits_loose) >= 2;
    });

    std::shared_ptr<FakeClosureHisto> LLLL_closure = std::make_shared<FakeClosureHisto>(
        histo, lep_bits, smp, SR_LLLL && extra_cut, CR_LLLl && extra_cut, CR_LLll && extra_cut, h_name + extra_cut.getName());

    closure_tests += LLLL_closure;

    if (!load_ff(ff_file, LLLL_closure->CR1_weighter(), "CR2_LLll" + cr_sub_sel, load_zjets_ff) ||
        !load_ff(ff_file, LLLL_closure->CR2_weighter(), "CR2_LLll" + cr_sub_sel, load_zjets_ff)) {
        return false;
    }
    plots +=
        LLLL_closure->get_plot({"Closure LLLL, " + smp.getName(), "FF: N^{baseline}#geq1", "Test: " + extra_cut.getTitle()}, canvas_opt);

    // 3 lepton reference closure
    ClosureCut SR_LLL("LLL", "N_{signal}=3", [emu_bits_signal, emu_bits_loose](FakeClosureTree& t) {
        return /*(t.ElecTrigger() || t.MuonTrigger()) && */ t.get_n_lep(emu_bits_signal) == 3 && t.get_n_lep(emu_bits_loose) == 0;
    });
    ClosureCut CR_LLl("LLl", "N_{signal}=2, N_{loose}=1", [emu_bits_signal, emu_bits_loose](FakeClosureTree& t) {
        return /*(t.ElecTrigger() || t.MuonTrigger()) && */ t.get_n_lep(emu_bits_signal) == 2 && t.get_n_lep(emu_bits_loose) == 1;
    });
    ClosureCut CR_Lll("Lll", "N_{signal}=1, N_{loose}=2", [emu_bits_signal, emu_bits_loose](FakeClosureTree& t) {
        return /*(t.ElecTrigger() || t.MuonTrigger()) &&*/ t.get_n_lep(emu_bits_signal) == 1 && t.get_n_lep(emu_bits_loose) == 2;
    });

    std::shared_ptr<FakeClosureHisto> LLL_closure = std::make_shared<FakeClosureHisto>(
        histo, lep_bits, smp, SR_LLL && extra_cut, CR_LLl && extra_cut, CR_Lll && extra_cut, h_name + extra_cut.getName());

    closure_tests += LLL_closure;
    if (!load_ff(ff_file, LLL_closure->CR1_weighter(), "CR2_Lll" + cr_sub_sel, load_zjets_ff) ||
        !load_ff(ff_file, LLL_closure->CR2_weighter(), "CR2_Lll" + cr_sub_sel, load_zjets_ff)) {
        return false;
    }
    plots += LLL_closure->get_plot({"Closure LLL, " + smp.getName(), "FF: N^{baseline}#geq1", "Test: " + extra_cut.getTitle()}, canvas_opt);

    //
    // 3 lepton 1 tau closure
    //
    ClosureCut SR_LLLT("LLLT", "N_{e/#mu}^{signal}=3, N_{#tau}^{signal}#geq1",
                       [emu_bits_signal, emu_bits_loose, tau_bits_signal](FakeClosureTree& t) {
                           return (t.ElecTrigger() || t.MuonTrigger()) && t.get_n_lep(emu_bits_signal) == 3 &&
                                  t.get_n_lep(tau_bits_signal) >= 1 && t.get_n_lep(emu_bits_loose) == 0;
                       });
    ClosureCut CR1_LLLT("CR1_LLLT", "N_{e/#mu}^{signal}=3, N_{#tau}^{signal}#geq1",
                        [emu_bits_signal, emu_bits_loose, tau_bits_signal, tau_bits_loose](FakeClosureTree& t) {
                            if (!t.ElecTrigger() && !t.MuonTrigger()) return false;
                            unsigned int n_sig_lep = t.get_n_lep(emu_bits_signal);
                            unsigned int n_loo_lep = t.get_n_lep(emu_bits_loose);

                            unsigned int n_sig_tau = t.get_n_lep(tau_bits_signal);
                            unsigned int n_loo_tau = t.get_n_lep(tau_bits_loose);

                            return (n_sig_lep == 3 && n_loo_lep == 0 && n_sig_tau == 0 && n_loo_tau >= 1) ||
                                   (n_sig_lep == 2 && n_loo_lep == 1 && n_sig_tau == 1);
                        });

    ClosureCut CR2_LLLT("CR2_LLLT", "N_{e/#mu}^{signal}=2, N_{#tau}^{signal}#geq1",
                        [emu_bits_signal, emu_bits_loose, tau_bits_signal, tau_bits_loose](FakeClosureTree& t) {
                            return (t.ElecTrigger() || t.MuonTrigger()) &&
                                   (t.get_n_lep(emu_bits_signal) == 2 && t.get_n_lep(emu_bits_loose) == 1 &&
                                    t.get_n_lep(tau_bits_signal) == 0 && t.get_n_lep(tau_bits_loose) >= 1);
                        });

    std::shared_ptr<FakeClosureHisto> LLLT_closure =
        std::make_shared<FakeClosureHisto>(histo, lep_bits | LepClass::MixedCR2, smp, SR_LLLT && extra_cut, CR1_LLLT && extra_cut,
                                           CR2_LLLT && extra_cut, h_name + extra_cut.getName());

    closure_tests += LLLT_closure;
    if (!load_ff(ff_file, LLLT_closure->CR1_weighter(), "CR2_LLlt" + cr_sub_sel, load_zjets_ff) ||
        !load_ff(ff_file, LLLT_closure->CR2_weighter(), "CR2_LLlt" + cr_sub_sel, load_zjets_ff)) {
        return false;
    }
    plots +=
        LLLT_closure->get_plot({"Closure LLLT, " + smp.getName(), "FF: N^{baseline}#geq1", "Test: " + extra_cut.getTitle()}, canvas_opt);

    //
    // 2 lepton 2 tau closure
    //
    ClosureCut SR_LLTT("LLTT", "N_{e/#mu}^{signal}=2, N_{#tau}^{signal}#geq2",
                       [emu_bits_signal, emu_bits_loose, tau_bits_signal](FakeClosureTree& t) {
                           return (t.ElecTrigger() || t.MuonTrigger()) && t.get_n_lep(emu_bits_signal) == 2 &&
                                  t.get_n_lep(tau_bits_signal) >= 2 && t.get_n_lep(emu_bits_loose) == 0;
                       });

    ClosureCut CR1_LLTT("CR1_LLTT", "N_{e/#mu}^{signal}=2, N_{#tau}^{signal}#geq2",
                        [emu_bits_signal, emu_bits_loose, tau_bits_signal, tau_bits_loose](FakeClosureTree& t) {
                            return (t.ElecTrigger() || t.MuonTrigger()) && t.get_n_lep(emu_bits_signal) == 2 &&
                                   t.get_n_lep(tau_bits_signal) == 1 && t.get_n_lep(emu_bits_loose) == 0 &&
                                   t.get_n_lep(tau_bits_loose) >= 1;
                        });

    ClosureCut CR2_LLTT("CR2_LLTT", "N_{e/#mu}^{signal}=2, N_{#tau}^{signal}#geq2",
                        [emu_bits_signal, emu_bits_loose, tau_bits_signal, tau_bits_loose](FakeClosureTree& t) {
                            return (t.ElecTrigger() || t.MuonTrigger()) && t.get_n_lep(emu_bits_signal) == 2 &&
                                   t.get_n_lep(tau_bits_signal) == 0 && t.get_n_lep(emu_bits_loose) == 0 &&
                                   t.get_n_lep(tau_bits_loose) >= 2;
                        });
    std::shared_ptr<FakeClosureHisto> LLTT_closure = std::make_shared<FakeClosureHisto>(
        histo, lep_bits, smp, SR_LLTT && extra_cut, CR1_LLTT && extra_cut, CR2_LLTT && extra_cut, h_name + extra_cut.getName());

    closure_tests += LLTT_closure;
    if (!load_ff(ff_file, LLTT_closure->CR1_weighter(), "CR2_LLtt" + cr_sub_sel, load_zjets_ff) ||
        !load_ff(ff_file, LLTT_closure->CR2_weighter(), "CR2_LLtt" + cr_sub_sel, load_zjets_ff)) {
        return false;
    }
    plots +=
        LLTT_closure->get_plot({"Closure LLTT, " + smp.getName(), "FF: N^{baseline}#geq1", "Test: " + extra_cut.getTitle()}, canvas_opt);

    return true;
}

int main(int argc, char* argv[]) {
    SetAtlasStyle();
    std::vector<std::string> in_files{

        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_1.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_2.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_3.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_4.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_5.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_6.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-03-03/SF_Trees_Tau/PowHegPy8_ttbar_diL_7.root",

    };
    /// I'm too lazy to call SumW for each histo type
    XAMPP::HistoTemplates::getHistoTemplater();
    // HistoFiller::getDefaultFiller()->setFillMode(HistoFillMode::singleThread);

    bool in_file_reset = false;
    bool do_atlas = true;
    bool load_zjets_ff = false;
    std::string tree_name = "DebugTree_Nominal";
    std::string out_dir = "Plots/ClosurePlots/";
    std::string fake_factor_file = "fake_results.root";
    std::string sample_name = "t#bar{t}";
    std::string non_closure_file = "";
    for (int k = 1; k < argc; ++k) {
        std::string current_arg(argv[k]);
        if (current_arg == "--inFile" && k + 1 < argc) {
            if (!in_file_reset) in_files.clear();
            in_file_reset = true;
            in_files.push_back(argv[k + 1]);
        } else if (current_arg == "--outDir" && k + 1 < argc) {
            out_dir = argv[k + 1];
        } else if (current_arg == "--treeName" && k + 1 < argc) {
            tree_name = argv[k + 1];
        } else if (current_arg == "--fakeFactorFile" && k + 1 < argc) {
            fake_factor_file = argv[k + 1];
        } else if (current_arg == "--sampleName" && k + 1 < argc) {
            sample_name = argv[k + 1];
        } else if (current_arg == "--noATLAS") {
            do_atlas = false;
        } else if (current_arg == "--isZJets") {
            load_zjets_ff = true;
        } else if (current_arg == "--nonClosureFile" && k + 1 < argc) {
            non_closure_file = argv[k + 1];
        }
    }
    ///
    /// Open the fake factor input file first
    ///
    std::shared_ptr<TFile> ff_file = XAMPP::Open(fake_factor_file);
    if (!ff_file || !ff_file->IsOpen()) { return EXIT_FAILURE; }

    /// Basic lepton multiplicity cuts

    ///
    ///     Prepare the extra topological
    ///     cuts and truth selections
    std::vector<ClosureCut> truth_selections{
        ClosureCut("Incl", "Combined", [](FakeClosureTree&) { return true; }),
        ClosureCut("dRGtr0p6", "#DeltaR(#it{l},#it{l})>0.6", [](FakeClosureTree& t) { return t.global_dR_minLep() > 0.6; }),

        ClosureCut("noB", "b-veto", [](FakeClosureTree& t) { return t.NumBJets() == 0; }),
        ClosureCut("noBdRGtr0p6", "b-veto, #DeltaR>0.6",
                   [](FakeClosureTree& t) { return t.NumBJets() == 0 && t.global_dR_minLep() > 0.6; }),
        ClosureCut("noBdRGtr0p6PtGtr30", "b-veto, #DeltaR>0.6 #vee p_{T} > 30 GeV",
                   [](FakeClosureTree& t) { return !t.NumBJets() && !t.has_collimated_lep_below(0.6, 30); }),

        // ClosureCut("noZBdRGtr0p6PtGtr30", "Z&b-veto, #DeltaR>0.6 #vee p_{T} > 30 GeV",
        //           [](FakeClosureTree& t) { return !t.IsBaseZVeto() && !t.NumBJets() && !t.has_collimated_lep_below(0.6, 30); }),
        //
        ClosureCut("BSel", "b-selection", [](FakeClosureTree& t) { return t.NumBJets() >= 1; }),
        ClosureCut("BSeldRGtr0p6", "b-selection, #DeltaR>0.6",
                   [](FakeClosureTree& t) { return t.NumBJets() >= 1 && t.global_dR_minLep() > 0.6; }),
        ClosureCut("BSeldRGtr0p6PtGtr30", "b-selection, #DeltaR>0.6 #vee p_{T} > 30 GeV",
                   [](FakeClosureTree& t) { return t.NumBJets() && !t.has_collimated_lep_below(0.6, 30); }),

        // ClosureCut("OneB", "b-selection", [](FakeClosureTree& t) { return t.NumBJets() == 1; }),
        // ClosureCut("OneBdRGtr0p6", "N_{b-jets}=1, #DeltaR>0.6",
        //           [](FakeClosureTree& t) { return t.NumBJets() == 1 && t.global_dR_minLep() > 0.6; }),
        // ClosureCut("OneBdRGtr0p6PtGtr30", "N_{b-jets}=1, #DeltaR>0.6 #vee p_{T} > 30 GeV",
        //            [](FakeClosureTree& t) { return t.NumBJets() == 1 && !t.has_collimated_lep_below(0.6, 30); }),

        // ClosureCut("TwoB", "b-selection", [](FakeClosureTree& t) { return t.NumBJets() == 2; }),
        // ClosureCut("TwoBdRGtr0p6", "N_{b-jets}=2, #DeltaR>0.6",
        //           [](FakeClosureTree& t) { return t.NumBJets() == 2 && t.global_dR_minLep() > 0.6; }),
        // ClosureCut("TwoBdRGtr0p6PtGtr30", "N_{b-jets}=2, #DeltaR>0.6 #vee p_{T} > 30 GeV",
        //           [](FakeClosureTree& t) { return t.NumBJets() == 2 && !t.has_collimated_lep_below(0.6, 30); }),

    };

    ///
    /// Now it's time to load the samples
    ///
    Sample<FakeClosureTree> mySample(sample_name);
    for (auto& in_file : in_files) { mySample.addFile(in_file, tree_name); }
    ///
    /// Define the the meff for closure
    ///
    std::shared_ptr<TH1> meff_template = XAMPP::MakeTH1({0, 100, 200, 300, 400, 500, 600, 1000, 1500, 2000});
    meff_template->GetXaxis()->SetTitle("m_{eff} [GeV]");
    meff_template->GetYaxis()->SetTitle("a.u.");

    //     std::shared_ptr<TH1> low_meff_template = std::make_shared<TH1D>("LowMeff", "Dummy;m_{eff} [GeV];a.u.", 40, 0, 1000);
    std::shared_ptr<TH1> low_meff_template = XAMPP::MakeTH1({0, 100, 200, 300, 400, 500, 600, 1000});
    low_meff_template->GetXaxis()->SetTitle("m_{eff} [GeV]");
    low_meff_template->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> met_template = XAMPP::MakeTH1({0, 25, 50, 75, 100, 150, 200, 250, 300});
    met_template->GetXaxis()->SetTitle("E^{miss}_{T} [GeV]");
    met_template->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> HtLep_template = XAMPP::MakeTH1({0, 25, 50, 75, 100, 150, 200, 250, 300});
    HtLep_template->GetXaxis()->SetTitle("H_{T}^{lep} [GeV]");
    HtLep_template->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> HtJet_template = XAMPP::MakeTH1({0, 25, 50, 75, 100, 150, 200, 250, 300});
    HtJet_template->GetXaxis()->SetTitle("H_{T}^{jets} [GeV]");
    HtJet_template->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1> n_jets_template = std::make_shared<TH1D>("NumJets", "Dummy;N_{jets}", 8, 0, 8);
    n_jets_template->GetXaxis()->SetTitle("N_{jets}");

    /// Closure against lepton properties
    std::shared_ptr<TH1> lep_pt = XAMPP::MakeTH1({5, 10, 15, 20, 30, 40, 50, 60, 80, 100});
    lep_pt->GetXaxis()->SetTitle("p_{T} [GeV]");

    std::shared_ptr<TH1> eta_template = std::make_shared<TH1D>("Eta1D", "Dummy", 27, 0, 2.7);
    eta_template->GetXaxis()->SetTitle("|#eta|");

    std::shared_ptr<TH1> dRTemplate = std::make_shared<TH1D>("dR", "Dummy", 50, 0, 5.);
    dRTemplate->GetXaxis()->SetTitle("#DeltaR(lep,lep)");

    std::shared_ptr<TH1> MassTemplate = std::make_shared<TH1D>("Mass", "dummy;m_{#it{ll}} [GeV]", 10, 0, 100);
    // std::shared_ptr<TH1> category_dR = XAMPP::MakeTH1({-3, -2, -1, 0, 1, 2, 3});
    // category_dR->GetXaxis()->SetTitle("close-by #it{l}#it{l}");
    std::shared_ptr<TH1> frac_template = std::make_shared<TH1D>("Fraction", "dummy", 10., 0, 1.);
    /// Plot styling
    CanvasOptions canvas_opt = CanvasOptions()
                                   .ratioAxisTitle("CR/SR")
                                   .yAxisTitle("a.u.")
                                   .extraXtitleOffset(1.3)
                                   .extraYtitleOffset(0.4)
                                   .labelStatusTag("Simulation Internal")
                                   .ratioMax(1.5)
                                   .ratioMin(0.)
                                   .legendStartX(-0.45)
                                   .doAtlasLabel(do_atlas)
                                   .logY(false)
                                   .yMaxExtraPadding(0.7)
                                   .overrideOutputDir(out_dir);

    std::vector<std::shared_ptr<FakeClosureHisto>> closure_tests;
    std::vector<PlotContent<TH1D>> plots;

    for (auto& truth : truth_selections) {
        std::string cr_str;
        std::string tr_name = truth.getName();
        if (tr_name.find("noB") != std::string::npos)
            cr_str = "__AND__noB";
        else if (tr_name.find("BSel") != std::string::npos)
            cr_str = "__AND__B";
        else if (tr_name.find("noZB") != std::string::npos)
            cr_str = "__AND__noZB";
        else if (tr_name.find("noZ1B") != std::string::npos)
            cr_str = "__AND__noZ1B";

        // else if (tr_name.find("OneB") != std::string::npos) cr_str = "__AND__1B";
        // else if (tr_name.find("TwoB") != std::string::npos) cr_str = "__AND__2B";

        if (!setup_closure(meff_template, ff_file, mySample, closure_tests, plots, "Meff", truth, LepClass::LepFlav, canvas_opt, cr_str,
                           load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        if (!setup_closure(low_meff_template, ff_file, mySample, closure_tests, plots, "LowMeff", truth, LepClass::LepFlav, canvas_opt,
                           cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }

        if (!setup_closure(HtLep_template, ff_file, mySample, closure_tests, plots, "HTLep", truth, LepClass::LepFlav, canvas_opt, cr_str,
                           load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        if (!setup_closure(HtJet_template, ff_file, mySample, closure_tests, plots, "HTJet", truth, LepClass::LepFlav, canvas_opt, cr_str,
                           load_zjets_ff)) {
            return EXIT_FAILURE;
        }

        frac_template->GetXaxis()->SetTitle("H_{T}^{jets} / m_{eff}");
        if (!setup_closure(frac_template, ff_file, mySample, closure_tests, plots, "HtJetFraction", truth, LepClass::LepFlav, canvas_opt,
                           cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }

        frac_template->GetXaxis()->SetTitle("E_{T}^{miss} / m_{eff}");
        if (!setup_closure(frac_template, ff_file, mySample, closure_tests, plots, "MetFraction", truth, LepClass::LepFlav, canvas_opt,
                           cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        frac_template->GetXaxis()->SetTitle("H_{T}^{lep} / m_{eff}");
        if (!setup_closure(frac_template, ff_file, mySample, closure_tests, plots, "HtLepFraction", truth, LepClass::LepFlav, canvas_opt,
                           cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        if (!setup_closure(met_template, ff_file, mySample, closure_tests, plots, "Met", truth, LepClass::LepFlav, canvas_opt, cr_str,
                           load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        if (!setup_closure(dRTemplate, ff_file, mySample, closure_tests, plots, "Ele_dR", truth,
                           LepClass::Ele | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        if (!setup_closure(dRTemplate, ff_file, mySample, closure_tests, plots, "Muo_dR", truth,
                           LepClass::Muo | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }

        eta_template->GetXaxis()->SetTitle("|#eta| (e)");
        if (!setup_closure(eta_template, ff_file, mySample, closure_tests, plots, "Ele_Eta", truth,
                           LepClass::Ele | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        eta_template->GetXaxis()->SetTitle("|#eta| (#mu)");
        if (!setup_closure(eta_template, ff_file, mySample, closure_tests, plots, "Muo_Eta", truth,
                           LepClass::Muo | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }

        lep_pt->GetXaxis()->SetTitle("p_{T} (e) [GeV]");
        if (!setup_closure(lep_pt, ff_file, mySample, closure_tests, plots, "Ele_Pt", truth,
                           LepClass::Ele | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        lep_pt->GetXaxis()->SetTitle("p_{T} (#mu) [GeV]");
        if (!setup_closure(lep_pt, ff_file, mySample, closure_tests, plots, "Muo_Pt", truth,
                           LepClass::Muo | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }

        if (!setup_closure(MassTemplate, ff_file, mySample, closure_tests, plots, "Ele_Mass", truth,
                           LepClass::Ele | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }

        if (!setup_closure(MassTemplate, ff_file, mySample, closure_tests, plots, "Muo_Mass", truth,
                           LepClass::Muo | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        lep_pt->GetXaxis()->SetTitle("p_{T} (#tau^{1P}) [GeV]");
        if (!setup_closure(lep_pt, ff_file, mySample, closure_tests, plots, "Tau1P_Pt", truth,
                           LepClass::Tau1P | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        lep_pt->GetXaxis()->SetTitle("p_{T} (#tau^{3P}) [GeV]");
        if (!setup_closure(lep_pt, ff_file, mySample, closure_tests, plots, "Tau3P_Pt", truth,
                           LepClass::Tau3P | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        lep_pt->GetXaxis()->SetTitle("p_{T} (#tau) [GeV]");
        if (!setup_closure(lep_pt, ff_file, mySample, closure_tests, plots, "Tau_Pt", truth,
                           LepClass::Tau | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        lep_pt->GetXaxis()->SetTitle("p_{T} (#tau) [GeV]");
        if (!setup_closure(lep_pt, ff_file, mySample, closure_tests, plots, "Tau_Pt", truth,
                           LepClass::Tau | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }

        eta_template->GetXaxis()->SetTitle("|#eta| (#tau^{1P})");
        if (!setup_closure(eta_template, ff_file, mySample, closure_tests, plots, "Tau1P_Eta", truth,
                           LepClass::Tau1P | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        eta_template->GetXaxis()->SetTitle("|#eta| (#tau^{3P})");
        if (!setup_closure(eta_template, ff_file, mySample, closure_tests, plots, "Tau3P_Eta", truth,
                           LepClass::Tau3P | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
        eta_template->GetXaxis()->SetTitle("|#eta| (#tau)");
        if (!setup_closure(eta_template, ff_file, mySample, closure_tests, plots, "Tau_Eta", truth,
                           LepClass::Tau | LepClass::Incl | LepClass::Baseline, canvas_opt, cr_str, load_zjets_ff)) {
            return EXIT_FAILURE;
        }
    }

    PlotUtils::startMultiPagePdfFile("AllClosurePlots", out_dir);
    std::sort(plots.begin(), plots.end(),
              [](const PlotContent<TH1D>& a, const PlotContent<TH1D>& b) { return a.getFileName() < b.getFileName(); });
    for (auto& plot : plots) {
        if (count_non_empty(plot)) DefaultPlotting::draw1D(plot);
    }
    PlotUtils::endMultiPagePdfFile("AllClosurePlots", out_dir);
    if (!non_closure_file.empty()) {
        std::shared_ptr<TFile> out_file = make_out_file(non_closure_file);
        if (!out_file) return EXIT_FAILURE;
        for (auto& cplot : closure_tests) { cplot->write_file(out_file.get()); }
    }
    return EXIT_SUCCESS;
}
