#include <FakeFactors4L/BaseFakeTreeUtils.h>
#include <FakeFactors4L/FakeBundle.h>
#include <FakeFactors4L/FakeClosureTree.h>
#include <FakeFactors4L/Utils.h>

///

#include <TFile.h>
#include <TH2D.h>
#include <TSystem.h>
#include <TTree.h>
#include <cstring>
#include <iostream>
#include <memory>
#include <string>

// using namespace XAMPP;
using namespace SUSY4L;

int main(int argc, char* argv[]) {
    SetAtlasStyle();
    std::vector<std::string> in_files{
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data15_periodD.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data15_periodE.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data15_periodF.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data15_periodG.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data15_periodH.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data15_periodJ.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data16_periodA.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data16_periodB.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data16_periodC.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data16_periodD.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data16_periodE.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data16_periodF.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data16_periodG.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data16_periodI.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data16_periodK.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data16_periodL.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data17_periodB.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data17_periodC.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data17_periodD.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data17_periodE.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data17_periodF.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data17_periodH.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data17_periodI.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data17_periodK.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data18_periodB.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data18_periodC.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data18_periodD.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data18_periodF.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data18_periodI.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data18_periodK.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data18_periodL.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data18_periodM.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data18_periodO.root",
        "/ptmp/mpp/maren/Cluster/OUTPUT/2020-02-28/4L_FakeTrees_altBaseline_inclTau/data18_periodQ.root",

    };
    /// I'm too lazy to call SumW for each histo type
    XAMPP::HistoTemplates::getHistoTemplater();

    bool in_file_reset = false;
    std::string tree_name = "DebugTree_Nominal";
    std::string out_dir = "Plots/dRTest/";
    for (int k = 1; k < argc; ++k) {
        std::string current_arg(argv[k]);
        if (current_arg == "--inFile" && k + 1 < argc) {
            if (!in_file_reset) in_files.clear();
            in_file_reset = true;
            in_files.push_back(argv[k + 1]);
        } else if (current_arg == "--outDir" && k + 1 < argc) {
            out_dir = argv[k + 1];
        } else if (current_arg == "--treeName" && k + 1 < argc) {
            tree_name = argv[k + 1];
        }
    }

    ///
    /// Open the fake factor input file first
    ///
    ClosureCut NoCrackMuons("NoCrack", "|#eta^{#mu}|>0.1", [](FakeClosureTree& t) {
        if (t.Muo_eta.size() == 0) return true;
        for (unsigned int i = 0; i < t.Muo_eta.size(); ++i) {
            if (std::fabs(t.Muo_eta(i)) < 0.1) return false;
        }
        return true;
    });
    /// Basic lepton multiplicity cuts
    BaseCut SR_LLLL("LLLL", "N_{signal}=4", [](BaseFakeTree& t) {
        return t.IsSigZVeto() && t.get_n_lep(LepClass::LightLep | LepClass::Signal | LepClass::Incl) == 4;
    });
    BaseCut CR_LLll("LLll", "N_{signal}=2", [](BaseFakeTree& t) {
        return t.IsSigZVeto() && t.get_n_lep(LepClass::LightLep | LepClass::Signal | LepClass::Incl) == 2 &&
               t.get_n_lep(LepClass::LightLep | LepClass::Loose | LepClass::Incl) >= 2 && t.global_dR_minLep() < 0.5;
    });

    ///
    /// Now it's time to load the samples
    ///
    Sample<BaseFakeTree> mySample("SampleNameNotUsedHere");
    for (auto& in_file : in_files) { mySample.addFile(in_file, tree_name); }

    /// Only consider the lepton HT
    std::shared_ptr<TH1D> dRTemplate = std::make_shared<TH1D>("dR", "dummy;#pm#DeltaR_{e/#mu};a.u", 20, -2., 2.);

    /// Plot styling
    CanvasOptions canvas_opt = CanvasOptions()
                                   .ratioAxisTitle("weighted baseline / SR")
                                   .yAxisTitle("a.u.")
                                   .extraXtitleOffset(1.3)
                                   .extraYtitleOffset(0.4)
                                   .labelStatusTag("Simulation Internal")
                                   .ratioMax(1.5)
                                   .ratioMin(0.)
                                   .legendStartX(-0.45)
                                   .doAtlasLabel(true)
                                   .logY(false)
                                   .yMaxExtraPadding(0.7)
                                   .overrideOutputDir(out_dir);

    IBaseFakeTreeFiller ele_func_provider(dRTemplate, LepClass::Ele | LepClass::Incl | LepClass::Baseline, "ele_dR");
    IBaseFakeTreeFiller muo_func_provider(dRTemplate, LepClass::Muo | LepClass::Incl | LepClass::Baseline, "muo_dR");

    auto ele_func = ele_func_provider.make_reader<BaseFakeTree>(dRTemplate->GetXaxis());
    auto muo_func = muo_func_provider.make_reader<BaseFakeTree>(dRTemplate->GetXaxis());

    BaseFiller1D ele_dR("Ele_dR", ele_func_provider.container_size<BaseFakeTree>(), ele_func_provider.lepton_selection<BaseFakeTree>(0),
                        [&ele_func](TH1D* h, BaseFakeTree& t, size_t i) { h->Fill(ele_func(t, i)); }, dRTemplate.get());

    BaseFiller1D muo_dR("Muo_dR", muo_func_provider.container_size<BaseFakeTree>(), muo_func_provider.lepton_selection<BaseFakeTree>(0),
                        [&muo_func](TH1D* h, BaseFakeTree& t, size_t i) { h->Fill(muo_func(t, i)); }, dRTemplate.get());

    BaseFiller1D eleV_dR("EleVanilla_dR", ele_func_provider.container_size<BaseFakeTree>(),
                         ele_func_provider.lepton_selection<BaseFakeTree>(LepClass::FailVanillaIso),
                         [&ele_func](TH1D* h, BaseFakeTree& t, size_t i) { h->Fill(ele_func(t, i)); }, dRTemplate.get());

    BaseFiller1D muoV_dR("MuoVanilla_dR", muo_func_provider.container_size<BaseFakeTree>(),
                         muo_func_provider.lepton_selection<BaseFakeTree>(LepClass::FailVanillaIso),
                         [&muo_func](TH1D* h, BaseFakeTree& t, size_t i) { h->Fill(muo_func(t, i)); }, dRTemplate.get());

    PlotPostProcessor<TH1D, TH1D> normaliseToUnitIntegral("NormToUnit", [](std::shared_ptr<TH1D> in) {
        in->Scale(1. / XAMPP::Integrate(in, 1, in->GetNbinsX()));
        return in;
    });

    std::vector<PlotContent<TH1D>> plots;
    plots += PlotContent<TH1D>(
        {
            Plot<TH1D>(mySample, SR_LLLL, muo_dR, normaliseToUnitIntegral, "muons (SR)", "PL",
                       PlotFormat().Color(kRed).MarkerStyle(kFullDiamond).ExtraDrawOpts("HIST E")),
            Plot<TH1D>(mySample, CR_LLll, muo_dR, normaliseToUnitIntegral, "muons (CR)", "PL",
                       PlotFormat().Color(kBlue).MarkerStyle(kFullSquare).ExtraDrawOpts("HIST E")),
        },
        {
            RatioEntry(0, 1, PlotUtils::efficiencyErrors),
        },
        {"data, normalized to unit area"}, "Muons", "AllDRChecks", canvas_opt);

    plots += PlotContent<TH1D>(
        {
            Plot<TH1D>(mySample, SR_LLLL, ele_dR, normaliseToUnitIntegral, "electrons (SR)", "PL",
                       PlotFormat().Color(kRed).MarkerStyle(kFullDiamond).ExtraDrawOpts("HIST E")),
            Plot<TH1D>(mySample, CR_LLll, ele_dR, normaliseToUnitIntegral, "electrons (CR)", "PL",
                       PlotFormat().Color(kBlue).MarkerStyle(kFullSquare).ExtraDrawOpts("HIST E")),
        },
        {
            RatioEntry(0, 1, PlotUtils::efficiencyErrors),
        },
        {"data"}, "Electrons", "AllDRChecks", canvas_opt);

    plots += PlotContent<TH1D>(
        {
            Plot<TH1D>(mySample, SR_LLLL, muoV_dR, normaliseToUnitIntegral, "muons - vanilla iso (SR)", "PL",
                       PlotFormat().Color(kRed).MarkerStyle(kFullDiamond).ExtraDrawOpts("HIST E")),
            Plot<TH1D>(mySample, CR_LLll, muoV_dR, normaliseToUnitIntegral, "muons - vanilla iso (CR)", "PL",
                       PlotFormat().Color(kBlue).MarkerStyle(kFullSquare).ExtraDrawOpts("HIST E")),
        },
        {
            RatioEntry(0, 1, PlotUtils::efficiencyErrors),
        },
        {"data, normalized to unit area"}, "Muons_Vanilla", "AllDRChecks", canvas_opt);

    plots += PlotContent<TH1D>(
        {
            Plot<TH1D>(mySample, SR_LLLL, eleV_dR, normaliseToUnitIntegral, "electrons - vanilla iso (SR)", "PL",
                       PlotFormat().Color(kRed).MarkerStyle(kFullDiamond).ExtraDrawOpts("HIST E")),
            Plot<TH1D>(mySample, CR_LLll, eleV_dR, normaliseToUnitIntegral, "electrons - vanilla iso (CR)", "PL",
                       PlotFormat().Color(kBlue).MarkerStyle(kFullSquare).ExtraDrawOpts("HIST E")),
        },
        {
            RatioEntry(0, 1, PlotUtils::efficiencyErrors),
        },
        {"data"}, "Electrons_Vanilla", "AllDRChecks", canvas_opt);

    PlotUtils::startMultiPagePdfFile("AllDRChecks", out_dir);
    for (auto& plot : plots) {
        if (count_non_empty(plot)) DefaultPlotting::draw1D(plot);
    }
    PlotUtils::endMultiPagePdfFile("AllDRChecks", out_dir);
    return EXIT_SUCCESS;
}
