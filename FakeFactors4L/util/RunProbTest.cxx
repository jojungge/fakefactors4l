#include <FakeFactors4L/FakeBundle.h>
#include <FakeFactors4L/FakeClosureTree.h>
#include <FakeFactors4L/Utils.h>

///

#include <TFile.h>
#include <TH2D.h>
#include <TSystem.h>
#include <TTree.h>
#include <cstring>
#include <iostream>
#include <memory>
#include <string>

// using namespace XAMPP;
using namespace SUSY4L;

int main(int argc, char* argv[]) {
    SetAtlasStyle();
    std::vector<std::string> in_files{
        //"/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-22/SF_Trees/PowHegPy8_ttbar_incl_1.root",
        //"/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-22/SF_Trees/PowHegPy8_ttbar_incl_2.root",
        //"/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-22/SF_Trees/PowHegPy8_ttbar_incl_3.root",
        //"/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-22/SF_Trees/PowHegPy8_ttbar_incl_4.root",
        //"/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-22/SF_Trees/PowHegPy8_ttbar_incl_5.root",
        //"/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-22/SF_Trees/PowHegPy8_ttbar_incl_6.root",

        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-24/SF_Trees_PiD/PowHegPy8_ttbar_diL_1.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-24/SF_Trees_PiD/PowHegPy8_ttbar_diL_2.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-24/SF_Trees_PiD/PowHegPy8_ttbar_diL_3.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-24/SF_Trees_PiD/PowHegPy8_ttbar_diL_4.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-24/SF_Trees_PiD/PowHegPy8_ttbar_diL_5.root",
        "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-02-24/SF_Trees_PiD/PowHegPy8_ttbar_diL_6.root",

    };
    /// I'm too lazy to call SumW for each histo type
    XAMPP::HistoTemplates::getHistoTemplater();

    bool in_file_reset = false;
    std::string tree_name = "DebugTree_Nominal";
    std::string out_dir = "Plots/ProbabilityPlots/";
    std::string fake_factor_file = "fake_results.root";
    for (int k = 1; k < argc; ++k) {
        std::string current_arg(argv[k]);
        if (current_arg == "--inFile" && k + 1 < argc) {
            if (!in_file_reset) in_files.clear();
            in_file_reset = true;
            in_files.push_back(argv[k + 1]);
        } else if (current_arg == "--outDir" && k + 1 < argc) {
            out_dir = argv[k + 1];
        } else if (current_arg == "--treeName" && k + 1 < argc) {
            tree_name = argv[k + 1];
        } else if (current_arg == "--fakeFactorFile" && k + 1 < argc) {
            fake_factor_file = argv[k + 1];
        }
    }

    ///
    /// Open the fake factor input file first
    ///
    std::shared_ptr<TFile> ff_file(TFile::Open(fake_factor_file.c_str(), "READ"));
    if (!ff_file || !ff_file->IsOpen()) {
        std::cerr << "Could not open " << fake_factor_file << std::endl;
        return EXIT_FAILURE;
    }
    ClosureCut NoCrackMuons("NoCrack", "|#eta^{#mu}|>0.1", [](FakeClosureTree& t) {
        if (t.Muo_eta.size() == 0) return true;
        for (unsigned int i = 0; i < t.Muo_eta.size(); ++i) {
            if (std::fabs(t.Muo_eta(i)) < 0.1) return false;
        }
        return true;
    });
    /// Basic lepton multiplicity cuts
    ClosureCut SR_LLLL("LLLL", "N_{signal}=4", [](FakeClosureTree& t) {
        return t.get_n_lep(LepClass::InclSignal) == 4 && t.get_n_lep(LepClass::AllLep) == 4;  // && t.global_dR_minLep() < 0.5;
    });
    ClosureCut CR_llll("llll", "N_{baseline}=4", [](FakeClosureTree& t) {
        return t.get_n_lep(LepClass::AllLep) == 4;  // && t.global_dR_minLep() < 0.5;
    });

    typedef std::pair<std::string, std::string> StringPair;
    // std::vector<StringPair> ff_regions{
    //    StringPair("LepIncl____AND__MeffIncl", "N_{#it{l}}^{baseline}#geq1"),
    // StringPair("TwoLeptons____AND__MeffIncl", "N_{lep}^{baseline}#geq2"),
    // StringPair("GtrTwoLep____AND__MeffIncl", "N_{#it{l}}^{baseline}#geq2"),
    // StringPair("ThreeLep____AND__MeffIncl", "N_{#it{l}}^{baseline}=3"),
    // StringPair("ThreeLepOneSig____AND__MeffIncl", "N_{#it{l}}^{baseline}=3,N_{#it{l}}^{signal}#geq1"),
    // StringPair("ThreeLepTwoSig____AND__MeffIncl", "N_{#it{l}}^{baseline}=3,N_{#it{l}}^{signal}#geq2"),
    // StringPair("TwoSigReals____AND__MeffIncl", "N_{#it{l}}^{signal,real}=2"),
    // StringPair("TwoSigRealsPt____AND__MeffIncl", "N_{#it{l}}^{signal,real}=2, p_{T}>27 GeV"),
    // };
    EffiWeighter eff_weighter;
    if (!eff_weighter.load_eff_weights(ff_file, "FF/NoTrigger__AND__LepIncl____AND__MeffIncl",
                                       "FF/NoTrigger__AND__LepIncl____AND__MeffIncl", "Pt_vs_AbsEta_vs_lepDR"))
        return EXIT_FAILURE;

    ///
    /// Now it's time to load the samples
    ///
    Sample<FakeClosureTree> mySample("SampleNameNotUsedHere");
    for (auto& in_file : in_files) { mySample.addFile(in_file, tree_name); }

    /// Only consider the lepton HT
    std::shared_ptr<TH1> HtLep_template = XAMPP::MakeTH1({0, 25, 50, 75, 100, 150, 200, 250, 300});
    HtLep_template->GetXaxis()->SetTitle("H_{T}^{lep} [GeV]");
    HtLep_template->GetYaxis()->SetTitle("a.u.");

    std::shared_ptr<TH1D> Pt_Template = std::make_shared<TH1D>(XAMPP::RandomString(45).c_str(), "dummy;p_{T} [GeV];a.u.", 10, 0, 100);
    std::shared_ptr<TH1D> Eta_Template = std::make_shared<TH1D>(XAMPP::RandomString(45).c_str(), "dummy;|#eta|;a.u.", 25, 0, 2.5);

    /// Plot styling
    CanvasOptions canvas_opt = CanvasOptions()
                                   .ratioAxisTitle("weighted baseline / SR")
                                   .yAxisTitle("a.u.")
                                   .extraXtitleOffset(1.3)
                                   .extraYtitleOffset(0.4)
                                   .labelStatusTag("Simulation Internal")
                                   .ratioMax(1.5)
                                   .ratioMin(0.)
                                   .legendStartX(-0.45)
                                   .doAtlasLabel(true)
                                   .logY(false)
                                   .yMaxExtraPadding(0.7)
                                   .overrideOutputDir(out_dir);

    ClosureFiller1D signal_LepHT("Signal_LepHT", [](TH1D* h, FakeClosureTree& t) { h->Fill(t.signal_lep_ht(), t.eventWeight()); },
                                 dynamic_cast<TH1D*>(HtLep_template.get()));

    ClosureFiller1D baseline_LepHT(
        "Baseline_LepHT",
        [&eff_weighter](TH1D* h, FakeClosureTree& t) { h->Fill(t.baseline_lep_ht(), t.eventWeight() * eff_weighter.get_weight(t)); },
        dynamic_cast<TH1D*>(HtLep_template.get()));
    ClosureFiller1D signal_pt("Signal_PT",
                              [](TH1D* h, FakeClosureTree& t) {
                                  for (size_t l = 0; l < t.get_n_lep(LepClass::AllLep); ++l) {
                                      h->Fill(t.get_pt(l, LepClass::AllLep), t.eventWeight());
                                  }
                              },
                              dynamic_cast<TH1D*>(Pt_Template.get()));

    ClosureFiller1D weighted_pt("WeightPt",
                                [&eff_weighter](TH1D* h, FakeClosureTree& t) {
                                    double ev_weight = t.eventWeight() * eff_weighter.get_weight(t);
                                    for (size_t l = 0; l < t.get_n_lep(LepClass::AllLep); ++l) {
                                        h->Fill(t.get_pt(l, LepClass::AllLep), ev_weight);
                                    }
                                },
                                dynamic_cast<TH1D*>(Pt_Template.get()));

    ClosureFiller1D signal_eta("SignalEta",
                               [](TH1D* h, FakeClosureTree& t) {
                                   for (size_t l = 0; l < t.get_n_lep(LepClass::AllLep); ++l) {
                                       h->Fill(t.get_eta(l, LepClass::AllLep), t.eventWeight());
                                   }
                               },
                               dynamic_cast<TH1D*>(Eta_Template.get()));

    ClosureFiller1D weighted_eta("WeightEta",
                                 [&eff_weighter](TH1D* h, FakeClosureTree& t) {
                                     double ev_weight = t.eventWeight() * eff_weighter.get_weight(t);
                                     for (size_t l = 0; l < t.get_n_lep(LepClass::AllLep); ++l) {
                                         h->Fill(t.get_eta(l, LepClass::AllLep), ev_weight);
                                     }
                                 },
                                 dynamic_cast<TH1D*>(Eta_Template.get()));

    std::vector<PlotContent<TH1D>> plots;
    plots += PlotContent<TH1D>(
        {

            Plot<TH1D>(mySample, SR_LLLL, signal_pt, "SR", "PL", PlotFormat().Color(kOrange).ExtraDrawOpts("HIST E")),
            Plot<TH1D>(mySample, CR_llll, weighted_pt, "Baseline", "PL", PlotFormat().Color(kCyan - 1).ExtraDrawOpts("HIST E")),
        },

        {
            RatioEntry(1, 0, PlotUtils::efficiencyErrors),

        },
        {"Probability test"}, "Prob_Test_Pt", "AllProbabilityPlots", canvas_opt);
    plots += PlotContent<TH1D>(
        {

            Plot<TH1D>(mySample, SR_LLLL, signal_eta, "SR", "PL", PlotFormat().Color(kOrange).ExtraDrawOpts("HIST E")),
            Plot<TH1D>(mySample, CR_llll, weighted_eta, "Baseline", "PL", PlotFormat().Color(kCyan - 1).ExtraDrawOpts("HIST E")),
        },

        {
            RatioEntry(1, 0, PlotUtils::efficiencyErrors),

        },
        {"Probability test"}, "Prob_Test_Eta", "AllProbabilityPlots", canvas_opt);

    plots += PlotContent<TH1D>(
        {

            Plot<TH1D>(mySample, SR_LLLL, signal_LepHT, "SR", "PL", PlotFormat().Color(kOrange).ExtraDrawOpts("HIST E")),
            Plot<TH1D>(mySample, CR_llll, baseline_LepHT, "Baseline", "PL", PlotFormat().Color(kCyan - 1).ExtraDrawOpts("HIST E")),
        },

        {
            RatioEntry(1, 0, PlotUtils::efficiencyErrors),

        },
        {"Probability test"}, "Prob_Test_HtLep", "AllProbabilityPlots", canvas_opt);

    PlotUtils::startMultiPagePdfFile("AllProbabilityPlots", out_dir);
    for (auto& plot : plots) {
        if (count_non_empty(plot)) DefaultPlotting::draw1D(plot);
    }
    PlotUtils::endMultiPagePdfFile("AllProbabilityPlots", out_dir);
    return EXIT_SUCCESS;
}
