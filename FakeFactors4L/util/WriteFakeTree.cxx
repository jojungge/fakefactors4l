#include <FakeFactors4L/FakeTreeWriter.h>
#include <FakeFactors4L/HistFitterTree.h>
#include <FakeFactors4L/Utils.h>
#include <TStopwatch.h>

// using namespace XAMPP;
using namespace SUSY4L;

bool load_ff(const std::shared_ptr<TFile>& ff_file, std::shared_ptr<FakeWeighter> weighter, const std::string& CR, bool load_zjets_ff) {
    bool succeed = true;
    if (CR.find("l") != std::string::npos) {
        if (!weighter->load_histos(
                LepClass::Ele, ff_file, load_zjets_ff ? "Pt_vs_AbsEta_Mll" : "Pt_vs_AbsEta_LooseSigLepDR", "NoTrigger__AND__LepIncl",
                "Pt_vs_AbsEta_vs_LooseSigLepDR", CR, "Pt", "ScaleFactors", [](BaseFakeTree& t, size_t i) {
                    return t.is_Lep(i, LepClass::LightLep | LepClass::Incl | LepClass::Baseline) && t.get_dR_minLep(i) > 0.6;
                }))
            succeed = false;

        if (!weighter->load_histos(
                LepClass::Muo, ff_file, load_zjets_ff ? "Pt_vs_AbsEta_Mll" : "Pt_vs_AbsEta_LooseSigLepDR", "NoTrigger__AND__LepIncl",
                "Pt_vs_AbsEta_vs_LooseSigLepDR", CR, "Pt", "ScaleFactors", [](BaseFakeTree& t, size_t i) {
                    return t.is_Lep(i, LepClass::LightLep | LepClass::Incl | LepClass::Baseline) && t.get_dR_minLep(i) > 0.6;
                }))
            succeed = false;

        if (!weighter->load_histos(LepClass::Ele, ff_file, "Pt_vs_FlavLepDR_lowDRComplement", "NoTrigger__AND__LepIncl", "Mass_vs_Pt", CR,
                                   "Pt", "ScaleFactors", [](BaseFakeTree& t, size_t i) {
                                       return t.is_Lep(i, LepClass::LightLep | LepClass::Incl | LepClass::Baseline) &&
                                              t.get_dR_minLep(i) < 0.6;
                                   }))
            succeed = false;

        if (!weighter->load_histos(LepClass::Muo, ff_file, "Pt_vs_FlavLepDR_lowDRComplement", "NoTrigger__AND__LepIncl", "Mass_vs_Pt", CR,
                                   "Pt", "ScaleFactors", [](BaseFakeTree& t, size_t i) {
                                       return t.is_Lep(i, LepClass::LightLep | LepClass::Incl | LepClass::Baseline) &&
                                              t.get_dR_minLep(i) < 0.6;
                                   }))
            succeed = false;
    }
    if (CR.find("t") != std::string::npos || CR.find("T") != std::string::npos) {
        for (int tau_multip : {LepClass::Tau1P, LepClass::Tau3P}) {
            if (!weighter->load_histos(tau_multip, ff_file, "Pt_vs_AbsEta", "NoTrigger__AND__LepIncl", "Pt_vs_AbsEta", CR, "Pt",
                                       "ScaleFactors")) {
                succeed = false;
            }
        }
    }
    if (weighter->n_weigthers() == 0) {
        std::cerr << "No weight has been added for CR " << CR << std::endl;
        return false;
    }
    return succeed;
}

int main(int argc, char* argv[]) {
    std::vector<std::string> in_files;

    std::string tree_name = "FakeTree_Nominal";
    std::string out_root_file = "test.root";

    std::string fake_factor_file_ttbar = "FakeFactors4L/fake_results_ttbar.root";
    std::string fake_factor_file_Zjets = "FakeFactors4L/fake_results_Zjets.root";
    for (int k = 1; k < argc; ++k) {
        std::string current_arg(argv[k]);
        if (current_arg == "--inFile" && k + 1 < argc) {
            in_files.push_back(argv[k + 1]);
        } else if (current_arg == "--outFile" && k + 1 < argc) {
            out_root_file = argv[k + 1];
        } else if (current_arg == "--fakeFile_ttbar" && k + 1 < argc) {
            fake_factor_file_ttbar = argv[k + 1];
        } else if (current_arg == "--fakeFile_Zjets" && k + 1 < argc) {
            fake_factor_file_Zjets = argv[k + 1];
        }
    }

    std::shared_ptr<TFile> ff_file_ttbar = XAMPP::Open(fake_factor_file_ttbar);
    std::shared_ptr<TFile> ff_file_Zjets = XAMPP::Open(fake_factor_file_Zjets);

    std::vector<std::shared_ptr<FakeWeighter>> fake_weighter;

    /// For testing purposes do not bail out if the fake-factor files are not available
    if (ff_file_ttbar && ff_file_Zjets) {
        // 4LnoZ

        ClosureCut CR_LLLlnoZnoB("LLLlnoZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->veto_bjets();
        });
        ClosureCut CR_LLllnoZnoB("LLllnoZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TnoZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->veto_bjets();
        });

        ClosureCut CR_LLLlnoZB("LLLlnoZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->select_bjets();
        });
        ClosureCut CR_LLllnoZB("LLllnoZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TnoZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->select_bjets();
        });

        ClosureCut CR_LLLlnoZBincl("LLLlnoZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->inclusive_bjets();
        });
        ClosureCut CR_LLllnoZBincl("LLllnoZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TnoZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->inclusive_bjets();
        });

        // 4LZ

        ClosureCut CR_LLLlZnoB("LLLlZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->veto_bjets();
        });
        ClosureCut CR_LLllZnoB("LLllZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->veto_bjets();
        });
        ClosureCut CR_LLLlZB("LLLlZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->select_bjets();
        });
        ClosureCut CR_LLllZB("LLllZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->select_bjets();
        });

        ClosureCut CR_LLLlZBincl("LLLlZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->inclusive_bjets();
        });
        ClosureCut CR_LLllZBincl("LLllZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->inclusive_bjets();
        });

        // 4LZZ

        ClosureCut CR_LLLlZZnoB("LLLlZZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TZZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->veto_bjets();
        });
        ClosureCut CR_LLllZZnoB("LLllZZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TZZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->veto_bjets();
        });

        ClosureCut CR_LLLlZZBincl("LLLlZZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TZZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->inclusive_bjets();
        });
        ClosureCut CR_LLllZZBincl("LLllZZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is4L0TZZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->inclusive_bjets();
        });

        // 3L1TnoZ

        ClosureCut CR_LLLtnoZnoB("LLLtnoZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->veto_bjets();
        });
        //         ClosureCut CR_LLTlnoZnoB("LLTlnoZnoB", "", [](FakeClosureTree& t) {
        //             FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
        //             return tree_ptr->Is3L1TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) &&
        //                    tree_ptr->veto_bjets();
        //         });
        ClosureCut CR_LLltnoZnoB("LLltnoZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TnoZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->veto_bjets();
        });

        ClosureCut CR_LLLtnoZB("LLLtnoZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->select_bjets();
        });
        //         ClosureCut CR_LLTlnoZB("LLTlnoZB", "", [](FakeClosureTree& t) {
        //             FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
        //             return tree_ptr->Is3L1TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) &&
        //                    tree_ptr->select_bjets ();
        //         });
        ClosureCut CR_LLltnoZB("LLltnoZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TnoZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->select_bjets();
        });

        ClosureCut CR_LLLtnoZBincl("LLLtnoZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->inclusive_bjets();
        });
        //         ClosureCut CR_LLTlnoZBincl("LLTlnoZBincl", "", [](FakeClosureTree& t) {
        //             FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
        //             return tree_ptr->Is3L1TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) &&
        //                    tree_ptr->inclusive_bjets();
        //         });
        ClosureCut CR_LLltnoZBincl("LLltnoZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TnoZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->inclusive_bjets();
        });

        // 3L1TZ

        ClosureCut CR_LLLtZnoB("LLLtZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->veto_bjets();
        });
        ClosureCut CR_LLltZnoB("LLltZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->veto_bjets();
        });

        ClosureCut CR_LLLtZB("LLLtZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->select_bjets();
        });
        ClosureCut CR_LLltZB("LLltZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->select_bjets();
        });

        ClosureCut CR_LLLtZBincl("LLLtZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->inclusive_bjets();
        });
        ClosureCut CR_LLltZBincl("LLltZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is3L1TZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->inclusive_bjets();
        });

        // 2L2TnoZ

        ClosureCut CR_LLTtnoZnoB("LLTtnoZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->veto_bjets();
        });
        ClosureCut CR_LLttnoZnoB("LLttnoZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TnoZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->veto_bjets();
        });

        ClosureCut CR_LLTtnoZB("LLTtnoZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->select_bjets();
        });
        ClosureCut CR_LLttnoZB("LLttnoZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TnoZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->select_bjets();
        });

        ClosureCut CR_LLTtnoZBincl("LLTtnoZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TnoZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->inclusive_bjets();
        });
        ClosureCut CR_LLttnoZBincl("LLttnoZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TnoZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->inclusive_bjets();
        });

        // 2L2TZ

        ClosureCut CR_LLTtZnoB("LLTtZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->veto_bjets();
        });
        ClosureCut CR_LLttZnoB("LLttZnoB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->veto_bjets();
        });

        ClosureCut CR_LLTtZB("LLTtZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->select_bjets();
        });
        ClosureCut CR_LLttZB("LLttZB", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->select_bjets();
        });

        ClosureCut CR_LLTtZBincl("LLTtZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TZ() && (tree_ptr->get_selection() & LepClass::CR1) && tree_ptr->inclusive_bjets();
        });
        ClosureCut CR_LLttZBincl("LLttZBincl", "", [](FakeClosureTree& t) {
            FakeTreeWriter* tree_ptr = (FakeTreeWriter*)(&t);
            return tree_ptr->Is2L2TZ() && (tree_ptr->get_selection() & LepClass::CR2) && tree_ptr->inclusive_bjets();
        });

        std::function<bool(unsigned int, ClosureCut&, const std::string&)> make_weighter =
            [&ff_file_ttbar, &ff_file_Zjets, &fake_weighter](unsigned int lep_mask, ClosureCut& CR, const std::string& CR_Path) {
                std::shared_ptr<FakeWeighter> w =
                    std::make_shared<FakeWeighter>(LepClass::Incl | LepClass::Baseline | lep_mask, lep_mask & LepClass::CR1 ? 1 : 2,
                                                   lep_mask & LepClass::CR1 ? 1 : -1, CR);
                if (!load_ff(ff_file_ttbar, w, CR_Path, false) || !load_ff(ff_file_Zjets, w, CR_Path, false)) return false;
                fake_weighter += w;
                return true;
            };

        /// 3L1l Z veto - b veto
        if (!make_weighter(LepClass::CR1, CR_LLLlnoZnoB, "CR2_LLll__AND__noZB")) return EXIT_FAILURE;
        /// 2L2l Z veto - bveto
        if (!make_weighter(LepClass::CR2, CR_LLllnoZnoB, "CR2_LLll__AND__noZB")) return EXIT_FAILURE;

        /// 3L1l Z veto - b selection
        if (!make_weighter(LepClass::CR1, CR_LLLlnoZB, "CR2_LLll__AND__noZ1B")) return EXIT_FAILURE;
        /// 2L2l Z veto - b selection
        if (!make_weighter(LepClass::CR2, CR_LLllnoZB, "CR2_LLll__AND__noZ1B")) return EXIT_FAILURE;

        /// 3L1l Z veto - b inclusive
        if (!make_weighter(LepClass::CR1, CR_LLLlnoZBincl, "CR2_LLll__AND__noZ")) return EXIT_FAILURE;
        /// 2L2l Z veto - b inclusive
        if (!make_weighter(LepClass::CR2, CR_LLllnoZBincl, "CR2_LLll__AND__noZ")) return EXIT_FAILURE;

        /// 3L1l Z selection - b veto
        if (!make_weighter(LepClass::CR1, CR_LLLlZnoB, "CR2_LLll__AND__ZnoB")) return EXIT_FAILURE;
        /// 2L2l Z selection - b veto
        if (!make_weighter(LepClass::CR2, CR_LLllZnoB, "CR2_LLll__AND__ZnoB")) return EXIT_FAILURE;

        /// 3L1l Z selection - b selection
        if (!make_weighter(LepClass::CR1, CR_LLLlZB, "CR2_LLll__AND__Z1B")) return EXIT_FAILURE;
        /// 2L2l Z selection - b selection
        if (!make_weighter(LepClass::CR2, CR_LLllZB, "CR2_LLll__AND__Z1B")) return EXIT_FAILURE;

        /// 3L1l Z selection - b inclusive
        if (!make_weighter(LepClass::CR1, CR_LLLlZBincl, "CR2_LLll__AND__Z")) return EXIT_FAILURE;
        /// 2L2l Z selection - b inclusive
        if (!make_weighter(LepClass::CR2, CR_LLllZBincl, "CR2_LLll__AND__Z")) return EXIT_FAILURE;

        /// 3L1l ZZ selection - b veto
        if (!make_weighter(LepClass::CR1, CR_LLLlZZnoB, "CR2_LLll__AND__ZZnoB")) return EXIT_FAILURE;
        /// 2L2l ZZ selection - b veto
        if (!make_weighter(LepClass::CR2, CR_LLllZZnoB, "CR2_LLll__AND__ZZnoB")) return EXIT_FAILURE;

        /// 3L1l ZZ selection - b inclusive
        if (!make_weighter(LepClass::CR1, CR_LLLlZZBincl, "CR2_LLll__AND__ZZ")) return EXIT_FAILURE;
        /// 2L2l ZZ selection - b inclusive
        if (!make_weighter(LepClass::CR2, CR_LLllZZBincl, "CR2_LLll__AND__ZZ")) return EXIT_FAILURE;

        /// 3L1t Z veto - b veto
        if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLLtnoZnoB, "CR2_LLlt__AND__noZB")) return EXIT_FAILURE;
        /// 3L1T 1l Z veto - b veto
        // if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLTlnoZnoB, "CR2_LLlt__AND__noZB")) return EXIT_FAILURE;
        /// 2L 1l 1t Z veto - b veto
        if (!make_weighter(LepClass::CR2 | LepClass::MixedCR2, CR_LLltnoZnoB, "CR2_LLlt__AND__noZB")) return EXIT_FAILURE;

        /// 3L1t Z veto - b selection
        if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLLtnoZB, "CR2_LLlt__AND__noZ1B")) return EXIT_FAILURE;
        /// 2L1T 1l Z veto - b selection
        // if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLTlnoZB, "CR2_LLlt__AND__noZ1B")) return EXIT_FAILURE;
        /// 2L 1l 1t Z veto - b selection
        if (!make_weighter(LepClass::CR2 | LepClass::MixedCR2, CR_LLltnoZB, "CR2_LLlt__AND__noZ1B")) return EXIT_FAILURE;

        /// 3L1t Z veto - b inclusive
        if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLLtnoZBincl, "CR2_LLlt__AND__noZ")) return EXIT_FAILURE;
        /// 2L1T 1l Z veto - b inclusive
        // if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLTlnoZBincl, "CR2_LLlt__AND__noZ")) return EXIT_FAILURE;
        /// 2L 1l 1t Z veto - b inclusive
        if (!make_weighter(LepClass::CR2 | LepClass::MixedCR2, CR_LLltnoZBincl, "CR2_LLlt__AND__noZ")) return EXIT_FAILURE;

        /// 3L1t Z selection - b bselection
        if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLLtZB, "CR2_LLlt__AND__noZ1B")) return EXIT_FAILURE;
        /// 2L1T 1l Z selection - b selection
        // if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLTlZB, "CR2_LLlt__AND__noZ1B")) return EXIT_FAILURE;
        /// 2L 1l 1t Z selection - b selection
        if (!make_weighter(LepClass::CR2 | LepClass::MixedCR2, CR_LLltZB, "CR2_LLlt__AND__noZ1B")) return EXIT_FAILURE;

        /// 3L1t Z selection - b veto
        if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLLtZnoB, "CR2_LLlt__AND__ZnoB")) return EXIT_FAILURE;
        /// 2L1T 1l Z selection - b veto
        // if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLTlZnoB, "CR2_LLlt__AND__ZnoB")) return EXIT_FAILURE;
        /// 2L 1l 1t Z selection - b veto
        if (!make_weighter(LepClass::CR2 | LepClass::MixedCR2, CR_LLltZnoB, "CR2_LLlt__AND__ZnoB")) return EXIT_FAILURE;

        /// 3L1t Z selection - b inclusive
        if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLLtZBincl, "CR2_LLlt__AND__Z")) return EXIT_FAILURE;
        /// 2L1T 1l Z selection - b inclusive
        // if (!make_weighter(LepClass::CR1 | LepClass::MixedCR2, CR_LLTlZBincl, "CR2_LLlt__AND__Z")) return EXIT_FAILURE;
        /// 2L 1l 1t Z selection - b inclusive
        if (!make_weighter(LepClass::CR2 | LepClass::MixedCR2, CR_LLltZBincl, "CR2_LLlt__AND__Z")) return EXIT_FAILURE;

        /// 2L1T1t z veto - b veto
        if (!make_weighter(LepClass::CR1, CR_LLTtnoZnoB, "CR2_LLtt__AND__noZB")) return EXIT_FAILURE;
        /// 2L2t z veto - b veto
        if (!make_weighter(LepClass::CR2, CR_LLttnoZnoB, "CR2_LLtt__AND__noZB")) return EXIT_FAILURE;

        /// 2L1T1t z veto - b selection
        if (!make_weighter(LepClass::CR1, CR_LLTtnoZB, "CR2_LLtt__AND__noZ1B")) return EXIT_FAILURE;
        /// 2L2t z veto - b selection
        if (!make_weighter(LepClass::CR2, CR_LLttnoZB, "CR2_LLtt__AND__noZ1B")) return EXIT_FAILURE;

        /// 2L1T1t z veto - b inclusive
        if (!make_weighter(LepClass::CR1, CR_LLTtnoZBincl, "CR2_LLtt__AND__noZ1B")) return EXIT_FAILURE;
        /// 2L2t z veto - b inclusive
        if (!make_weighter(LepClass::CR2, CR_LLttnoZBincl, "CR2_LLtt__AND__noZ1B")) return EXIT_FAILURE;

        /// 2L1T1t z selection - b veto
        if (!make_weighter(LepClass::CR1, CR_LLTtZnoB, "CR2_LLtt__AND__ZnoB")) return EXIT_FAILURE;
        /// 2L2t z selection - b veto
        if (!make_weighter(LepClass::CR2, CR_LLttZnoB, "CR2_LLtt__AND__ZnoB")) return EXIT_FAILURE;

        /// 2L1T1t z selection - b inclusive
        if (!make_weighter(LepClass::CR1, CR_LLTtZBincl, "CR2_LLtt__AND__Z")) return EXIT_FAILURE;
        /// 2L2T  z selection - b inclusive
        if (!make_weighter(LepClass::CR2, CR_LLttZBincl, "CR2_LLtt__AND__Z")) return EXIT_FAILURE;

        /// 2L1T1t z selection - b inclusive
        if (!make_weighter(LepClass::CR1, CR_LLTtZB, "CR2_LLtt__AND__Z1B")) return EXIT_FAILURE;
        /// 2L2T  z selection - b inclusive
        if (!make_weighter(LepClass::CR2, CR_LLttZB, "CR2_LLtt__AND__Z1B")) return EXIT_FAILURE;
    }
    /// Prepare the new file
    std::shared_ptr<TFile> NewFile = make_out_file(out_root_file);
    std::unique_ptr<TTree> NewTree = std::make_unique<TTree>("Reducible_Nominal", "Reducible estimate");
    HistFitterTree hf_tree(NewTree.get());

    /// Setup the systematics of the weighters and
    /// make the branches in the tree
    for (auto& ff : fake_weighter) {
        ff->setup_systematics();
        hf_tree.register_systematics(ff);
    }

    /// Normalize all process fractions back to unity

    ProcessFractionHolder::instance()->normalize_process_fractions();

    /// Setup the counters and timer
    Long64_t tot_events(0), proc_events(0);
    size_t n_files(in_files.size()), proc_files(1);
    TStopwatch watch;
    watch.Start();

    /// Run over the events
    for (auto& in_file : in_files) {
        std::shared_ptr<TFile> InFile = XAMPP::Open(in_file);
        if (!InFile) return EXIT_FAILURE;
        TTree* t = nullptr;
        InFile->GetObject(tree_name.c_str(), t);
        if (!t) {
            std::cerr << "Could not read tree " << tree_name << std::endl;
            return EXIT_FAILURE;
        }
        tot_events += t->GetEntries();
        delete t;
    }

    Long64_t print_interval = tot_events * 1.e-4;
    if (print_interval < 1000) print_interval = 1000;

    for (auto& in_file : in_files) {
        std::shared_ptr<TFile> InFile = XAMPP::Open(in_file);
        if (!InFile) return EXIT_FAILURE;
        TTree* t = nullptr;
        InFile->GetObject(tree_name.c_str(), t);
        if (!t) { return EXIT_FAILURE; }

        FakeTreeWriter InputTree(t);

        Long64_t ev_in_tree = InputTree.getEntries();

        if (proc_files == 1) {
            std::cout << " Start to run over approximately " << tot_events << " events in " << n_files << " files." << std::endl;
        }

        for (Long64_t i = 0; i < ev_in_tree; ++i) {
            InputTree.getEntry(i);
            /// Write the standard branches to the n-tuple
            hf_tree.isMC.set(InputTree.isMC());
            hf_tree.isCR1.set(InputTree.get_selection() & LepClass::CR1);
            hf_tree.EventNumber.set(InputTree.EventNumber());
            hf_tree.GGM0Weight.set(InputTree.isMC() ? InputTree.GGM0Weight() : 1.);
            hf_tree.GGM100Weight.set(InputTree.isMC() ? InputTree.GGM100Weight() : 1.);
            hf_tree.GGM25Weight.set(InputTree.isMC() ? InputTree.GGM25Weight() : 1.);
            hf_tree.GGM75Weight.set(InputTree.isMC() ? InputTree.GGM75Weight() : 1.);
            hf_tree.LumiBlock.set(InputTree.LumiBlock());
            hf_tree.Met.set(InputTree.Met());
            hf_tree.Met_phi.set(InputTree.Met_phi());
            hf_tree.Mll_Z1.set(InputTree.Mll_Z1());
            hf_tree.Mll_Z2.set(InputTree.Mll_Z2());
            hf_tree.Mt_LepMax.set(InputTree.Mt_LepMax());
            hf_tree.Mt_LepMin.set(InputTree.Mt_LepMin());
            hf_tree.NormWeight.set(InputTree.eventWeight());
            hf_tree.runNumber.set(InputTree.runNumber());
            hf_tree.xSection_Uncertainty.set(InputTree.isMC() ? InputTree.xSection_Uncertainty() : 0.);

            unsigned int n_comb = InputTree.n_permutations(InputTree.get_selection());
            for (unsigned int comb = 0; comb < n_comb; ++comb) {
                unsigned short b_modes = InputTree.possible_b_selection_modes(comb);
                if (!b_modes) { continue; }
                /// Propagate the electrons to the tree
                hf_tree.SigEle_IFFType.outVec().clear();
                hf_tree.SigEle_d0Sig.outVec().clear();
                hf_tree.SigEle_eta.outVec().clear();
                hf_tree.SigEle_phi.outVec().clear();
                hf_tree.SigEle_pt.outVec().clear();
                hf_tree.SigEle_q.outVec().clear();
                hf_tree.SigEle_z0Sin.outVec().clear();

                for (auto e : InputTree.write_particles(LepClass::Ele, comb)) {
                    hf_tree.SigEle_IFFType.outVec().push_back(InputTree.isMC() ? InputTree.Ele_IFFType(e) : -1);
                    hf_tree.SigEle_d0Sig.outVec().push_back(InputTree.Ele_d0Sig(e));
                    hf_tree.SigEle_eta.outVec().push_back(InputTree.Ele_eta(e));
                    hf_tree.SigEle_phi.outVec().push_back(InputTree.Ele_phi(e));
                    hf_tree.SigEle_pt.outVec().push_back(InputTree.Ele_pt(e));
                    hf_tree.SigEle_q.outVec().push_back(InputTree.Ele_q(e));
                    hf_tree.SigEle_z0Sin.outVec().push_back(InputTree.Ele_z0Sin(e));
                }
                hf_tree.SigMuo_IFFType.outVec().clear();
                hf_tree.SigMuo_d0Sig.outVec().clear();
                hf_tree.SigMuo_eta.outVec().clear();
                hf_tree.SigMuo_phi.outVec().clear();
                hf_tree.SigMuo_pt.outVec().clear();
                hf_tree.SigMuo_q.outVec().clear();
                hf_tree.SigMuo_z0Sin.outVec().clear();

                /// Now it's time for the muons
                for (auto m : InputTree.write_particles(LepClass::Muo, comb)) {
                    hf_tree.SigMuo_IFFType.outVec().push_back(InputTree.isMC() ? InputTree.Muo_IFFType(m) : -1);
                    hf_tree.SigMuo_d0Sig.outVec().push_back(InputTree.Muo_d0Sig(m));
                    hf_tree.SigMuo_eta.outVec().push_back(InputTree.Muo_eta(m));
                    hf_tree.SigMuo_phi.outVec().push_back(InputTree.Muo_phi(m));
                    hf_tree.SigMuo_pt.outVec().push_back(InputTree.Muo_pt(m));
                    hf_tree.SigMuo_q.outVec().push_back(InputTree.Muo_q(m));
                    hf_tree.SigMuo_z0Sin.outVec().push_back(InputTree.Muo_z0Sin(m));
                }

                hf_tree.SigTau_eta.outVec().clear();
                hf_tree.SigTau_phi.outVec().clear();
                hf_tree.SigTau_prongs.outVec().clear();
                hf_tree.SigTau_pt.outVec().clear();
                hf_tree.SigTau_q.outVec().clear();
                /// Taus are a bit more simple than that
                for (auto t : InputTree.write_particles(LepClass::Tau, comb)) {
                    hf_tree.SigTau_eta.outVec().push_back(InputTree.Tau_eta(t));
                    hf_tree.SigTau_phi.outVec().push_back(InputTree.Tau_phi(t));
                    hf_tree.SigTau_prongs.outVec().push_back(InputTree.Tau_prongs(t));
                    hf_tree.SigTau_pt.outVec().push_back(InputTree.Tau_pt(t));
                    hf_tree.SigTau_q.outVec().push_back(InputTree.Tau_q(t));
                }

                hf_tree.SigJet_bjet.outVec().clear();
                hf_tree.SigJet_eta.outVec().clear();
                hf_tree.SigJet_phi.outVec().clear();
                hf_tree.SigJet_pt.outVec().clear();
                /// Only write non-overlapping jets
                for (auto& j : InputTree.write_particles(LepClass::Jets, comb)) {
                    hf_tree.SigJet_bjet.outVec().push_back(InputTree.SigJet_bjet(j));
                    hf_tree.SigJet_eta.outVec().push_back(InputTree.SigJet_eta(j));
                    hf_tree.SigJet_phi.outVec().push_back(InputTree.SigJet_phi(j));
                    hf_tree.SigJet_pt.outVec().push_back(InputTree.SigJet_pt(j));
                }
                hf_tree.Meff.set(InputTree.baseline_meff(comb));

                /// Overwrite the multiplicities
                unsigned int n_sig_ele = hf_tree.SigEle_pt.outVec().size();
                unsigned int n_sig_muo = hf_tree.SigMuo_pt.outVec().size();
                unsigned int n_sig_tau = hf_tree.SigTau_pt.outVec().size();

                unsigned int n_loose_ele = InputTree.get_n_lep(LepClass::Baseline | LepClass::Incl | LepClass::Ele) - n_sig_ele;
                unsigned int n_loose_muo = InputTree.get_n_lep(LepClass::Baseline | LepClass::Incl | LepClass::Muo) - n_sig_muo;
                unsigned int n_loose_tau = InputTree.get_n_lep(LepClass::Baseline | LepClass::Incl | LepClass::Tau) - n_sig_tau;

                hf_tree.NSignalElec.set(n_sig_ele);
                hf_tree.NSignalMuon.set(n_sig_muo);
                hf_tree.NSignalLep.set(n_sig_muo + n_sig_ele);
                hf_tree.NSignalTaus.set(n_sig_tau);

                hf_tree.NLooseElectrons.set(n_loose_ele);
                hf_tree.NLooseMuons.set(n_loose_muo);
                hf_tree.NLooseLep.set(n_loose_ele + n_loose_muo);
                hf_tree.NLooseTaus.set(n_loose_tau);

                hf_tree.NJets.set(InputTree.num_jets(comb));
                hf_tree.NBJets.set(InputTree.num_bjets(comb));

                unsigned short first_b = XAMPP::min_bit(b_modes);
                unsigned short last_b = XAMPP::max_bit(b_modes);

                for (unsigned int short b_sel = first_b; b_sel <= last_b; ++b_sel) {
                    if (!(b_modes & (1 << b_sel))) continue;
                    InputTree.set_b_selection_mode(1 << b_sel);

                    /// Booleans of the event selection
                    hf_tree.IsCR_ZZ.set(InputTree.IsCR_ZZ(comb));
                    hf_tree.IsCR_ttZ.set(InputTree.IsCR_ttZ(comb));
                    hf_tree.IsSR0A.set(InputTree.IsSR0A(comb));
                    hf_tree.IsSR0A_bincl.set(InputTree.IsSR0A_bincl(comb));
                    hf_tree.IsSR0B.set(InputTree.IsSR0B(comb));
                    hf_tree.IsSR0B_bincl.set(InputTree.IsSR0B_bincl(comb));
                    hf_tree.IsSR0C.set(InputTree.IsSR0C(comb));
                    hf_tree.IsSR0C_bveto.set(InputTree.IsSR0C_bveto(comb));
                    hf_tree.IsSR0D.set(InputTree.IsSR0D(comb));
                    hf_tree.IsSR0E.set(InputTree.IsSR0E(comb));
                    hf_tree.IsSR0F.set(InputTree.IsSR0F(comb));
                    hf_tree.IsSR0G.set(InputTree.IsSR0G(comb));
                    hf_tree.IsSR1A.set(InputTree.IsSR1A(comb));
                    hf_tree.IsSR1A_bincl.set(InputTree.IsSR1A_bincl(comb));
                    hf_tree.IsSR1A_bonly.set(InputTree.IsSR1A_bonly(comb));
                    hf_tree.IsSR1B.set(InputTree.IsSR1B(comb));
                    hf_tree.IsSR1C.set(InputTree.IsSR1C(comb));
                    hf_tree.IsSR2A.set(InputTree.IsSR2A(comb));
                    hf_tree.IsSR2A_bincl.set(InputTree.IsSR2A_bincl(comb));
                    hf_tree.IsSR2B.set(InputTree.IsSR2B(comb));
                    hf_tree.IsSR2C.set(InputTree.IsSR2C(comb));
                    hf_tree.IsVR0.set(InputTree.IsVR0(comb));
                    hf_tree.IsVR0ZZ.set(InputTree.IsVR0ZZ(comb));
                    hf_tree.IsVR0ZZ_bincl.set(InputTree.IsVR0ZZ_bincl(comb));
                    hf_tree.IsVR0_bincl.set(InputTree.IsVR0_bincl(comb));
                    hf_tree.IsVR0_ttZ.set(InputTree.IsVR0_ttZ(comb));
                    hf_tree.IsVR1.set(InputTree.IsVR1(comb));
                    hf_tree.IsVR1Z.set(InputTree.IsVR1Z(comb));
                    hf_tree.IsVR1Z_bincl.set(InputTree.IsVR1Z_bincl(comb));
                    hf_tree.IsVR1_bincl.set(InputTree.IsVR1_bincl(comb));
                    hf_tree.IsVR2.set(InputTree.IsVR2(comb));
                    hf_tree.IsVR2Z.set(InputTree.IsVR2Z(comb));
                    hf_tree.IsVR2Z_bincl.set(InputTree.IsVR2Z_bincl(comb));
                    hf_tree.IsVR2_bincl.set(InputTree.IsVR2_bincl(comb));

                    hf_tree.IsRealCR_ZZ.set(InputTree.IsCR_ZZ(comb));
                    hf_tree.IsRealCR_ttZ.set(InputTree.IsCR_ttZ(comb));
                    hf_tree.IsRealSR0A.set(InputTree.IsSR0A(comb));
                    hf_tree.IsRealSR0A_bincl.set(InputTree.IsSR0A_bincl(comb));
                    hf_tree.IsRealSR0B.set(InputTree.IsSR0B(comb));
                    hf_tree.IsRealSR0B_bincl.set(InputTree.IsSR0B_bincl(comb));
                    hf_tree.IsRealSR0C.set(InputTree.IsSR0C(comb));
                    hf_tree.IsRealSR0C_bveto.set(InputTree.IsSR0C_bveto(comb));
                    hf_tree.IsRealSR0D.set(InputTree.IsSR0D(comb));
                    hf_tree.IsRealSR0E.set(InputTree.IsSR0E(comb));
                    hf_tree.IsRealSR0F.set(InputTree.IsSR0F(comb));
                    hf_tree.IsRealSR0G.set(InputTree.IsSR0G(comb));
                    hf_tree.IsRealSR1A.set(InputTree.IsSR1A(comb));
                    hf_tree.IsRealSR1A_bincl.set(InputTree.IsSR1A_bincl(comb));
                    hf_tree.IsRealSR1A_bonly.set(InputTree.IsSR1A_bonly(comb));
                    hf_tree.IsRealSR1B.set(InputTree.IsSR1B(comb));
                    hf_tree.IsRealSR1C.set(InputTree.IsSR1C(comb));
                    hf_tree.IsRealSR2A.set(InputTree.IsSR2A(comb));
                    hf_tree.IsRealSR2A_bincl.set(InputTree.IsSR2A_bincl(comb));
                    hf_tree.IsRealSR2B.set(InputTree.IsSR2B(comb));
                    hf_tree.IsRealSR2C.set(InputTree.IsSR2C(comb));
                    hf_tree.IsRealVR0.set(InputTree.IsVR0(comb));
                    hf_tree.IsRealVR0ZZ.set(InputTree.IsVR0ZZ(comb));
                    // hf_tree.IsRealVR0ZZ_bincl.set(InputTree.IsVR0ZZ_bincl(comb));
                    hf_tree.IsRealVR0_bincl.set(InputTree.IsVR0_bincl(comb));
                    hf_tree.IsRealVR0_ttZ.set(InputTree.IsVR0_ttZ(comb));
                    hf_tree.IsRealVR1.set(InputTree.IsVR1(comb));
                    hf_tree.IsRealVR1Z.set(InputTree.IsVR1Z(comb));
                    hf_tree.IsRealVR1Z_bincl.set(InputTree.IsVR1Z_bincl(comb));
                    hf_tree.IsRealVR1_bincl.set(InputTree.IsVR1_bincl(comb));
                    hf_tree.IsRealVR2.set(InputTree.IsVR2(comb));
                    hf_tree.IsRealVR2Z.set(InputTree.IsVR2Z(comb));
                    hf_tree.IsRealVR2Z_bincl.set(InputTree.IsVR2Z_bincl(comb));
                    hf_tree.IsRealVR2_bincl.set(InputTree.IsVR2_bincl(comb));

                    // hf_tree.Is2L2TZ.set(InputTree.inclusive_bjets() && InputTree.Is2L2TZ());
                    hf_tree.Is2L2TnoZ.set(InputTree.inclusive_bjets() && InputTree.Is2L2TnoZ());
                    // hf_tree.Is3L1T.set(InputTree.inclusive_bjets() && InputTree.Is3L1T());
                    hf_tree.Is3L1TZ.set(InputTree.inclusive_bjets() && InputTree.Is3L1TZ());
                    hf_tree.Is3L1TnoZ.set(InputTree.inclusive_bjets() && InputTree.Is3L1TnoZ());

                    hf_tree.Is2L2TZ_bonly.set(InputTree.Is2L2TZ_bonly(comb));
                    // hf_tree.Is2L2T_bonly.set(InputTree.Is2L2T_bonly(comb));
                    hf_tree.Is2L2TnoZ_bonly.set(InputTree.Is2L2TnoZ_bonly(comb));
                    hf_tree.Is3L1TZ_bonly.set(InputTree.Is3L1TZ_bonly(comb));
                    // hf_tree.Is3L1T_bonly.set(InputTree.Is3L1T_bonly(comb));
                    hf_tree.Is3L1TnoZ_bonly.set(InputTree.Is3L1TnoZ_bonly(comb));

                    // hf_tree.Is2L2T_bveto.set(InputTree.Is2L2T_bveto(comb));
                    hf_tree.Is2L2TZ_bveto.set(InputTree.Is2L2TZ_bveto(comb));
                    hf_tree.Is2L2TnoZ_bveto.set(InputTree.Is2L2TnoZ_bveto(comb));
                    hf_tree.Is3L1TZ_bveto.set(InputTree.Is3L1TZ_bveto(comb));
                    // hf_tree.Is3L1T_bveto.set(InputTree.Is3L1T_bveto(comb));
                    hf_tree.Is3L1TnoZ_bveto.set(InputTree.Is3L1TnoZ_bveto(comb));

                    // hf_tree.Is4L.set(InputTree.inclusive_bjets() && InputTree.Is4L0T());
                    hf_tree.Is4LZ.set(InputTree.inclusive_bjets() && InputTree.Is4L0TZ());
                    hf_tree.Is4LZZ.set(InputTree.inclusive_bjets() && InputTree.Is4L0TZZ());
                    hf_tree.Is4LnoZ.set(InputTree.inclusive_bjets() && InputTree.Is4L0TnoZ());

                    hf_tree.Is4LZZ_bveto.set(InputTree.veto_bjets() && InputTree.Is4L0TZZ_bveto());
                    hf_tree.Is4LZ_bveto.set(InputTree.veto_bjets() && InputTree.Is4L0TZ_bveto());
                    // hf_tree.Is4L_bveto.set(InputTree.veto_bjets() && InputTree.Is4L0T_bveto());
                    hf_tree.Is4LnoZ_bveto.set(InputTree.veto_bjets() && InputTree.Is4L0TnoZ_bveto());

                    hf_tree.Is4LnoZ_bonly.set(InputTree.select_bjets() && InputTree.Is4L0TnoZ_bonly());
                    hf_tree.Is4LZ_bonly.set(InputTree.select_bjets() && InputTree.Is4L0TZ_bonly());

                    // hf_tree.IsReal2L2TZ.set(InputTree.inclusive_bjets() && InputTree.Is2L2TZ());
                    hf_tree.IsReal2L2TnoZ.set(InputTree.inclusive_bjets() && InputTree.Is2L2TnoZ());
                    // hf_tree.IsReal3L1T.set(InputTree.inclusive_bjets() && InputTree.Is3L1T());
                    hf_tree.IsReal3L1TZ.set(InputTree.inclusive_bjets() && InputTree.Is3L1TZ());
                    hf_tree.IsReal3L1TnoZ.set(InputTree.inclusive_bjets() && InputTree.Is3L1TnoZ());

                    hf_tree.IsReal2L2TZ_bonly.set(InputTree.Is2L2TZ_bonly(comb));
                    // hf_tree.IsReal2L2T_bonly.set(InputTree.Is2L2T_bonly(comb));
                    hf_tree.IsReal2L2TnoZ_bonly.set(InputTree.Is2L2TnoZ_bonly(comb));
                    hf_tree.IsReal3L1TZ_bonly.set(InputTree.Is3L1TZ_bonly(comb));
                    // hf_tree.IsReal3L1T_bonly.set(InputTree.Is3L1T_bonly(comb));
                    hf_tree.IsReal3L1TnoZ_bonly.set(InputTree.Is3L1TnoZ_bonly(comb));

                    // hf_tree.IsReal2L2T_bveto.set(InputTree.Is2L2T_bveto(comb));
                    hf_tree.IsReal2L2TZ_bveto.set(InputTree.Is2L2TZ_bveto(comb));
                    hf_tree.IsReal2L2TnoZ_bveto.set(InputTree.Is2L2TnoZ_bveto(comb));
                    hf_tree.IsReal3L1TZ_bveto.set(InputTree.Is3L1TZ_bveto(comb));
                    // hf_tree.IsReal3L1T_bveto.set(InputTree.Is3L1T_bveto(comb));
                    hf_tree.IsReal3L1TnoZ_bveto.set(InputTree.Is3L1TnoZ_bveto(comb));

                    // hf_tree.IsReal4L.set(InputTree.inclusive_bjets() && InputTree.Is4L0T());
                    hf_tree.IsReal4LZ.set(InputTree.inclusive_bjets() && InputTree.Is4L0TZ());
                    hf_tree.IsReal4LZZ.set(InputTree.inclusive_bjets() && InputTree.Is4L0TZZ());
                    hf_tree.IsReal4LnoZ.set(InputTree.inclusive_bjets() && InputTree.Is4L0TnoZ());

                    hf_tree.IsReal4LZZ_bveto.set(InputTree.veto_bjets() && InputTree.Is4L0TZZ_bveto());
                    hf_tree.IsReal4LZ_bveto.set(InputTree.veto_bjets() && InputTree.Is4L0TZ_bveto());
                    // hf_tree.IsReal4L_bveto.set(InputTree.veto_bjets() && InputTree.Is4L0T_bveto());
                    hf_tree.IsReal4LnoZ_bveto.set(InputTree.veto_bjets() && InputTree.Is4L0TnoZ_bveto());

                    hf_tree.IsReal4LnoZ_bonly.set(InputTree.select_bjets() && InputTree.Is4L0TnoZ_bonly());
                    hf_tree.IsReal4LZ_bonly.set(InputTree.select_bjets() && InputTree.Is4L0TZ_bonly());

                    /// Last but the most important find the appropiate FF weight and fill
                    /// it to the tree
                    std::vector<std::shared_ptr<FakeWeighter>>::iterator w_itr =
                        std::find_if(fake_weighter.begin(), fake_weighter.end(),
                                     [&InputTree](const std::shared_ptr<FakeWeighter>& w) { return w->get_selection()(InputTree); });

                    hf_tree.fill_ff_weight(w_itr != fake_weighter.end() ? (*w_itr) : nullptr, InputTree, comb);
                    std::vector<std::shared_ptr<FakeWeighter>>::iterator check_itr =
                        std::find_if(w_itr + 1, fake_weighter.end(),
                                     [&InputTree](const std::shared_ptr<FakeWeighter>& w) { return w->get_selection()(InputTree); });

                    if (check_itr != fake_weighter.end()) {
                        InputTree.dump_event();
                        throw std::runtime_error(Form("Multiple fake-weighter have been assigned... %s and %s",
                                                      (*w_itr)->get_selection().getName().c_str(),
                                                      (*check_itr)->get_selection().getName().c_str()));
                    }
                    hf_tree.fill_non_closure(InputTree);
                    hf_tree.fill();
                }
            }
            if (i % print_interval == 0) {
                double t2 = watch.RealTime();
                std::cout << "Entry " << proc_events << " / " << tot_events << " (" << std::setprecision(2)
                          << (float)proc_events / (float)tot_events * 100. << "%) in file " << proc_files << " / " << n_files
                          << std::setprecision(6) << " @ " << XAMPP::TimeHMS(t2);
                std::cout << ". Physics Event Rate: " << std::setprecision(3) << (proc_events / t2 / 1000)
                          << " kHz, E.T.A.: " << XAMPP::TimeHMS(t2 * ((float)tot_events / (float)proc_events - 1.));
                std::cout << " (updating screen each " << print_interval << " events)";
                std::cout << std::endl;
                watch.Continue();
            }
            ++proc_events;
        }
        /// Delete the tree to avoid memory leaks... Yeah
        /// ROOT is not as clever
        delete t;
        ++proc_files;
    }
    NewFile->WriteObject(NewTree.get(), NewTree->GetName());
    NewTree.reset();
}
